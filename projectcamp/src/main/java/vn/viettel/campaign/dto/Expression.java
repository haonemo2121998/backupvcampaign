/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dto;

import java.io.Serializable;

/**
 *
 * @author ABC
 */
public class Expression implements Serializable {

    private String valueEx;
    private String contentEx;
    private String operatorEx;

    public String getValueEx() {
        return valueEx;
    }

    public void setValueEx(String valueEx) {
        this.valueEx = valueEx;
    }

    public String getContentEx() {
        return contentEx;
    }

    public void setContentEx(String contentEx) {
        this.contentEx = contentEx;
    }

    public String getOperatorEx() {
        return operatorEx;
    }

    public void setOperatorEx(String operatorEx) {
        this.operatorEx = operatorEx;
    }
    
    
}
