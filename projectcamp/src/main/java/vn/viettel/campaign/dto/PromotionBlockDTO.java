package vn.viettel.campaign.dto;

/**
 *
 * @author ConKC
 */
public class PromotionBlockDTO {

    private Long blockId;
    private String blockName;
    private String description;
    private Long categoryId;

    public PromotionBlockDTO() {
    }

    public PromotionBlockDTO(Long blockId, Long categoryId) {
        this.blockId = blockId;
        this.categoryId = categoryId;
    }

    public Long getBlockId() {
        return blockId;
    }

    public void setBlockId(Long blockId) {
        this.blockId = blockId;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

}
