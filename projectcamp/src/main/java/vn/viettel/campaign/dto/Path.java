/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.entities.EvaluationInputObject;

/**
 *
 * @author SON
 */
public class Path {

    private int id;
    private String ObjectName;
    private List<EvaluationInputObject> lstEvaluationInputObject = new ArrayList<>();
    private String filter;
    private EvaluationInputObject inputObject = new EvaluationInputObject();

    private String dataCondition;
    private String dataFunction;

    public static boolean isStringNullOrEmpty1(String input) {
        if (input == null || input.trim().equalsIgnoreCase("") || input.trim().equalsIgnoreCase(" ")) {
            return true;
        }
        return false;
    }

    public String getDataCondition() {
        if (isStringNullOrEmpty1(this.dataCondition)) {
            this.dataCondition = "";
            if (getFilter() != null) {
                String[] result = getFilter().split(";");
                if (result.length == 2 || result.length == 1) {
                    dataCondition = result[0];
                }
            }
        }
        return dataCondition;
    }

    public void setDataCondition(String dataCondition) {
        this.dataCondition = dataCondition;
    }

    public String getDataFunction() {
        if (DataUtil.isStringNullOrEmpty(this.dataFunction)) {
            this.dataFunction = "";
            if (getFilter() != null) {
                String[] result = getFilter().split(";");
                if (result.length == 2) {
                    dataFunction = result[1];
                }
            }

        }
        return dataFunction;
    }

    public void setDataFunction(String dataFunction) {
        this.dataFunction = dataFunction;
    }

    public EvaluationInputObject getInputObject() {
        return inputObject;
    }

    public void setInputObject(EvaluationInputObject inputObject) {
        this.inputObject = inputObject;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObjectName() {
        return ObjectName;
    }

    public void setObjectName(String ObjectName) {
        this.ObjectName = ObjectName;
    }

    public List<EvaluationInputObject> getLstEvaluationInputObject() {
        return lstEvaluationInputObject;
    }

    public void setLstEvaluationInputObject(List<EvaluationInputObject> lstEvaluationInputObject) {
        this.lstEvaluationInputObject = lstEvaluationInputObject;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }
}
