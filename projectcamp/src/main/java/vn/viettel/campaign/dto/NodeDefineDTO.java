/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dto;

import java.io.Serializable;

/**
 *
 * @author ABC
 */
public class NodeDefineDTO implements Serializable {

    private String operatorDT;

    public String getOperatorDT() {
        return operatorDT;
    }

    public void setOperatorDT(String operatorDT) {
        this.operatorDT = operatorDT;
    }
}
