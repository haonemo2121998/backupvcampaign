/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dto;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;
import vn.viettel.campaign.common.DateUtils;
import vn.viettel.campaign.constants.Constants;

/**
 *
 * @author ConKC
 */
public class RequestDTO {

    private Long id;
    private String transactionId;
    private Integer factor;
    private Long objectId;
    private Integer objectType;
    private Integer requestType;
    private Integer serviceId;
    private String serviceName;
    private Integer errorCode;
    private String state;
    private String errorDescription;
    private Float percentage;
    private Date requestTime;
    private Date finishTime;
    private String requestTimeStr;
    private String finishTimeStr;
    private String requestTypeStr;
    private String factorStr;
    private Boolean isRemove;

    public RequestDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getFactor() {
        return factor;
    }

    public void setFactor(Integer factor) {
        this.factor = factor;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Integer getObjectType() {
        return objectType;
    }

    public void setObjectType(Integer objectType) {
        this.objectType = objectType;
    }

    public Integer getRequestType() {
        return requestType;
    }

    public void setRequestType(Integer requestType) {
        this.requestType = requestType;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        if (Objects.nonNull(serviceId)) {
            if (Constants.ServiceId.UPLOAD_SEGMENT == serviceId) {
                return Constants.ServiceName.UPLOAD_SEGMENT;
            } else if (Constants.ServiceId.DELETE_SEGMENT == serviceId) {
                return Constants.ServiceName.DELETE_SEGMENT;
            } else if (Constants.ServiceId.UPLOAD_BLACKLIST == serviceId) {
                return Constants.ServiceName.UPLOAD_BLACKLIST;
            } else if (Constants.ServiceId.UPDATE_BLACKLIST == serviceId) {
                return Constants.ServiceName.UPDATE_BLACKLIST;
            } else if (Constants.ServiceId.DELETE_BLACKLIST == serviceId) {
                return Constants.ServiceName.DELETE_BLACKLIST;
            } else if (Constants.ServiceId.UPLOAD_SPECIAL_OFFER == serviceId) {
                return Constants.ServiceName.UPLOAD_SPECIAL_OFFER;
            } else if (Constants.ServiceId.UPDATE_SPECIAL_OFFER == serviceId) {
                return Constants.ServiceName.UPDATE_SPECIAL_OFFER;
            } else if (Constants.ServiceId.DELETE_SPECIAL_OFFER == serviceId) {
                return Constants.ServiceName.DELETE_SPECIAL_OFFER;
            }
        }
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getState() {
        if (Objects.nonNull(errorCode)) {
            if (Constants.ErrorCode.RUNNING == errorCode) {
                return Constants.ErrorCodeName.RUNNING;
            } else if (Constants.ErrorCode.SUCCESS == errorCode) {
                return Constants.ErrorCodeName.SUCCESS;
            } else if (Constants.ErrorCode.FAILED == errorCode) {
                return Constants.ErrorCodeName.FAILED;
            } else if (Constants.ErrorCode.INIT == errorCode) {
                return Constants.ErrorCodeName.INIT;
            } else if (Constants.ErrorCode.ACCEPT == errorCode) {
                return Constants.ErrorCodeName.ACCEPT;
            }
        }
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public Float getPercentage() {
        return percentage;
    }

    public void setPercentage(Float percentage) {
        this.percentage = percentage;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getRequestTimeStr() {
        return requestTimeStr;
    }

    public void setRequestTimeStr(String requestTimeStr) {
        this.requestTimeStr = requestTimeStr;
    }

    public String getFinishTimeStr() {
        return finishTimeStr;
    }

    public void setFinishTimeStr(String finishTimeStr) {
        this.finishTimeStr = finishTimeStr;
    }

    public String getRequestTypeStr() {
        if (Objects.nonNull(requestType)) {
            if (Constants.RequestType.REQUEST_SERVICE == requestType) {
                return Constants.RequestTypeName.REQUEST_SERVICE;
            } else if (Constants.RequestType.FORCE_STOP_SERVICE == requestType) {
                return Constants.RequestTypeName.FORCE_STOP_SERVICE;
            }
        }
        return requestTypeStr;
    }

    public void setRequestTypeStr(String requestTypeStr) {
        this.requestTypeStr = requestTypeStr;
    }

    public String getFactorStr() {
        if (Objects.nonNull(factor)) {
            if (Constants.Factor.WEB_FACTOR == factor) {
                return Constants.FactorName.WEB_FACTOR;
            } else if (Constants.Factor.CBA_FACTOR == factor) {
                return Constants.FactorName.CBA_FACTOR;
            } else if (Constants.Factor.OTHER_FACTOR == factor) {
                return Constants.FactorName.OTHER_FACTOR;
            }
        }
        return factorStr;
    }

    public void setFactorStr(String factorStr) {
        this.factorStr = factorStr;
    }

    public Boolean getIsRemove() {
        this.isRemove = true;
        if (Constants.ErrorCode.INIT == errorCode) {
            if (StringUtils.isNotBlank(requestTimeStr)) {
                Date date = DateUtils.stringToDate(requestTimeStr, "yyyy-MM-dd HH:mm:ss");
                long time = 0;
                if (date != null) {
                    time = new Date().getTime() - date.getTime();
                }
                if (TimeUnit.MILLISECONDS.toHours(time) < 24) {
                    this.isRemove = false;
                }
            }
        }
        return isRemove;
    }

    public void setIsRemove(Boolean isRemove) {
        this.isRemove = isRemove;
    }
}
