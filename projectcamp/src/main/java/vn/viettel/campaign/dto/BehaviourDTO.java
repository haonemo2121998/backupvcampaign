package vn.viettel.campaign.dto;

/**
 *
 * @author ConKC
 */
public class BehaviourDTO {

    private Long promotionType;
    private Long promotionId;
    private String promotionName;
    private Long promotionBlockId;
    private Long categoryId;
    private Long notifyId;
    private Long ocsParentId;

    public BehaviourDTO() {
    }

    public Long getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(Long promotionType) {
        this.promotionType = promotionType;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public Long getPromotionBlockId() {
        return promotionBlockId;
    }

    public void setPromotionBlockId(Long promotionBlockId) {
        this.promotionBlockId = promotionBlockId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return promotionName;
    }

    public Long getNotifyId() {
        return notifyId;
    }

    public void setNotifyId(Long notifyId) {
        this.notifyId = notifyId;
    }

    public Long getOcsParentId() {
        return ocsParentId;
    }

    public void setOcsParentId(Long ocsParentId) {
        this.ocsParentId = ocsParentId;
    }

}
