/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dto;

/**
 *
 * @author ADMIN
 */
public class MembershipFuntion {
    private Double a,b,c,d;
    private String stringA,stringB,stringC,stringD;

    public String getStringA() {
        return stringA;
    }

    public void setStringA(String stringA) {
        this.stringA = stringA;
    }

    public String getStringB() {
        return stringB;
    }

    public void setStringB(String stringB) {
        this.stringB = stringB;
    }

    public String getStringC() {
        return stringC;
    }

    public void setStringC(String stringC) {
        this.stringC = stringC;
    }

    public String getStringD() {
        return stringD;
    }

    public void setStringD(String stringD) {
        this.stringD = stringD;
    }
    public Double getA() {
        return a;
    }

    public void setA(Double a) {
        this.a = a;
    }

    public Double getB() {
        return b;
    }

    public void setB(Double b) {
        this.b = b;
    }

    public Double getC() {
        return c;
    }

    public void setC(Double c) {
        this.c = c;
    }

    public Double getD() {
        return d;
    }

    public void setD(Double d) {
        this.d = d;
    }

    
    
    

    
    private String linear;
    private String triangular;
    private String Trapezoidal;
    private String l;
    private String gamma;



    public String getLinear() {
        this.linear = "vld(max((x-a)/(b-a),0))";
        return linear;
    }

    public void setLinear(String linear) {
        this.linear = linear;
    }

    public String getTriangular() {
        this.triangular = "max(min(min((x-a)/(b-a),1),(c-x)/(c-b)),0)";
        return triangular;
    }

    public void setTriangular(String triangular) {
        this.triangular = triangular;
    }

    public String getTrapezoidal() {
        this.Trapezoidal = "max(min(min((x-a)/(b-a),1),(d-x)/(d-c)),0)";
        return Trapezoidal;
    }

    public void setTrapezoidal(String Trapezoidal) {
        this.Trapezoidal = Trapezoidal;
    }

    public String getL() {
        this.l = "max(min((b-x)/(b-a),1),0)";
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }

    public String getGamma() {
        this.gamma = "max(min((x-a)/(b-a),1),0)";
        return gamma;
    }

    public void setGamma(String gamma) {
        this.gamma = gamma;
    }
}
