package vn.viettel.campaign.dto;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Transient;
import java.util.List;

@Data
@Builder
public class NotifyTemplateDetailDTO {

    private Long id;
    private String name;
    private String description;
    private Long categoryId;
    private Integer templateType;
    private List<NotifyContentDTO> contents;

    @Transient
    private static NotifyTemplateDetailDTO defaultValue;

    public static NotifyTemplateDetailDTO getDefault() {
        if (defaultValue == null) {
            defaultValue = NotifyTemplateDetailDTO.builder().build();
        }
        return defaultValue;
    }
}
