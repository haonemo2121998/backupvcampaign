/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dto;

import java.util.List;
import vn.viettel.campaign.entities.FunctionParam;

/**
 *
 * @author ADMIN
 */
public class FunctionTable {

    private Long functionId;
    private String functionName;
    private String[] lstParam;
    private int paramType;
    private List<FunctionParam> lstFunctionParam;

    public List<FunctionParam> getLstFunctionParam() {
        return lstFunctionParam;
    }

    public void setLstFunctionParam(List<FunctionParam> lstFunctionParam) {
        this.lstFunctionParam = lstFunctionParam;
    }
    public Long getFunctionId() {
        return functionId;
    }

    public void setFunctionId(Long functionId) {
        this.functionId = functionId;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }


    public int getParamType() {
        return paramType;
    }

    public void setParamType(int paramType) {
        this.paramType = paramType;
    }

    public String[] getLstParam() {
        return lstParam;
    }

    public void setLstParam(String[] lstParam) {
        this.lstParam = lstParam;
    }


    public FunctionTable() {
    }

}
