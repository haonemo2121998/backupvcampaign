/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dto;

/**
 *
 * @author ConKC
 */
public class RequestStatusDTO {

    private long running;
    private long success;
    private long failed;
    private long init;
    private long accept;

    public RequestStatusDTO() {
    }

    public long getRunning() {
        return running;
    }

    public void setRunning(long running) {
        this.running = running;
    }

    public long getSuccess() {
        return success;
    }

    public void setSuccess(long success) {
        this.success = success;
    }

    public long getFailed() {
        return failed;
    }

    public void setFailed(long failed) {
        this.failed = failed;
    }

    public long getInit() {
        return init;
    }

    public void setInit(long init) {
        this.init = init;
    }

    public long getAccept() {
        return accept;
    }

    public void setAccept(long accept) {
        this.accept = accept;
    }

}
