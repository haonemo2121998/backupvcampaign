package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;
/**
 * @author truongbx
 */
@Entity
@Table(name = "function_param")
public class FunctionParam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FUNCTION_PARAM_ID")
    private long functionParamId;
    @Column(name = "FUNCTION_PARAM_NAME")
    private String functionParamName;
    @Column(name = "VALUE_TYPE")
    private Long valueType;
    @Transient
    private long functionParamIndex;
    @Transient
    private long functionId;
    @Transient
    String fieldValueString;
    @Transient
    Long fieldValueNumber;
    @Transient
    Double fieldValueDouble;
    @Transient
    Date fieldValueDate;
    @Transient
    long fieldZoneId;
    @Transient
    String fieldZoneName;
    @Transient
    long fieldZoneMapId;
    
    @Transient
    String valueStringDouble;

    public String getValueStringDouble() {
        return valueStringDouble;
    }

    public void setValueStringDouble(String valueStringDouble) {
        this.valueStringDouble = valueStringDouble;
    }

    @Transient
    String value;
    @Transient
    String valueMax;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValueMax() {
        return valueMax;
    }

    public void setValueMax(String valueMax) {
        this.valueMax = valueMax;
    }

    public long getFunctionParamId() {
        return functionParamId;
    }

    public void setFunctionParamId(long functionParamId) {
        this.functionParamId = functionParamId;
    }

    public String getFunctionParamName() {
        return functionParamName;
    }

    public void setFunctionParamName(String functionParamName) {
        this.functionParamName = functionParamName;
    }

    public Long getValueType() {
        return valueType;
    }

    public void setValueType(Long valueType) {
        this.valueType = valueType;
    }

    public long getFunctionParamIndex() {
        return functionParamIndex;
    }

    public void setFunctionParamIndex(long functionParamIndex) {
        this.functionParamIndex = functionParamIndex;
    }

    public long getFunctionId() {
        return functionId;
    }

    public void setFunctionId(long functionId) {
        this.functionId = functionId;
    }

    public String getFieldValueString() {
        return fieldValueString;
    }

    public void setFieldValueString(String fieldValueString) {
        this.fieldValueString = fieldValueString;
    }

    public Long getFieldValueNumber() {
        return fieldValueNumber;
    }

    public void setFieldValueNumber(Long fieldValueNumber) {
        this.fieldValueNumber = fieldValueNumber;
    }

    public Date getFieldValueDate() {
        return fieldValueDate;
    }

    public void setFieldValueDate(Date fieldValueDate) {
        this.fieldValueDate = fieldValueDate;
    }

    public long getFieldZoneId() {
        return fieldZoneId;
    }

    public void setFieldZoneId(long fieldZoneId) {
        this.fieldZoneId = fieldZoneId;
    }

    public long getFieldZoneMapId() {
        return fieldZoneMapId;
    }

    public void setFieldZoneMapId(long fieldZoneMapId) {
        this.fieldZoneMapId = fieldZoneMapId;
    }

    public String getFieldZoneName() {
        return fieldZoneName;
    }

    public void setFieldZoneName(String fieldZoneName) {
        this.fieldZoneName = fieldZoneName;
    }

    public Double getFieldValueDouble() {
        return fieldValueDouble;
    }

    public void setFieldValueDouble(Double fieldValueDouble) {
        this.fieldValueDouble = fieldValueDouble;
    }
}
