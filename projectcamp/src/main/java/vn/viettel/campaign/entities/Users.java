package vn.viettel.campaign.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "users")
public class Users implements Serializable {

    @Id
    @Column(name = "USER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    @Column(name = "USER_NAME")
    private String userName;
    @Column(name = "FULL_NAME")
    private String fullName;
    @Column(name = "PASS_WORD")
    private String passWord;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "DEPT_ID")
    private Long deptId;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "GENDER")
    private Long gender;
    @Column(name = "DATE_OF_BIRTH")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateOfBirth;
    @Column(name = "BIRTH_PLACE")
    private String birthPlace;
    @Column(name = "IDENTITY_CARD")
    private String identifyCard;
    @Column(name = "ISSUE_PLACE_IDENT")
    private String issuePlaceIdent;
    @Column(name = "ISSUE_DATE_IDENT")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date issueDateIdent;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "VALID_FROM")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date validFrom;
    @Column(name = "VALID_TO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date validTo;
    @Column(name = "CHECK_IP")
    private Long checkIp;
    @Column(name = "IP")
    private String ip;
    @Column(name = "LAST_CHANGE_PASSWORD")
    private String lastChangePassword;
    @Column(name = "CREATE_USER")
    private String createUser;
    @Column(name = "CREATE_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date createDate;
    @Column(name = "LOGIN_FAILURE_COUNT")
    private Long loginFailurCount;
    @Column(name = "LAST_LOCK_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date lastLockDate;
    @Column(name = "UPDATE_USER")
    private String updateUser;
    @Column(name = "UPDATE_DATE")
    //HaBM2-20012020: Fix not change updateTime
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateDate;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getGender() {
        return gender;
    }

    public void setGender(Long gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getIdentifyCard() {
        return identifyCard;
    }

    public void setIdentifyCard(String identifyCard) {
        this.identifyCard = identifyCard;
    }

    public String getIssuePlaceIdent() {
        return issuePlaceIdent;
    }

    public void setIssuePlaceIdent(String issuePlaceIdent) {
        this.issuePlaceIdent = issuePlaceIdent;
    }

    public Date getIssueDateIdent() {
        return issueDateIdent;
    }

    public void setIssueDateIdent(Date issueDateIdent) {
        this.issueDateIdent = issueDateIdent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Long getCheckIp() {
        return checkIp;
    }

    public void setCheckIp(Long checkIp) {
        this.checkIp = checkIp;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getLastChangePassword() {
        return lastChangePassword;
    }

    public void setLastChangePassword(String lastChangePassword) {
        this.lastChangePassword = lastChangePassword;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getLoginFailurCount() {
        return loginFailurCount;
    }

    public void setLoginFailurCount(Long loginFailurCount) {
        this.loginFailurCount = loginFailurCount;
    }

    public Date getLastLockDate() {
        return lastLockDate;
    }

    public void setLastLockDate(Date lastLockDate) {
        this.lastLockDate = lastLockDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
