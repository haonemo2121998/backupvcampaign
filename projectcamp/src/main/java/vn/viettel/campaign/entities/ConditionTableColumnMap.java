package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "condition_table_column_map")
public class ConditionTableColumnMap {
  @Id
  @Column(name = "CONDITION_TABLE_COLUMN_ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long conditionTableColumnId;
  @Column(name = "CONDITION_TABLE_ID")
  private long conditionTableId;
  @Column(name = "COLUMN_ID")
  private long columnId;


  public long getConditionTableColumnId() {
    return conditionTableColumnId;
  }

  public void setConditionTableColumnId(long conditionTableColumnId) {
    this.conditionTableColumnId = conditionTableColumnId;
  }


  public long getConditionTableId() {
    return conditionTableId;
  }

  public void setConditionTableId(long conditionTableId) {
    this.conditionTableId = conditionTableId;
  }


  public long getColumnId() {
    return columnId;
  }

  public void setColumnId(long columnId) {
    this.columnId = columnId;
  }

}
