/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "result_table_rule_result_map")
public class ResultTableRuleResultMap implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RESULT_TABLE_RULE_RESULT_ID")
    private Long resultTableRuleResultId;
    @Basic(optional = false)
    @Column(name = "RESULT_TABLE_ID")
    private long resultTableId;
    @Basic(optional = false)
    @Column(name = "RULE_RESULT_ID")
    private long ruleResultId;

    public ResultTableRuleResultMap() {
    }

    public ResultTableRuleResultMap(Long resultTableRuleResultId) {
        this.resultTableRuleResultId = resultTableRuleResultId;
    }

    public ResultTableRuleResultMap(Long resultTableRuleResultId, long resultTableId, long ruleResultId) {
        this.resultTableRuleResultId = resultTableRuleResultId;
        this.resultTableId = resultTableId;
        this.ruleResultId = ruleResultId;
    }

    public Long getResultTableRuleResultId() {
        return resultTableRuleResultId;
    }

    public void setResultTableRuleResultId(Long resultTableRuleResultId) {
        this.resultTableRuleResultId = resultTableRuleResultId;
    }

    public long getResultTableId() {
        return resultTableId;
    }

    public void setResultTableId(long resultTableId) {
        this.resultTableId = resultTableId;
    }

    public long getRuleResultId() {
        return ruleResultId;
    }

    public void setRuleResultId(long ruleResultId) {
        this.ruleResultId = ruleResultId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (resultTableRuleResultId != null ? resultTableRuleResultId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResultTableRuleResultMap)) {
            return false;
        }
        ResultTableRuleResultMap other = (ResultTableRuleResultMap) object;
        if ((this.resultTableRuleResultId == null && other.resultTableRuleResultId != null) || (this.resultTableRuleResultId != null && !this.resultTableRuleResultId.equals(other.resultTableRuleResultId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vn.viettel.campaign.entities.autogen.ResultTableRuleResultMap[ resultTableRuleResultId=" + resultTableRuleResultId + " ]";
    }
    
}
