package vn.viettel.campaign.entities;

import java.io.Serializable;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;

import javax.persistence.*;
import java.util.List;

/**
 * @author truongbx
 */
@Entity
@Table(name = "pre_process_unit")
public class PreProcessUnit extends BaseCategory implements Serializable {

    @Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRE_PROCESS_ID")
    private long preProcessId;
    @Column(name = "PRE_PROCESS_NAME")
    private String preProcessName;
    @Column(name = "PRE_PROCESS_TYPE")
    private Integer preProcessType;
    @Column(name = "DEFAULT_VALUE")
    private Long defaultValue;
    @Column(name = "INPUT_FIELDS")
    private String inputFields;
    @Column(name = "SPECIAL_FIELDS")
    private String specialFields;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CATEGORY_ID")
    private Long categoryId;
    @Column(name = "TYPE")
    private Long type;

    @Transient
    private Long inputMode;
    @Transient
    private Long inputModeSecond;
    @Transient
    private String inputFieldsSecond;
    @Transient
    private String inputFieldsOne;
    @Transient
    private String specialFieldsTmp;
    @Transient
    private List<ProcessValue> lstProcessValue;

    @Transient
    private boolean usingCurTime;
    @Transient
    private boolean staticInput;
    @Transient
    private String otherString = "";
    @Transient
    private String otherStringSecond = "";
    @Transient
    private boolean usingParameter = false;
    @Transient
    private boolean equalAtEndValue = false;
    @Transient
    private String columnName;
    @Transient
    private String filter;

    //<editor-fold defaultstate="collapsed" desc="get/set">
    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getFilter() {
        return preProcessName;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public List<ProcessValue> getLstProcessValue() {
        return lstProcessValue;
    }

    public void setLstProcessValue(List<ProcessValue> lstProcessValue) {
        this.lstProcessValue = lstProcessValue;
    }

    public boolean getUsingCurTime() {
        return usingCurTime;
    }

    public void setUsingCurTime(boolean usingCurTime) {
        this.usingCurTime = usingCurTime;
    }

    public String getSpecialFieldsTmp() {
        return specialFieldsTmp;
    }

    public void setSpecialFieldsTmp(String specialFieldsTmp) {
        this.specialFieldsTmp = specialFieldsTmp;
    }

    public long getPreProcessId() {
        return preProcessId;
    }

    public void setPreProcessId(long preProcessId) {
        this.preProcessId = preProcessId;
    }

    public String getPreProcessName() {
        return preProcessName;
    }

    public void setPreProcessName(String preProcessName) {
        this.preProcessName = preProcessName;
    }

    public Integer getPreProcessType() {
        return preProcessType;
    }

    public void setPreProcessType(Integer preProcessType) {
        this.preProcessType = preProcessType;
    }

    public Long getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Long defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getSpecialFields() {
        return specialFields;
    }

    public void setSpecialFields(String specialFields) {
        this.specialFields = specialFields;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
    //</editor-fold>

    public String getInputFieldsSecond() {
        Long value = this.inputModeSecond == null ? 1 : this.inputModeSecond;
        this.inputFieldsSecond = "type:" + value + "|" + this.otherStringSecond;
        return this.inputFieldsSecond;
    }

    public void setInputFieldsSecond(String inputFieldsSecond) {
        this.inputFieldsSecond = inputFieldsSecond;
    }

    public String getOtherStringSecond() {
        return otherStringSecond;
    }

    public void setOtherStringSecond(String otherStringSecond) {
        this.otherStringSecond = otherStringSecond.replaceAll("\\{}", "");
    }

    public String getInputFields() {
        Long value = this.inputMode == null ? 1 : this.inputMode;
        this.inputFields = "type:" + value + "|" + this.otherString;
        return this.inputFields;
    }

    public String getInputFieldsOne() {
        Long value = this.inputMode == null ? 1 : this.inputMode;
        this.inputFieldsOne = "type:" + value + "|" + this.otherString;
        return this.inputFieldsOne;
    }

    public void setInputFieldsOne(String inputFieldsOne) {
        this.inputFieldsOne = inputFieldsOne;
    }

    public String getOriginInputField() {
        return this.inputFields;
    }

    public void setInputFields(String inputFields) {
        this.inputFields = inputFields;
    }

    @Override
    public long getParentId() {
        return this.getCategoryId();
    }

    @Override
    public void setParentId(Long id) {
        setCategoryId(id);
    }

    @Override
    public String getTreeType() {
        return "preprocess_string";
    }

    @Override
    public String getName() {
        return this.preProcessName;
    }

    @Override
    public Class getParentType() {
        return Category.class;
    }

    public Long getInputMode() {
        if (inputMode == null) {
            inputMode = 1L;
        }
        return inputMode;
    }

    public void setInputMode(Long inputMode) {
        this.inputMode = inputMode;
    }

    public Long getInputModeSecond() {
        if (inputModeSecond == null) {
            inputModeSecond = 1L;
        }
        return inputModeSecond;
    }

    public void setInputModeSecond(Long inputModeSecond) {
        this.inputModeSecond = inputModeSecond;
    }

    /**
     * Su dung trong truong hop co 2 inputFields
     *
     * @param data
     */
    public void asMapMultiFields(PreProcessUnit data) {
        this.preProcessId = data.getPreProcessId();
        this.preProcessName = data.getPreProcessName();
        this.preProcessType = data.getPreProcessType();
        this.defaultValue = data.getDefaultValue();
        this.inputFields = data.getOriginInputField();
        if (!DataUtil.isNullOrEmpty(data.getSpecialFields())) {
            this.specialFieldsTmp = data.getSpecialFields().substring(data.getSpecialFields().indexOf(":") + 1,
                    data.getSpecialFields().length());
            this.specialFields = data.getSpecialFields();
        }
        this.description = data.getDescription();
        this.categoryId = data.getCategoryId();

        List<String> lstInputFields = DataUtil.splitListFile(data.getOriginInputField(), "%");
        if (!DataUtil.isNullOrEmpty(lstInputFields)) {
            int i = 0;
            for (String ifields : lstInputFields) {
                if (i == 0) {
                    int index = ifields.indexOf("|");
                    String inputMode = ifields.substring(index - 1, index);
                    this.otherString = ifields.substring(index + 1).replaceAll("\\{}", "");
                    this.inputMode = Long.parseLong(inputMode);
                }
                if (i == 1) {
                    int index = ifields.indexOf("|");
                    String inputMode = ifields.substring(index - 1, index);
                    this.otherStringSecond = ifields.substring(index + 1).replaceAll("\\{}", "");
                    this.inputModeSecond = Long.parseLong(inputMode);
                }
                i++;
            }
        }

    }

    public void asMap(PreProcessUnit data) {
        this.preProcessId = data.getPreProcessId();
        this.preProcessName = data.getPreProcessName();
        this.preProcessType = data.getPreProcessType();
        this.defaultValue = data.getDefaultValue();
        this.inputFields = data.getOriginInputField();
        int index = this.inputFields.indexOf("|");
        String inputMode = this.inputFields.substring(index - 1, index);
        this.otherString = this.inputFields.substring(index + 1);
        this.otherString = otherString.replaceAll("\\{}", "");
        this.specialFields = data.getSpecialFields();
        this.inputMode = Long.parseLong(inputMode);
        if ((preProcessType.equals(Constants.PRE_PROCESS_UNIT_TIME)
                || preProcessType.equals(Constants.PRE_PROCESS_UNIT_DATE)) && !DataUtil.isNullOrEmpty(data.getSpecialFields())) {
            String using = data.getSpecialFields().substring(data.getSpecialFields().indexOf(":") + 1,
                    data.getSpecialFields().length());
            this.usingCurTime = DataUtil.safeEqual("true", using);
            this.specialFields = data.getSpecialFields();
        }
        this.description = data.getDescription();
        this.categoryId = data.getCategoryId();
        if (preProcessType.equals(Constants.PRE_PROCESS_UNIT_NUMBER)) {
            String specialField = specialFields.substring(specialFields.indexOf(":") + 1);
            if (specialField.equalsIgnoreCase("true")) {
                usingParameter = true;
            } else {
                usingParameter = false;
            }
        }
        if (preProcessType.equals(Constants.PRE_PROCESS_UNIT_NUMBER_RANGE)) {
            String[] specialNumberRange = specialFields.split(";");
            if (specialNumberRange.length != 2) {
                return;
            }
            String specialField = specialNumberRange[0].substring(specialNumberRange[0].indexOf(":") + 1);
            if (specialField.equalsIgnoreCase("true")) {
                usingParameter = true;
            } else {
                usingParameter = false;
            }
            String equaValue = specialNumberRange[1].substring(specialNumberRange[1].indexOf(":") + 1);
            if (equaValue.equalsIgnoreCase("true")) {
                equalAtEndValue = true;
            } else {
                equalAtEndValue = false;
            }
        }

    }

    public String getOtherString() {
        return otherString;
    }

    public void setOtherString(String otherString) {
        this.otherString = otherString.replaceAll("\\{}", "");
    }

    public boolean isUsingParameter() {
        return usingParameter;
    }

    public void setUsingParameter(boolean usingParameter) {
        this.usingParameter = usingParameter;
    }

    public boolean getStaticInput() {
        return staticInput;
    }

    public void setStaticInput(boolean staticInput) {
        this.staticInput = staticInput;
    }

    public boolean isEqualAtEndValue() {
        return equalAtEndValue;
    }

    public void setEqualAtEndValue(boolean equalAtEndValue) {
        this.equalAtEndValue = equalAtEndValue;
    }
}
