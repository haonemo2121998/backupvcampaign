package vn.viettel.campaign.entities;
import vn.viettel.campaign.dto.ResultTableDTO;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "row_ct")
public class RowCt {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ROW_ID")
  private long rowId;
  @Column(name = "VALUE")
  private String value;
  @Column(name = "ROW_INDEX")
  private long rowIndex;
  @Transient
  private List<ResultTableDTO> lstResultTableDTO;

  public List<ResultTableDTO> getLstResultTableDTO() {
    return lstResultTableDTO;
  }

  public void setLstResultTableDTO(List<ResultTableDTO> lstResultTableDTO) {
    this.lstResultTableDTO = lstResultTableDTO;
  }

  public long getRowId() {
    return rowId;
  }

  public void setRowId(long rowId) {
    this.rowId = rowId;
  }


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }


  public long getRowIndex() {
    return rowIndex;
  }

  public void setRowIndex(long rowIndex) {
    this.rowIndex = rowIndex;
  }

}
