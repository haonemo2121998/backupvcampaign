package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "function")
public class Function {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "FUNCTION_ID")
  private long functionId;
  @Column(name = "FUNCTION_NAME")
  private String functionName;
  @Column(name = "FUNCTION_DISPLAY")
  private String functionDisplay;
  @Column(name = "NUMBER_PARAMETER")
  private Long numberParameter;
  @Column(name = "ALIAS")
  private Long alias;
  @Column(name = "DESCRIPTION")
  private String description;
  @Column(name = "CATEGORY_ID")
  private Long categoryId;
  @Column(name = "TYPE")
  private Long type;


  public long getFunctionId() {
    return functionId;
  }

  public void setFunctionId(long functionId) {
    this.functionId = functionId;
  }


  public String getFunctionName() {
    return functionName;
  }

  public void setFunctionName(String functionName) {
    this.functionName = functionName;
  }


  public Long getNumberParameter() {
    return numberParameter;
  }

  public void setNumberParameter(Long numberParameter) {
    this.numberParameter = numberParameter;
  }


  public Long getAlias() {
    return alias;
  }

  public void setAlias(Long alias) {
    this.alias = alias;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }


  public Long getType() {
    return type;
  }

  public void setType(Long type) {
    this.type = type;
  }

  public String getFunctionDisplay() {
    return functionDisplay;
  }

  public void setFunctionDisplay(String functionDisplay) {
    this.functionDisplay = functionDisplay;
  }
}
