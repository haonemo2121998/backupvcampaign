/*
 * Copyright 2009-2014 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package vn.viettel.campaign.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author truongbx
 */
@Entity
@Table(name = "RULE")
@Getter
@Setter
public class Rule extends BaseCategory implements Serializable {

    @Id
    @Column(name = "RULE_ID")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ruleId;
    @Column(name = "DESCRIPTION")
    public String description;
    @Column(name = "RULE_NAME")
    public String ruleName;
    @Column(name = "CATEGORY_ID")
    private long categoryId;
    //HaBM2: Update change structure
    @Column(name = "CDR_SERVICE_IDS")
    private String cdrServiceId;
    
    @Transient
    public String segmentName;
    @Transient
    public long segmentId;
    @Transient
    public Integer priority;
    @Transient
    public Segment segment;
    @Transient
    private String name;
    
    public Rule() {
    }

    public Segment getSegment() {
        return segment;
    }

    public void setSegment(Segment segment) {
        if (segment!= null){
            this.segmentName = segment.getSegmentName();
            this.segmentId = segment.getSegmentId();
        }
        this.segment = segment;
    }


    public void setName(String name) {
        this.name = name;
    }
    @Override
    public long getParentId() {
        return getCategoryId();
    }

    @Override
    public String getTreeType() {
        return "rule_online";
    }

    @Override
    public void setParentId(Long id) {
        setCategoryId(id);
    }

    @Override
    public String getName() {
        return this.ruleName;
    }

    @Override
    public Class getParentType() {
        return Category.class;
    }
    public void asMap(Rule rule){
        this.ruleId = rule.getRuleId();
        this.ruleName = rule.getRuleName();
        this.description = rule.getDescription();
        this.categoryId = rule.getCategoryId();
        this.cdrServiceId = rule.getCdrServiceId();
    }
}
