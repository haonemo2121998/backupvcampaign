/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ABC
 */
@Entity
@Table(name = "assessment_rule_pre_process_unit_map")
public class AssessmentRulePreProcessUnitMap implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long Id;
    @Basic(optional = false)
    @Column(name = "RULE_ID")
    private Long ruleId;
    @Basic(optional = false)
    @Column(name = "PRE_PROCESS_UNIT_ID")
    private Long preProcessUnitId;

    public AssessmentRulePreProcessUnitMap() {
    }

    public AssessmentRulePreProcessUnitMap(Long Id, Long ruleId, Long preProcessUnitId) {
        this.Id = Id;
        this.ruleId = ruleId;
        this.preProcessUnitId = preProcessUnitId;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public Long getPreProcessUnitId() {
        return preProcessUnitId;
    }

    public void setPreProcessUnitId(Long preProcessUnitId) {
        this.preProcessUnitId = preProcessUnitId;
    }

}
