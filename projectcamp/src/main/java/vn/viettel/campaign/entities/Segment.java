package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "segment")
public class Segment implements Serializable,TreeItemBase{

    @Id
    @Column(name = "SEGMENT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long segmentId;
    @Column(name = "SEGMENT_NAME")
    private String segmentName;
    @Column(name = "PARENT_ID")
    private Long parentId;
    @Column(name = "SEGMENT_SIZE")
    private Long segmentSize;
    @Column(name = "PATH")
    private String path;
    @Column(name = "FAILED_PATH")
    private String failedPath;
    @Column(name = "TEMP_PATH")
    private String tempPath;
    @Column(name = "HIST_PATH")
    private String histPath;
    @Column(name = "CONTROL_PERCENTAGE")
    private Long controlPercentage;
    @Column(name = "APPLICATION_ID")
    private Long applicationId;
    @Column(name = "EXP_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expDate;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CATEGORY_ID")
    private Long categoryId;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "EFF_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date effDate;
    @Transient
    private int priorityGroup;
    @Transient
    private String name;
    @Transient
    private boolean isNew = false;

    //<editor-fold defaultstate="collapsed" desc="get/set">
    public String getFilter() {
        return getName();
    }

    public Long getSegmentId() {
        return segmentId == null ? -1 : segmentId;
    }

    public void setSegmentId(Long segmentId) {
        this.segmentId = segmentId;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getSegmentSize() {
        return segmentSize;
    }

    public void setSegmentSize(Long segmentSize) {
        this.segmentSize = segmentSize;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getControlPercentage() {
        return controlPercentage;
    }

    public void setControlPercentage(Long controlPercentage) {
        this.controlPercentage = controlPercentage;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public Date getExpDate() {
        return expDate;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Date getEffDate() {
        return effDate;
    }

    public void setEffDate(Date effDate) {
        this.effDate = effDate;
    }

    public int getPriorityGroup() {
        return priorityGroup;
    }

    public void setPriorityGroup(int priorityGroup) {
        this.priorityGroup = priorityGroup;
    }

    public String getName() {
        return this.segmentName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public String getFailedPath() {
        return failedPath;
    }

    public void setFailedPath(String failedPath) {
        this.failedPath = failedPath;
    }

    public String getTempPath() {
        return tempPath;
    }

    public void setTempPath(String tempPath) {
        this.tempPath = tempPath;
    }

    public String getHistPath() {
        return histPath;
    }

    public void setHistPath(String histPath) {
        this.histPath = histPath;
    }
    //</editor-fold>

    public void mapData(Segment segment) {
        this.segmentId = segment.getSegmentId();
        this.segmentName = segment.getSegmentName();
        this.parentId = segment.getParentId();
        this.segmentSize = segment.getSegmentSize();
        this.path = segment.getPath();
        this.failedPath = segment.getFailedPath();
        this.tempPath = segment.getTempPath();
        this.histPath = segment.getHistPath();
        this.controlPercentage = segment.getControlPercentage();
        this.applicationId = segment.getApplicationId();
        this.expDate = segment.getExpDate();
        this.description = segment.getDescription();
        this.categoryId = segment.getCategoryId();
        this.isActive = segment.getIsActive();
        this.effDate = segment.getEffDate();
    }

    //HaBM2: add fix - add new segment for campaign onine/offline
    public String getKey() {
        return segmentName + "_" + segmentId + "_" + this.hashCode();
    }
}
