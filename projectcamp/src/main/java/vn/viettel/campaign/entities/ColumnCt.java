package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "column_ct")
public class ColumnCt {
  @Id
  @Column(name = "COLUMN_ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long columnId;
  @Column(name = "COLUMN_NAME")
  private String columnName;
  @Column(name = "PRE_PROCESS_ID")
  private long preProcessId;
  @Column(name = "COLUMN_INDEX")
  private long columnIndex;
  @Transient
  private String name;

  @Transient
  private String preprocessName;

  public long getColumnId() {
    return columnId;
  }

  public void setColumnId(long columnId) {
    this.columnId = columnId;
  }


  public String getColumnName() {
    return columnName;
  }

  public void setColumnName(String columnName) {
    this.columnName = columnName;
  }


  public long getPreProcessId() {
    return preProcessId;
  }

  public void setPreProcessId(long preProcessId) {
    this.preProcessId = preProcessId;
  }


  public long getColumnIndex() {
    return columnIndex;
  }

  public void setColumnIndex(long columnIndex) {
    this.columnIndex = columnIndex;
  }

  public String getName() {
    return this.columnName;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPreprocessName() {
    return preprocessName;
  }

  public void setPreprocessName(String preprocessName) {
    this.preprocessName = preprocessName;
  }
  public String getFilter() {
    return getName();
  }
}
