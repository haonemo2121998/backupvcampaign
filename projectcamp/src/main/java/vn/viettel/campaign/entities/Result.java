/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 *
 * @author Lifesup
 */
@Entity
@Table(name = "result")
public class Result implements Serializable {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RESULT_ID")
    private Long resultId;
    @Basic(optional = false)
    @Column(name = "RESULT_TYPE")
    private int resultType;
    @Column(name = "STEP_INDEX")
    private Integer stepIndex;
    @Column(name = "PROMOTION_BLOCK_ID")
    private BigInteger promotionBlockId;

    public Result() {
    }

    public Result(Long resultId) {
        this.resultId = resultId;
    }

    public Result(Long resultId, int resultType) {
        this.resultId = resultId;
        this.resultType = resultType;
    }

    public Long getResultId() {
        return resultId;
    }

    public void setResultId(Long resultId) {
        this.resultId = resultId;
    }

    public int getResultType() {
        return resultType;
    }

    public void setResultType(int resultType) {
        this.resultType = resultType;
    }

    public Integer getStepIndex() {
        return stepIndex;
    }

    public void setStepIndex(Integer stepIndex) {
        this.stepIndex = stepIndex;
    }

    public BigInteger getPromotionBlockId() {
        return promotionBlockId;
    }

    public void setPromotionBlockId(BigInteger promotionBlockId) {
        this.promotionBlockId = promotionBlockId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (resultId != null ? resultId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Result)) {
            return false;
        }
        Result other = (Result) object;
        if ((this.resultId == null && other.resultId != null) || (this.resultId != null && !this.resultId.equals(other.resultId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vn.viettel.campaign.entities.gen.Result[ resultId=" + resultId + " ]";
    }
    
}
