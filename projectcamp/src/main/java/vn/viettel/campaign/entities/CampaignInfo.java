package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "campaign_info")
public class CampaignInfo {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "CAMPAIGN_INFO_ID")
  private long campaignInfoId;
  @Column(name = "CAMPAIGN_ID")
  private Long campaignId;
  @Column(name = "SEGMENT_ID")
  private Long segmentId;
  @Column(name = "RULE_ID")
  private Long ruleId;
  @Column(name = "PRIORITY")
  private Integer priority;
  @Column(name = "PRIORITY_GROUP")
  private Integer priorityGroup;


  public long getCampaignInfoId() {
    return campaignInfoId;
  }

  public void setCampaignInfoId(long campaignInfoId) {
    this.campaignInfoId = campaignInfoId;
  }


  public Long getCampaignId() {
    return campaignId;
  }

  public void setCampaignId(Long campaignId) {
    this.campaignId = campaignId;
  }


  public Long getSegmentId() {
    return segmentId;
  }

  public void setSegmentId(Long segmentId) {
    this.segmentId = segmentId;
  }


  public Long getRuleId() {
    return ruleId;
  }

  public void setRuleId(Long ruleId) {
    this.ruleId = ruleId;
  }


  public Integer getPriority() {
    return priority;
  }

  public void setPriority(Integer priority) {
    this.priority = priority;
  }


  public Integer getPriorityGroup() {
    return priorityGroup;
  }

  public void setPriorityGroup(Integer priorityGroup) {
    this.priorityGroup = priorityGroup;
  }

}
