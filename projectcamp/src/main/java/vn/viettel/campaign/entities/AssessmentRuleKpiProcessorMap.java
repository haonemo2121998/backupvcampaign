/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "assessment_rule_kpi_processor_map")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AssessmentRuleKpiProcessorMap.findAll", query = "SELECT a FROM AssessmentRuleKpiProcessorMap a")
    , @NamedQuery(name = "AssessmentRuleKpiProcessorMap.findById", query = "SELECT a FROM AssessmentRuleKpiProcessorMap a WHERE a.id = :id")
    , @NamedQuery(name = "AssessmentRuleKpiProcessorMap.findByRuleId", query = "SELECT a FROM AssessmentRuleKpiProcessorMap a WHERE a.ruleId = :ruleId")
    , @NamedQuery(name = "AssessmentRuleKpiProcessorMap.findByProcessorId", query = "SELECT a FROM AssessmentRuleKpiProcessorMap a WHERE a.processorId = :processorId")})
public class AssessmentRuleKpiProcessorMap implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "RULE_ID")
    private long ruleId;
    @Basic(optional = false)
    @Column(name = "PROCESSOR_ID")
    private long processorId;

    public AssessmentRuleKpiProcessorMap() {
    }

    public AssessmentRuleKpiProcessorMap(Long id) {
        this.id = id;
    }

    public AssessmentRuleKpiProcessorMap(Long id, long ruleId, long processorId) {
        this.id = id;
        this.ruleId = ruleId;
        this.processorId = processorId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getRuleId() {
        return ruleId;
    }

    public void setRuleId(long ruleId) {
        this.ruleId = ruleId;
    }

    public long getProcessorId() {
        return processorId;
    }

    public void setProcessorId(long processorId) {
        this.processorId = processorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssessmentRuleKpiProcessorMap)) {
            return false;
        }
        AssessmentRuleKpiProcessorMap other = (AssessmentRuleKpiProcessorMap) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vn.viettel.campaign.entities.AssessmentRuleKpiProcessorMap[ id=" + id + " ]";
    }
    
}
