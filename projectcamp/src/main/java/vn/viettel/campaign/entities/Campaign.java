/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import vn.viettel.campaign.constants.Constants;

/**
 * @author ConKC
 */
@Entity
@Table(name = "CAMPAIGN")
public class Campaign implements Serializable,TreeItemBase {

    @Id
    @Column(name = "CAMPAIGN_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long campaignId;
    @Column(name = "CAMPAIGN_NAME")
    private String name;
    @Column(name = "CAMPAIGN_TYPE")
    private Integer campaignType;
    @Column(name = "IS_DEFAULT")
    private Integer isDefault;
    @Column(name = "EFF_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date effDate;
    @Column(name = "EXP_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expiredDate;
    @Column(name = "PRIORITY")
    private Integer priority;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CATEGORY_ID")
    private Long categoryId;
    @Column(name = "STATUS")
    private Integer status;
    @Column(name = "SPECIAL_PROM_ID")
    private Long specialPromId;
    @Column(name = "BLACKLIST_ID")
    private Long blacklistId;

    public Campaign() {
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(Integer campaignType) {
        this.campaignType = campaignType;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public boolean getDefaultStatus() {
        if (isDefault == null) {
            return true;
        }
        return isDefault == 1 ? true : false;
    }

    public void setDefaultStatus(boolean val) {
        if (val) {
            isDefault = 1;
        } else {
            isDefault = 0;
        }
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getEffDate() {
        return effDate;
    }

    public void setEffDate(Date effDate) {
        this.effDate = effDate;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSpecialPromId() {
        return specialPromId;
    }

    public void setSpecialPromId(Long specialPromId) {
        this.specialPromId = specialPromId;
    }

    public Long getBlacklistId() {
        return blacklistId;
    }

    public void setBlacklistId(Long blacklistId) {
        this.blacklistId = blacklistId;
    }

    public void mapData(Campaign campaign) {
        this.name = campaign.getName();
        this.campaignType = campaign.getCampaignType();
        this.isDefault = campaign.getIsDefault();
        this.effDate = campaign.getEffDate();
        this.expiredDate = campaign.getExpiredDate();
        this.priority = campaign.getPriority();
        this.description = campaign.getDescription();
        this.categoryId = campaign.getCategoryId();
        this.status = campaign.getStatus();
        this.specialPromId = campaign.getSpecialPromId();
        this.blacklistId = campaign.getBlacklistId();
    }

    @Override
    public String toString() {
        return Integer.toString(Constants.CAMPAIGN_ONLINE_TYPE) + "," + Integer.toString(Constants.CAMPAIGN_OFFLINE_TYPE);
    }

    public String getFilter() {
        return getName();
    }
}
