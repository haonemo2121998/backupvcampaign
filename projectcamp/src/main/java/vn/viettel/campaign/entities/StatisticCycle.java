package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "statistic_cycle")
public class StatisticCycle {
    @Id
    @Column(name = "ID")
    private long id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "CYCLE_UNIT_ID")
    private Long cycleUnitId;
    @Column(name = "CYCLE_UNIT_TIMES")
    private Long cycleUnitTimes;
    @Column(name = "MAX_NOTIFY")
    private Long maxNotify;
    @Column(name = "CATEGORY_ID")
    private long categoryId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCycleUnitId() {
        return cycleUnitId;
    }

    public void setCycleUnitId(Long cycleUnitId) {
        this.cycleUnitId = cycleUnitId;
    }

    public Long getCycleUnitTimes() {
        return cycleUnitTimes;
    }

    public void setCycleUnitTimes(Long cycleUnitTimes) {
        this.cycleUnitTimes = cycleUnitTimes;
    }

    public Long getMaxNotify() {
        return maxNotify;
    }

    public void setMaxNotify(Long maxNotify) {
        this.maxNotify = maxNotify;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }
    public String getFilter() {
        return getName();
    }
}
