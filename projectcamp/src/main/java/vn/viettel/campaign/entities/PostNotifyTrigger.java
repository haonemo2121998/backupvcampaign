package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "post_notify_trigger")
public class PostNotifyTrigger {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "POST_NOTIFY_TRIGGER_ID")
	private long postNotifyTriggerId;
	@Column(name = "OCS_BEHAVIOUR_ID")
	private Long ocsBehaviourId;
	@Column(name = "ERROR_CODE")
	private Long errorCode;
	@Column(name = "COMMAND_ID")
	private Long commandId;
	@Column(name = "NOTIFY_ID")
	private Long notifyId;

	@Transient
	private String notifyName;

	public long getPostNotifyTriggerId() {
		return postNotifyTriggerId;
	}

	public void setPostNotifyTriggerId(long postNotifyTriggerId) {
		this.postNotifyTriggerId = postNotifyTriggerId;
	}


	public Long getOcsBehaviourId() {
		return ocsBehaviourId;
	}

	public void setOcsBehaviourId(Long ocsBehaviourId) {
		this.ocsBehaviourId = ocsBehaviourId;
	}


	public Long getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}


	public Long getCommandId() {
		return commandId;
	}

	public void setCommandId(Long commandId) {
		this.commandId = commandId;
	}


	public Long getNotifyId() {
		return notifyId;
	}

	public void setNotifyId(Long notifyId) {
		this.notifyId = notifyId;
	}

	public String getNotifyName() {
		return notifyName;
	}

	public void setNotifyName(String notifyName) {
		this.notifyName = notifyName;
	}
}
