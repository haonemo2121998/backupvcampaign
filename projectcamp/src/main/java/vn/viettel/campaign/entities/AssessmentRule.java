/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ABC
 */
@Entity
@Table(name = "assessment_rule")
public class AssessmentRule extends BaseCategory implements Serializable, TreeItemBase {

    private static final Long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RULE_ID")
    private Long ruleId;
    @Basic(optional = false)
    @Column(name = "RULE_NAME")
    private String ruleName;
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    public AssessmentRule() {
    }

    public AssessmentRule(Long ruleId, String ruleName, String description, Long categoryId) {
        this.ruleId = ruleId;
        this.ruleName = ruleName;
        this.description = description;
        this.categoryId = categoryId;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getFilter() {
        return getRuleName();
    }

    public void mapData(AssessmentRule assessmentRule) {
        this.ruleId = assessmentRule.getRuleId();
        this.ruleName = assessmentRule.getRuleName();
        this.description = assessmentRule.getDescription();
        this.categoryId = assessmentRule.getCategoryId();
    }

    @Override
    public long getParentId() {
        return  this.getCategoryId();
    }

    @Override
    public void setParentId(Long id) {
         setCategoryId(id);
    }

    @Override
    public String getTreeType() {
        return "object";
    }

    @Override
    public String getName() {
        return this.ruleName;
    }

    @Override
    public Class getParentType() {
        return Category.class;
    }
}
