package vn.viettel.campaign.entities;


import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author truongbx
 */
@Entity
@Table(name = "blacklist")
public class BlackList implements Serializable {

  @Id
  @Column(name = "BLACKLIST_ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long blacklistId;
  @Column(name = "BLACKLIST_NAME")
  private String name;
  @Column(name = "CATEGORY_ID")
  private Long categoryId;
  @Column(name = "PATH")
  private String path;
  @Column(name = "DESCRIPTION")
  private String description;
  @Column(name = "TEMP_PATH")
  private String tempPath;
  @Column(name = "HIST_PATH")
  private String histPath;
  @Column(name = "FAILED_PATH")
  private String failedPath;


  public Long getBlacklistId() {
    return blacklistId;
  }

  public void setBlacklistId(Long blacklistId) {
    this.blacklistId = blacklistId;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }


  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public String getTempPath() {
    return tempPath;
  }

  public void setTempPath(String tempPath) {
    this.tempPath = tempPath;
  }


  public String getHistPath() {
    return histPath;
  }

  public void setHistPath(String histPath) {
    this.histPath = histPath;
  }


  public String getFailedPath() {
    return failedPath;
  }

  public void setFailedPath(String failedPath) {
    this.failedPath = failedPath;
  }
  public String getFilter() {
    return getName();
  }
}
