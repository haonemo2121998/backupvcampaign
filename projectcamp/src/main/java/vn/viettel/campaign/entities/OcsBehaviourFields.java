package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "ocs_behaviour_fields")
public class OcsBehaviourFields {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "BEHAVIOUR_FIELD_ID")
	private long behaviourFieldId;
	@Column(name = "OCS_BEHAVIOUR_ID")
	private long ocsBehaviourId;
	@Column(name = "CRA_FIELD_ID")
	private long craFieldId;
	@Column(name = "DATA_TYPE")
	private long dataType;
	@Column(name = "VALUE")
	private String value;
	@Column(name = "SOURCE_TYPE")
	private Long sourceType;
	@Transient
	private long templateType;
	@Transient
	private String templateTypeName;
	@Transient
	private String name;

	public long getBehaviourFieldId() {
		return behaviourFieldId;
	}

	public void setBehaviourFieldId(long behaviourFieldId) {
		this.behaviourFieldId = behaviourFieldId;
	}


	public long getOcsBehaviourId() {
		return ocsBehaviourId;
	}

	public void setOcsBehaviourId(long ocsBehaviourId) {
		this.ocsBehaviourId = ocsBehaviourId;
	}


	public long getCraFieldId() {
		return craFieldId;
	}

	public void setCraFieldId(long craFieldId) {
		this.craFieldId = craFieldId;
	}


	public long getDataType() {
		return dataType;
	}

	public void setDataType(long dataType) {
		this.dataType = dataType;
	}


	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}


	public Long getSourceType() {
		return sourceType;
	}

	public void setSourceType(Long sourceType) {
		this.sourceType = sourceType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getTemplateType() {
		return templateType;
	}

	public void setTemplateType(long templateType) {
		this.templateType = templateType;
	}

	public String getTemplateTypeName() {
		if (templateType == 1) {
			templateTypeName = "Exact value";
		} else if (templateType == 2) {
			templateTypeName = "Customize";
		}
		return templateTypeName;
	}

	public void setTemplateTypeName(String templateTypeName) {
		this.templateTypeName = templateTypeName;
	}
	public String getFilter() {
		return getName();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof  OcsBehaviourFields)){
			return false;
		}
		if (this.behaviourFieldId != 0 && ((OcsBehaviourFields) obj).getBehaviourFieldId() == this.behaviourFieldId){
			return true;
		}
		return false;
	}
}
