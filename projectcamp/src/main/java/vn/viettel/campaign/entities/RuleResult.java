/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "rule_result")

public class RuleResult implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RULE_RESULT_ID")
    private Long ruleResultId;
    @Basic(optional = false)
    @Column(name = "RESULT_ID")
    private long resultId;
    @Basic(optional = false)
    @Column(name = "ROW_INDEX")
    private int rowIndex;

    public RuleResult() {
    }

    public RuleResult(Long ruleResultId) {
        this.ruleResultId = ruleResultId;
    }

    public RuleResult(Long ruleResultId, long resultId, int rowIndex) {
        this.ruleResultId = ruleResultId;
        this.resultId = resultId;
        this.rowIndex = rowIndex;
    }

    public Long getRuleResultId() {
        return ruleResultId;
    }

    public void setRuleResultId(Long ruleResultId) {
        this.ruleResultId = ruleResultId;
    }

    public long getResultId() {
        return resultId;
    }

    public void setResultId(long resultId) {
        this.resultId = resultId;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ruleResultId != null ? ruleResultId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RuleResult)) {
            return false;
        }
        RuleResult other = (RuleResult) object;
        if ((this.ruleResultId == null && other.ruleResultId != null) || (this.ruleResultId != null && !this.ruleResultId.equals(other.ruleResultId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vn.viettel.campaign.entities.autogen.RuleResult[ ruleResultId=" + ruleResultId + " ]";
    }
    
}
