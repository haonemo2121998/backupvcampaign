package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "parameter")
public class Parameter {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PARAMETER_ID")
	private long parameterId;
	@Column(name = "OWNER_LEVEL")
	private Long ownerLevel;
	@Column(name = "FOR_TEMPLATE")
	private Long forTemplate;
	@Column(name = "PARAMETER_NAME")
	private String parameterName;
	@Column(name = "PARAMETER_VALUE")
	private String parameterValue;
	@Column(name = "CATEGORY_ID")
	private Long categoryId;

	@Transient
	private String name;

	public long getParameterId() {
		return parameterId;
	}

	public void setParameterId(long parameterId) {
		this.parameterId = parameterId;
	}


	public Long getOwnerLevel() {
		return ownerLevel;
	}

	public void setOwnerLevel(Long ownerLevel) {
		this.ownerLevel = ownerLevel;
	}


	public Long getForTemplate() {
		return forTemplate;
	}

	public void setForTemplate(Long forTemplate) {
		this.forTemplate = forTemplate;
	}


	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}


	public String getParameterValue() {
		return parameterValue;
	}

	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}


	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return this.parameterValue;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getFilter() {
		return getParameterName();
	}
}
