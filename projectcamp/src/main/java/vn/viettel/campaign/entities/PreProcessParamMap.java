package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "pre_process_param_map")
public class PreProcessParamMap {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "PRE_PROCESS_PARAM_MAP")
  private long preProcessParamMap;
  @Column(name = "PROCESS_PARAM_ID")
  private long processParamId;
  @Column(name = "PRE_PROCESS_ID")
  private long preProcessId;


  public long getPreProcessParamMap() {
    return preProcessParamMap;
  }

  public void setPreProcessParamMap(long preProcessParamMap) {
    this.preProcessParamMap = preProcessParamMap;
  }


  public long getProcessParamId() {
    return processParamId;
  }

  public void setProcessParamId(long processParamId) {
    this.processParamId = processParamId;
  }


  public long getPreProcessId() {
    return preProcessId;
  }

  public void setPreProcessId(long preProcessId) {
    this.preProcessId = preProcessId;
  }

}
