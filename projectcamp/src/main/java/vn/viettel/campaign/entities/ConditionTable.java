package vn.viettel.campaign.entities;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "condition_table")
@Getter
@Setter
public class ConditionTable {

  @Id
  @Column(name = "CONDITION_TABLE_ID")
//  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long conditionTableId;
  @Column(name = "DESCRIPTION")
  private String description;
  @Column(name = "CATEGORY_ID")
  private long categoryId;
  @Column(name = "CONDITION_TABLE_NAME")
  private String conditionTableName;
  @Column(name = "DEFAULT_RESULT_INDEX")
  private long defaultResultIndex;
  @Transient
  private String name ;


  public String getName() {
    return this.conditionTableName;
  }

  public void setName(String name) {
    this.name = name;
  }
  public String getFilter() {
    return getName();
  }
}
