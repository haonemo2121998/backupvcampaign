package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "field_id_ocs")
public class FieldIdOcs {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "FIELD_ID")
  private long fieldId;
  @Column(name = "DATA_TYPE")
  private long dataType;
  @Column(name = "NAME")
  private String name;


  public long getFieldId() {
    return fieldId;
  }

  public void setFieldId(long fieldId) {
    this.fieldId = fieldId;
  }


  public long getDataType() {
    return dataType;
  }

  public void setDataType(long dataType) {
    this.dataType = dataType;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  public String getFilter() {
    return getName();
  }

}
