/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author ABC
 */
@Entity
@Table(name = "campaign_evaluation_info")
public class CampaignEvaluationInfo extends BaseCategory implements Serializable, TreeItemBase {

    private static final Long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CAMPAIGN_EVALUATION_INFO_ID")
    private Long campaignEvaluationInfoId;
    @Basic(optional = false)
    @Column(name = "CAMPAIGN_EVALUATION_INFO_NAME")
    private String campaignEvaluationInfoName;
    @Basic(optional = false)
    @Column(name = "CAMPAIGN_ID")
    private Long campaignId;
    @Basic(optional = false)
    @Column(name = "SCHEDULE_ID")
    private Long scheduleId;
    @Column(name = "REASIONING_METHOD")
    private Integer reasioningMethod;
    @Column(name = "IS_DEFAULT")
    private Boolean isDefault;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Transient
    private String scheduleName;
    @Transient
    private String name;
    @Transient
    private String ruleName;
    @Transient
    private Integer isDefaultCampaign;
    @Transient
    private List<FuzzyRule> listFuzzyRule;

    public List<FuzzyRule> getListFuzzyRule() {
        return listFuzzyRule;
    }

    public void setListFuzzyRule(List<FuzzyRule> listFuzzyRule) {
        this.listFuzzyRule = listFuzzyRule;
    }
    
    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Integer getIsDefaultCampaign() {
        return isDefaultCampaign;
    }

    public void setIsDefaultCampaign(Integer isDefaultCampaign) {
        this.isDefaultCampaign = isDefaultCampaign;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CampaignEvaluationInfo() {
    }

    public CampaignEvaluationInfo(Long campaignEvaluationInfoId, String campaignEvaluationInfoName, Long campaignId, Long scheduleId, Integer reasioningMethod, Boolean isDefault, String description, Long categoryId) {
        this.campaignEvaluationInfoId = campaignEvaluationInfoId;
        this.campaignEvaluationInfoName = campaignEvaluationInfoName;
        this.campaignId = campaignId;
        this.scheduleId = scheduleId;
        this.reasioningMethod = reasioningMethod;
        this.isDefault = isDefault;
        this.description = description;
        this.categoryId = categoryId;
    }

    public Long getCampaignEvaluationInfoId() {
        return campaignEvaluationInfoId;
    }

    public void setCampaignEvaluationInfoId(Long campaignEvaluationInfoId) {
        this.campaignEvaluationInfoId = campaignEvaluationInfoId;
    }

    public String getCampaignEvaluationInfoName() {
        return campaignEvaluationInfoName;
    }

    public void setCampaignEvaluationInfoName(String campaignEvaluationInfoName) {
        this.campaignEvaluationInfoName = campaignEvaluationInfoName;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Integer getReasioningMethod() {
        return reasioningMethod;
    }

    public void setReasioningMethod(Integer reasioningMethod) {
        this.reasioningMethod = reasioningMethod;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
    
    public String getFilter() {
        return getCampaignEvaluationInfoName();
    }

    public void mapData(CampaignEvaluationInfo info) {
        this.campaignEvaluationInfoId = info.getCampaignEvaluationInfoId();
        this.campaignEvaluationInfoName = info.getCampaignEvaluationInfoName();
        this.campaignId = info.getCampaignId();
        this.scheduleId = info.getScheduleId();
        this.reasioningMethod = info.getReasioningMethod();
        this.isDefault = info.getIsDefault();
        this.description = info.getDescription();
        this.categoryId = info.getCategoryId();
    }

    @Override
    public long getParentId() {
        return this.categoryId;
    }

    @Override
    public void setParentId(Long id) {
        setCategoryId(id);
    }

    @Override
    public String getTreeType() {
        return "object";
    }

    @Override
    public Class getParentType() {
         return Category.class;
    }

}
