package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author truongbx
 */
@Entity
@Table(name = "ussd_scenario")
public class UssdScenario {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SCENARIO_ID")
    private long scenarioId;
    @Column(name = "SCENARIO_NAME")
    private String scenarioName;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CATEGORY_ID")
    private long categoryId;
    @Transient
    private String name;
    @Transient
    private List<ScenarioNode> lstScenarioNode;

    public List<ScenarioNode> getLstScenarioNode() {
        return lstScenarioNode;
    }

    public void setLstScenarioNode(List<ScenarioNode> lstScenarioNode) {
        this.lstScenarioNode = lstScenarioNode;
    }

    public String getName() {
        return this.scenarioName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getScenarioId() {
        return scenarioId;
    }

    public void setScenarioId(long scenarioId) {
        this.scenarioId = scenarioId;
    }


    public String getScenarioName() {
        return scenarioName;
    }

    public void setScenarioName(String scenarioName) {
        this.scenarioName = scenarioName;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }
    public String getFilter() {
        return getName();
    }
}
