package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "ocs_behaviour_extend_fields")
public class OcsBehaviourExtendFields {

  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  @Column(name = "BEHAVIOUR_ID")
  private long behaviourId;
  @Column(name = "EXTEND_FIELD_ID")
  private long extendFieldId;
  @Column(name = "VALUE")
  private String value;
  @Transient
  private String name;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getBehaviourId() {
    return behaviourId;
  }

  public void setBehaviourId(long behaviourId) {
    this.behaviourId = behaviourId;
  }


  public long getExtendFieldId() {
    return extendFieldId;
  }

  public void setExtendFieldId(long extendFieldId) {
    this.extendFieldId = extendFieldId;
  }


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  public String getFilter() {
    return getName();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof  OcsBehaviourExtendFields)){
      return false;
    }
    if (this.id != 0 && ((OcsBehaviourExtendFields) obj).getId() == this.getId()){
      return true;
    }
    return false;
  }
}
