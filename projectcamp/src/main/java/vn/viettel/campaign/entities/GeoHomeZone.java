package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "geo_home_zone")
public class GeoHomeZone {
  @Id
  @Column(name = "GEO_HOME_ZONE_ID")
  private long geoHomeZoneId;
  @Column(name = "GEO_HOME_ZONE_TYPE")
  private long geoHomeZoneType;
  @Column(name = "GEO_HOME_ZONE_NAME")
  private String geoHomeZoneName;
  @Column(name = "GEO_HOME_ZONE_CODE")
  private String geoHomeZoneCode;
  @Column(name = "DESCRIPTION")
  private String description;
  @Column(name = "CATEGORY_ID")
  private Long categoryId;
  @Transient
  private String name;

  public String getName() {
    return geoHomeZoneName;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getGeoHomeZoneId() {
    return geoHomeZoneId;
  }

  public void setGeoHomeZoneId(long geoHomeZoneId) {
    this.geoHomeZoneId = geoHomeZoneId;
  }

  public long getGeoHomeZoneType() {
    return geoHomeZoneType;
  }

  public void setGeoHomeZoneType(long geoHomeZoneType) {
    this.geoHomeZoneType = geoHomeZoneType;
  }

  public String getGeoHomeZoneName() {
    return geoHomeZoneName;
  }

  public void setGeoHomeZoneName(String geoHomeZoneName) {
    this.geoHomeZoneName = geoHomeZoneName;
  }

  public String getGeoHomeZoneCode() {
    return geoHomeZoneCode;
  }

  public void setGeoHomeZoneCode(String geoHomeZoneCode) {
    this.geoHomeZoneCode = geoHomeZoneCode;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }
}
