package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "campaign_schedule")
public class CampaignSchedule {

	@Id
//  @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SCHEDULE_ID")
	private long scheduleId;
	@Column(name = "SCHEDULE_NAME")
	private String scheduleName;
	@Column(name = "DESCRIPTION")
	private String description;
	@Column(name = "SCHEDULE_TYPE")
	private long scheduleType;
	@Column(name = "CAMPAIGN_ID")
	private Long campaignId;
	@Column(name = "SCHEDULE_PATTERN")
	private String schedulePattern;
	@Column(name = "MODE")
	private long mode;
	@Transient
	private Date chooseDate;
	@Transient
	private int hour;
	@Transient
	private int minute;
	@Transient
	private int second;

	public long getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(long scheduleId) {
		this.scheduleId = scheduleId;
	}


	public String getScheduleName() {
		return scheduleName;
	}

	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public long getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(long scheduleType) {
		this.scheduleType = scheduleType;
	}


	public Long getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}


	public String getSchedulePattern() {
		return schedulePattern;
	}

	public void setSchedulePattern(String schedulePattern) {
		this.schedulePattern = schedulePattern;
	}

	public Date getChooseDate() {
		return chooseDate;
	}

	public void setChooseDate(Date chooseDate) {
		this.chooseDate = chooseDate;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	public long getMode() {
		return mode;
	}

	public void setMode(long mode) {
		this.mode = mode;
	}
}
