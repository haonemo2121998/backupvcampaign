package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "map_command")
public class MapCommand {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private long id;
  @Column(name = "COMMAND_ID")
  private Long commandId;
  @Column(name = "COMMAND_NAME")
  private String commandName;
  @Column(name = "PROTOCOL")
  private Long protocol;
  @Column(name = "RESULT_CODE")
  private Long resultCode;
  @Column(name = "SMSC_CODE")
  private Long smscCode;
  @Column(name = "ACCT_BOOK_TYPE_FOR_CDR")
  private String acctBookTypeForCdr;
  @Column(name = "DESTINATION")
  private String destination;

  public String getDestination() {
    return destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }


  public Long getCommandId() {
    return commandId;
  }

  public void setCommandId(Long commandId) {
    this.commandId = commandId;
  }


  public String getCommandName() {
    return commandName;
  }

  public void setCommandName(String commandName) {
    this.commandName = commandName;
  }


  public Long getProtocol() {
    return protocol;
  }

  public void setProtocol(Long protocol) {
    this.protocol = protocol;
  }


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public Long getResultCode() {
    return resultCode;
  }

  public void setResultCode(Long resultCode) {
    this.resultCode = resultCode;
  }


  public Long getSmscCode() {
    return smscCode;
  }

  public void setSmscCode(Long smscCode) {
    this.smscCode = smscCode;
  }


  public String getAcctBookTypeForCdr() {
    return acctBookTypeForCdr;
  }

  public void setAcctBookTypeForCdr(String acctBookTypeForCdr) {
    this.acctBookTypeForCdr = acctBookTypeForCdr;
  }

}
