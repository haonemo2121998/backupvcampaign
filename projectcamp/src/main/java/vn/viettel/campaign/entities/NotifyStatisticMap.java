package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "notify_statistic_map")
public class NotifyStatisticMap {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private long id;
  @Column(name = "NOTIFY_ID")
  private long notifyId;
  @Column(name = "STATISTIC_CYCLE_ID")
  private long statisticCycleId;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getNotifyId() {
    return notifyId;
  }

  public void setNotifyId(long notifyId) {
    this.notifyId = notifyId;
  }


  public long getStatisticCycleId() {
    return statisticCycleId;
  }

  public void setStatisticCycleId(long statisticCycleId) {
    this.statisticCycleId = statisticCycleId;
  }

}
