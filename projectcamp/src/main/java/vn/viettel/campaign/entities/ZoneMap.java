package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "zone_map")
public class ZoneMap {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ZONE_MAP_ID")
  private long zoneMapId;
  @Column(name = "ZONE_MAP_NAME")
  private String zoneMapName;
  @Column(name = "DESCRIPTION")
  private String description;
  @Column(name = "CATEGORY_ID")
  private Long categoryId;
  @Transient
  private String name;

  public String getName() {
    return zoneMapName;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getZoneMapId() {
    return zoneMapId;
  }

  public void setZoneMapId(long zoneMapId) {
    this.zoneMapId = zoneMapId;
  }


  public String getZoneMapName() {
    return zoneMapName;
  }

  public void setZoneMapName(String zoneMapName) {
    this.zoneMapName = zoneMapName;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }
  public String getFilter() {
    return getName();
  }
}
