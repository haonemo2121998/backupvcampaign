/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author SON
 */
@Entity
@Table(name = "evaluation_input_object")
public class EvaluationInputObject implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "OBJECT_ID")
    private Long objectId;
    @Basic(optional = false)
    @Column(name = "OBJECT_NAME")
    private String objectName;
    @Basic(optional = false)
    @Column(name = "OBJECT_TYPE")
    private int objectType;
    @Basic(optional = false)
    @Column(name = "OBJECT_DATA_TYPE")
    private int objectDataType;
    @Basic(optional = false)
    @Column(name = "OBJECT_PARENT_ID")
    private Long objectParentId;
    @Basic(optional = false)
    @Column(name = "DESCRIPTION")
    private String description;

    public EvaluationInputObject() {
    }

    public EvaluationInputObject(Long objectId) {
        this.objectId = objectId;
    }

    public EvaluationInputObject(Long objectId, String objectName, int objectType, int objectDataType, long objectParentId, String description) {
        this.objectId = objectId;
        this.objectName = objectName;
        this.objectType = objectType;
        this.objectDataType = objectDataType;
        this.objectParentId = objectParentId;
        this.description = description;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public int getObjectType() {
        return objectType;
    }

    public void setObjectType(int objectType) {
        this.objectType = objectType;
    }

    public int getObjectDataType() {
        return objectDataType;
    }

    public void setObjectDataType(int objectDataType) {
        this.objectDataType = objectDataType;
    }

    public Long getObjectParentId() {
        return objectParentId;
    }

    public void setObjectParentId(Long objectParentId) {
        this.objectParentId = objectParentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (objectId != null ? objectId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EvaluationInputObject)) {
            return false;
        }
        EvaluationInputObject other = (EvaluationInputObject) object;
        if ((this.objectId == null && other.objectId != null) || (this.objectId != null && !this.objectId.equals(other.objectId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vn.viettel.campaign.entities.EvaluationInputObject[ objectId=" + objectId + " ]";
    }
    
}
