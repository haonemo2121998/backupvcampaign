package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "ocs_behaviour_template_extend_fields")
public class OcsBehaviourTemplateExtendFields {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private long id;
  @Column(name = "BEHAVIOUR_TEMPLATE_ID")
  private Long behaviourTemplateId;
  @Column(name = "EXTEND_FIELD_ID")
  private Long extendFieldId;
  @Transient
  private String name;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public Long getBehaviourTemplateId() {
    return behaviourTemplateId;
  }

  public void setBehaviourTemplateId(Long behaviourTemplateId) {
    this.behaviourTemplateId = behaviourTemplateId;
  }


  public Long getExtendFieldId() {
    return extendFieldId;
  }

  public void setExtendFieldId(Long extendFieldId) {
    this.extendFieldId = extendFieldId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  public OcsBehaviourExtendFields toBehaviourExtendField(Long behaviourId){
    OcsBehaviourExtendFields ocsBehaviourExtendFields = new OcsBehaviourExtendFields();
    ocsBehaviourExtendFields.setExtendFieldId(this.extendFieldId);
    ocsBehaviourExtendFields.setName(this.name);
    ocsBehaviourExtendFields.setBehaviourId(behaviourId);
    return ocsBehaviourExtendFields;
  }
  public String getFilter() {
    return getName();
  }
}
