package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "scenario_trigger")
public class ScenarioTrigger {

  @Id
//  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "SCENARIO_TRIGGER_ID")
  private long scenarioTriggerId;
  @Column(name = "TRIGGER_NAME")
  private String triggerName;
  @Column(name = "ALIAS")
  private String alias;
  @Column(name = "DESCRIPTION")
  private String description;
  @Column(name = "CATEGORY_ID")
  private long categoryId;
  @Column(name = "USSD_SCENARIO_ID")
  private long ussdScenarioId;
  @Column(name = "IS_RETRY")
  private long isRetry;
  @Column(name = "CYCLE_ID")
  private long cycleId;
  @Transient
  private String name;
  @Transient
  private String ussdName;

  public String getUssdName() {
    return ussdName;
  }

  public void setUssdName(String ussdName) {
    this.ussdName = ussdName;
  }

  public String getName() {
    return this.triggerName;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getScenarioTriggerId() {
    return scenarioTriggerId;
  }

  public void setScenarioTriggerId(long scenarioTriggerId) {
    this.scenarioTriggerId = scenarioTriggerId;
  }


  public String getTriggerName() {
    return triggerName;
  }

  public void setTriggerName(String triggerName) {
    this.triggerName = triggerName;
  }


  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(long categoryId) {
    this.categoryId = categoryId;
  }


  public long getUssdScenarioId() {
    return ussdScenarioId;
  }

  public void setUssdScenarioId(long ussdScenarioId) {
    this.ussdScenarioId = ussdScenarioId;
  }


  public long getIsRetry() {
    return isRetry;
  }

  public void setIsRetry(long isRetry) {
    this.isRetry = isRetry;
  }


  public long getCycleId() {
    return cycleId;
  }

  public void setCycleId(long cycleId) {
    this.cycleId = cycleId;
  }
  public String getFilter() {
    return getName();
  }
}
