package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "note_content")
public class NoteContent {

    @Id
//  @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NODE_CONTENT_ID")
    private long nodeContentId;
    @Column(name = "NODE_ID")
    private long nodeId;
    @Column(name = "LANGUAGE_ID")
    private long languageId;
    @Column(name = "CONTENT")
    private String content;
    @Transient
    private String languageName;

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public long getNodeContentId() {
        return nodeContentId;
    }

    public void setNodeContentId(long nodeContentId) {
        this.nodeContentId = nodeContentId;
    }


    public long getNodeId() {
        return nodeId;
    }

    public void setNodeId(long nodeId) {
        this.nodeId = nodeId;
    }


    public long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
