package vn.viettel.campaign.entities;
import javax.persistence.*;
import java.util.Date;

/**
*
* @author truongbx
*/
@Entity
@Table(name = "retry_cycle")
public class RetryCycle {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private long id;
  @Column(name = "NAME")
  private String name;
  @Column(name = "NOTE")
  private String note;
  @Column(name = "CYCLE_UNIT_ID")
  private long cycleUnitId;
  @Column(name = "CYCLE_UNIT_TIMES")
  private long cycleUnitTimes;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }


  public long getCycleUnitId() {
    return cycleUnitId;
  }

  public void setCycleUnitId(long cycleUnitId) {
    this.cycleUnitId = cycleUnitId;
  }


  public long getCycleUnitTimes() {
    return cycleUnitTimes;
  }

  public void setCycleUnitTimes(long cycleUnitTimes) {
    this.cycleUnitTimes = cycleUnitTimes;
  }
  public String getFilter() {
    return getName();
  }
}
