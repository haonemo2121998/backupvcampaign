package vn.viettel.campaign.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * @author truongbx
 */
@Entity
@Table(name = "ocs_behaviour_template")
public class OcsBehaviourTemplate extends BaseCategory {

	@Id
	@Column(name = "TEMPLATE_ID")
	private long templateId;
	@Column(name = "TEMPLATE_NAME")
	private String templateName;
	@Column(name = "COMMAND_CONTENT")
	private String commandContent;
	@Column(name = "DESCRIPTION")
	private String description;
	@Column(name = "CATEGORY_ID")
	private Long categoryId;
	@Column(name = "COMMAND_CODE")
	private Long commandCode;
	@Column(name = "COMMAND_NAME")
	private String commandName;


	public long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}


	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}


	public String getCommandContent() {
		return commandContent;
	}

	public void setCommandContent(String commandContent) {
		this.commandContent = commandContent;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}


	public Long getCommandCode() {
		return commandCode;
	}

	public void setCommandCode(Long commandCode) {
		this.commandCode = commandCode;
	}


	public String getCommandName() {
		return commandName;
	}

	public void setCommandName(String commandName) {
		this.commandName = commandName;
	}

	@Override
	public long getParentId() {
		return getCategoryId();
	}

	@Override
	public String getTreeType() {
		return "ocs_template";
	}

	@Override
	public void setParentId(Long id) {
		setCategoryId(id);
	}

	@Override
	public String getName() {
		return this.templateName;
	}

	@Override
	public Class getParentType() {
		return Category.class;
	}

	public void asMap(OcsBehaviourTemplate data) {
		this.templateId = data.getTemplateId();
		this.templateName = data.getTemplateName();
		this.commandContent = data.getCommandContent();
		this.description = data.getDescription();
		this.categoryId = data.getCategoryId();
		this.commandCode = data.getCommandCode();
		this.commandName = data.getCommandName();
	}
}
