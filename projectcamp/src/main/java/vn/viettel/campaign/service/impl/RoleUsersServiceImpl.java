/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.RolesUsersDao;
import vn.viettel.campaign.entities.RoleUser;
import vn.viettel.campaign.service.RolesUsersService;

@Service
public class RoleUsersServiceImpl implements RolesUsersService {

    @Autowired
    private RolesUsersDao rolesUsersDaoImpl;

    @Override
    public Boolean saveRoleUser(RoleUser ru) {
        return rolesUsersDaoImpl.saveRoleUser(ru);
    }

    @Override
    public Boolean updateRoleUser(RoleUser ru) {
        return rolesUsersDaoImpl.updateRoleUser(ru);
    }

    @Override
    public void deleteRoleUserByUserId(Long userId) {
        rolesUsersDaoImpl.deleteRoleUserByUserId(userId);
    }

    @Override
    public void deleteRoleUserByRoleId(Long roleId) {
        rolesUsersDaoImpl.deleteRoleUserByRoleId(roleId);
    }

    @Override
    public List<RoleUser> findByUserId(Long userId) {
        return rolesUsersDaoImpl.findByUserId(userId);
    }
}
