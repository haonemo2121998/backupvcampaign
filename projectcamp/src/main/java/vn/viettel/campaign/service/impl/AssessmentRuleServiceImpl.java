/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.AssessmentRuleDAOInterface;
import vn.viettel.campaign.dao.AssessmentRuleKpiProcessorMapDAOInterfae;
import vn.viettel.campaign.dao.AssessmentRulePreProcessUnitMapDAO;
import vn.viettel.campaign.dao.CampaignEvaluationInfoDAOInterface;
import vn.viettel.campaign.dao.CriteriaValueDAO;
import vn.viettel.campaign.dao.FuzzyRuleDAOInterface;
import vn.viettel.campaign.dao.KpiProcessorCriteriaMapDAO;
import vn.viettel.campaign.dao.KpiProcessorDAO;
import vn.viettel.campaign.dao.PpuEvaluationDAO;
import vn.viettel.campaign.dao.PreProcessValueMapDAOInterface;
import vn.viettel.campaign.dao.ProcessValueDAOInterface;
import vn.viettel.campaign.dao.ResultClassDAOInfterface;
import vn.viettel.campaign.dao.RuleEvaluationInfoDAOInterface;
import vn.viettel.campaign.dao.SegmentEvaluationInfoDAOInterface;
import vn.viettel.campaign.dao.UssdScenarioDAOInterface;
import vn.viettel.campaign.entities.AssessmentRule;
import vn.viettel.campaign.entities.AssessmentRuleKpiProcessorMap;
import vn.viettel.campaign.entities.AssessmentRulePreProcessUnitMap;
import vn.viettel.campaign.entities.CampaignEvaluationInfo;
import vn.viettel.campaign.entities.CriteriaValue;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.KpiProcessor;
import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.ResultClass;
import vn.viettel.campaign.entities.KpiProcessorCriteriaValueMap;
import vn.viettel.campaign.entities.PreProcessValueMap;
import vn.viettel.campaign.entities.ProcessValue;
import vn.viettel.campaign.entities.RuleEvaluationInfo;
import vn.viettel.campaign.entities.SegmentEvaluationInfo;
import vn.viettel.campaign.service.AssessmentRuleService;

/**
 *
 * @author ABC
 */
@Service
public class AssessmentRuleServiceImpl implements AssessmentRuleService {

    @Autowired
    private  KpiProcessorDAO kpiProcessorDAO;
    @Autowired
    private ResultClassDAOInfterface resultClassDAOInfterface;
    @Autowired
    private FuzzyRuleDAOInterface fuzzyRuleDAOInterface;
    @Autowired
    private PpuEvaluationDAO ppuEvaluationDAO;
    @Autowired
    private KpiProcessorCriteriaMapDAO kpiProcessorCriteriaMapDAO;
    @Autowired
    private CriteriaValueDAO criteriaValueDAO;
    @Autowired
    private PreProcessValueMapDAOInterface preProcessValueMapDAOInterface;
    @Autowired
    private ProcessValueDAOInterface processValueDAOInterface;
    @Autowired
    private AssessmentRuleDAOInterface assessmentRuleDAOInterface;
    @Autowired
    private AssessmentRulePreProcessUnitMapDAO assessmentRulePreProcessUnitMapDAO;
    @Autowired
    private AssessmentRuleKpiProcessorMapDAOInterfae assessmentRuleKpiProcessorMapDAOIterface;
    @Autowired
    private RuleEvaluationInfoDAOInterface ruleEvaluationInfoDAOInterface;
    @Autowired
    private UssdScenarioDAOInterface ussdScenarioDAO;
    @Autowired
    private SegmentEvaluationInfoDAOInterface segmentEvaluationInfoDAOInterface;
    @Autowired
    private CampaignEvaluationInfoDAOInterface campaignEvaluationInfoDAOInterface;

    @Override
    public List<PpuEvaluation> getListPpuEvaluationByRuleId(Long id) {
        return ppuEvaluationDAO.getListPpuEvaluationPPUByRuleId(id);
    }

    @Override
    public List<KpiProcessor> getListKpiProcessorByRuleId(Long ruleId) {
        return kpiProcessorDAO.getListKpiProcessorByRuleId(ruleId);
    }

    @Override
    public List<ResultClass> getListResultClassByAssessmentRuleId(Long ruleId) {
        return resultClassDAOInfterface.getListResultClassByAssessmentRuleId(ruleId);
    }

    @Override
    public List<FuzzyRule> getListFuzzyRuleByRuleId(Long ruleId) {
        return fuzzyRuleDAOInterface.getListFuzzyByRuleId(ruleId);
    }

    @Override
    public List<PpuEvaluation> getListPpuEvaluationTree(List<Long> categoryIds) {
        return ppuEvaluationDAO.getListPpuEvaluationTree(categoryIds);
    }

    @Override
    public FuzzyRule getNextFuzzyRule() {
        return fuzzyRuleDAOInterface.getNextSequenceFuzzyRule();
    }

    @Override
    public List<KpiProcessorCriteriaValueMap> getListkpiProcessorCriteriaValueMapByProsessorId(Long processorId) {
        return kpiProcessorCriteriaMapDAO.getListkpiProcessorCriteriaValueMapByProsessorId(processorId);
    }

    @Override
    public List<CriteriaValue> getListCriteriaValueDAO(List<Long> longs) {
        return criteriaValueDAO.getListCriteriaValueById(longs);
    }

    @Override
    public List<PreProcessValueMap> getListPreProcessValueMapByPreProcessId(Long preProcessId) {
        return preProcessValueMapDAOInterface.getListPreProcessValueMapByPreProcessId(preProcessId);
    }

    @Override
    public List<ProcessValue> getListProcessValueByListId(List<Long> longs) {
        return processValueDAOInterface.getListProcessValueByListId(longs);
    }

    @Override
    public AssessmentRule createOrUpdateAssessmenRule(AssessmentRule assessmentRule) {
        return assessmentRuleDAOInterface.createOrUpdateAssessmenRule(assessmentRule);
    }

    @Override
    public boolean deleteAssessmentRule(Long ruleId) {
        return assessmentRuleDAOInterface.deleteAssessmentRule(ruleId);
    }

    @Override
    public AssessmentRulePreProcessUnitMap createOrUpdateAssessmentRuleProcessUnitMap(AssessmentRulePreProcessUnitMap assessmentRulePreProcessUnitMap) {
        return assessmentRulePreProcessUnitMapDAO.createOrUpdateAssessmentRuleProcessUnitMap(assessmentRulePreProcessUnitMap);
    }

    @Override
    public boolean deleteAssessmentRulePreProcessUnitMap(Long Id) {
        return assessmentRulePreProcessUnitMapDAO.deleteAssessmentRulePreProcessUnitMap(Id);
    }

    @Override
    public AssessmentRuleKpiProcessorMap createOrUpdateAssessmentRuleKpiProcessorMap(AssessmentRuleKpiProcessorMap assessmentRuleKpiProcessorMap) {
        return assessmentRuleKpiProcessorMapDAOIterface.createOrUpdateAssessmentRuleKpiProcessorMap(assessmentRuleKpiProcessorMap);
    }

    @Override
    public boolean deleteAssessmentRuleKpiProcessorMap(Long id) {
        return assessmentRuleKpiProcessorMapDAOIterface.deleteAssessmentRuleKpiProcessorMap(id);
    }

    @Override
    public AssessmentRulePreProcessUnitMap getNextAssessmentRulePreProcessUnitMap() {
        return assessmentRulePreProcessUnitMapDAO.getNextAssessmentRulePreProcessUnitMap();
    }

    @Override
    public List<AssessmentRulePreProcessUnitMap> getAllAssessmentRulePreProcessUnitMapById(Long id) {
        return assessmentRulePreProcessUnitMapDAO.getAllAssessmentRulePreProcessUnitMapById(id);
    }

    @Override
    public List<AssessmentRuleKpiProcessorMap> getAlllistAssessmentRuleKpiProcessorMapById(Long id) {
        return assessmentRuleKpiProcessorMapDAOIterface.getAlllistAssessmentRuleKpiProcessorMapById(id);
    }

    @Override
    public AssessmentRuleKpiProcessorMap getNextAssessmentRuleKpiProcessorMap() {
        return assessmentRuleKpiProcessorMapDAOIterface.getNextAssessmentRuleKpiProcessorMap();
    }

    @Override
    public ResultClass doCreateOrUpdateResultClass(ResultClass resultClass) {
        return resultClassDAOInfterface.doCreateOrUpdateResultClass(resultClass);
    }

    @Override
    public boolean deleteResultClass(Long id) {
        return resultClassDAOInfterface.deleteResultClass(id);
    }

    @Override
    public ResultClass getNextSequenceResultClass() {
        return resultClassDAOInfterface.getNextSequenceResultClass();
    }

    @Override
    public FuzzyRule doCreateOrUpdateFuzzyRule(FuzzyRule fuzzyRule) {
        return fuzzyRuleDAOInterface.doCreateOrUpdateFuzzyRule(fuzzyRule);
    }

    @Override
    public boolean deleteFuzzyRule(Long id) {
        return fuzzyRuleDAOInterface.deleteFuzzyRule(id);
    }

    @Override
    public AssessmentRulePreProcessUnitMap findAssessmentRulePreProcessUnitMapById(Long id) {
        return assessmentRulePreProcessUnitMapDAO.findAssessmentRulePreProcessUnitMapById(id);
    }

    @Override
    public AssessmentRuleKpiProcessorMap findAssessmentRuleKpiProcessorMapById(Long id) {
        return assessmentRuleKpiProcessorMapDAOIterface.findAssessmentRuleKpiProcessorMapById(id);
    }

    @Override
    public AssessmentRule getNextAssessmentRule() {
        return assessmentRuleDAOInterface.getNextAssessmentRule();
    }

    @Override
    public List<RuleEvaluationInfo> findRuleEvaluationInfoById(Long assessmentRuleId) {
        return ruleEvaluationInfoDAOInterface.findRuleEvaluationInfoById(assessmentRuleId);
    }

    @Override
    public List<KpiProcessor> findAllKpiProcessor(List<Long> categoryIds) {
        return kpiProcessorDAO.findAllKpiProcessor(categoryIds);
    }

    @Override
    public long getNextSequense(String tableName) {
        return ussdScenarioDAO.getNextSequense(tableName);
    }

    @Override
    public AssessmentRule findAssessmentRuleById(Long id) {
        return assessmentRuleDAOInterface.findAssessmentRuleById(id);
    }

    @Override
    public List<FuzzyRule> getListFuzzyRuleOfSegment(Long segmentId, Long campaignEvaluationInfoId) {
        return fuzzyRuleDAOInterface.getListFuzzyRuleOfSegment(segmentId, campaignEvaluationInfoId);
    }

    @Override
    public List<FuzzyRule> getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(Long campaignId, Long campaignEvaluationIfoId) {
        return fuzzyRuleDAOInterface.getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(campaignId, campaignEvaluationIfoId);
    }

    @Override
    public List<CampaignEvaluationInfo> getListCampaignEvalurationMapAssessment(Long assessmentRuleId) {
       return campaignEvaluationInfoDAOInterface.getListCampaignEvalurationMapAssessment(assessmentRuleId);
    }

    @Override
    public List<SegmentEvaluationInfo> getListSegmentEvaluationInfoMapAssessment(Long assessmentRuleId) {
        return segmentEvaluationInfoDAOInterface.getListSegmentEvaluationInfoMapAssessment(assessmentRuleId);
    }

    @Override
    public ProcessValue checkValueName(String name,Long preProcessId) {
        return processValueDAOInterface.checkValueName(name,preProcessId);
    }

    @Override
    public CriteriaValue chekCriteriaValueName(String name,Long processorId) {
        return criteriaValueDAO.chekCriteriaValueName(name,processorId);
    }

}
