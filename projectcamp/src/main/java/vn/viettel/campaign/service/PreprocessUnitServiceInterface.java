package vn.viettel.campaign.service;

import vn.viettel.campaign.entities.*;

import java.util.List;

/**
 * @author truongbx
 * @date 9/15/2019
 */
public interface PreprocessUnitServiceInterface extends BaseBusinessInterface<PreProcessUnit> {

    PreProcessUnit getNextSequense();

    boolean checkExistPreProcessName(String preProcessName, Long preProcessId);

    List<InputObject> getLstInputObject();

    List<InputObject> getLstCondition(long inputObjectId);

    List<Function> getLstFunction();

    List<Function> getLstFunctionTypeOne();

    List<Zone> getLstZone();

    List<ZoneMap> getLstZoneMap();

    List<FunctionParam> getLstFunctionParam(Long functionId);

    List<ProcessValue> getLstProcessValue(Long ppId);

    List<ProcessParam> getLstProcessParam(Long ppid);
//	PreProcessUnit findById(Long id);

    public boolean onSavePreprocessUnit(PreProcessUnit preProcessUnit,
            List<ProcessParam> lstProcessParams, List<ProcessValue> lstProcessValue);

    public boolean onUpdatePreprocessUnit(PreProcessUnit preProcessUnit,
            List<ProcessParam> lstProcessParams, List<ProcessValue> lstProcessValue);

    public boolean onDeletePreprocessUnit(Long preProcessId);

    public boolean checkDeletePreprocessUnit(Long preProcessId);

    public List<ProcessParam> getListProcessParamByPreProcessId(Long preProcessId);

    public List<ProcessValue> getListProcessValueByPreProcessId(Long peProcessId);

   
}
