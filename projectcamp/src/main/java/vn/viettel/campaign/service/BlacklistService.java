package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.BlackList;

/**
 *
 * @author ConKC
 */
public interface BlacklistService extends BaseBusinessInterface<BlackList> {

    List<BlackList> findBacklistByCategory(final List<Long> categoryIds);

    List<BlackList> findBacklistByName(final String backlistByName);

    long onSaveBlacklist(BlackList blacklist);

    boolean checkDuplicate(final String name, final Long id);

    boolean checkDuplicatePath(final String path, final Long id);
}
