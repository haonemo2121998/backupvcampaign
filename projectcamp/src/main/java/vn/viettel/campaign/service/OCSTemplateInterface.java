package vn.viettel.campaign.service;

import vn.viettel.campaign.entities.*;

import java.util.List;

/**
 * @author truongbx
 * @date 9/15/2019
 */
public interface OCSTemplateInterface extends BaseBusinessInterface<OcsBehaviourTemplate> {
	List<MapCommand> getLstMapConmand();
	List<OcsBehaviourTemplateFields> getLstFieldOfTemplate(long id);
	List<OcsBehaviourTemplateExtendFields> getLstExtendFieldOfTemplate(long id);
	List<OcsBehaviourTemplateFields> getRemainFieldOfTemplate(long id , List<Long> ids);
	List<OcsBehaviourTemplateExtendFields> getRemainExtendFieldOfTemplate(long id , List<Long> lstExtendIds);
	OcsBehaviourTemplate getNextSequense();
	ExtendField getNextExtendSequense();
	boolean checkExtendFieldName(String name);
	boolean checkTemplateName(String name,Long id);
	public void insertExtendField(ExtendField extendField);
	public boolean onSaveBehaviourTemplate(OcsBehaviourTemplate ocsBehaviourTemplate ,
										   List<OcsBehaviourTemplateFields> lstField , List<OcsBehaviourTemplateExtendFields> lstExtendFields);
	public boolean onUpdateBehaviourTemplate(OcsBehaviourTemplate ocsBehaviourTemplate ,
										   List<OcsBehaviourTemplateFields> lstField , List<OcsBehaviourTemplateExtendFields> lstExtendFields);
	public boolean onDeleteBehaviourTemplate(Long templateId);
	boolean checkOcsTemplateInUse(Long templateId);
}
