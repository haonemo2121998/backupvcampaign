package vn.viettel.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.dao.*;
import vn.viettel.campaign.dao.notifytemplate.NotifyContentDAO;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.OCSBehaviourInterface;
import vn.viettel.campaign.service.OCSTemplateInterface;

import javax.annotation.PostConstruct;
import java.util.List;
import org.hibernate.criterion.Order;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Service
public class OCSBehaviourServiceImpl extends BaseBusinessImpl<OcsBehaviour, OcsBehaviourDAOInterface> implements OCSBehaviourInterface {

    @Autowired
    OcsBehaviourDAOInterface ocsBehaviourDAO;
    @Autowired
    NotifyTriggerDAOInterface notifyTriggerDAO;
    @Autowired
    PostNotifyTriggerDaoInterface postNotifyTriggerDAO;
    @Autowired
    NotifyContentDAO notifyContentDAO;

//	@Autowired
//	OCSTemplateFieldDaoInterface oCSTemplateFieldDAO;
//	@Autowired
//	OCSTemplateExtendFieldDaoInterface OCSTemplateExtendFieldDAO;
//	@Autowired
//	ExtendFieldDaoInterface extendFieldDAO;
    @PostConstruct
    public void setupService() {
        this.tdao = ocsBehaviourDAO;
        this.modelClass = OcsBehaviour.class;
    }

    @Override
    public OcsBehaviour getNextSequense() {
        return ocsBehaviourDAO.getNextSequence();
    }

    @Override
    public <T> List<T> getLstNotifyTrigger(List<Long> lstCategory) {
        return notifyTriggerDAO.getLstNotifyTrigger(lstCategory);
    }

    @Override
    public List<PostNotifyTrigger> getLstPostNotifyTrigger(long behaviourId) {
        return postNotifyTriggerDAO.getLstPostNotigyTriggerByOcsBehaviourId(behaviourId);
    }

    @Override
    public boolean onSaveOCSBehaviour(OcsBehaviour ocsBehaviour, List<OcsBehaviourFields> lstField,
            List<OcsBehaviourExtendFields> lstExtendField, List<PostNotifyTrigger> lstNotifyTrigger) {
        String result = ocsBehaviourDAO.onSaveOCSBehaviour(ocsBehaviour, lstField, lstExtendField, lstNotifyTrigger);
        if (!result.equalsIgnoreCase(Responses.SUCCESS.getName())) {
            return false;
        }
        return true;
    }

    @Override
    public boolean onUpdateOCSBehaviour(OcsBehaviour ocsBehaviour, List<OcsBehaviourFields> lstField,
            List<OcsBehaviourExtendFields> lstExtendField, List<PostNotifyTrigger> lstNotifyTrigger) {
        String result = ocsBehaviourDAO.onUpdateOCSBehaviour(ocsBehaviour, lstField, lstExtendField, lstNotifyTrigger);
        if (!result.equalsIgnoreCase(Responses.SUCCESS.getName())) {
            return false;
        }
        return true;
    }

    @Override
    public boolean onDeleteOcsBehaviour(Long behaviourId) {
        return ocsBehaviourDAO.onDeleteOcsBehaviour(behaviourId);
    }

    @Override
    public List<OcsBehaviourFields> getLstBehaviourField(Long behaviourId, Long templateId) {
        return ocsBehaviourDAO.getLstBehaviourField(behaviourId, templateId);
    }

    @Override
    public List<OcsBehaviourExtendFields> getLstBehaviourExtendField(Long behaviourId, Long templateId) {
        return ocsBehaviourDAO.getLstBehaviourExtendField(behaviourId, templateId);
    }

    @Override
    public List<OcsBehaviourTemplateFields> getLstBehaviourTemplateField(Long templateId) {
        return ocsBehaviourDAO.getLstBehaviourTemplateField(templateId);
    }

    @Override
    public List<OcsBehaviourTemplateExtendFields> getLstBehaviourTemplateExtendField(Long templateId) {
        return ocsBehaviourDAO.getLstBehaviourTemplateExtendField(templateId);
    }

    @Override
    public boolean checkExisOCSBehaviourName(String BehaviourName, Long behaviourId) {
        return ocsBehaviourDAO.checkExistOCSBehaviourName(BehaviourName, behaviourId);
    }

    @Override
    public boolean validateDeleteNotifyTrigger(Long notifyId) {
        List lst1 = ocsBehaviourDAO.getLstBehaviourByNotifyId(notifyId);
        List lst2 = postNotifyTriggerDAO.getLstPostNotigyTriggerByNotifyId(notifyId);
        return DataUtil.isNullOrEmpty(lst1) && DataUtil.isNullOrEmpty(lst2);
    }

    @Override
    public List<NotifyContent> getNotifyContentByNotifyId(Long notifyId) {
        return notifyContentDAO.getlstByNotifyId(notifyId);
    }

    @Override
    public boolean checkOcsUseInPromotionBlock(Long ocsId) {
        return ocsBehaviourDAO.checkOcsUseInPromotionBlock(ocsId);
    }

    @Override
    public boolean checkOcsUseInUSSDScenario(Long ocsId) {
        return ocsBehaviourDAO.checkOcsUseInUSSDScenario(ocsId);
    }
}
