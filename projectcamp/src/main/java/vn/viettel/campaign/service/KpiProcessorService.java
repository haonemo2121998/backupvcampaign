/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.KpiProcessor;

/**
 *
 * @author SON
 */
public interface KpiProcessorService {
    List<KpiProcessor> getLstKpiProcessor();
    KpiProcessor findOneById(Long id);
    KpiProcessor onSaveOrUpdateKpiProcessor(KpiProcessor kpiProcessor);
    Boolean onDeleteKpiProcessor(KpiProcessor kpiProcessor);
    KpiProcessor getNextSequense();
    KpiProcessor onSaveKpiProcessor(KpiProcessor kpiProcessor);
    KpiProcessor onUpdateKpiProcessor(KpiProcessor kpiProcessor);
    KpiProcessor checkExitsProcessName(String name, Long id);
    KpiProcessor checkExitsCriteriaName(String name, Long id);
    Boolean checkDeleteKpiProcessor(KpiProcessor kpiProcessor);
}
