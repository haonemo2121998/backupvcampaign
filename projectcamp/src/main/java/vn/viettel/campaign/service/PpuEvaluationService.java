/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.ProcessParam;
import vn.viettel.campaign.entities.ProcessValue;

/**
 *
 * @author admin
 */
public interface PpuEvaluationService {
    PpuEvaluation onSaveOrUpdatePpuEvaluation(PpuEvaluation ppuEvaluation);
    PpuEvaluation getNextSequense();
    boolean onDeletePpuEvaluation(PpuEvaluation ppuEvaluation);
    List<ProcessValue> getListProcessValue(Long id);
    List<ProcessParam> getListProcessParam(Long id);
    boolean checkExitsProcessorName(String name, Long id);
    boolean checkExitsCriteriaName(String name, Long id);
    boolean checkExitsValueName(Long processValue, String name, Long id);
    boolean checkExitsValueId(Long processValue, Long valueId, Long id);
    boolean checkDeletePPUEvaluation(Long id);
    PpuEvaluation findOneById(Long id);
}
