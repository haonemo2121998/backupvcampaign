/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

/**
 *
 * @author ConKC
 */
public interface UtilsService {

    boolean loadProperty();

    String getTex(final String key);

    String getTex(final String key, final String defaultValue);

    boolean getConfig(final String key);
    
    Object getObject(final String key);
    
    Object getObject(final String key, final String defaultValue);
}
