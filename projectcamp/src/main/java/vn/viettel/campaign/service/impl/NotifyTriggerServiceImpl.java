/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dao.*;
import vn.viettel.campaign.dto.ConditionTableDTO;
import vn.viettel.campaign.dto.NotifyTemplateDetailDTO;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.ConditionTableService;
import vn.viettel.campaign.service.NotifyTemplateService;
import vn.viettel.campaign.service.NotifyTriggerService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TOPICA
 */
@Service
public class NotifyTriggerServiceImpl implements NotifyTriggerService {


    @Autowired
    NotifyTriggerDAOInterface notifyTriggerDAO;

    @Autowired
    NotifyValuesDAOInterface notifyValuesDAO;

    @Autowired
    StatisticCycleDAOInterface statisticCycleDAO;

    @Autowired
    NotifyStatisticMapDAOInterface notifyStatisticMapDAO;

    @Autowired
    RetryCycleDAOInterface retryCycleDAO;

    @Autowired
    LanguageDAOInterface languageDAO;

    @Autowired
    CycleUnitDAOInterface cycleUnitDAO;

    @Autowired
    DataTypeDAOInterface dataTypeDAO;

    @Autowired
    private NotifyTemplateService notifyTemplateService;

    @Override
    public List<RetryCycle> getAllRetryCycle() {
        return retryCycleDAO.getAll(RetryCycle.class);
    }

    @Override
    public List<DataType> getAllDataType() {
        return dataTypeDAO.getAll(DataType.class);
    }

    @Override
    public List<Language> getAllLanguage() {
        return languageDAO.getAll(Language.class);
    }

    @Override
    public List<CycleUnit> getAllCycleUnit() {
        return cycleUnitDAO.getAll(CycleUnit.class);
    }

    @Override
    public List<NotifyTrigger> getByNotifyTemplateId(Long id) {
        return notifyTriggerDAO.getByNotifyTemplateId(id);
    }

    @Override
    public void doSave(NotifyTrigger notifyTrigger) {
        if (DataUtil.isNullObject(notifyTrigger)) {
            return;
        }
        // delete old data
        notifyValuesDAO.deleteByNotifyTriggerId(notifyTrigger.getNotifyTriggerId());
//        statisticCycleDAO.deleteByNotifyTriggerId(notifyTrigger.getNotifyTriggerId());
        notifyStatisticMapDAO.deleteByNotifyTriggerId(notifyTrigger.getNotifyTriggerId());
        // insert new data
        notifyTrigger.setNotifyTemplateId(notifyTrigger.getNotifyTemplateDetailDTO().getId());
        notifyTriggerDAO.saveOrUpdate(notifyTrigger);
        List<NotifyValues> notifyValues = notifyTrigger.getNotifyValues();
        if (!DataUtil.isNullOrEmpty(notifyValues)) {
            for (NotifyValues item : notifyValues) {
                item.setNotifyValueId(0);
//                item.setDataType(Constants.NotifyTrigger.DATA_TYPE);
                item.setTriggerNotifyId(notifyTrigger.getNotifyTriggerId());
                item.setTemplateParamId(notifyTrigger.getNotifyTemplateDetailDTO().getId());
                notifyValuesDAO.saveOrUpdate(item);
            }
        }
        List<StatisticCycle> statisticCycles = notifyTrigger.getStatisticCycles();
        if (!DataUtil.isNullOrEmpty(statisticCycles)) {
            for (StatisticCycle item : statisticCycles) {
//                statisticCycleDAO.saveOrUpdate(item);
                NotifyStatisticMap notifyStatisticMap = new NotifyStatisticMap();
                notifyStatisticMap.setNotifyId(notifyTrigger.getNotifyTriggerId());
                notifyStatisticMap.setStatisticCycleId(item.getId());
                notifyStatisticMapDAO.saveOrUpdate(notifyStatisticMap);
            }
        }
    }

    @Override
    public void doDelete(long id) {
        notifyTriggerDAO.deleteById(NotifyTrigger.class, id);
        notifyValuesDAO.deleteByNotifyTriggerId(id);
//        statisticCycleDAO.deleteByNotifyTriggerId(id);
        notifyStatisticMapDAO.deleteByNotifyTriggerId(id);
    }

    @Override
    public NotifyTrigger getDetail(long id) {

        NotifyTrigger notifyTrigger = notifyTriggerDAO.findById(NotifyTrigger.class, id);
        notifyTrigger.setStatisticCycles(statisticCycleDAO.findByNotifyTriggerId(id));
        notifyTrigger.setNotifyValues(notifyValuesDAO.findByNotifyTriggerId(id));
        NotifyTemplateDetailDTO notifyTemplateDetailDTO = notifyTemplateService.getDetail(notifyTrigger.getNotifyTemplateId());
        notifyTrigger.setNotifyTemplateDetailDTO(notifyTemplateDetailDTO);
        return notifyTrigger;
    }

    @Override
    public List<StatisticCycle> findStatisticCycleById(long id) {
        return statisticCycleDAO.findById(id);
    }

    @Override
    public boolean validateAdd(long id, String name) {
        // ko co ban ghi nao trung ten thi return true
        return DataUtil.isNullObject(notifyTriggerDAO.getByName(id, name));
    }

    @Override
    public List<NotifyStatisticMap> findByNotifyIdAndCycleId(long notifyId, long cycleId) {
        return notifyStatisticMapDAO.findByNotifyIdAndCycleId(notifyId, cycleId);
    }
}
