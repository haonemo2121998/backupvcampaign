/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.ProcessValueDAO;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.ProcessValue;
import vn.viettel.campaign.service.ProcessValueService;

/**
 *
 * @author admin
 */
@Service
public class ProcessValueServiceImpl implements ProcessValueService {

    @Autowired
    private ProcessValueDAO processValueDAO;

    @Override
    public ProcessValue onSaveOrUpdateProcessValue(ProcessValue processValue) {
        processValueDAO.onSaveOrUpdateProcessValue(processValue);
        return processValue;
    }

    @Override
    public boolean onDeleteProcessValue(PpuEvaluation ppuEvaluation) {
        if(ppuEvaluation != null){
            processValueDAO.onDeleteProcessValue(ppuEvaluation);
            return true;
        }
        return false;
    }

    @Override
    public List<ProcessValue> getLstProcessValue() {
        return processValueDAO.getLstProcessValue();
    }

    @Override
    public List<ProcessValue> getLstProcessValue(Long id) {
        return processValueDAO.getLstProcessValue(id);
    }

    @Override
    public ProcessValue getNextSequence() {
        return processValueDAO.getNextSequence();
    }

    @Override
    public Boolean checkSavePreProcessValueMap(Long processValueId, Long ppuId) {
        if(processValueDAO.checkSavePreProcessValueMap(processValueId,ppuId) != null){
            return true;
        }
        return false;
    }

    @Override
    public boolean onDeleteProcessValue(Long processValueId, Long ppuId) {
        if(processValueId != null && ppuId != null){
            processValueDAO.onDeleteProcessValue(processValueId, ppuId);
            return true;
        }
        return false;
    }

    @Override
    public Boolean checkDeleteProcessValue(Long ppuId, String name) {
        if(ppuId != null && name != null && processValueDAO.checkDeleteProcessValue(ppuId, name).size() != 0){
            return true;
        }
        return false;
    }

    @Override
    public List<FuzzyRule> getListFuzzyForProcessValue(Long ppuId) {   
        return processValueDAO.getListFuzzyForProcessValue(ppuId).isEmpty() ? new ArrayList<>() : processValueDAO.getListFuzzyForProcessValue(ppuId);
    }

}
