package vn.viettel.campaign.service;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.TreeNodeType;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.TreeItemBase;

import java.util.List;

public class TreeBaseService<T extends TreeItemBase> {

    protected TreeNode createTree(List<Category> listCat, List<T> items, TreeNodeType itemType) {
        TreeNode catRootNode = getRootNoteCatagory(listCat);
        buildCatagoryTreeCondition(catRootNode.getChildren().get(0), listCat, items, itemType);
        return catRootNode;
    }


    private TreeNode getRootNoteCatagory(List<Category> listCat) {
        // root node off tree (parent id is null)
        Category catRoot = null;
        for (Category it : listCat) {
            if (it.getParentId() == null) {
                catRoot = it;
            }
        }       // root node in framework (not show in view)
        TreeNode rootNode = new DefaultTreeNode(null, null);
        TreeNode catRootNode = new DefaultTreeNode("category", catRoot, rootNode);
//        catRootNode.setSelectable(false);
        return rootNode;
    }

    public void buildCatagoryTreeCondition(TreeNode parentNodeCat, List<Category> listCat, List<T> items, TreeNodeType itemType) {
        Category parentCat = (Category) parentNodeCat.getData();
        for (Category cat : listCat) {
            if (DataUtil.safeEqual(cat.getParentId(), parentCat.getCategoryId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
//                childNodeCat.setSelectable(false);
                buildCatagoryTreeCondition(childNodeCat, listCat, items, itemType);
            }
        }
        for (TreeItemBase item : items) {
            if (item.getCategoryId().equals(parentCat.getCategoryId())) {
                new DefaultTreeNode(itemType.name(), item, parentNodeCat);
            }
        }
    }
}
