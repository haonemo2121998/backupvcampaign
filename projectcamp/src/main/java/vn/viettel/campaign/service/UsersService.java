/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.Users;

public interface UsersService {

    public List<Users> getLstUsersByName(String name, Long objectId);

    public Boolean saveUsers(Users obj);

    public Boolean updateUsers(Users obj);

    public List<Users> getLstUsers();

    public void changePassword(Long userId, String newpass, String oldpass);

}
