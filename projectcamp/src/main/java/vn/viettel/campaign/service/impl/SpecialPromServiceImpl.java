package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.entities.SpecialProm;
import vn.viettel.campaign.service.SpecialPromService;

import javax.annotation.PostConstruct;
import vn.viettel.campaign.dao.SpecialpromDao;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Service
public class SpecialPromServiceImpl extends BaseBusinessImpl<SpecialProm, SpecialpromDao> implements SpecialPromService {

    @Autowired
    private SpecialpromDao specialpromDaoImpl;

    @PostConstruct
    public void setupService() {
        this.tdao = specialpromDaoImpl;
        this.modelClass = SpecialProm.class;
    }

    @Override
    public List<SpecialProm> findSpecialpromByCategory(final List<Long> categoryIds) {
        return specialpromDaoImpl.findSpecialpromByCategory(categoryIds);
    }

    @Override
    public List<SpecialProm> findSpecialpromByName(String specialpromByName) {
        return specialpromDaoImpl.findSpecialpromByName(specialpromByName);
    }

    @Override
    public boolean checkDuplicate(final String name, final Long id) {
        return specialpromDaoImpl.checkDuplicate(name, id);
    }

    @Override
    public boolean checkDuplicatePath(final String path, final Long id) {
        return specialpromDaoImpl.checkDuplicatePath(path, id);
    }

    @Override
    public long onSaveSpecialProm(SpecialProm specialProm) {
        return specialpromDaoImpl.onSaveSpecialProm(specialProm);
    }
}
