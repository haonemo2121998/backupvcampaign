package vn.viettel.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.dao.RuleDAOInterface;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.RuleServiceInterface;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Service
public class RuleServiceImpl extends BaseBusinessImpl<Rule, RuleDAOInterface> implements RuleServiceInterface {

    @Autowired
    RuleDAOInterface ruleDao;

    @PostConstruct
    public void setupService() {
        this.tdao = ruleDao;
        this.modelClass = Rule.class;
    }

    @Override
    public Rule getNextSequense() {
        return ruleDao.getNextSequense();
    }

    @Override
    public boolean onSaveRule(Rule rule, List<ResultTable> resultTables) {
        String result = ruleDao.onSaveRule(rule, resultTables);
        if (!result.equalsIgnoreCase(Responses.SUCCESS.getName())) {
            return false;
        }
        return true;
    }

    @Override
    public boolean onUpdateRule(Rule rule, List<ResultTable> resultTables) {
        String result = ruleDao.onUpdateRule(rule, resultTables);
        if (!result.equalsIgnoreCase(Responses.SUCCESS.getName())) {
            return false;
        }
        return true;
    }

    @Override
    public boolean onDeleteRule(Long ruleId) {
        return ruleDao.onDeleteRule(ruleId);
    }

    @Override
    public boolean checkRuleInUse(Long ruleId) {
        return ruleDao.checkRuleInUse(ruleId);
    }
}
