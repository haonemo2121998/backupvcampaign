/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.ResultTableRuleResultMap;

public interface ResultTableRuleResultMapService {
    List<ResultTableRuleResultMap> finAllResultTableRuleResultMap();
    ResultTableRuleResultMap updateOrSave(ResultTableRuleResultMap resultTableRuleResultMap);
    ResultTableRuleResultMap getResultTableRuleResultMapById(Long resultTableRuleResultId);
}
