/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.UsersDao;
import vn.viettel.campaign.entities.Users;
import vn.viettel.campaign.service.UsersService;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersDao usersDaoImpl;

    @Override
    public List<Users> getLstUsersByName(String name, Long objectId) {
        return usersDaoImpl.getLstUsersByName(name, objectId);
    }

    @Override
    public Boolean saveUsers(Users obj) {
        return usersDaoImpl.saveUsers(obj);
    }

    @Override
    public Boolean updateUsers(Users obj) {
        return usersDaoImpl.updateUsers(obj);
    }

    @Override
    public List<Users> getLstUsers() {
        return usersDaoImpl.getLstUsers();
    }

    @Override
    public void changePassword(Long userId, String newpass, String oldpass) {
        usersDaoImpl.changePassword(userId, newpass);
    }

}
