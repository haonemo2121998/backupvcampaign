/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dao.BlockContainerDao;
import vn.viettel.campaign.dao.CategoryDao;
import vn.viettel.campaign.dao.PromotionBlockDao;
import vn.viettel.campaign.dto.BehaviourDTO;
import vn.viettel.campaign.dto.PromotionBlockDTO;
import vn.viettel.campaign.entities.BlockContainer;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.PromotionBlock;
import vn.viettel.campaign.service.PromotionBlockService;

/**
 *
 * @author ConKC
 */
@Service
public class PromotionBlockImpl implements PromotionBlockService {

    @Autowired
    private PromotionBlockDao promotionBlockDaoImpl;

    @Autowired
    private BlockContainerDao blockContainerDaoImpl;

    @Autowired
    private CategoryDao categoryDaoImpl;

    @Autowired
    private TreeServiceImpl treeService;

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public List<PromotionBlock> findByCategory(final List<Long> categoryIds) {
        return promotionBlockDaoImpl.findByCategory(categoryIds);
    }

    @Override
    public List<BehaviourDTO> findBehaviourByPromotionBlockId(final Long promotionBlockId) {
        return promotionBlockDaoImpl.findBehaviourByPromotionBlockId(promotionBlockId);
    }

    @Override
    public List<BehaviourDTO> findOcsBehaviour(final List<Long> ids) {
        return promotionBlockDaoImpl.findOcsBehaviour(ids);
    }

    @Override
    public List<BehaviourDTO> findNotifyTrigger(final List<Long> ids) {
        return promotionBlockDaoImpl.findNotifyTrigger(ids);
    }

    @Override
    public List<BehaviourDTO> findScenarioTrigger(final List<Long> ids) {
        return promotionBlockDaoImpl.findScenarioTrigger(ids);
    }

    @Override
    public boolean checkDuplicate(final String name, final Long id) {
        return promotionBlockDaoImpl.checkDuplicate(name, id);
    }

    @Override
    public TreeNode genTreeBehaviours(final List<Long> ids, final List<BehaviourDTO> behaviours, final Long prmType) {
        if (behaviours != null && !behaviours.isEmpty()) {
            List<BehaviourDTO> behaviourDTOs = new ArrayList<>();
            List<Long> categoryIds = new ArrayList<>();
            if (ids != null && !ids.isEmpty()) {
                if (Objects.equals(prmType, Constants.PromotionType.OCS_BEHAVIOUR)) {
                    behaviours.stream().map((behaviour) -> {
                        if (Objects.nonNull(behaviour.getPromotionId())
                                && !ids.contains(behaviour.getPromotionId())) {
                            behaviourDTOs.add(behaviour);
                        }
                        return behaviour;
                    }).filter((behaviour) -> (Objects.nonNull(behaviour.getCategoryId())
                            && !categoryIds.contains(behaviour.getCategoryId())))
                            .forEach((behaviour) -> {
                                categoryIds.add(behaviour.getCategoryId());
                            });
                } else if (Objects.equals(prmType, Constants.PromotionType.NOTIFY_TRIGGER)) {
                    behaviours.stream().map((behaviour) -> {
                        if (Objects.nonNull(behaviour.getPromotionId())
                                && !ids.contains(behaviour.getPromotionId())) {
                            behaviourDTOs.add(behaviour);
                        }
                        return behaviour;
                    }).filter((behaviour) -> (Objects.nonNull(behaviour.getCategoryId())
                            && !categoryIds.contains(behaviour.getCategoryId())))
                            .forEach((behaviour) -> {
                                categoryIds.add(behaviour.getCategoryId());
                            });
                } else if (Objects.equals(prmType, Constants.PromotionType.SCENARIO_TRIGGER)) {
                    behaviours.stream().map((behaviour) -> {
                        if (Objects.nonNull(behaviour.getPromotionId())
                                && !ids.contains(behaviour.getPromotionId())) {
                            behaviourDTOs.add(behaviour);
                        }
                        return behaviour;
                    }).filter((behaviour) -> (Objects.nonNull(behaviour.getCategoryId())
                            && !categoryIds.contains(behaviour.getCategoryId())))
                            .forEach((behaviour) -> {
                                categoryIds.add(behaviour.getCategoryId());
                            });
                }
            } else {
                behaviours.stream()
                        .filter((behaviour) -> (Objects.nonNull(behaviour.getCategoryId())
                                && !categoryIds.contains(behaviour.getCategoryId())))
                        .forEach((behaviour) -> {
                            categoryIds.add(behaviour.getCategoryId());
                        });
                behaviourDTOs.addAll(behaviours);
            }

            List<Category> categorys = categoryDaoImpl.findCategoryByIds(categoryIds);
            List<Object> objectLst = new ArrayList<>(behaviourDTOs);
            if (categorys != null && categorys.size() > 0) {
                return treeService.createTreeCategoryAndComponentObj(categorys, objectLst);
            }
        }
        return new DefaultTreeNode();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean savePrmBlock(final PromotionBlockDTO promotionBlockDTO, final List<BehaviourDTO> behaviourDTOs, final boolean isUpdate) throws Exception {
        boolean rsPrm = true;
        PromotionBlock promotionBlock = generationPrm(promotionBlockDTO);
        DataUtil.trimObject(promotionBlock);
        String rs;
        if (isUpdate) {
            rs = promotionBlockDaoImpl.update(promotionBlock);
            if (StringUtils.isNotBlank(rs) && Responses.SUCCESS.getName().equals(rs)) {
                promotionBlockDaoImpl.deleteBlockContainerByPrmId(promotionBlock.getId());
                List<BlockContainer> blockContainers = generationBehaviours(behaviourDTOs, promotionBlock);
                if (!blockContainers.isEmpty()) {
                    String rs1 = blockContainerDaoImpl.save(blockContainers);
                    if (!Responses.SUCCESS.getName().equals(rs1)) {
                        rsPrm = false;
                    }
                }
            } else {
                rsPrm = false;
            }
        } else {
            rs = promotionBlockDaoImpl.save(promotionBlock);
            if (StringUtils.isNotBlank(rs) && Long.valueOf(rs) > 0) {
                promotionBlockDaoImpl.deleteBlockContainerByPrmId(promotionBlock.getId());
                List<BlockContainer> blockContainers = generationBehaviours(behaviourDTOs, promotionBlock);
                if (!blockContainers.isEmpty()) {
                    String rs1 = blockContainerDaoImpl.save(blockContainers);
                    if (!Responses.SUCCESS.getName().equals(rs1)) {
                        rsPrm = false;
                    }
                }
            } else {
                rsPrm = false;
            }
        }
        return rsPrm;
    }

    @Override
    public boolean deletePrmBlock(final PromotionBlock promotionBlock) {
        if (Objects.nonNull(promotionBlock)) {
            try {
                promotionBlockDaoImpl.deleteBlockContainerByPrmId(promotionBlock.getId());
                promotionBlockDaoImpl.deleteByObject(promotionBlock);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean checkInUseInResult(Long promotionBlockId) {
        return promotionBlockDaoImpl.checkInUseResult(promotionBlockId);
    }

    private PromotionBlock generationPrm(final PromotionBlockDTO promotionBlockDTO) {
        PromotionBlock block = new PromotionBlock();
        block.setId(promotionBlockDTO.getBlockId());
        if (StringUtils.isNotBlank(promotionBlockDTO.getBlockName())) {
            block.setName(promotionBlockDTO.getBlockName().trim());
        }
        block.setCategoryId(promotionBlockDTO.getCategoryId());
        block.setDescription(promotionBlockDTO.getDescription());
        return block;
    }

    private List<BlockContainer> generationBehaviours(final List<BehaviourDTO> behaviourDTOs, final PromotionBlock block) {
        List<BlockContainer> containers = new ArrayList<>();
        if (behaviourDTOs != null && !behaviourDTOs.isEmpty()) {
            behaviourDTOs.forEach(item -> {
                BlockContainer container = new BlockContainer();
                container.setPromotionBlockId(block.getId());
                container.setPromId(item.getPromotionId());
                container.setPromType(item.getPromotionType());
                container.setOcsParentId(item.getOcsParentId());
                containers.add(container);
            });
        }
        return containers;
    }

    @Override
    public Long genPrmBlockId() {
        try {
            return promotionBlockDaoImpl.genPrmBlockId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public PromotionBlock findById(long id) {
        return promotionBlockDaoImpl.findById(PromotionBlock.class, id);
    }

    @Override
    public long checkUsePromotionBlock(Long promType, Long promId) {
        try {
            return promotionBlockDaoImpl.checkUsePromotionBlock(promType, promId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
