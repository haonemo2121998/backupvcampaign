/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;

import vn.viettel.campaign.dto.ConditionTableDTO;
import vn.viettel.campaign.entities.Result;
import vn.viettel.campaign.entities.ResultTable;

public interface ResultTableService {
    List<ResultTable> finAllResultTable();

    ResultTable updateOrSave(ResultTable resultTable);

    ResultTable updateOrSave(ResultTable resultTable, Result defaultResult, List<ConditionTableDTO> conditionTableDTOS, ConditionTableDTO conditionTableDefault);

    ResultTable getResultTableById(Long resultTableId);

    List<ResultTable> getListToCheckDuplicate(String resultTableName, Long resultTableId);

    void doDelete(ResultTable resultTable);

    long checkUseRule(Long resultTableId);
}
