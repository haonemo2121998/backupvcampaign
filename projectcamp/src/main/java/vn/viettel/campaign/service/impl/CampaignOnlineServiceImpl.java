/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Order;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.poseidon.domain.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.*;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.CampaignOnlineService;
import vn.viettel.campaign.service.PreprocessUnitInterface;

/**
 * @author ConKC
 */
@Service
public class CampaignOnlineServiceImpl implements CampaignOnlineService {

	@Autowired
	SegmentDAOInterface segmentDAO;
	@Autowired
	RuleDAOInterface ruleDAO;
	@Autowired
	ResultTableDAOInterface resultTableDAO;
	@Autowired
	ConditionTableDAOInterface conditionTableDAO;
	@Autowired
	ColumnCTDAOInterface columnCTDAO;
	@Autowired
	PreprocessUnitDaoInterface preprocessUnitDAO;
	@Autowired
	CdrServiceDAOInterface cdrServiceDAO;
	@Autowired
	InvitationDAOInterface invitationDAO;

	@Autowired
	RuleResultTableMapInterface ruleResultTableMapDAO;

	@Autowired
	CampaignDAOInterface campaignDAO;

	@Autowired
	CampaignScheduleDAOInterface campaignScheduleDAO;

	@Override
	public List<Segment> getListSegmentByCampaignId(Long campaignId) {
		return segmentDAO.getLstSegment(campaignId);
	}

	@Override
	public List<Rule> getListRuleByCampaignId(Long campaignId) {
		return ruleDAO.getLstRuleByCampaignId(campaignId);
	}

	@Override
	public List<Rule> getLstRuleForDefaultCampaign(long campaignId) {
		return ruleDAO.getLstRuleForDefaultCampaign(campaignId);
	}

	@Override
	public List<InvitationPriority> getListInvitationByCampaignId(Long campaignId) {
		return invitationDAO.getInvitationByCampaignId(campaignId);
	}

	@Override
	public List<InvitationPriority> getInvitationForDefaultCampaign(long campaignId) {
		return invitationDAO.getInvitationForDefaultCampaign(campaignId);
	}

	@Override
	public List<InvitationPriority> getNotifyTriggerByRuleId(long ruleId, String segmentName, long segmentId) {
		return invitationDAO.getNotifyTriggerByRuleId(ruleId, segmentName, segmentId);
	}

	@Override
	public List<ResultTable> getListResultByRuleId(Long ruleId) {
		return resultTableDAO.getLstRuleResultTableByRuleIds(ruleId);
	}

	@Override
	public ConditionTable getListConditionById(Long id) {
		return conditionTableDAO.findById(ConditionTable.class, id);
	}

	@Override
	public List<ColumnCt> getListColumnCtById(Long id) {
		return columnCTDAO.getLstColumnctByConditionTableId(id);
	}

	@Override
	public List<PreProcessUnit> getListPPUByConditionId(Long id) {
		return preprocessUnitDAO.getLstPPUByConditionId(id);
	}

	@Override
	public List<CdrService> getAllCdrService() {
		return cdrServiceDAO.getAll(CdrService.class, Order.asc("name"));
	}

	@Override
	public List<ResultTable> getAllResultTable() {
		return resultTableDAO.getAll(ResultTable.class);
	}

	@Override
	public boolean saveNewRule(Rule rule, List<ResultTable> resultTables) {
		String keyId = ruleDAO.save(rule);
		try {
			Long id = Long.parseLong(keyId);
			List<RuleResultTableMap> lst = new ArrayList<>();
			for (ResultTable item : resultTables) {
				RuleResultTableMap ruleResultTableMap = new RuleResultTableMap();
				ruleResultTableMap.setRuleId(id);
				ruleResultTableMap.setResultTableId(item.getResultTableId());
				ruleResultTableMap.setResultTableIndex(resultTables.indexOf(item) + 1);
				lst.add(ruleResultTableMap);
			}
			ruleResultTableMapDAO.save(lst);
		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	public String onSaveNewCampaign(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
									List<InvitationPriority> lstInvitation) {
		String result = campaignDAO.onSaveNewCampaign(campaign, lstCampaignInfo, lstInvitation);
		return result;
	}

	@Override
	public String onUpdateCampaign(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
								   List<InvitationPriority> lstInvitation) {
		String result = campaignDAO.onUpdateCampaign(campaign, lstCampaignInfo, lstInvitation);
		return result;
	}

	@Override
	public boolean checkExisCampaignName(String campaignName, Long campaignId) {
		return campaignDAO.checkExisCampaignName(campaignName, campaignId);
	}

	@Override
	public boolean checExisRuleName(String ruleName,Long ruleId) {
		return ruleDAO.checkExisRuleName(ruleName,ruleId);
	}

	@Override
	public boolean checExisScheduleName(String scheduleName) {
		return campaignScheduleDAO.checkExisScheduleName(scheduleName);
	}

	@Override
	public boolean deleteCampaign(Long campaignId) {
		return campaignDAO.deleteCampaign(campaignId);
	}

	@Override
	public Campaign getCampaignById(Long campaignId) {
		return campaignDAO.findById(Campaign.class,campaignId);
	}

	@Override
	public Rule getNextRule() {
		return ruleDAO.getNextSequense();
	}

	@Override
	public List<CampaignSchedule> getListCampaignSchedule(Long campaignId) {
		return campaignScheduleDAO.getLstCampaignSchedule(campaignId);
	}

	@Override
	public CampaignSchedule getNextSchedule() {
		return campaignScheduleDAO.getNextSchedule();
	}

	@Override
	public String onSaveNewCampaignOffline(Campaign campaign, List<CampaignInfo> lstCampaignInfo, List<InvitationPriority> lstInvitation, List<CampaignSchedule> lstCampaignSchedules) {
		return campaignDAO.onSaveNewCampaignOffline(campaign,lstCampaignInfo,lstInvitation,lstCampaignSchedules);
	}

	@Override
	public String onUpdateCampaignOffline(Campaign campaign, List<CampaignInfo> lstCampaignInfo, List<InvitationPriority> lstInvitation, List<CampaignSchedule> lstCampaignSchedules) {
		return campaignDAO.onUpdateCampaignOffline(campaign,lstCampaignInfo,lstInvitation,lstCampaignSchedules);
	}

	@Override
	public boolean deleteCampaignOffline(Long campaignId) {
		return campaignDAO.deleteCampaignOffline(campaignId);
	}

	@Override
	public Campaign getNextSequense() {
		return campaignDAO.getNextSequence();
	}

	@Override
	public TreeNode createDocumentsByCampaignId(Long campaignId) {
		//root
		TreeNode root1 = new DefaultTreeNode(new Document("Files", "-", "Folder"), null);

		//Segment 1
		TreeNode segment1 = new DefaultTreeNode(new Document("Segment 1", "-", "Folder"), root1);
		//level 2
		TreeNode rule1 = new DefaultTreeNode("document", new Document("Rule 1", "-", "Pages Document"), segment1);
		//level 3
		TreeNode lstCdr = new DefaultTreeNode("document", new Document("List cdr", "-", "Pages Document"), rule1);
		TreeNode result1 = new DefaultTreeNode("document", new Document("Result table 1", "-", "Pages Document"), rule1);
		TreeNode result2 = new DefaultTreeNode("document", new Document("Result table 2", "-", "Pages Document"), rule1);
		//level 4
		TreeNode voice = new DefaultTreeNode("document", new Document("Voice", "-", "Pages Document"), lstCdr);
		TreeNode data = new DefaultTreeNode("document", new Document("Data", "-", "Pages Document"), lstCdr);
		TreeNode sms = new DefaultTreeNode("document", new Document("Sms", "-", "Pages Document"), lstCdr);
		TreeNode condition = new DefaultTreeNode("document", new Document("Condition 1", "-", "Pages Document"), result2);
		//level 5
		TreeNode ppu = new DefaultTreeNode("picture", new Document("PPU 1", "-", "checkbox"), condition);

		//Segment 2
		TreeNode segment2 = new DefaultTreeNode(new Document("Segment 2", "-", "Folder"), root1);
		//level 2
		TreeNode rule = new DefaultTreeNode("document", new Document("Rule 1", "-", "Pages Document"), segment2);
		TreeNode rule2 = new DefaultTreeNode("document", new Document("Rule 2", "-", "Pages Document"), segment2);
		//level 3
		TreeNode resultseg2 = new DefaultTreeNode("document", new Document("Result table 1", "-", "Pages Document"), rule);
		TreeNode result2seg2 = new DefaultTreeNode("document", new Document("Result table 2", "-", "Pages Document"), rule2);
		TreeNode result3seg2 = new DefaultTreeNode("document", new Document("Result table 3", "-", "Pages Document"), rule2);
		//level 4
		TreeNode dtion1 = new DefaultTreeNode("document", new Document("Condition 1", "-", "Pages Document"), resultseg2);
		TreeNode dtion2 = new DefaultTreeNode("document", new Document("Condition 2", "-", "Pages Document"), result2seg2);
		//level 5
		TreeNode ppu1 = new DefaultTreeNode("picture", new Document("PPU 1", "-", "checkbox"), dtion1);
		TreeNode ppu2 = new DefaultTreeNode("picture", new Document("PPU 2", "-", "checkbox"), dtion2);
		TreeNode ppu3 = new DefaultTreeNode("picture", new Document("PPU 3", "-", "checkbox"), dtion2);

		return root1;
	}


}
