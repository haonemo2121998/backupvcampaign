package vn.viettel.campaign.service;

import org.hibernate.criterion.Order;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface BaseBusinessInterface<T> {

    public T findById(long id);

    public List<T> getAll();

    public List<T> getAll(Order order);

    public String saveOrUpdate(T t);

    public String deleteById(long id);

    public String deleteByObject(T obj);

    public String update(T obj);

    public String save(List<T> lstObj);

    public String save(T obj);

    public long saveAndGetId(T obj) throws Exception;
}
