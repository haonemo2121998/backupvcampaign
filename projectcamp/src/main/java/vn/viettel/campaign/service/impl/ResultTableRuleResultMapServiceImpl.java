/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.ResultTableRuleResultMapDao;
import vn.viettel.campaign.entities.ResultTableRuleResultMap;
import vn.viettel.campaign.service.ResultTableRuleResultMapService;


@Service
public class ResultTableRuleResultMapServiceImpl implements ResultTableRuleResultMapService {

    @Autowired
    private ResultTableRuleResultMapDao resultTableRuleResultMapDaoImpl;

    @Override
    public List<ResultTableRuleResultMap> finAllResultTableRuleResultMap() {
        return resultTableRuleResultMapDaoImpl.finAllResultTableRuleResultMap();
    }

    @Override
    public ResultTableRuleResultMap updateOrSave(ResultTableRuleResultMap resultTableRuleResultMap) {
        return resultTableRuleResultMapDaoImpl.updateOrSave(resultTableRuleResultMap);
    }

    @Override
    public ResultTableRuleResultMap getResultTableRuleResultMapById(Long resultTableRuleResultId) {
        return resultTableRuleResultMapDaoImpl.getResultTableRuleResultMapById(resultTableRuleResultId);
    }

   
    
}
