package vn.viettel.campaign.service;

import java.util.List;
import org.primefaces.model.TreeNode;
import vn.viettel.campaign.dto.BehaviourDTO;
import vn.viettel.campaign.dto.PromotionBlockDTO;
import vn.viettel.campaign.entities.PromotionBlock;

/**
 *
 * @author ConKC
 */
public interface PromotionBlockService {

    List<PromotionBlock> findByCategory(final List<Long> categoryIds);

    List<BehaviourDTO> findBehaviourByPromotionBlockId(final Long promotionBlockId);

    List<BehaviourDTO> findOcsBehaviour(final List<Long> ids);

    List<BehaviourDTO> findNotifyTrigger(final List<Long> ids);

    List<BehaviourDTO> findScenarioTrigger(final List<Long> ids);

    TreeNode genTreeBehaviours(final List<Long> ids, final List<BehaviourDTO> behaviours, final Long prmType);

    boolean checkDuplicate(final String name, final Long id);

    boolean savePrmBlock(final PromotionBlockDTO promotionBlockDTO, final List<BehaviourDTO> behaviourDTOs, final boolean isUpdate) throws Exception;

    boolean deletePrmBlock(final PromotionBlock promotionBlock);
    boolean checkInUseInResult( Long promotionBlockId);

    Long genPrmBlockId();

    PromotionBlock findById(long id);

    long checkUsePromotionBlock(Long promType, Long promId);
}
