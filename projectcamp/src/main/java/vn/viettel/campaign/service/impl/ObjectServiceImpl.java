/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.ObjectDao;
import vn.viettel.campaign.entities.Objects;
import vn.viettel.campaign.service.ObjectService;

@Service
public class ObjectServiceImpl implements ObjectService {

    @Autowired
    private ObjectDao objectDaoImpl;

    @Override
    public List<Objects> getLstObjectByName(String name, Long objectId) {
        return objectDaoImpl.getLstObjectByName(name, objectId);
    }

    @Override
    public List<Objects> getLstObjectByCode(String code, Long objectId) {
        return objectDaoImpl.getLstObjectByCode(code, objectId);
    }

    @Override
    public Boolean saveObject(Objects obj) {
        return objectDaoImpl.saveObject(obj);
    }

    @Override
    public Boolean updateObject(Objects obj) {
        return objectDaoImpl.updateObject(obj);
    }
    
    @Override
    public List<Objects> getLstObjects(){
        return objectDaoImpl.getLstObjects();
    }

    @Override
    public List<Objects> getLstObjectsByRole(Long[] roleId) {
        return objectDaoImpl.getLstObjectsByRole(roleId);
    }

}
