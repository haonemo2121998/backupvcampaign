/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.RuleResultDao;
import vn.viettel.campaign.entities.RuleResult;
import vn.viettel.campaign.service.RuleResultService;

@Service
public class RuleResultServiceImpl implements RuleResultService {

    @Autowired
    private RuleResultDao ruleResultDaoImpl;

    @Override
    public List<RuleResult> finAllRuleResult() {
        return ruleResultDaoImpl.finAllRuleResult();
    }

    @Override
    public RuleResult updateOrSave(RuleResult ruleResult) {
        return ruleResultDaoImpl.updateOrSave(ruleResult);
    }

    @Override
    public RuleResult getRuleResultById(Long ruleResultId) {
        return ruleResultDaoImpl.getRuleResultById(ruleResultId);
    }
}
