/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.SegmentDao;
import vn.viettel.campaign.entities.Segment;
import vn.viettel.campaign.service.SegmentService;

/**
 *
 * @author ConKC
 */
@Service
public class SegmentServiceImpl extends BaseBusinessImpl<Segment, SegmentDao> implements SegmentService {

    @Autowired
    private SegmentDao segmentDaoImpl;

    @PostConstruct
    public void setupService() {
        this.tdao = segmentDaoImpl;
        this.modelClass = Segment.class;
    }

    @Override
    public List<Segment> findSegmentByCategory(final List<Long> categoryIds) {
        return segmentDaoImpl.findSegmentByCategory(categoryIds);
    }

    @Override
    public List<Segment> findSegmentByName(String name) {
        return segmentDaoImpl.findSegmentByName(name);
    }

    @Override
    public boolean checkDuplicate(final String name, final Long id) {
        return segmentDaoImpl.checkDuplicate(name, id);
    }

    @Override
    public boolean checkDuplicatePath(final String path, final Long id) {
        return segmentDaoImpl.checkDuplicatePath(path, id);
    }

    @Override
    public long onSaveSegment(Segment segment) {
        return segmentDaoImpl.onSaveSegment(segment);
    }
}
