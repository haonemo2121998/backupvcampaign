/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.Result;
import vn.viettel.campaign.entities.RoleObject;
import vn.viettel.campaign.entities.RoleUser;
import vn.viettel.campaign.entities.Roles;

public interface ResultService {
    public List<Result> getResultByRowIndexAndResultTableId(long rowIndex, long resultTableId);
    List<Result> finAllResult();
    Object getSequence();  
    Result updateOrSave(Result result);
    Result getResultById(Long resultId);
}
