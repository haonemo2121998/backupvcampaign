/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import vn.viettel.campaign.entities.*;

import java.util.List;

/**
 * @author ConKC
 */
public interface ScenarioTriggerService {
    public ScenarioTrigger findById(long id);

    public void doSave(ScenarioTrigger scenarioTrigger);

    public void doDelete(long id);

    public List<UssdScenario> getAllUssdScenario();

    public List<ScenarioNode> getScenarioNodeByUssdId(long id);

    public boolean validateAdd(long id, String name);
}
