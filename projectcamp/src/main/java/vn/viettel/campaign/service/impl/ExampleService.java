/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.impl.ExamplarDao;

@Service
public class ExampleService {
    
    @Autowired
    private ExamplarDao examplarDao;
    
    public void getInfoDao(){
        examplarDao.getLst();
    }
}
