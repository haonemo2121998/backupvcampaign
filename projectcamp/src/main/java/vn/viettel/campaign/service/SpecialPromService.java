package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.entities.SpecialProm;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface SpecialPromService extends BaseBusinessInterface<SpecialProm> {

    List<SpecialProm> findSpecialpromByCategory(final List<Long> categoryIds);

    List<SpecialProm> findSpecialpromByName(final String specialpromByName);

    long onSaveSpecialProm(SpecialProm specialProm);

    boolean checkDuplicate(final String name, final Long id);
    
    boolean checkDuplicatePath(final String path, final Long id);
}
