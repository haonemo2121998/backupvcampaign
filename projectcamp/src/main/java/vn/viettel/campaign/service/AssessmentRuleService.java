/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;
import vn.viettel.campaign.dao.AssessmentRulePreProcessUnitMapDAO;
import vn.viettel.campaign.dao.CriteriaValueDAO;
import vn.viettel.campaign.entities.AssessmentRule;
import vn.viettel.campaign.entities.AssessmentRuleKpiProcessorMap;
import vn.viettel.campaign.entities.AssessmentRulePreProcessUnitMap;
import vn.viettel.campaign.entities.CampaignEvaluationInfo;
import vn.viettel.campaign.entities.CriteriaValue;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.KpiProcessor;
import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.ResultClass;
import vn.viettel.campaign.entities.KpiProcessorCriteriaValueMap;
import vn.viettel.campaign.entities.PreProcessValueMap;
import vn.viettel.campaign.entities.ProcessValue;
import vn.viettel.campaign.entities.RuleEvaluationInfo;
import vn.viettel.campaign.entities.SegmentEvaluationInfo;

/**
 *
 * @author ABC
 */
public interface AssessmentRuleService {

    public List<PpuEvaluation> getListPpuEvaluationByRuleId(Long id);

    public List<PpuEvaluation> getListPpuEvaluationTree(List<Long> categoryIds);

    public List<KpiProcessor> getListKpiProcessorByRuleId(Long ruleId);

    public List<ResultClass> getListResultClassByAssessmentRuleId(Long ruleId);

    public ResultClass doCreateOrUpdateResultClass(ResultClass resultClass);

    public boolean deleteResultClass(Long id);

    public ResultClass getNextSequenceResultClass();

    public List<FuzzyRule> getListFuzzyRuleByRuleId(Long ruleId);

    public FuzzyRule getNextFuzzyRule();

    public FuzzyRule doCreateOrUpdateFuzzyRule(FuzzyRule fuzzyRule);

    public boolean deleteFuzzyRule(Long id);

    public List<KpiProcessorCriteriaValueMap> getListkpiProcessorCriteriaValueMapByProsessorId(Long processorId);

    public List<CriteriaValue> getListCriteriaValueDAO(List<Long> longs);

    public List<PreProcessValueMap> getListPreProcessValueMapByPreProcessId(Long preProcessId);

    public List<ProcessValue> getListProcessValueByListId(List<Long> longs);

    public AssessmentRule createOrUpdateAssessmenRule(AssessmentRule assessmentRule);

    public AssessmentRule getNextAssessmentRule();

    public AssessmentRule findAssessmentRuleById(Long id);

    public boolean deleteAssessmentRule(Long ruleId);

    public AssessmentRulePreProcessUnitMap createOrUpdateAssessmentRuleProcessUnitMap(AssessmentRulePreProcessUnitMap assessmentRulePreProcessUnitMap);

    public boolean deleteAssessmentRulePreProcessUnitMap(Long Id);

    public AssessmentRulePreProcessUnitMap getNextAssessmentRulePreProcessUnitMap();

    public List<AssessmentRulePreProcessUnitMap> getAllAssessmentRulePreProcessUnitMapById(Long id);

    public AssessmentRuleKpiProcessorMap createOrUpdateAssessmentRuleKpiProcessorMap(AssessmentRuleKpiProcessorMap assessmentRuleKpiProcessorMap);

    public boolean deleteAssessmentRuleKpiProcessorMap(Long id);

    public List<AssessmentRuleKpiProcessorMap> getAlllistAssessmentRuleKpiProcessorMapById(Long id);

    public AssessmentRuleKpiProcessorMap getNextAssessmentRuleKpiProcessorMap();

    public AssessmentRulePreProcessUnitMap findAssessmentRulePreProcessUnitMapById(Long id);

    public AssessmentRuleKpiProcessorMap findAssessmentRuleKpiProcessorMapById(Long id);

    public List<RuleEvaluationInfo> findRuleEvaluationInfoById(Long assessmentRuleId);

    public List<KpiProcessor> findAllKpiProcessor(List<Long> categoryIds);

    public long getNextSequense(String tableName);

    public List<CampaignEvaluationInfo> getListCampaignEvalurationMapAssessment(Long assessmentRuleId);

    public List<SegmentEvaluationInfo> getListSegmentEvaluationInfoMapAssessment(Long assessmentRuleId);

    public List<FuzzyRule> getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(Long campaignId, Long campaignEvaluationIfoId);

    public List<FuzzyRule> getListFuzzyRuleOfSegment(Long segmentId, Long campaignEvaluationInfoId);

    public ProcessValue checkValueName(String name,Long preProcessId);

    public CriteriaValue chekCriteriaValueName(String name,Long processorId);
}
