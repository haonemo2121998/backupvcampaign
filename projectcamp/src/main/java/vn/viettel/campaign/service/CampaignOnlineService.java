/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service;

import java.util.List;

import org.primefaces.model.TreeNode;
import vn.viettel.campaign.entities.*;

/**
 * @author ConKC
 */

public interface CampaignOnlineService {

	Campaign getNextSequense();
	List<Segment> getListSegmentByCampaignId(final Long campaignId);

	List<InvitationPriority> getListInvitationByCampaignId(final Long campaignId);

	List<InvitationPriority> getInvitationForDefaultCampaign(long campaignId);

	List<Rule> getListRuleByCampaignId(final Long campaignId);

	List<Rule> getLstRuleForDefaultCampaign(long campaignId);

	TreeNode createDocumentsByCampaignId(final Long campaignId);

	List<ResultTable> getListResultByRuleId(Long ruleId);

	List<InvitationPriority> getNotifyTriggerByRuleId(long ruleId, String segmentName, long segmentId);

	ConditionTable getListConditionById(Long id);

	List<ColumnCt> getListColumnCtById(Long id);
	List<PreProcessUnit> getListPPUByConditionId(Long id);
	List<CdrService> getAllCdrService();

	List<ResultTable> getAllResultTable();

	boolean saveNewRule(Rule rule, List<ResultTable> resultTables);

	public String onSaveNewCampaign(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
									List<InvitationPriority> lstInvitation);

	public String onUpdateCampaign(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
									List<InvitationPriority> lstInvitation);
	public String onSaveNewCampaignOffline(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
									List<InvitationPriority> lstInvitation,List<CampaignSchedule> lstCampaignSchedules);

	public String onUpdateCampaignOffline(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
								   List<InvitationPriority> lstInvitation,List<CampaignSchedule> lstCampaignSchedules);
	public  boolean checkExisCampaignName(String campaignName,Long campaignId);
	public  boolean checExisRuleName(String ruleName,Long ruleId);
	public  boolean checExisScheduleName(String scheduleName);
	public boolean deleteCampaign(Long campaignId);
	public boolean deleteCampaignOffline(Long campaignId);
	public Campaign getCampaignById(Long campaignId);
	public Rule getNextRule();
	public CampaignSchedule getNextSchedule();
	public List<CampaignSchedule> getListCampaignSchedule(Long campaignId);
}
