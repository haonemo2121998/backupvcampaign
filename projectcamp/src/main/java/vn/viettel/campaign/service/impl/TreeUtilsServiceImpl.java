/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.service.impl;

import java.util.List;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.TreeItemBase;
import vn.viettel.campaign.service.TreeUtilsService;

/**
 *
 * @author ABC
 */
@Service
public class TreeUtilsServiceImpl implements TreeUtilsService {

   @Override
    public <T extends TreeItemBase> TreeNode createTreeCategoryAndComponentCampaignEvaluation(List<Category> listCat, List<T> listT) {

        // root node in framework (not show in view)
        TreeNode rootNode = new DefaultTreeNode(null, null);

        // root node off tree (parent id is null)
        for (Category it : listCat) {
            if (it.getParentId() == null) {
                Category catRoot = it;
                TreeNode catRootNode = new DefaultTreeNode("category", catRoot, rootNode);
                genTreeCategoryAndComponent(catRootNode, listCat, listT, catRoot);
            }
        }

        return rootNode;
    }
	@Override
    public <T extends TreeItemBase> TreeNode createTreeCategoryAndComponentObject(List<Category> listCat, List<T> listT) {

        // root node in framework (not show in view)
        TreeNode rootNode = new DefaultTreeNode(null, null);

        // root node off tree (parent id is null)
        for (Category it : listCat) {
            if (it.getParentId() == null) {
                Category catRoot = it;
                TreeNode catRootNode = new DefaultTreeNode("category", catRoot, rootNode);
                genTreeCategoryAndComponent(catRootNode, listCat, listT, catRoot);
            }
        }

        return rootNode;
    }

    public <T extends TreeItemBase> void genTreeCategoryAndComponent(TreeNode parentNodeCat, List<Category> listCat, List<T> listT, Category parentCat) {
        // add componentNode
        for (Category cat : listCat) {
            if (parentCat.getCategoryId().equals(cat.getParentId())) {
                TreeNode childNodeCat = new DefaultTreeNode("category", cat, parentNodeCat);
                genTreeCategoryAndComponent(childNodeCat, listCat, listT, cat);
            }
        }
        for (T t : listT) {
            if (t.getCategoryId().equals(parentCat.getCategoryId())) {
                new DefaultTreeNode("object", t, parentNodeCat);
            }
        }

        
    }
    
}
