/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.constants;

/**
 *
 * @author ConKC
 */
public class Constants {

    public static final String CAMPAIGN = "Campaign";
    public static final String CAMPAIGN_EVALUATION_INFO = "CampaignEvaluationInfo";
    public static final String ASSESSMENT_RULE = "AssessmentRule";
    public static final String SEGMENT_TABLE = "Segment";
    public static final String KPI_PROCESSOR = "KpiProcessor";
    public static final String PPU_EVALUATION = "PpuEvaluation";
    public static final String COMBINE = "Combine";
    public static final String CONDITION_TABLE = "ConditionTable";
    public static final String PRE_PROCESS_UNIT = "PreProcessUnit";
    public static final int CAMPAIGN_ONLINE_TYPE = 1;
    public static final int CAMPAIGN_OFFLINE_TYPE = 2;
    public static final int PRE_PROCESS_UNIT_TYPE = 3;
    public static final int CONDITION_TABLE_TYPE = 4;
    public static final int NOTIFY_TRIGGER_TYPE = 7;
    public static final int SCENARIO_TRIGGER_TYPE = 8;
    public static final int USSD_SCENARIO_TYPE = 100;
    public static final int RESULT_TABLE_TYPE = 5;

    public static final int PRE_PROCESS_UNIT_STRING = 1;
    public static final int PRE_PROCESS_UNIT_NUMBER = 2;
    public static final int PROCESS_UNIT_EXIST_ELEMENT = 3;
    public static final int PRE_PROCESS_UNIT_TIME = 4;
    public static final int PRE_PROCESS_UNIT_DATE = 5;
    public static final int PRE_PROCESS_UNIT_NUMBER_RANGE = 6;
    public static final int PRE_PROCESS_UNIT_COMPARE_NUMBER = 7;
    public static final int PROCESS_UNIT_ZONE = 8;
    public static final int PRE_PROCESS_UNIT_SAME_ELEMENT = 9;

    public static final long PROM_TYPE_1 = 1;
    public static final long PROM_TYPE_2 = 2;
    public static final long PROM_TYPE_3 = 3;

    public static final long PPU_TYPE_RULE = 1;
    public static final long PPU_TYPE_EVALUATION = 2;

    public static final int SEGMENT = 12;
    public static final int BLACKLIST_TYPE = 15;
    public static final int SPECIAL_OFFER_TYPE = 16;

    public static final String REMOTE_GROWL = "remoteGlobalForm:remoteGrowl";

    public static class ErrorCode {

        public static final int RUNNING = 1;
        public static final int SUCCESS = 2;
        public static final int FAILED = 3;
        public static final int INIT = 4;
        public static final int ACCEPT = 5;
    }

    public static class ErrorCodeName {

        public static final String RUNNING = "RUNNING";
        public static final String SUCCESS = "SUCCESS";
        public static final String FAILED = "FAILED";
        public static final String INIT = "INIT";
        public static final String ACCEPT = "ACCEPT";
    }

    public static class TableName {

        public static final String SCENARIO_NOTE = "SCENARIO_NOTE";
        public static final String NOTIFY_CONTENT = "NOTIFY_CONTENT";
        public static final String NOTIFY_TRIGGER = "NOTIFY_TRIGGER";
        public static final String RESULT_TABLE = "RESULT_TABLE";
        public static final String SCENARIO_TRIGGER = "SCENARIO_TRIGGER";
        public static final String USSD_SCENARIO = "USSD_SCENARIO";
        public static final String CONDITION_TABLE = "CONDITION_TABLE";
        public static final String STATISTIC_CYCLE = "STATISTIC_CYCLE";
        public static final String NOTIFY_TEMPLATE = "NOTIFY_TEMPLATE";
        public static final String NODE_CONTENT = "NODE_CONTENT";
        public static final String CATEGORY = "CATEGORY";
    }

    public static class ServiceId {

        public static final int UPLOAD_SEGMENT = 31;
        public static final int DELETE_SEGMENT = 32;
        public static final int UPLOAD_BLACKLIST = 33;
        public static final int UPDATE_BLACKLIST = 34;
        public static final int DELETE_BLACKLIST = 35;
        public static final int UPLOAD_SPECIAL_OFFER = 36;
        public static final int UPDATE_SPECIAL_OFFER = 37;
        public static final int DELETE_SPECIAL_OFFER = 38;
    }

    public static class ServiceName {

        public static final String UPLOAD_SEGMENT = "UPLOAD_SEGMENT";
        public static final String DELETE_SEGMENT = "DELETING_SEGMENT";
        public static final String UPLOAD_BLACKLIST = "UPLOAD_BLACKLIST";
        public static final String UPDATE_BLACKLIST = "UPDATE_BLACKLIST";
        public static final String DELETE_BLACKLIST = "DELETE_BLACKLIST";
        public static final String UPLOAD_SPECIAL_OFFER = "UPLOAD_SPECIAL_OFFER";
        public static final String UPDATE_SPECIAL_OFFER = "UPDATE_SPECIAL_OFFER";
        public static final String DELETE_SPECIAL_OFFER = "DELETE_SEPCIAL_OFFER";
    }

    public class Factor {

        public static final int WEB_FACTOR = 10;
        public static final int CBA_FACTOR = 11;
        public static final int OTHER_FACTOR = 12;
    }

    public class FactorName {

        public static final String WEB_FACTOR = "WEB FACTOR";
        public static final String CBA_FACTOR = "CBA FACTOR";
        public static final String OTHER_FACTOR = "OTHER FACTOR";
    }

    public static class ResultType {

        public static final long NORMAL = 1L;
        public static final long SKIP = 2L;
        public static final long NO_ACTION = 3L;
        public static final long EXIT_ALL = 4L;
        public static final long EXIT_RULE = 5L;
        public static final long EXIT_SEGMENT = 6L;
        public static final long EXIT_CAMPIGN = 7L;
    }

    public static class RequestType {

        public static final int REQUEST_SERVICE = 21;
        public static final int FORCE_STOP_SERVICE = 22;
    }

    public static class RequestTypeName {

        public static final String REQUEST_SERVICE = "REQUEST SERVICE";
        public static final String FORCE_STOP_SERVICE = "FORCE STOP SERVICE";
    }

    public static class ObjectType {

        public static final String SEGMENT = "1";
        public static final String BLACK_LIST = "2";
        public static final String SPECIAL_OFFER = "3";
    }

    public static class ObjectName {

        public static final String SEGMENT = "Segment";
        public static final String BLACK_LIST = "BlackList";
        public static final String SPECIAL_OFFER = "Special Offer";
    }

    public static class ActionValue {

        public static final String UPLOAD = "1";
        public static final String DELETE = "2";
        public static final String UPDATE = "3";
    }

    public static class ActionName {

        public static final String UPLOAD = "Upload";
        public static final String DELETE = "Delete";
        public static final String UPDATE = "Update";
    }

    public static class CatagoryType {

        public static final int CATEGORY_USSD_SCENARIO_TYPE = 0;
        public static final int CATEGORY_CAMPAIGN_ONLINE_TYPE = 1;
        public static final int CATEGORY_CAMPAIGN_OFFLINE_TYPE = 2;
        public static final int CATEGORY_PRE_PROCESS_UNIT_TYPE = 3;
        public static final int CATEGORY_CONDITION_TABLE_TYPE = 4;
        public static final int CATEGORY_RESULT_TABLE_TYPE = 5;
        public static final int CATEGORY_PROMOTION_BLOCK_TYPE = 6;
        public static final int CATEGORY_NOTIFY_TRIGGER_TYPE = 7;
        public static final int CATEGORY_SCENARIO_TRIGGER_TYPE = 8;
        public static final int CATEGORY_OCS_BEHAVIOUR_TYPE = 9;
        public static final int CATEGORY_NOTIFY_TEMPLATE_TYPE = 10;
        public static final int CATEGORY_OCS_BEHAVIOUR_TEMPLATE_TYPE = 11;
        public static final int CATEGORY_SEGMENT_TYPE = 12;
        public static final int CATEGORY_RULE_ONLINE_TYPE = 13;
        public static final int CATEGORY_RULE_OFFLINE_TYPE = 14;
        public static final int CATEGORY_BLACLIST_TYPE = 15;
        public static final int CATEGORY_SPECIALPROM_TYPE = 16;
        public static final int CATEGORY_PRE_PROCESS_UNIT_STRING_TYPE = 17;
        public static final int CATEGORY_PRE_PROCESS_UNIT_NUMBER_TYPE = 18;
        public static final int CATEORY_PRE_PROCESS_UNIT_TIME_TYPE = 19;
        public static final int CATEORY_PRE_PROCESS_UNIT_DATE_TYPE = 20;
        public static final int CATEORY_PRE_PROCESS_UNIT_NUMBER_RANGE_TYPE = 21;
        public static final int CATEGORY_PRE_PROCESS_UNIT_COMPARE_NUMBER_TYPE = 22;
        public static final int CATEORY_PRE_PROCESS_UNIT_EXIST_ELEMENT_TYPE = 23;
        public static final int CATEORY_PRE_PROCESS_UNIT_ZONE_TYPE = 24;
        public static final int CATEORY_PRE_PROCESS_UNIT_SAME_ELEMENT_TYPE = 25;
        public static final int CATEORY_ZONE_TYPE = 26;
        public static final int CATEORY_ZONE_MAP_TYPE = 27;
        public static final int CATEORY_GEO_HOME_ZONE_TYPE = 28;
        public static final int CATEGORY_PARAMETER_TYPE = 29;
        public static final int CATEORY_STATISTIC_CYCLE_TYPE = 30;
        public static final int CATEGORY_CAMPAIGN_EVALUATION_INFO_TYPE = 31;
        public static final int CATEGORY_ASSESSMENT_RULE_TYPE = 32;
        public static final int CATEORY_KPI_PROCESSOR_TYPE = 33;
        public static final int CATEGORY_PRE_PROCESS_UNIT_INFO_TYPE = 34;
		public static final int CATEORY_PPU_EVALUATION_TYPE = 34;
    }

    public static class PromotionType {

        public static final Long OCS_BEHAVIOUR = 1l;
        public static final Long NOTIFY_TRIGGER = 2l;
        public static final Long SCENARIO_TRIGGER = 3l;
    }

    public static class NotifyTrigger {

        public static final Long DATA_TYPE = 4L;
    }

    public static class CreateRequest {

        public static final String OPERATOR_ADD = "4";
        public static final String OPERATOR_REMOVE = "5";
    }

    public static class SpringProperty {

        public static final String UPLOAD_PATH = "upload_path";
        public static final String ALLOW_TIME_REQUEST = "allow_time_request";

        public static final String SEGMENT_PATH = "hdfs_path.segment.path";
        public static final String SEGMENT_TEMP_PATH = "hdfs_path.segment.temp";
        public static final String SEGMENT_HIST_PATH = "hdfs_path.segment.hist";
        public static final String SEGMENT_FAILED_PATH = "hdfs_path.segment.failed";

        public static final String BLACKLIST_PATH = "hdfs_path.blacklist.path";
        public static final String BLACKLIST_TEMP_PATH = "hdfs_path.blacklist.temp";
        public static final String BLACKLIST_HIST_PATH = "hdfs_path.blacklist.hist";
        public static final String BLACKLIST_FAILED_PATH = "hdfs_path.blacklist.failed";

        public static final String SPECIAL_OFFER_PATH = "hdfs_path.specialOffer.path";
        public static final String SPECIAL_OFFER_TEMP_PATH = "hdfs_path.specialOffer.temp";
        public static final String SPECIAL_OFFER_HIST_PATH = "hdfs_path.specialOffer.hist";
        public static final String SPECIAL_OFFER_FAILED_PATH = "hdfs_path.specialOffer.failed";
    }
}
