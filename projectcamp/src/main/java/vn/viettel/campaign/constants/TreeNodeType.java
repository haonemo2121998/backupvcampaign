package vn.viettel.campaign.constants;

public enum TreeNodeType {
    category, document, rule, result, condition, column
}