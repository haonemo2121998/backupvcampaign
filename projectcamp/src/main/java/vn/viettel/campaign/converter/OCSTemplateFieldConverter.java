package vn.viettel.campaign.converter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;
import vn.viettel.campaign.entities.CdrService;
import vn.viettel.campaign.entities.OcsBehaviourTemplate;
import vn.viettel.campaign.entities.OcsBehaviourTemplateFields;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author truongbx
 * @date 9/2/2019
 */
@FacesConverter(value = "oCSTemplateFieldConverter")
public class OCSTemplateFieldConverter implements Converter {
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
		DualListModel<OcsBehaviourTemplateFields> model = (DualListModel<OcsBehaviourTemplateFields>) ((PickList) uiComponent).getValue();
		for (OcsBehaviourTemplateFields ocsBehaviourTemplate : model.getSource()) {
			if (ocsBehaviourTemplate.getCraFieldId() == Long.parseLong(s)) {
				return ocsBehaviourTemplate;
			}
		}
		for (OcsBehaviourTemplateFields ocsBehaviourTemplate : model.getTarget()) {
			if (ocsBehaviourTemplate.getCraFieldId() == Long.parseLong(s)) {
				return ocsBehaviourTemplate;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
		return ((OcsBehaviourTemplateFields) o).getCraFieldId()+"";
	}
}
