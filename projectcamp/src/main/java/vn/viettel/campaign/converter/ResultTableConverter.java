package vn.viettel.campaign.converter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;
import vn.viettel.campaign.entities.CdrService;
import vn.viettel.campaign.entities.ResultTable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author truongbx
 * @date 9/2/2019
 */
@FacesConverter(value = "resultTableConverter")
public class ResultTableConverter implements Converter {
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
		DualListModel<ResultTable> model = (DualListModel<ResultTable>) ((PickList) uiComponent).getValue();
		for (ResultTable resultTable : model.getSource()) {
			if (resultTable.getResultTableId() == Long.parseLong(s)) {
				return resultTable;
			}
		}
		for (ResultTable resultTable : model.getTarget()) {
			if (resultTable.getResultTableId() == Long.parseLong(s)) {
				return resultTable;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
		return ((ResultTable) o).getResultTableId()+"";
	}
}
