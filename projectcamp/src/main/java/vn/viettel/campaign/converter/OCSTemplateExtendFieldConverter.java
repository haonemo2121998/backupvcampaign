package vn.viettel.campaign.converter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;
import vn.viettel.campaign.entities.OcsBehaviourTemplateExtendFields;
import vn.viettel.campaign.entities.OcsBehaviourTemplateFields;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author truongbx
 * @date 9/2/2019
 */
@FacesConverter(value = "oCSTemplateExtendFieldConverter")
public class OCSTemplateExtendFieldConverter implements Converter {
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
		DualListModel<OcsBehaviourTemplateExtendFields> model = (DualListModel<OcsBehaviourTemplateExtendFields>) ((PickList) uiComponent).getValue();
		for (OcsBehaviourTemplateExtendFields ocsBehaviourTemplateExtendFields : model.getSource()) {
			if (ocsBehaviourTemplateExtendFields.getExtendFieldId() == Long.parseLong(s)) {
				return ocsBehaviourTemplateExtendFields;
			}
		}
		for (OcsBehaviourTemplateExtendFields ocsBehaviourTemplateExtendFields : model.getTarget()) {
			if (ocsBehaviourTemplateExtendFields.getExtendFieldId() == Long.parseLong(s)) {
				return ocsBehaviourTemplateExtendFields;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
		return ((OcsBehaviourTemplateExtendFields) o).getExtendFieldId()+"";
	}
}
