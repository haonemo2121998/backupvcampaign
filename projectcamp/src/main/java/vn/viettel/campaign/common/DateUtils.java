/**
 *
 */
package vn.viettel.campaign.common;

import org.apache.log4j.Logger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;

public class DateUtils {

    private static final Logger LOG = Logger.getLogger(DateUtils.class);
    public static final String SECOND_REGEX = "((([0-9]|[0-5][0-9])(-([0-9]|[0-5][0-9]))?,)*([0-9]|[0-5][0-9])(-([0-9]|[0-5][0-9]))?)"
            + "|(([0-9]|[0-5][0-9])/([0-9]|[0-5][0-9]))"
            + "|([\\?])"
            + "|([\\*])";

    public static final String MINUTE_REGEX = "((([0-9]|[0-5][0-9])(-([0-9]|[0-5][0-9]))?,)*([0-9]|[0-5][0-9])(-([0-9]|[0-5][0-9]))?)"
            + "|(([0-9]|[0-5][0-9])/([0-9]|[0-5][0-9]))"
            + "|([\\?])"
            + "|([\\*])";
    
    public static final String MINUTE_REGEX_CAMPAIGN = "((([0-9]|[0-5][0-9])(-([0-9]|[0-5][0-9]))?,)*([0-9]|[0-5][0-9])(-([0-9]|[0-5][0-9]))?)"
            + "|(([0-9]|[0-5][0-9])/([0-9]|[0-5][0-9]))"
            + "|([\\?])"
            + "|([\\*])";

    public static final String HOUR_REGEX = "((([0-9]|[0-1][0-9]|[2][0-3])(-([0-9]|[0-1][0-9]|[2][0-3]))?,)*([0-9]|[0-1][0-9]|[2][0-3])(-([0-9]|[0-1][0-9]|[2][0-3]))?)"
            + "|(([0-9]|[0-1][0-9]|[2][0-3])/([0-9]|[0-1][0-9]|[2][0-3]))"
            + "|([\\?])"
            + "|([\\*])";

    public static final String DAY_OF_MONTH_REGEX = "((([1-9]|[0][1-9]|[1-2][0-9]|[3][0-1])(-([1-9]|[0][1-9]|[1-2][0-9]|[3][0-1]))?,)*([1-9]|[0][1-9]|[1-2][0-9]|[3][0-1])(-([1-9]|[0][1-9]|[1-2][0-9]|[3][0-1]))?(C)?)"
            + "|(([1-9]|[0][1-9]|[1-2][0-9]|[3][0-1])/([1-9]|[0][1-9]|[1-2][0-9]|[3][0-1])(C)?)"
            + "|(L(-[0-9])?)"
            + "|(L(-[1-2][0-9])?)"
            + "|(L(-[3][0-1])?)"
            + "|(LW)"
            + "|([1-9]W)"
            + "|([1-3][0-9]W)"
            + "|([\\?])"
            + "|([\\*])";

    public static final String MONTH_REGEX = "((([1-9]|0[1-9]|1[0-2])(-([1-9]|0[1-9]|1[0-2]))?,)*([1-9]|0[1-9]|1[0-2])(-([1-9]|0[1-9]|1[0-2]))?)"
            + "|(([1-9]|0[1-9]|1[0-2])/([1-9]|0[1-9]|1[0-2]))"
            + "|(((JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(-(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?,)*(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(-(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?)"
            + "|((JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)/(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))"
            + "|([\\?])"
            + "|([\\*])";

    public static final String DAY_OF_WEEK_REGEX = "(([1-7](-([1-7]))?,)*([1-7])(-([1-7]))?)"
            + "|([1-7]/([1-7]))"
            + "|(((MON|TUE|WED|THU|FRI|SAT|SUN)(-(MON|TUE|WED|THU|FRI|SAT|SUN))?,)*(MON|TUE|WED|THU|FRI|SAT|SUN)(-(MON|TUE|WED|THU|FRI|SAT|SUN))?(C)?)"
            + "|((MON|TUE|WED|THU|FRI|SAT|SUN)/(MON|TUE|WED|THU|FRI|SAT|SUN)(C)?)"
            + "|(([1-7]|(MON|TUE|WED|THU|FRI|SAT|SUN))(L|LW)?)"
            + "|(([1-7]|MON|TUE|WED|THU|FRI|SAT|SUN)#([1-7])?)"
            + "|([\\?])"
            + "|([\\*])";

    public static final String YEAR_REGEX = "([\\*])?"
            + "|((19[7-9][0-9])|(20[0-9][0-9]))?"
            + "|(((19[7-9][0-9])|(20[0-9][0-9]))/((19[7-9][0-9])|(20[0-9][0-9])))?"
            + "|((((19[7-9][0-9])|(20[0-9][0-9]))(-((19[7-9][0-9])|(20[0-9][0-9])))?,)*((19[7-9][0-9])|(20[0-9][0-9]))(-((19[7-9][0-9])|(20[0-9][0-9])))?)?";
    public static final String DEFAULT_TIME_ZONE = "GMT+07:00";
    public static final String DEFAULT_DATE_FORMAT_PATTERN = "yyyyMMdd";
    public static final String ISO_DATE_TIME_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String SOLR_ISO_DATE_TIME_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String YYYYMMDDTHHMMSSSXXX = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    public static final String VN_DATE_FORMAT_FULL = "dd/MM/yyyy HH:mm:ss";
    public static final String ISO_TIME_FORMAT_PATTERN = "HH:mm:ss";
    public static final String ISO_DATE_FORMAT_PATTERN = "yyyy-MM-dd";
    public static final String TMS_DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String COUNTDOWN_DATE_FORMAT_PATTERN = "yyyy/MM/dd";
    public static final String SHORT_DATE_TIME_FORMAT_PATTERN = "yyyy/MM/dd HH:mm";
    public static final String SHORT_DATE_TIME_FORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public static final String TIME_FORMAT_PATTERN = "HHmmss";
    public static final String VN_DATE_FORMAT_PATTERN = "dd/MM/yyyy";
    public static final String COUNTDOWN_DATETIME_FORMAT_PATTERN = "yyyy/MM/dd HH:mm:ss";
    public static final String COOKIE_DATE_TIME_FORMAT_PATTERN = "ddMMyyyyyHHmmss";
    public static final String YYMMDD_FORMAT_PATTERN = "ddMMyyyy";
    public static final String EEEE_FORMAT_PATTERN = "EEEE";
    public static final String LEAD_TIME_FORMAT_PATTERN = "HH:mm";
    public static final String DATETIME_PPU_PATTERN = "yyyy/MM/dd/HH/mm/ss";
    public static final String DATETIME_PPU_SRING = "yyyy/MM/dd/HH/mm/ss";
    public static final String DATETIME_PPU_ZONE = "yyyy%MM%dd%HH%mm%ss";
    public static String formatDatetoString(final Date date, final String pattern) {
        String res = "";
        try {
            if (date != null && StringUtils.isNotEmpty(pattern)) {
                res = new SimpleDateFormat(pattern).format(date);
            }
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return res;
    }
    public static Date stringToDate(final String str, final String pattern) {
        if (str == null || pattern == null) {
            return null;
        }
        try {
            return new SimpleDateFormat(pattern).parse(str);
        } catch (final ParseException e) {
            LOG.debug(e.getMessage(), e);
            return null;
        }
    }
    public static String dateToYYYYMMDDStr(final Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(DEFAULT_DATE_FORMAT_PATTERN).format(date);
    }

    public static String dateToYYYYMMDDHHMMStr(final Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(SHORT_DATE_TIME_FORMAT_PATTERN).format(date);
    }
    
     public static String dateToYYYYMMDDHHMMSSStr(final Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(SHORT_DATE_TIME_FORMAT_YYYYMMDDHHMMSS).format(date);
    }

    public static String getTimeFrom(final Date date) {
        return new SimpleDateFormat(TIME_FORMAT_PATTERN).format(date);
    }

    public static Date parseDOBDate(final String dateStr) {
        return parseDateTime(dateStr, DEFAULT_DATE_FORMAT_PATTERN);
    }

    public static Date parseDateIso(final String dateStr) {
        return parseDateTime(dateStr, ISO_DATE_FORMAT_PATTERN);
    }

    public static Date parseDateTimeIso(final String dateStr) {
        return parseDateTime(dateStr, ISO_DATE_TIME_FORMAT_PATTERN);
    }

    private static Date parseDateTime(final String dateStr, final String pattern) {
        try {
            if (StringUtils.isEmpty(dateStr) || StringUtils.isEmpty(pattern)) {
                return null;
            }
            return new SimpleDateFormat(pattern).parse(dateStr);
        } catch (final ParseException e) {
            LOG.error(e.getMessage(), e);
            return null;
        }
    }

    public static String formatDateIso(final Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(ISO_DATE_FORMAT_PATTERN).format(date);
    }

    public static String formatDateToString(final Date date) {
        if (date == null) {
            return "";
        }
        return new SimpleDateFormat(VN_DATE_FORMAT_PATTERN).format(date);
    }

    public static String formatTimeIso(final Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(ISO_TIME_FORMAT_PATTERN).format(date);
    }

    public static String formatLeadTimeDetail(final Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(LEAD_TIME_FORMAT_PATTERN).format(date);
    }

    public static TimeZone getTimeZoneForLeadTimeDetail() {
        return new SimpleDateFormat(LEAD_TIME_FORMAT_PATTERN).getTimeZone();
    }

    public static String formatDateTimeIso(final Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(ISO_DATE_TIME_FORMAT_PATTERN).format(date);
    }

    public static Date stringToDate(final String str, final SimpleDateFormat format) {
        if (str == null || format == null) {
            return null;
        }
        try {
            return format.parse(str);
        } catch (final ParseException e) {
            LOG.debug(e.getMessage(), e);
            return null;
        }
    }

    public static Date stringToCurrentDateWithIsoTime(final String isoTime) {
        if (StringUtils.isBlank(isoTime)) {
            return null;
        }

        try {
            final Date dateTime = new SimpleDateFormat(ISO_TIME_FORMAT_PATTERN).parse(isoTime);
            final Calendar isoTimeCal = Calendar.getInstance();
            isoTimeCal.setTime(dateTime);
            final int currentYear = Calendar.getInstance().get(Calendar.YEAR);
            isoTimeCal.set(Calendar.YEAR, currentYear);
            return isoTimeCal.getTime();
        } catch (final ParseException e) {
            LOG.debug(e.getMessage(), e);
            return null;
        }
    }

    public static int compareDate(final Date d1, final Date d2) {
        if (d1 == null || d2 == null) {
            throw new IllegalArgumentException("Date can not be null!");
        }

        final Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);

        final Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d2);
        cal2.set(Calendar.HOUR_OF_DAY, 0);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);

        return cal1.equals(cal2) ? 0 : cal1.after(cal2) ? 1 : -1;
    }

    public static Date createDateFrom(final Date d1, final Date d2, int days) {
        if (d1 == null || d2 == null) {
            throw new IllegalArgumentException("Date can not be null!");
        }

        final Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);
        cal1.add(Calendar.DATE, days);
        cal1.set(Calendar.HOUR_OF_DAY, d2.getHours());
        cal1.set(Calendar.MINUTE, d2.getMinutes());
        cal1.set(Calendar.SECOND, d2.getSeconds());
        cal1.set(Calendar.MILLISECOND, 0);

        return cal1.getTime();
    }

    public static int compareDate(final Date d1, final Date d2, final Date compare) {
        if (d1 == null || d2 == null || compare == null) {
            throw new IllegalArgumentException("Date can not be null!");
        }

        final Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);

        final Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d2);
        cal2.set(Calendar.HOUR_OF_DAY, 0);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);

        final Calendar cal3 = Calendar.getInstance();
        cal3.setTime(compare);
        cal3.set(Calendar.HOUR_OF_DAY, 0);
        cal3.set(Calendar.MINUTE, 0);
        cal3.set(Calendar.SECOND, 0);
        cal3.set(Calendar.MILLISECOND, 0);

        return cal3.equals(cal1) || cal3.equals(cal2) ? 0 : cal3.after(cal1) && cal3.before(cal2) ? 1 : -1;
    }

    public static DateTimeData compareDate(final Calendar openDateFrom, final Calendar openDateTo, final DateTimeData offDays) {

        DateTimeData dateTimeDataWorkingFinal = new DateTimeData();

        boolean isBeginClosed = false;
        boolean isEndClosed = false;
        if (openDateFrom.equals(offDays.getFromDate()) || openDateFrom.equals(offDays.getToDate()) || (openDateFrom.after(offDays.getFromDate()) && openDateFrom.before(offDays.getToDate()))) {
            isBeginClosed = true;
        }

        if (openDateTo.equals(offDays.getToDate()) || openDateTo.equals(offDays.getToDate()) || (openDateTo.after(offDays.getFromDate()) && openDateTo.before(offDays.getToDate()))) {
            isEndClosed = true;
        }

        if (isBeginClosed && isEndClosed) {
            return null;
        }

        if (!isBeginClosed) {
            dateTimeDataWorkingFinal.setFromDate(openDateFrom);
        } else {
            dateTimeDataWorkingFinal.setFromDate(openDateFrom.before(offDays.getToDate()) ? offDays.getToDate() : openDateFrom);
        }

        if (!isEndClosed) {
            dateTimeDataWorkingFinal.setToDate(openDateTo);
        } else {
            dateTimeDataWorkingFinal.setToDate(openDateTo.after(offDays.getFromDate()) ? offDays.getFromDate() : openDateTo);
        }

        return dateTimeDataWorkingFinal;
    }

    public static int compareDateTime(final Date d1, final Date d2, final Date t1, final Date t2, final Date startCompare, final Date endCompare) {
        if (d1 == null || d2 == null || startCompare == null) {
            throw new IllegalArgumentException("Date can not be null!");
        }

        int result = 0;

        final Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);
        cal1.set(Calendar.HOUR_OF_DAY, t1.getHours());
        cal1.set(Calendar.MINUTE, t1.getMinutes());
        cal1.set(Calendar.SECOND, t1.getSeconds());
        cal1.set(Calendar.MILLISECOND, 0);

        final Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d2);
        cal2.set(Calendar.HOUR_OF_DAY, t2.getHours());
        cal2.set(Calendar.MINUTE, t2.getMinutes());
        cal2.set(Calendar.SECOND, t2.getSeconds());
        cal2.set(Calendar.MILLISECOND, 0);

        final Calendar startCal = Calendar.getInstance();
        startCal.setTime(startCompare);
        startCal.set(Calendar.MILLISECOND, 0);

        final Calendar endCal = Calendar.getInstance();
        endCal.setTime(endCompare);
        endCal.set(Calendar.MILLISECOND, 0);

        if ((startCal.equals(cal1) || startCal.equals(cal2) || (startCal.after(cal1) && startCal.before(cal2)))
                && (endCal.equals(cal1) || endCal.equals(cal2) || (endCal.after(cal1) && endCal.before(cal2)))) {
            result = 1;
        }
        return result;
    }

    public static int compareTime(final Date beginTime, final Date endTime, final Date compareTime) {
        Calendar beginCal = Calendar.getInstance();
        beginCal.setTime(compareTime);
        beginCal.set(Calendar.HOUR_OF_DAY, beginTime.getHours());
        beginCal.set(Calendar.MINUTE, beginTime.getMinutes());
        beginCal.set(Calendar.SECOND, beginTime.getSeconds());
        beginCal.set(Calendar.MILLISECOND, 0);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(compareTime);
        endCal.set(Calendar.HOUR_OF_DAY, endTime.getHours());
        endCal.set(Calendar.MINUTE, endTime.getMinutes());
        endCal.set(Calendar.SECOND, endTime.getSeconds());
        endCal.set(Calendar.MILLISECOND, 0);

        Calendar compareCal = Calendar.getInstance();
        compareCal.setTime(compareTime);

        return (compareCal.after(beginCal) && compareCal.before(endCal)) || compareCal.equals(beginCal) || compareCal.equals(endCal) ? 1 : 0;
    }

    public static int compareTime2(final Date beginTime, final Date endTime, final Date compareTime) {
        Calendar beginCal = Calendar.getInstance();
        beginCal.setTime(beginTime);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endTime);

        Calendar compareCal = Calendar.getInstance();
        compareCal.setTime(compareTime);

        int i;

        if (compareCal.before(beginCal)) {
            i = -1;
        } else if (compareCal.equals(beginCal) || compareCal.equals(endCal)) {
            i = 0;
        } else if (compareCal.after(beginCal) && compareCal.before(endCal)) {
            i = 1;
        } else {
            i = 2;
        }

        return i;
    }

    public static int compareDateTime(final Date beginTime, final Date endTime, final Date compareTime) {
        Calendar beginCal = Calendar.getInstance();
        beginCal.setTime(beginTime);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endTime);

        Calendar compareCal = Calendar.getInstance();
        compareCal.setTime(compareTime);

        return (compareCal.after(beginCal) && compareCal.before(endCal)) || compareCal.equals(beginCal) || compareCal.equals(endCal) ? 1 : 0;
    }

    public static int compareTime3(final Date beginTime, final Date compareTime) {
        Calendar beginCal = Calendar.getInstance();
        beginCal.setTime(beginTime);

        Calendar compareCal = Calendar.getInstance();
        compareCal.setTime(compareTime);

        return compareCal.before(beginCal) ? -1 : compareCal.equals(beginCal) ? 0 : 1;
    }

    public static int compareTime(final Date beginTime, final Date endTime, final Date startConfirmTime, final Date endConfirmTime) {
        int result = 0;
        Calendar beginCal = Calendar.getInstance();
        beginCal.setTime(startConfirmTime);
        beginCal.set(Calendar.HOUR_OF_DAY, beginTime.getHours());
        beginCal.set(Calendar.MINUTE, beginTime.getMinutes());
        beginCal.set(Calendar.SECOND, beginTime.getSeconds());
        beginCal.set(Calendar.MILLISECOND, 0);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(startConfirmTime);
        endCal.set(Calendar.HOUR_OF_DAY, endTime.getHours());
        endCal.set(Calendar.MINUTE, endTime.getMinutes());
        endCal.set(Calendar.SECOND, endTime.getSeconds());
        endCal.set(Calendar.MILLISECOND, 0);

        Calendar startConfirmCal = Calendar.getInstance();
        startConfirmCal.setTime(startConfirmTime);

        Calendar endConfrimCal = Calendar.getInstance();
        endConfrimCal.setTime(endConfirmTime);

        if (beginCal.equals(startConfirmCal) || (beginCal.after(startConfirmCal) && beginCal.before(endConfirmTime))
                || (endCal.equals(startConfirmCal) || endCal.after(startConfirmCal) && endCal.before(endConfirmTime))) {
            result = 1;
        }
        return result;
    }

    public static int compareTime(final Calendar beginTime, final Calendar endTime, final Date startConfirmTime, final Date endConfirmTime) {
        int result = 0;
        Calendar startConfirmCal = Calendar.getInstance();
        startConfirmCal.setTime(startConfirmTime);

        Calendar endConfrimCal = Calendar.getInstance();
        endConfrimCal.setTime(endConfirmTime);

        if (startConfirmCal.equals(beginTime) || (startConfirmCal.after(beginTime) && startConfirmCal.before(endTime))
                || (endConfrimCal.after(beginTime) && endConfrimCal.before(endTime))) {
            result = 1;
        }

        if (beginTime.equals(startConfirmCal) || (beginTime.after(startConfirmCal) && beginTime.before(endConfrimCal))
                || (endTime.after(startConfirmCal) && endTime.before(endConfrimCal))) {
            return 1;
        }

        return result;
    }

    public static int compareTime(final Date beginTime, final Date endTime, final Date compareTime, Boolean isSameDay) {
        Calendar beginCal = Calendar.getInstance();
        beginCal.setTime(compareTime);
        beginCal.set(Calendar.HOUR_OF_DAY, beginTime.getHours());
        beginCal.set(Calendar.MINUTE, beginTime.getMinutes());
        beginCal.set(Calendar.SECOND, beginTime.getSeconds());
        beginCal.set(Calendar.MILLISECOND, 0);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(compareTime);
        endCal.set(Calendar.HOUR_OF_DAY, endTime.getHours());
        endCal.set(Calendar.MINUTE, endTime.getMinutes());
        endCal.set(Calendar.SECOND, endTime.getSeconds());
        endCal.set(Calendar.MILLISECOND, 0);

        Calendar compareCal = Calendar.getInstance();
        compareCal.setTime(compareTime);

        if (isSameDay != null && !isSameDay) {
            if (compareCal.after(endCal)) {
                endCal.add(Calendar.DATE, 1);
            } else {
                beginCal.add(Calendar.DATE, -1);
            }
        }

        return (compareCal.after(beginCal) && compareCal.before(endCal)) || compareCal.equals(beginCal) || compareCal.equals(endCal) ? 1 : 0;
    }

    public static Date setTime(final Date date, final Date time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, time.getHours());
        calendar.set(Calendar.MINUTE, time.getMinutes());
        calendar.set(Calendar.SECOND, time.getSeconds());
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Calendar setTimeCal(final Date date, final Date time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, time.getHours());
        calendar.set(Calendar.MINUTE, time.getMinutes());
        calendar.set(Calendar.SECOND, time.getSeconds());
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    public static Date setTime(final Date date, final Date time, boolean isSameDay) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, time.getHours());
        calendar.set(Calendar.MINUTE, time.getMinutes());
        calendar.set(Calendar.SECOND, time.getSeconds());
        calendar.set(Calendar.MILLISECOND, 0);
        if (!isSameDay) {
            calendar.add(Calendar.DATE, 1);
        }
        return calendar.getTime();
    }

    public static Calendar setTime(final Calendar calendar, final Date time) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(calendar.getTime());
        cal.set(Calendar.HOUR_OF_DAY, time.getHours());
        cal.set(Calendar.MINUTE, time.getMinutes());
        cal.set(Calendar.SECOND, time.getSeconds());
        cal.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    /**
     * check date1 after date2 X hours
     *
     * @param d1
     * @param d2
     * @param x
     * @return
     */
    public static boolean isAfterXHours(final Date d1, final Date d2, final int x) {
        if (d1 == null || d2 == null) {
            throw new IllegalArgumentException("Date can not be null!");
        }

        final Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);

        final Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d2);
        cal2.add(Calendar.HOUR_OF_DAY, x);

        return cal1.after(cal2);
    }

    public static Date stringYYYYMMDDToDate(final String dateStr) {
        try {
            if (StringUtils.isEmpty(dateStr)) {
                return null;
            }
            return new SimpleDateFormat(DEFAULT_DATE_FORMAT_PATTERN).parse(dateStr);
        } catch (final ParseException e) {
            LOG.debug(e.getMessage(), e);
            return null;
        }
    }

    public static Date stringYYYYMMDDHHMMToDate(final String dateStr) {
        try {
            if (StringUtils.isEmpty(dateStr)) {
                return null;
            }
            return new SimpleDateFormat(SHORT_DATE_TIME_FORMAT_PATTERN).parse(dateStr);
        } catch (final ParseException e) {
            LOG.debug(e.getMessage(), e);
            return null;
        }
    }

    /**
     *
     * @param dateString (yyyy-MM-dd HH:mm:ss)
     * @return date
     */
    public static Date getTmsDate(final String dateString) {
        try {
            final Date date = new SimpleDateFormat(TMS_DATE_FORMAT_PATTERN).parse(dateString);
            return date;
        } catch (final Exception e) {
            LOG.debug(e.getMessage(), e);
            return null;
        }
    }

    /**
     * @param pattern
     * @param dateString
     * @return Date
     */
    public static Date formatDateByPattern(final String pattern, final String dateString) {
        try {
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            final Date date = simpleDateFormat.parse(dateString);
            return date;
        } catch (final Exception e) {
            LOG.debug(e.getMessage(), e);
            return null;
        }
    }

    public static String convertDateToStringYYYYMMDD(final Date indate) {
        String dateString = null;
        if (indate == null) {
            return dateString;
        }
        try {
            dateString = new SimpleDateFormat(COUNTDOWN_DATE_FORMAT_PATTERN).format(indate);
        } catch (final Exception e) {
            LOG.debug(e.getMessage(), e);
        }
        return dateString;
    }

    public static String getDateString(final Date date, final String pattern) {
        if (date == null) {
            return null;
        }
        try {
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            return simpleDateFormat.format(date);
        } catch (final Exception e) {
            LOG.debug(e.getMessage(), e);
        }
        return null;
    }

    public static Date parseStrToISODate(final String date) throws ParseException {
        return new SimpleDateFormat(ISO_DATE_FORMAT_PATTERN).parse(date);
    }

    public static Date setTimeIntoDate(final Date indate, final String time) {
        String dateString = null;
        if (indate == null || StringUtils.isEmpty(time)) {
            return indate;
        }
        try {
            dateString = new SimpleDateFormat(COUNTDOWN_DATE_FORMAT_PATTERN).format(indate) + " " + time.trim();
            return new SimpleDateFormat(SHORT_DATE_TIME_FORMAT_PATTERN).parse(dateString);
        } catch (final Exception e) {
            LOG.debug(e.getMessage(), e);
        }
        return null;
    }

    public static String getTime(final Date indate) {
        if (indate == null) {
            return null;
        }

        return new SimpleDateFormat("HH:mm").format(indate);

    }

    public static TimeDiffirence getDateStringDistance(final Date dateStart, final Date dateStop) {
        try {
            //in milliseconds
            final long diff = dateStop.getTime() - dateStart.getTime();

            if (diff <= 0) {
                final TimeDiffirence tf = new TimeDiffirence();
                tf.setDay(0l);
                tf.setHour(0l);
                tf.setMin(0l);
                tf.setSecond(0l);
                return tf;
            }

            final long diffSeconds = diff / 1000 % 60;
            final long diffMinutes = diff / (60 * 1000) % 60;
            final long diffHours = diff / (60 * 60 * 1000) % 24;
            final long diffDays = diff / (24 * 60 * 60 * 1000);

            final TimeDiffirence tf = new TimeDiffirence();
            tf.setDay(diffDays);
            tf.setHour(diffHours);
            tf.setMin(diffMinutes);
            tf.setSecond(diffSeconds);

            return tf;

        } catch (final Exception e) {
            LOG.debug("there is issue ", e);
        }
        return null;
    }

    /**
     * startOfToday
     *
     * @return
     */
    public static Date startOfToday() {
        final Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR_OF_DAY, 0);
        return now.getTime();
    }

    public static Date dateAfter(final Date date, final int number) {
        final Calendar now = Calendar.getInstance();
        now.setTime(date);
        now.add(Calendar.DATE, number);
        return now.getTime();
    }

    /*
     * Get today at 0hh:0mm:0ss
     *
     * @param date
     *
     * @return start of date
     */
    public static Date startOfDate(final Date date) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Date(cal.getTimeInMillis());
    }

    /**
     * Get start next date of "from"
     *
     * @param from
     * @param addDay > 0 will be in future, < 0 will be in last @return Date
     */
    public static Date startOfNextDate(final Date from, final int addDay) {
        final Calendar cal = Calendar.getInstance();
        final Date start = addDays(from, addDay); // add 1 more day
        cal.setTime(start);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Date(cal.getTimeInMillis());
    }

    /**
     * @param date
     * @param days
     * @return Date
     */
    public static Date addDays(final Date date, final int days) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    public static Date addHours(final Date date, final int hours) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR_OF_DAY, hours); //minus number would decrement the days
        return cal.getTime();
    }

    public static Date addMinutes(final Date date, final int minutes) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes); //minus number would decrement the days
        return cal.getTime();
    }

    /**
     * convert delivery time from date and time string
     *
     * @param deliveryDate
     * @param deliveryTime
     * @return date object
     */
    public static Date convertDeliveryTime(final Date deliveryDate, final String deliveryTime) {
        final StringBuilder deliveryDateFrom = new StringBuilder();
        deliveryDateFrom.append(convertDateToStringYYYYMMDD(deliveryDate));
        deliveryDateFrom.append(" ");
        deliveryDateFrom.append(deliveryTime);
        return DateUtils.formatDateByPattern("yyyy/MM/dd HH:mm", deliveryDateFrom.toString());
    }

    /**
     *
     * @param onlineTo
     * @param configExpirationDate
     * @return
     */
    public static Date getVoucherExpirationDate(final Date onlineTo, final Date configExpirationDate) {
        Date res = null;
        if (onlineTo != null && configExpirationDate != null) {
            if (DateUtils.compareDate(onlineTo, configExpirationDate) < 0) {
                res = onlineTo;
            } else {
                res = configExpirationDate;
            }
        } else if (onlineTo == null && configExpirationDate != null) {
            res = configExpirationDate;
        } else if (onlineTo != null && configExpirationDate == null) {
            res = onlineTo;
        }
        return res;
    }

    public static Date getUTCDate(Date utcDate) {
        if (utcDate == null) {
            return null;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(TMS_DATE_FORMAT_PATTERN);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String utcDateStr = sdf.format(utcDate);

        return formatDateByPattern(TMS_DATE_FORMAT_PATTERN, utcDateStr);
    }

    public static int diffDays(Date start, Date end) {
        final long diff = (end != null ? end.getTime() : 0) - (start != null ? start.getTime() : 0);
        final long diffDays = diff / (24 * 60 * 60 * 1000);
        return (int) diffDays;
    }

    public static int diffDays2(Date start, Date end) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(start);
        c1.set(Calendar.HOUR_OF_DAY, 0);
        c1.set(Calendar.MINUTE, 0);
        c1.set(Calendar.SECOND, 0);
        c1.set(Calendar.MILLISECOND, 0);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(end);
        c2.set(Calendar.HOUR_OF_DAY, 0);
        c2.set(Calendar.MINUTE, 0);
        c2.set(Calendar.SECOND, 0);
        c2.set(Calendar.MILLISECOND, 0);

        Date d1 = c1.getTime();
        Date d2 = c2.getTime();

        long diff = d2.getTime() - d1.getTime();
        final long diffDays = diff / (24 * 60 * 60 * 1000);
        return (int) diffDays;
    }

    /**
     * Set the time of the given Date
     *
     * @param date
     * @param hourOfDay
     * @param minute
     * @param second
     *
     * @return new instance of java.util.Date with the time set
     */
    public static Date setTime(final Date date, final int hourOfDay, final int minute, final int second) {
        final GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        gc.set(Calendar.HOUR_OF_DAY, hourOfDay);
        gc.set(Calendar.MINUTE, minute);
        gc.set(Calendar.SECOND, second);
        return gc.getTime();
    }

    public static Date parseDateTimeFromCookie(final String dateStr) {
        return parseDateTime(dateStr, COOKIE_DATE_TIME_FORMAT_PATTERN);
    }

    public static Date setTime(final Date date, String timeString) {
        List<Integer> times = Arrays.stream(timeString.split(":")).map(Integer::parseInt).collect(Collectors.toList());
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, times.get(0));
        cal.set(Calendar.MINUTE, times.get(1));
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    public final static Long getLongTimeWithoutMillisecond(final Date date) {
        if (Objects.isNull(date)) {
            return null;
        }
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    public final static Long getLongDateWithoutTime(final Date date) {
        if (Objects.isNull(date)) {
            return null;
        }
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return getLongDateWithoutTime(cal);
    }

    public final static Long getLongDateWithoutTime(final Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    public final static Long getLongTimeWithoutMillisecond(final Calendar cal) {
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    public final static Date parseVINDate(final String inputDateStr) {
        if (StringUtils.isEmpty(inputDateStr)) {
            return null;
        }
        try {
            final SimpleDateFormat formatter = new SimpleDateFormat(VN_DATE_FORMAT_PATTERN);
            formatter.setLenient(false);
            final Date date = formatter.parse(inputDateStr);
            return date;
        } catch (final Exception ex) {
            return null;
        }
    }

    public static String getWeekDay(final Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(EEEE_FORMAT_PATTERN).format(date).toUpperCase();
    }

    public static Date getDateWithoutTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getDateWithEndOfDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getDateTimeIncludeHour(Date date, Integer hour, Integer minute) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getCurrentDate() {
        return Calendar.getInstance().getTime();
    }

    public static boolean compareDateEquasSmall(final Date date1, final Date date2) {
        // i < 0 nghĩa là date1 < date2
        // i = 0 nghĩa là date1 = date2

        if (date1.compareTo(date2) < 0 || (date1.compareTo(date2)) == 0) {
            return true;
        }
        return false;
    }

    public static boolean compareDateEquasBig(final Date date1, final Date date2) {
        // i > 0 nghĩa là date1 > date2
        // i = 0 nghĩa là date1 = date2

        if (date1.compareTo(date2) > 0 || (date1.compareTo(date2)) == 0) {
            return true;
        }
        return false;
    }

    public static Date addSecond(final Date date, final int second) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.SECOND, second); //second number would decrement the days
        return cal.getTime();
    }

    public static void main(String[] args) {
        String year = "13";
        String year2 = "JAN";
        String year3 = "fđf";
        System.out.println(year.matches(MONTH_REGEX));
        System.out.println(year2.matches(MONTH_REGEX));
        System.out.println(year3.matches(MONTH_REGEX));
    }
}
