package vn.viettel.campaign.common;


import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.component.selectonemenu.SelectOneMenu;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import java.util.List;

public class UIDisablePanel {
    /**
     * Disable all the children components
     *
     * @param uiComponentName
     */
    public static void disableUIComponent(String uiComponentName, boolean disable) {
        UIComponent component = FacesContext.getCurrentInstance()
                .getViewRoot().findComponent(uiComponentName);
        if (component != null) {
            disableAll(component, disable);
        }
    }


    private static void disableAll(UIComponent root, boolean disable) {
        List<UIComponent> components = root.getChildren();
        for (UIComponent component : components) {
//            logger.info(component.getClass().getTypeName());
            if (component instanceof DataTable) {
                System.out.println("table");
                disableAll(component, disable);
            }
            if (component instanceof InputText) {
                ((InputText) component).setDisabled(disable);

            } else if (component instanceof InputNumber) {
                ((InputNumber) component).setDisabled(disable);

            } else if (component instanceof InputTextarea) {
                ((InputTextarea) component).setDisabled(disable);

            } else if (component instanceof HtmlInputText) {
                ((HtmlInputText) component).setDisabled(disable);

            } else if (component instanceof SelectOneMenu) {
                ((SelectOneMenu) component).setDisabled(disable);

            } else if (component instanceof SelectBooleanCheckbox) {
                ((SelectBooleanCheckbox) component).setDisabled(disable);

            } else if (component instanceof CommandButton) {
                ((CommandButton) component).setDisabled(disable);
            }
            disableAll(component, disable);
        }
    }
}