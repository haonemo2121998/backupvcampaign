package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.Segment;

/**
 *
 * @author ConKC
 */
public interface SegmentDao extends BaseDAOInteface<Segment> {

    List<Segment> findSegmentByCategory(final List<Long> categoryIds);

    List<Segment> getSegmentBuildTree(Long id);

    List<Segment> findSegmentByName(final String segmentName);
    
    public Segment getSegmentByName(String segmentName);

    long onSaveSegment(Segment segment);

    boolean checkDuplicate(final String name, final Long id);

    boolean checkDuplicatePath(final String path, final Long id);
}
