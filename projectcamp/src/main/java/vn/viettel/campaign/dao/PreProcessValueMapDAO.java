/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.PreProcessValueMap;

/**
 *
 * @author admin
 */
public interface PreProcessValueMapDAO {
    void onSaveOrUpdatePreProcessValueMap(PreProcessValueMap preProcessValueMap);
    void onDeletePreProcessValueMap(PreProcessValueMap preProcessValueMap);
}
