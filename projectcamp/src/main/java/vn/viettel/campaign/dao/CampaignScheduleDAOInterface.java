package vn.viettel.campaign.dao;

import org.hibernate.Session;
import vn.viettel.campaign.entities.CampaignSchedule;
import vn.viettel.campaign.entities.CdrService;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface CampaignScheduleDAOInterface extends BaseDAOInteface<CampaignSchedule> {

    public List<CampaignSchedule> getLstCampaignSchedule(Long campaignId);

    public CampaignSchedule getCampaignScheduleByScheduleId(Long id);

    public CampaignSchedule getNextSchedule();

    void deleteCampaignScheduleByCampaignId(Session session, Long campaignId);

    public boolean checkExisScheduleName(String scheduleName);
    
    public CampaignSchedule getNextSequneceCampaignSchedule();
    
    public CampaignSchedule doCreateOrUpdateCampaignSchedule(CampaignSchedule campaignSchedule);
    
    public boolean deleteCampaignSchedule(Long id);
}
