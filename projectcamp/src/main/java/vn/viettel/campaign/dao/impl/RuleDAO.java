package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.dao.RuleDAOInterface;
import vn.viettel.campaign.dao.RuleResultTableMapInterface;
import vn.viettel.campaign.entities.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author truongbx
 * @date 8/26/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class RuleDAO extends BaseDAOImpl<Rule> implements RuleDAOInterface {
	@Autowired
	RuleResultTableMapInterface ruleResultTableMapDAO;

	@Override
	public List<Rule> getLstRuleByCampaignId(long campaignId) {
		String sql = "SELECT a.PRIORITY priority,b.RULE_ID ruleId,b.RULE_NAME ruleName, c.SEGMENT_NAME segmentName , c.SEGMENT_ID segmentId  " +
				"FROM campaign_info a " +
				"INNER JOIN RULE b " +
				"INNER JOIN segment c " +
				"on a.RULE_ID = b.RULE_ID " +
				" and a.SEGMENT_ID = c.SEGMENT_ID  " +
				" WHERE a.CAMPAIGN_ID =:campaignId";

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("priority", StandardBasicTypes.INTEGER);
		query.addScalar("ruleId", StandardBasicTypes.LONG);
		query.addScalar("segmentId", StandardBasicTypes.LONG);
		query.addScalar("ruleName", StandardBasicTypes.STRING);
		query.addScalar("segmentName", StandardBasicTypes.STRING);
		query.setResultTransformer(Transformers.aliasToBean(Rule.class));
		query.setParameter("campaignId", campaignId);
		return query.list();
	}

	@Override
	public List<Rule> getLstRuleForDefaultCampaign(long campaignId) {
		String sql = "SELECT a.PRIORITY priority,b.RULE_ID ruleId,b.RULE_NAME ruleName, 'all' segmentName , a.SEGMENT_ID segmentId  " +
				"FROM campaign_info a " +
				"INNER JOIN RULE b " +
				"on a.RULE_ID = b.RULE_ID " +
				" WHERE a.CAMPAIGN_ID =:campaignId";

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("priority", StandardBasicTypes.INTEGER);
		query.addScalar("ruleId", StandardBasicTypes.LONG);
		query.addScalar("segmentId", StandardBasicTypes.LONG);
		query.addScalar("ruleName", StandardBasicTypes.STRING);
		query.addScalar("segmentName", StandardBasicTypes.STRING);
		query.setResultTransformer(Transformers.aliasToBean(Rule.class));
		query.setParameter("campaignId", campaignId);
		return query.list();
	}

	@Override
	public boolean checkExisRuleName(String ruleName, Long ruleId) {
		List<Rule> lst = getSession().createQuery("from Rule where lower(ruleName) = :ruleName and ruleId <> :ruleId ")
				.setParameter("ruleName", ruleName.trim().toLowerCase())
				.setParameter("ruleId", ruleId)
				.list();
		if (lst != null && lst.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public Rule getNextSequense() {
		String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"RULE\"";
		SQLQuery queryUpdate = getSession().createSQLQuery(update);
		queryUpdate.executeUpdate();
		String sql = "SELECT table_id ruleId  from SEQ_TABLE WHERE TABLE_NAME = \"RULE\"";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("ruleId", StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(Rule.class));
		return (Rule) query.uniqueResult();
	}

	@Override
	public String onSaveRule(Rule rule, List<ResultTable> resultTables) {
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			save(session, rule);
			List<RuleResultTableMap> lst = new ArrayList<>();
			for (ResultTable item : resultTables) {
				RuleResultTableMap ruleResultTableMap = new RuleResultTableMap();
				ruleResultTableMap.setRuleId(rule.getRuleId());
				ruleResultTableMap.setResultTableId(item.getResultTableId());
				ruleResultTableMap.setResultTableIndex(resultTables.indexOf(item) + 1);
				lst.add(ruleResultTableMap);
			}
			String result = ruleResultTableMapDAO.save(lst);
			if (!Responses.SUCCESS.getName().equalsIgnoreCase(result)) {
				throw new Exception();
			}
			tx.commit();

		} catch (Exception ex) {
			ex.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
			return Responses.ERROR.getName();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return Responses.SUCCESS.getName();
	}

	@Override
	public boolean onDeleteRule(Long ruleId) {
		Session session = null;
		Transaction tx = null;

		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			deleteById(Rule.class, ruleId, session);
			ruleResultTableMapDAO.deleteResultTableMap(session, ruleId);
			tx.commit();

		} catch (Exception ex) {
			ex.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return true;
	}

	@Override
	public String onUpdateRule(Rule rule, List<ResultTable> resultTables) {
		Session session = null;
		Transaction tx = null;

		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			update(session, rule);

			ruleResultTableMapDAO.deleteResultTableMap(session, rule.getRuleId());
			List<RuleResultTableMap> lst = new ArrayList<>();
			for (ResultTable item : resultTables) {
				RuleResultTableMap ruleResultTableMap = new RuleResultTableMap();
				ruleResultTableMap.setRuleId(rule.getRuleId());
				ruleResultTableMap.setResultTableId(item.getResultTableId());
				ruleResultTableMap.setResultTableIndex(resultTables.indexOf(item) + 1);
				lst.add(ruleResultTableMap);
			}
			String result = ruleResultTableMapDAO.save(session, lst);
			if (!Responses.SUCCESS.getName().equalsIgnoreCase(result)) {
				throw new Exception();
			}
			tx.commit();

		} catch (Exception ex) {
			ex.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
			return Responses.ERROR.getName();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return Responses.SUCCESS.getName();
	}

	@Override
	public boolean checkRuleInUse(Long ruleId) {
		List<CampaignInfo> lstObj = getSession().createQuery("from CampaignInfo where  ruleId =:ruleId")
				.setParameter("ruleId", ruleId)
				.list();
		if (lstObj == null || lstObj.isEmpty()) {
			return false;
		}
		return true;
	}
}
