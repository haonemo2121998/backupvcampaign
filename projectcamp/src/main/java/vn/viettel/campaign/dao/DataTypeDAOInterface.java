package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.*;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface DataTypeDAOInterface extends BaseDAOInteface<DataType> {

}
