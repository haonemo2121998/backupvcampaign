package vn.viettel.campaign.dao;

import org.hibernate.Session;
import vn.viettel.campaign.entities.Invitation;
import vn.viettel.campaign.entities.InvitationPriority;
import vn.viettel.campaign.entities.NotifyTrigger;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface InvitationDAOInterface extends BaseDAOInteface<InvitationPriority>{
	List<InvitationPriority> getInvitationByCampaignId(long campaignId);
	 List<InvitationPriority> getNotifyTriggerByRuleId(long ruleId,String segmentName , long segmentId);
	 List<InvitationPriority> getInvitationForDefaultCampaign(long campaignId);
	void deleteInviByCampaignId(Session session , Long campaignId);
}
