package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.OCSBehaviourFieldDaoInterface;
import vn.viettel.campaign.dao.OCSTemplateFieldDaoInterface;
import vn.viettel.campaign.entities.OcsBehaviourFields;
import vn.viettel.campaign.entities.OcsBehaviourTemplateFields;

import javax.persistence.Query;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class OCSBehaviourFieldDAO extends BaseDAOImpl<OcsBehaviourFields> implements OCSBehaviourFieldDaoInterface {

	@Override
	public List<OcsBehaviourFields> getLstFieldOfOCSBehaviour(long ocsBehaviourId,long templateId) {
		String sql = "SELECT " +
				"a.OCS_BEHAVIOUR_ID ocsBehaviourId, " +
				"a.BEHAVIOUR_FIELD_ID behaviourFieldId, " +
				"a.CRA_FIELD_ID craFieldId, " +
				"a.DATA_TYPE dataType, " +
				"a.VALUE VALUE, " +
				"a.SOURCE_TYPE sourceType, " +
				"c.SOURCE_TYPE templatetype, " +
				"b.NAME name  " +
				"FROM " +
				"ocs_behaviour_fields a " +
				"INNER JOIN ocs_behaviour d " +
				"INNER JOIN field_id_ocs b " +
				"INNER JOIN ocs_behaviour_template_fields c " +
				"on a.CRA_FIELD_ID = c.CRA_FIELD_ID " +
				"and d.TEMPLATE_ID = c.BEHAVIOUR_TEMPLATE_ID " +
				"WHERE " +
				"a.CRA_FIELD_ID = b.FIELD_ID  " +
				"AND a.OCS_BEHAVIOUR_ID = d.BEHAVIOUR_ID " +
				"AND d.TEMPLATE_ID =:templateId " +
				"AND a.OCS_BEHAVIOUR_ID =:ocsBehaviourId";

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("ocsBehaviourId", StandardBasicTypes.LONG);
		query.addScalar("behaviourFieldId", StandardBasicTypes.LONG);
		query.addScalar("craFieldId", StandardBasicTypes.LONG);
		query.addScalar("dataType", StandardBasicTypes.LONG);
		query.addScalar("value", StandardBasicTypes.STRING);
		query.addScalar("sourceType", StandardBasicTypes.LONG);
		query.addScalar("templateType", StandardBasicTypes.LONG);
		query.addScalar("name", StandardBasicTypes.STRING);
		query.setResultTransformer(Transformers.aliasToBean(OcsBehaviourFields.class));
		query.setParameter("templateId", templateId);
		query.setParameter("ocsBehaviourId", ocsBehaviourId);
		return query.list();
	}

	@Override
	public void deleteOCSBehaviourFieldByTemplateId(Session session, Long ocsBehaviourId) {
		String hql = "delete from OcsBehaviourFields where ocsBehaviourId =:ocsBehaviourId";
		Query query = session.createQuery(hql);
		query.setParameter("ocsBehaviourId", ocsBehaviourId);
		Integer num = query.executeUpdate();
		getLog().info(" number row delete at " + ocsBehaviourId + " is " + num);

	}

}
