package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.BlackList;
import vn.viettel.campaign.entities.NoteContent;
import vn.viettel.campaign.entities.ScenarioNode;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface NoteContentDAOInterface extends BaseDAOInteface<NoteContent>{
    public List<NoteContent> findByNoteId(long nodeId);
    public List<NoteContent> findByNoteIds(List<Long> nodeIds);
    public void deleteByNoteIds(long scenarioId);
}
