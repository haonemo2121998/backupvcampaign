/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.RuleEvaluationInfoDAOInterface;
import vn.viettel.campaign.entities.RuleEvaluationInfo;

/**
 *
 * @author ABC
 */
@Repository
public class RuleEvaluationInfoDAOImpl extends BaseDAOImpl<RuleEvaluationInfo> implements RuleEvaluationInfoDAOInterface {

    @Override
    public List<RuleEvaluationInfo> getListRuleEvaluationInfoByOwnerId(Long campaignEvaluationInfoId) {
        String sql = "select"
                + " a.RULE_EVALUATION_INFO_ID as ruleEvaluationInfoId,"
                + " a.OWNER_ID as ownerId,"
                + " c.CAMPAIGN_EVALUATION_INFO_NAME as campaignEvaluationInfoName,"
                + " a.ASSESSMENT_RULE_ID as assessmentRuleId,"
                + " a.RULE_REASIONING_METHOD as ruleReasioningMethod,"
                + " a.RULE_WEIGHT as ruleWeight,"
                + " a.DESCRIPTION as description,"
                + " a.RULE_LEVEL as ruleLevel,"
                + " b.RULE_NAME as ruleName"
                + " from rule_evaluation_info as a inner join assessment_rule as b on a.ASSESSMENT_RULE_ID=b.RULE_ID"
                + " inner join campaign_evaluation_info as c on a.OWNER_ID=c.CAMPAIGN_EVALUATION_INFO_ID"
                + " where a.OWNER_ID=:campaignEvaluationInfoId"
                + " and a.RULE_LEVEL=3";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("ruleEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("campaignEvaluationInfoName", StandardBasicTypes.STRING);
        query.addScalar("assessmentRuleId", StandardBasicTypes.LONG);
        query.addScalar("ruleReasioningMethod", StandardBasicTypes.LONG);
        query.addScalar("ruleWeight", StandardBasicTypes.DOUBLE);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("ruleLevel", StandardBasicTypes.LONG);
        query.addScalar("ruleName", StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(RuleEvaluationInfo.class));
        query.setParameter("campaignEvaluationInfoId", campaignEvaluationInfoId);
        return query.list();
    }

    @Override
    public RuleEvaluationInfo getNextSequenceRuleEvaluation() {
        String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"rule_evaluation_info\"";
        SQLQuery queryUpdate = getSession().createSQLQuery(update);
        queryUpdate.executeUpdate();
        String sql = "SELECT table_id ruleEvaluationInfoId  from SEQ_TABLE WHERE TABLE_NAME = \"rule_evaluation_info\"";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("ruleEvaluationInfoId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(RuleEvaluationInfo.class));
        return (RuleEvaluationInfo) query.uniqueResult();
    }

    @Override
    public RuleEvaluationInfo doCreateOrUpdateRuleEvaluationInfo(RuleEvaluationInfo ruleEvaluationInfo) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            doSaveOrUpdate(ruleEvaluationInfo);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return ruleEvaluationInfo;
    }

    @Override
    public boolean deleteRuleEvalautionInfo(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            deleteById(RuleEvaluationInfo.class, id);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    @Override
    public List<RuleEvaluationInfo> getListRuleEvaluationInfoOfSegment(Long segmentEvaluationInfoId) {
        String sql = "select"
                + " a.RULE_EVALUATION_INFO_ID as ruleEvaluationInfoId,"
                + " a.OWNER_ID as ownerId,"
                + " a.ASSESSMENT_RULE_ID as assessmentRuleId,"
                + " a.RULE_REASIONING_METHOD as ruleReasioningMethod,"
                + " a.RULE_WEIGHT as ruleWeight,"
                + " a.DESCRIPTION as description,"
                + " a.RULE_LEVEL as ruleLevel,"
                + " b.RULE_NAME as ruleName"
                + " from rule_evaluation_info as a inner join assessment_rule as b on a.ASSESSMENT_RULE_ID=b.RULE_ID"
                + " inner join segment_evaluation_info as c on a.OWNER_ID=c.SEGMENT_EVALUATION_INFO_ID"
                + " where a.RULE_LEVEL = 2 and a.OWNER_ID=:segmentEvaluationInfoId";

        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("ruleEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("assessmentRuleId", StandardBasicTypes.LONG);
        query.addScalar("ruleReasioningMethod", StandardBasicTypes.LONG);
        query.addScalar("ruleWeight", StandardBasicTypes.DOUBLE);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("ruleLevel", StandardBasicTypes.LONG);
        query.addScalar("ruleName", StandardBasicTypes.STRING);
      
        
        query.setResultTransformer(Transformers.aliasToBean(RuleEvaluationInfo.class));
        query.setParameter("segmentEvaluationInfoId", segmentEvaluationInfoId);
        return query.list();
    }

    @Override
    public List<RuleEvaluationInfo> findRuleEvaluationInfoById(Long assessmentRuleId) {
        String sql = "select"
                + " a.RULE_EVALUATION_INFO_ID as ruleEvaluationInfoId,"
                + " a.OWNER_ID as ownerId,"
                + " a.ASSESSMENT_RULE_ID as assessmentRuleId,"
                + " a.RULE_REASIONING_METHOD as ruleReasioningMethod,"
                + " a.RULE_WEIGHT as ruleWeight,"
                + " a.DESCRIPTION as description,"
                + " a.RULE_LEVEL as ruleLevel"
                + " from rule_evaluation_info as a"
                + " where a.ASSESSMENT_RULE_ID=:assessmentRuleId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("ruleEvaluationInfoId", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("assessmentRuleId", StandardBasicTypes.LONG);
        query.addScalar("ruleReasioningMethod", StandardBasicTypes.LONG);
        query.addScalar("ruleWeight", StandardBasicTypes.DOUBLE);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("ruleLevel", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(RuleEvaluationInfo.class));
        query.setParameter("assessmentRuleId", assessmentRuleId);
        return query.list();
    }

}
