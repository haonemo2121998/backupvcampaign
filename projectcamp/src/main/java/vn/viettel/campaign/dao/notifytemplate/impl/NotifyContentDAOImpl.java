package vn.viettel.campaign.dao.notifytemplate.impl;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.BaseAbstract;
import vn.viettel.campaign.dao.notifytemplate.NotifyContentDAO;
import vn.viettel.campaign.entities.NotifyContent;

import java.util.Collections;
import java.util.List;

@Repository("notifyContentDAO")
@Transactional(rollbackFor = Exception.class)
public class NotifyContentDAOImpl extends BaseAbstract<NotifyContent> implements NotifyContentDAO {

    private static final Logger log = Logger.getLogger(NotifyContentDAOImpl.class.getName());

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<NotifyContent> getAllByTemplateId(Long templateId) {
        try {
            final String sql = "from NotifyContent where notifyTemplateId = :templateId";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(sql);
            query.setParameter("templateId", templateId);
            query.setCacheable(true);
            List<NotifyContent> templates = query.list();
            return templates;
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
        return Collections.EMPTY_LIST;
    }

    @Override
    public List<NotifyContent> getlstByNotifyId(Long notifyId) {
        String sql = "SELECT a.LANGUAGE_ID notifyLanguageId, a.CONTENT content , a.NOTIFY_CONTENT_ID contentId " +
                "FROM " +
                "notify_content a " +
                "INNER JOIN notify_template b " +
                "INNER JOIN notify_trigger c " +
                "WHERE a.NOTIFY_TEMPLATE_ID = b.TEMPLATE_ID " +
                "and b.TEMPLATE_ID = c.NOTIFY_TEMPLATE_ID " +
                "and c.NOTIFY_TRIGGER_ID =:notifyId";

        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.addScalar("notifyLanguageId", StandardBasicTypes.INTEGER);
        query.addScalar("contentId", StandardBasicTypes.LONG);
        query.addScalar("content", StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(NotifyContent.class));
        query.setParameter("notifyId", notifyId);
        return query.list();
    }

    @Override
    public NotifyContent save(NotifyContent content) {
        try {
            NotifyContent response = this.create(content);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void removeContent(Long contentId) {
        deleteById(NotifyContent.class, contentId);
    }

    @Override
    public void deleteAllByTemplateId(Long templateId) {
        try {
            String sql = "DELETE FROM notify_content WHERE  NOTIFY_TEMPLATE_ID = :templateid";
            sessionFactory.getCurrentSession()
                    .createSQLQuery(sql).setParameter("templateid", templateId).executeUpdate();
        } catch (Exception ex) {
            log.error("Exception " + ex);
        }
    }

    @Override
    public List<NotifyContent> saveContents(List<NotifyContent> contents) {
        for(NotifyContent content : contents) {
            this.create(content);
        }
        return contents;
    }
}
