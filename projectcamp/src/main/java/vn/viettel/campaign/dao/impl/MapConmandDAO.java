package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.MapCommandDaoInterface;
import vn.viettel.campaign.dao.OCSTemplateDaoInterface;
import vn.viettel.campaign.entities.MapCommand;
import vn.viettel.campaign.entities.OcsBehaviourTemplate;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class MapConmandDAO extends BaseDAOImpl<MapCommand> implements MapCommandDaoInterface {
}
