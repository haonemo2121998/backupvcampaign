package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.NotifyValues;
import vn.viettel.campaign.entities.RetryCycle;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface RetryCycleDAOInterface extends BaseDAOInteface<RetryCycle>{
}
