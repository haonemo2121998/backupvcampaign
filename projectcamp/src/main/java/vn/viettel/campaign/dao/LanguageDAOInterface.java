package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.BlackList;
import vn.viettel.campaign.entities.Language;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface LanguageDAOInterface extends BaseDAOInteface<Language>{
}
