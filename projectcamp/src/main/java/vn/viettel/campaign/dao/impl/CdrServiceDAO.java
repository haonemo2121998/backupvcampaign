package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.CdrServiceDAOInterface;
import vn.viettel.campaign.dao.ConditionTableDAOInterface;
import vn.viettel.campaign.entities.CdrService;
import vn.viettel.campaign.entities.ConditionTable;

/**
 * @author truongbx
 * @date 09/01/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class CdrServiceDAO extends BaseDAOImpl<CdrService> implements CdrServiceDAOInterface {


}
