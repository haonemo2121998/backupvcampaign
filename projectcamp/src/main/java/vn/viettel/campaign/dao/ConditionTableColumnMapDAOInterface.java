package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.ConditionTableColumnMap;

/**
 *
 */
public interface ConditionTableColumnMapDAOInterface extends BaseDAOInteface<ConditionTableColumnMap> {
    public void deleteByConditionTableId(Long conditionTableId);
}
