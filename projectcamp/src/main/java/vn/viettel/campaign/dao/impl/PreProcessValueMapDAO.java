package vn.viettel.campaign.dao.impl;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.PreProcessParamMapDAOInterface;
import vn.viettel.campaign.dao.PreProcessValueMapDAOInterface;
import vn.viettel.campaign.entities.Campaign;
import vn.viettel.campaign.entities.PreProcessParamMap;
import vn.viettel.campaign.entities.PreProcessValueMap;

import javax.persistence.Query;

import org.hibernate.SQLQuery;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;

/**
 *
 */
@Repository
public class PreProcessValueMapDAO extends BaseDAOImpl<PreProcessValueMap> implements PreProcessValueMapDAOInterface {

	@Override
	public void deletePreProcessValueMapByPPUid(Session session, Long preProcessId) {

		String hql = "DELETE FROM process_value  WHERE PROCESS_VALUE_ID in ( " +
				"SELECT a.PROCESS_VALUE_ID " +
				"FROM pre_process_value_map a " +
				"WHERE a.PRE_PROCESS_ID = :preProcessId )";
		SQLQuery deletePreprocessValue = session.createSQLQuery(hql);
		deletePreprocessValue.setParameter("preProcessId", preProcessId);
		Integer num = deletePreprocessValue.executeUpdate();

        String hql1 = "delete from PreProcessValueMap where preProcessId  =:preProcessId";
        Query query = session.createQuery(hql1);
        query.setParameter("preProcessId", preProcessId);
        int num1 = query.executeUpdate();
        getLog().info("delete Invitation by campaign " + preProcessId + " num : " + num1);
    }

    @Override
    public List<PreProcessValueMap> getListPreProcessValueMapByPreProcessId(Long preProcessId) {
        String sql="select"
                + " a.PRE_PROCESS_VALUE_ID as preProcessValueId,"
                + " a.PRE_PROCESS_ID as preProcessId,"
                + " a.PROCESS_VALUE_ID as processValueId"
                + " from pre_process_value_map as a"
                + " where a.PRE_PROCESS_ID=:preProcessId";
        SQLQuery query=getSession().createSQLQuery(sql);
        query.addScalar("preProcessValueId", StandardBasicTypes.LONG);
        query.addScalar("preProcessId", StandardBasicTypes.LONG);
        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.setParameter("preProcessId", preProcessId);
        query.setResultTransformer(Transformers.aliasToBean(PreProcessValueMap.class));
        return query.list();
                
    }

    
}
