package vn.viettel.campaign.dao;

import org.hibernate.Session;
import vn.viettel.campaign.entities.CampaignInfo;
import vn.viettel.campaign.entities.ScenarioTrigger;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface ScenarioTriggerDAOInterface extends BaseDAOInteface<ScenarioTrigger>{
    public ScenarioTrigger getByName(long id, String name);
}
