/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.ProcessParam;

/**
 *
 * @author admin
 */
public interface ProcessParamDAO {
    void onSaveOrUpdateProcessParam(ProcessParam processParam);
    void onDeleteProcessParam(PpuEvaluation ppuEvaluation);
    void onDeleteProcessParam(Long processParamId, Long ppuId);
     ProcessParam checkOnSavePreProcessParamMap(Long processParamId, Long ppuId);
}
