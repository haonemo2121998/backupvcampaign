package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.ProcessValue;

import java.util.List;

/**
 *
 */
public interface ProcessValueDAOInterface extends BaseDAOInteface<ProcessValue> {

    ProcessValue findByValueId(long valueId);

    List<ProcessValue> findByValueIds(List<Long> valueIds);

    List<ProcessValue> findByValuePreProcessIds(List<Long> preProcessIds);

    List<ProcessValue> findProcessValueForDefaulTable(Long ppuId);

    List<ProcessValue> findProProcessValueByPre_process_id(Long pre_process_id);

    List<ProcessValue> getListProcessValueByListId(List<Long> longs);
    
    public ProcessValue checkValueName(String name,Long preProcessId);

}
