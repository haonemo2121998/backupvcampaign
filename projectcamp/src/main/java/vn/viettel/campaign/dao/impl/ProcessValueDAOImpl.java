/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.entities.ProcessValue;
import vn.viettel.campaign.dao.ProcessValueDAO;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.PpuEvaluation;

/**
 *
 * @author admin
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class ProcessValueDAOImpl extends BaseDAOImpl implements ProcessValueDAO {

    @Override
    public void onSaveOrUpdateProcessValue(ProcessValue processValue) {
        getSession().saveOrUpdate(processValue);
    }

    @Override
    public void onDeleteProcessValue(PpuEvaluation ppuEvaluation) {
        String sql = "Delete from process_value where process_value_id in (select process_value_id from pre_process_value_map where pre_process_id = :id)";
        getSession().createSQLQuery(sql).setParameter("id", ppuEvaluation.getPreProcessId()).executeUpdate();
        String sql1 = "Delete from pre_process_value_map where pre_process_id = :id1";
        getSession().createSQLQuery(sql1).setParameter("id1", ppuEvaluation.getPreProcessId()).executeUpdate();

    }

    @Override
    public List<ProcessValue> getLstProcessValue() {
        String sql = "SELECT  b.PROCESS_VALUE_ID processValueId ,"
                + "b.VALUE_ID valueId , b.VALUE_NAME valueName , "
                + "b.VALUE_INDEX valueIndex, "
                + "b.VALUE_COLOR valueColor, "
                + "b.DESCRIPTION description "
                + " FROM  pre_process_value_map a "
                + " INNER JOIN  process_value b "
                + " WHERE a.PROCESS_VALUE_ID = b.PROCESS_VALUE_ID "
                + " ORDER BY b.VALUE_INDEX asc ";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.addScalar("valueId", StandardBasicTypes.LONG);
        query.addScalar("valueName", StandardBasicTypes.STRING);
        query.addScalar("valueIndex", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("valueColor", StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));
        return query.list();
    }

    @Override
    public List<ProcessValue> getLstProcessValue(Long id) {
        String sql = "SELECT  b.PROCESS_VALUE_ID processValueId ,"
                + "b.VALUE_ID valueId , b.VALUE_NAME valueName , "
                + "b.VALUE_INDEX valueIndex, "
                + "b.VALUE_COLOR valueColor, "
                + "b.DESCRIPTION description "
                + "FROM  pre_process_value_map a "
                + "INNER JOIN  process_value b "
                + "WHERE a.PROCESS_VALUE_ID = b.PROCESS_VALUE_ID "
                + "AND a.PRE_PROCESS_ID =:id "
                + "ORDER BY b.VALUE_INDEX asc ";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.addScalar("valueId", StandardBasicTypes.LONG);
        query.addScalar("valueName", StandardBasicTypes.STRING);
        query.addScalar("valueIndex", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("valueColor", StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));
        query.setParameter("id", id);
        return query.list();
    }

    @Override
    public ProcessValue getNextSequence() {
        String update = "UPDATE seq_table set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = 'PROCESS_VALUE' ";
        getSession().createSQLQuery(update).executeUpdate();
        String sql = "SELECT TABLE_ID as processValueId from seq_table WHERE TABLE_NAME = 'PROCESS_VALUE'";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));
        return (ProcessValue) query.uniqueResult();
    }

    @Override
    public ProcessValue checkSavePreProcessValueMap(Long processValueId, Long ppuId) {
        String sql = "select PROCESS_VALUE_ID as processValueId from pre_process_value_map WHERE PROCESS_VALUE_ID=:processValueId AND PRE_PROCESS_ID=:ppuId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("processValueId", StandardBasicTypes.LONG);
        query.setParameter("processValueId", processValueId);
        query.setParameter("ppuId", ppuId);
        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));
        return (ProcessValue) query.uniqueResult();
    }

    @Override
    public void onDeleteProcessValue(Long processValueId, Long ppuId) {
        String sql = "Delete from process_value where process_value_id=:processValueId";
        getSession().createSQLQuery(sql).setParameter("processValueId", processValueId).executeUpdate();
        getSession().createSQLQuery("SET SQL_SAFE_UPDATES=0;");
        String sql1 = "Delete from pre_process_value_map where pre_process_id =:ppuId and process_value_id=:processValueId";
        getSession().createSQLQuery(sql1).setParameter("ppuId", ppuId).setParameter("processValueId", processValueId).executeUpdate();
    }

    @Override
    public List<ProcessValue> checkDeleteProcessValue(Long ppuId, String name) {
        String sql = "select process_value.VALUE_NAME as valueName from process_value "
                + " inner join pre_process_value_map on process_value.process_value_id = pre_process_value_map.process_value_id"
                + " inner join pre_process_unit on pre_process_value_map.pre_process_id = pre_process_unit.pre_process_id"
                + " inner join assessment_rule_pre_process_unit_map on pre_process_unit.pre_process_id = assessment_rule_pre_process_unit_map.pre_process_unit_id"
                + " inner join assessment_rule on assessment_rule.rule_id = assessment_rule_pre_process_unit_map.rule_id"
                + " where pre_process_unit.pre_process_id =:ppuId and process_value.value_name =:name";
        
        SQLQuery query = getSession().createSQLQuery(sql).setParameter("ppuId", ppuId).setParameter("name", name);
        query.addScalar("valueName",StandardBasicTypes.STRING);
        query.setResultTransformer(Transformers.aliasToBean(ProcessValue.class));
        
        return query.list();
    }
    
    @Override
    public List<FuzzyRule> getListFuzzyForProcessValue(Long ppuId) {
        String sql = "select distinct"
                + " a.FUZZY_RULE_ID as fuzzyRuleId,"
                + " a.OWNER_ID as ownerId,"
                + " a.CLASS_ID as classId,"
                + " a.RULE_LEVEL as ruleLevel,"
                + " a.WEIGHT as weight,"
                + " a.DISPLAY_EXPRESSION as displayExpression,"
                + " a.EXPRESSION as expression,"
                + " a.FUZZY_OPERATOR as fuzzyOperator,"
                + " a.DESCRIPTION as description"
                + " from fuzzy_rule as a"
                + " inner join assessment_rule on assessment_rule.rule_id = a.owner_id"
                + " inner join assessment_rule_pre_process_unit_map on assessment_rule.rule_id = assessment_rule_pre_process_unit_map.rule_id"
                + " inner join pre_process_value_map on pre_process_value_map.pre_process_id = assessment_rule_pre_process_unit_map.pre_process_unit_id"
                + " inner join process_value on process_value.process_value_id = pre_process_value_map.process_value_id"
                + " where a.RULE_LEVEL=1 and pre_process_value_map.pre_process_id =:ppuId";

        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("fuzzyRuleId", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("classId", StandardBasicTypes.LONG);
        query.addScalar("ruleLevel", StandardBasicTypes.LONG);
        query.addScalar("weight", StandardBasicTypes.DOUBLE);
        query.addScalar("displayExpression", StandardBasicTypes.STRING);
        query.addScalar("expression", StandardBasicTypes.STRING);
        query.addScalar("fuzzyOperator", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);
        
        query.setParameter("ppuId", ppuId);
        query.setResultTransformer(Transformers.aliasToBean(FuzzyRule.class));
        
        return query.list();
    }
}
