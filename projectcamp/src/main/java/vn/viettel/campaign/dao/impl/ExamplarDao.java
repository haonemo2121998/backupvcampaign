/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository(value = "customerDAO")
@Transactional(rollbackFor = Exception.class)
public class ExamplarDao {

    @Autowired
    private SessionFactory sessionFactory;

    public void getLst() {
        Session session = this.sessionFactory.getCurrentSession();
//        List lst = session.createQuery("from AppRole").list();
//        
//        System.out.print("===================== " + lst);
    }
}
