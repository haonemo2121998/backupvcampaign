package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.RowCtDAOInterface;
import vn.viettel.campaign.entities.RowCt;
import vn.viettel.campaign.entities.Rule;

import java.util.List;

/**
 *
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class RowCtDAO extends BaseDAOImpl<RowCt> implements RowCtDAOInterface {
    @Override
    public List<RowCt> getLstRowCtByConditionTableId(long conditionTableId) {
        String sql = "SELECT\n" +
                "\trow_ct.row_id rowId, row_ct.value, row_ct.ROW_INDEX rowIndex \n" +
                "FROM\n" +
                "\tcondition_table_row_map\n" +
                "\tINNER JOIN row_ct \n" +
                "WHERE\n" +
                "\tcondition_table_row_map.ROW_ID = row_ct.ROW_ID \n" +
                "\tAND condition_table_row_map.CONDITION_TABLE_ID = :conditionTableId \n" +
                "ORDER BY\n" +
                "\trow_ct.ROW_INDEX";

        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("rowId", StandardBasicTypes.LONG);
        query.addScalar("value", StandardBasicTypes.STRING);
        query.addScalar("rowIndex", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(RowCt.class));
        query.setParameter("conditionTableId", conditionTableId);
        return query.list();
    }

    @Override
    public void deleteByConditionTableId(Long conditionTableId) {
        try {
            if (!DataUtil.isNullObject(conditionTableId)) {
                String sql = "DELETE FROM row_ct \n" +
                        "WHERE\n" +
                        "\trow_ct.ROW_ID IN ( SELECT condition_table_row_map.ROW_ID FROM condition_table_row_map WHERE condition_table_row_map.CONDITION_TABLE_ID = :conditionTableId )";
                getSession().createSQLQuery(sql).setParameter("conditionTableId", conditionTableId).executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
