/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.PpuEvaluation;
import vn.viettel.campaign.entities.ProcessParam;
import vn.viettel.campaign.entities.ProcessValue;

/**
 *
 * @author admin
 */
public interface PpuEvaluationDAO {

    void onSaveOrUpdatePpuEvaluation(PpuEvaluation ppuEvaluation);

    PpuEvaluation getNextSequense();

    void onDeletePpuEvaluation(PpuEvaluation ppuEvaluation);

    List<ProcessValue> getListProcessValue(Long id);

    List<ProcessParam> getListProcessParam(Long id);

    List<PpuEvaluation> checkExitsProcessorName(String name, Long id);

    List<PpuEvaluation> checkExitsCriteriaName(String name, Long id);

    List<PpuEvaluation> checkExitsValueName(Long processValue, String name, Long id);

    List<PpuEvaluation> checkExitsValueId(Long processValue, Long valueId, Long id);

    List<PpuEvaluation> checkDeletePPUEvaluation(Long id);

    PpuEvaluation findOneById(Long id);

    public List<PpuEvaluation> getListPpuEvaluationPPUByRuleId(Long idd);

    public List<PpuEvaluation> getListPpuEvaluationTree(List<Long> categoryIds);
}
