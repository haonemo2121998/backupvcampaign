/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.BaseAbstract;
import vn.viettel.campaign.dao.ResultDao;
import vn.viettel.campaign.dao.ResultTableDao;
import vn.viettel.campaign.dao.RuleResultDao;
import vn.viettel.campaign.dto.ResultTableDTO;
import vn.viettel.campaign.entities.Result;
import vn.viettel.campaign.entities.ResultTable;
import vn.viettel.campaign.entities.RowCt;
import vn.viettel.campaign.entities.RuleResult;


@Repository
@Transactional(rollbackFor = Exception.class)
public class ResultTableDaoImpl extends  BaseDAOImpl<ResultTable> implements ResultTableDao {
    private static final Logger log = Logger.getLogger(ResultTableDaoImpl.class.getName());

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<ResultTable> finAllResultTable() {
        Session session = this.sessionFactory.getCurrentSession();
        List<ResultTable> lstObj;
        lstObj = session.createQuery("from result_table").list();
        return lstObj.size() > 0 ? lstObj : null;    
    }

    @Override
    public ResultTable updateOrSave(ResultTable resultTable) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(resultTable);
        return resultTable;
    } 

    @Override
    public ResultTable getResultTableById(Long resultTableId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<ResultTable> lstObj;
        lstObj = session.createQuery("from ResultTable where resultTableId = :resultTableId")
                .setParameter("resultTableId", resultTableId).list();
        return lstObj.size() > 0 ? lstObj.get(0) : null;
    }  

    @Override
    public List<ResultTable> getListToCheckDuplicate(String resultTableName, Long resultTableId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<ResultTable> lstObj;
        if (resultTableId != null) {
            lstObj = session.createQuery("from ResultTable where lower(resultTableName) = :resultTableName and resultTableId != :resultTableId")
                    .setParameter("resultTableName", resultTableName.toLowerCase())
                    .setParameter("resultTableId", resultTableId)
                    .list();
        } else {
            lstObj = session.createQuery("from ResultTable where lower(resultTableName) = :resultTableName ").setParameter("resultTableName", resultTableName.toLowerCase()).list();
        }
        return lstObj.size() > 0 ? lstObj : null;
    }

    @Override
    public Long checkUseRule(Long resultTableId) {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("select COUNT(*) from rule_result_table_map where RESULT_TABLE_ID = :resultTableId");
        SQLQuery query = getSession().createSQLQuery(sqlQuery.toString()).setParameter("resultTableId", resultTableId);
        return ((Number) query.uniqueResult()).longValue();
    }

    @Override
    public List<ResultTableDTO> getListResultTableDTO(Long conditionTableId, Long rowIndex) {
        String sql = "select a.RESULT_TABLE_ID resultTableId, a.CONDITION_TABLE_ID conditionTableId,\n" +
                "b.RESULT_TABLE_RULE_RESULT_ID resultTableRuleResultId, b.RULE_RESULT_ID ruleResultId, \n" +
                "c.RESULT_ID resultId, c.ROW_INDEX rowIndex\n" +
                "from result_table a, result_table_rule_result_map b, rule_result c, result d where \n" +
                "a.RESULT_TABLE_ID = b.RESULT_TABLE_ID\n" +
                "and b.RULE_RESULT_ID = c.RULE_RESULT_ID\n" +
                "and c.RESULT_ID = d.RESULT_ID\n" +
                "and a.CONDITION_TABLE_ID = :conditionTableId and c.ROW_INDEX = :rowIndex";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("resultTableId", StandardBasicTypes.LONG);
        query.addScalar("conditionTableId", StandardBasicTypes.LONG);
        query.addScalar("resultTableRuleResultId", StandardBasicTypes.LONG);
        query.addScalar("ruleResultId", StandardBasicTypes.LONG);
        query.addScalar("resultId", StandardBasicTypes.LONG);
        query.addScalar("rowIndex", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(ResultTableDTO.class));
        query.setParameter("conditionTableId", conditionTableId);
        query.setParameter("rowIndex", rowIndex);
        return query.list();
    }
}
