package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.OCSBehaviourExtendFieldDaoInterface;
import vn.viettel.campaign.dao.OCSTemplateExtendFieldDaoInterface;
import vn.viettel.campaign.entities.OcsBehaviourExtendFields;
import vn.viettel.campaign.entities.OcsBehaviourTemplateExtendFields;

import javax.persistence.Query;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class OCSBehaviourExtendFieldDAO extends BaseDAOImpl<OcsBehaviourExtendFields> implements OCSBehaviourExtendFieldDaoInterface {
	@Override
	public List<OcsBehaviourExtendFields> getLstExtendFieldOfOcsBehaviour(long ocsBehaviourId , Long templateId) {
		String sql = "SELECT b.NAME name , b.ID extendFieldId ,a.BEHAVIOUR_ID behaviourId, a.VALUE value " +
				"FROM " +
				"ocs_behaviour_extend_fields a " +
				"INNER JOIN extend_field b  " +
				"INNER JOIN ocs_behaviour_template_extend_fields c  " +
				"WHERE " +
				"a.EXTEND_FIELD_ID = b.ID  " +
				"AND b.ID = c.EXTEND_FIELD_ID " +
				"AND c.BEHAVIOUR_TEMPLATE_ID =:templateId " +
				"AND a.BEHAVIOUR_ID =:ocsBehaviourId";

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("name", StandardBasicTypes.STRING);
		query.addScalar("extendFieldId", StandardBasicTypes.LONG);
		query.addScalar("value", StandardBasicTypes.STRING);
		query.addScalar("behaviourId", StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(OcsBehaviourExtendFields.class));
		query.setParameter("ocsBehaviourId", ocsBehaviourId);
		query.setParameter("templateId", templateId);
		return query.list();
	}

	@Override
	public void deleteOcsBehaviourExtendFieldByOcsBehavourId(Session session, Long ocsBehaviourId) {
		String hql = "delete from OcsBehaviourExtendFields where behaviourId =:ocsBehaviourId";
		Query query = session.createQuery(hql);
		query.setParameter("ocsBehaviourId", ocsBehaviourId);
		Integer num = query.executeUpdate();
		getLog().info(" number row delete at " + ocsBehaviourId + " is " + num);
	}

}
