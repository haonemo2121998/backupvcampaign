package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.NotifyTrigger;
import vn.viettel.campaign.entities.Rule;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface NotifyTriggerDAOInterface extends BaseDAOInteface<NotifyTrigger>{
	List<NotifyTrigger> getNotifyTriggerByRuleIds(List<Long> ruleIds);
	<T>List<T> getLstNotifyTrigger(List<Long> lstCategory);
	List<NotifyTrigger> getByNotifyTemplateId(Long id);

	public NotifyTrigger getByName(long id, String name);
}
