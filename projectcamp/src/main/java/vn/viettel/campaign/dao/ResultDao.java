package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.Result;



/**
 *
 * @author ConKC
 */
public interface ResultDao {
    public List<Result> getResultByRowIndexAndResultTableId(long rowIndex, long resultTableId);
    List<Result> finAllResult();
    Object getSequence();
    Result updateOrSave(Result result);
    Result getResultById(Long resultId);
    void deleteByResultTableId(long id);
    void deleteByDefaultResultId(long id);
}
