package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.ColumnCTDAOInterface;
import vn.viettel.campaign.dao.RuleDAOInterface;
import vn.viettel.campaign.entities.ColumnCt;
import vn.viettel.campaign.entities.Rule;

import java.util.List;

/**
 * @author truongbx
 * @date 8/26/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class ColumnCTDAO extends BaseDAOImpl<ColumnCt> implements ColumnCTDAOInterface {
    @Override
    public List<ColumnCt> getLstColumnctByConditionTableId(Long conditionId) {
        String sql = "SELECT a.COLUMN_ID columnId,a.COLUMN_NAME columnName, a.PRE_PROCESS_ID preProcessId, a.COLUMN_INDEX columnIndex , c.PRE_PROCESS_NAME preprocessName " +
                "FROM column_ct a " +
                "INNER JOIN condition_table_column_map b " +
                "LEFT JOIN pre_process_unit c ON a.PRE_PROCESS_ID = c.PRE_PROCESS_ID " +
                "WHERE " +
                "a.COLUMN_ID = b.COLUMN_ID and "+
                "b.CONDITION_TABLE_ID =:conditionId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("columnId", StandardBasicTypes.LONG);
        query.addScalar("preprocessName", StandardBasicTypes.STRING);
        query.addScalar("columnName", StandardBasicTypes.STRING);
        query.addScalar("preProcessId", StandardBasicTypes.LONG);
        query.addScalar("columnIndex", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(ColumnCt.class));
        query.setParameter("conditionId", conditionId);
        return query.list();
    }

    @Override
    public void deleteByConditionTableId(Long conditionTableId) {
        try {
            if (!DataUtil.isNullObject(conditionTableId)) {
                String sql = "DELETE FROM column_ct \n" +
                        "WHERE\n" +
                        "\tcolumn_ct.COLUMN_ID IN ( SELECT condition_table_column_map.COLUMN_ID FROM condition_table_column_map WHERE condition_table_column_map.CONDITION_TABLE_ID = :conditionTableId )";
                getSession().createSQLQuery(sql).setParameter("conditionTableId", conditionTableId).executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
