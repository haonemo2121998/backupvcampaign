/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.CriteriaValueDAO;
import vn.viettel.campaign.entities.CriteriaValue;
import vn.viettel.campaign.entities.FuzzyRule;
import vn.viettel.campaign.entities.KpiProcessorCriteriaValueMap;

/**
 *
 * @author SON
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class CriteriaValueDAOImpl extends BaseDAOImpl<CriteriaValue> implements CriteriaValueDAO {

    @Override
    public List<CriteriaValue> getLstCriteriaValueByKpiProcessorId(Long id) {
        String sql = "SELECT a.CRITERIA_VALUE_ID as criteriaValueId,"
                + " CRITERIA_VALUE_NAME as criteriaValueName,"
                + " FUZZY_FUNCTION_TYPE as fuzzyFunctionType,"
                + " MEMBERSHIP_FUNCTION as membershipFunction,"
                + " DESCRIPTION as description"
                + " FROM criteria_value a inner join kpi_processor_criteria_value_map b"
                + " ON a.CRITERIA_VALUE_ID = b.CRITERIA_VALUE_ID"
                + " WHERE b.PROCESSOR_ID =:id";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("criteriaValueId", StandardBasicTypes.LONG);
        query.addScalar("criteriaValueName", StandardBasicTypes.STRING);
        query.addScalar("fuzzyFunctionType", StandardBasicTypes.INTEGER);
        query.addScalar("membershipFunction", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(CriteriaValue.class));
        query.setParameter("id", id);

        return query.list();
    }

    @Override
    public CriteriaValue getNextSequense() {
        String update = "UPDATE seq_table SET TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"CRITERIA_VALUE\" ";
        getSession().createSQLQuery(update).executeUpdate();
        String sql = "SELECT TABLE_ID as criteriaValueId FROM  seq_table WHERE TABLE_NAME = \"CRITERIA_VALUE\"";
        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("criteriaValueId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(CriteriaValue.class));

        return (CriteriaValue) query.uniqueResult();
    }

    @Override
    public void onSave(CriteriaValue criteriaValue) {
        getSession().save(criteriaValue);
    }

    @Override
    public void onUpdate(CriteriaValue criteriaValue) {
        getSession().update(criteriaValue);
    }

    @Override
    public void onDelete(Long id) {
        String sql = "Delete from criteria_value where criteria_value_id in (select criteria_value_id from kpi_processor_criteria_value_map where processor_id = :id)";
        getSession().createSQLQuery(sql).setParameter("id", id).executeUpdate();
        String sql1 = "Delete from kpi_processor_criteria_value_map where processor_id = :id1";
        getSession().createSQLQuery(sql1).setParameter("id1", id).executeUpdate();
    }

    @Override
    public CriteriaValue findOneById(Long id) {
        String sql = "SELECT CRITERIA_VALUE_ID as criteriaValueId,"
                + " CRITERIA_VALUE_NAME as criteriaValueName,"
                + " FUZZY_FUNCTION_TYPE as fuzzyFunctionType,"
                + " MEMBERSHIP_FUNCTION as membershipFunction,"
                + " DESCRIPTION as description"
                + " FROM criteria_value "
                + " WHERE CRITERIA_VALUE_ID =:id";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("criteriaValueId", StandardBasicTypes.LONG);
        query.addScalar("criteriaValueName", StandardBasicTypes.STRING);
        query.addScalar("fuzzyFunctionType", StandardBasicTypes.INTEGER);
        query.addScalar("membershipFunction", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(CriteriaValue.class));
        query.setParameter("id", id);

        return (CriteriaValue) query.uniqueResult();
    }

    @Override
    public void onSaveOrUpdateCriteriaValue(CriteriaValue criteriaValue) {
        getSession().saveOrUpdate(criteriaValue);
    }

    @Override
    public void onSaveKpiCriteriaValueMap(KpiProcessorCriteriaValueMap kpiProcessorCriteriaValueMap) {
        getSession().saveOrUpdate(kpiProcessorCriteriaValueMap);
    }

    @Override
    public CriteriaValue checkExitsCriteriaName(String name, Long id, Long processorId) {
        String sql = "SELECT a.CRITERIA_VALUE_ID as criteriaValueId,"
                + " CRITERIA_VALUE_NAME as criteriaValueName,"
                + " FUZZY_FUNCTION_TYPE as fuzzyFunctionType,"
                + " MEMBERSHIP_FUNCTION as membershipFunction,"
                + " DESCRIPTION as description"
                + " FROM criteria_value a inner join kpi_processor_criteria_value_map b"
                + " ON a.CRITERIA_VALUE_ID = b.CRITERIA_VALUE_ID"
                + " WHERE b.PROCESSOR_ID =:processorId and a.CRITERIA_VALUE_ID != :id and a.CRITERIA_VALUE_NAME = :name";

        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("criteriaValueId", StandardBasicTypes.LONG);
        query.addScalar("criteriaValueName", StandardBasicTypes.STRING);
        query.addScalar("fuzzyFunctionType", StandardBasicTypes.INTEGER);
        query.addScalar("membershipFunction", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);

        query.setResultTransformer(Transformers.aliasToBean(CriteriaValue.class));
        query.setParameter("processorId", processorId);
        query.setParameter("id", id);
        query.setParameter("name", name);

        return (CriteriaValue) query.uniqueResult();
    }

    @Override
    public List<KpiProcessorCriteriaValueMap> getLstKpiProcessorCriteriaValueMap() {
        String sql = "select ID as id,"
                + " PROCESSOR_ID as processor_id,"
                + " CRITERIA_VALUE_ID as criteriaValueId"
                + " FROM criteria_value a inner join kpi_processor_criteria_value_map b"
                + " ON a.CRITERIA_VALUE_ID = b.CRITERIA_VALUE_ID";
        SQLQuery query = getSession().createSQLQuery(sql);
        return query.list();
    }

    @Override
    public void onDeleteCriteriaValue(Long processorId, Long CriteriaValueId) {
        String delete1 = "delete from criteria_value where CRITERIA_VALUE_ID =:CriteriaValueId";
        getSession().createSQLQuery(delete1).setParameter("CriteriaValueId", CriteriaValueId).executeUpdate();
        String delete2 = "delete from kpi_processor_criteria_value_map where CRITERIA_VALUE_ID =:CriteriaValueId AND PROCESSOR_ID =:processorId";
        getSession().createSQLQuery(delete2).setParameter("CriteriaValueId", CriteriaValueId).setParameter("processorId", processorId).executeUpdate();
    }

    @Override
    public List<CriteriaValue> checkDeleteCriteriaValue(Long kpiId, String name) {
        String sql = "select distinct criteria_value.* from criteria_value "
                + " inner join kpi_processor_criteria_value_map on criteria_value.criteria_value_id = kpi_processor_criteria_value_map.criteria_value_id"
                + " inner join kpi_processor on kpi_processor.processor_id = kpi_processor_criteria_value_map.processor_id"
                + " inner join assessment_rule_kpi_processor_map on assessment_rule_kpi_processor_map.processor_id = kpi_processor.processor_id"
                + " inner join assessment_rule on assessment_rule.rule_id = assessment_rule_kpi_processor_map.rule_id"
                + " where kpi_processor.processor_id =:kpiId and criteria_value.criteria_value_name =:name";
        SQLQuery query = getSession().createSQLQuery(sql).setParameter("kpiId", kpiId).setParameter("name", name);
        return query.list();
    }

    @Override
    public List<CriteriaValue> getListCriteriaValueById(List<Long> longs) {
        String sql = "select"
                + " a.CRITERIA_VALUE_ID as criteriaValueId,"
                + " a.CRITERIA_VALUE_NAME as criteriaValueName,"
                + " a.FUZZY_FUNCTION_TYPE as fuzzyFunctionType,"
                + " a.MEMBERSHIP_FUNCTION as membershipFunction"
                + " from criteria_value as a"
                + " where a.CRITERIA_VALUE_ID in(:longs)";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("criteriaValueId", StandardBasicTypes.LONG);
        query.addScalar("criteriaValueName", StandardBasicTypes.STRING);
        query.addScalar("fuzzyFunctionType", StandardBasicTypes.INTEGER);
        query.addScalar("membershipFunction", StandardBasicTypes.STRING);
        query.setParameterList("longs", longs);

        query.setResultTransformer(Transformers.aliasToBean(CriteriaValue.class));
        return query.list();
    }

    @Override
    public CriteriaValue chekCriteriaValueName(String name, Long processId) {
        String sql = "select"
                + " a.CRITERIA_VALUE_ID as criteriaValueId,"
                + " a.CRITERIA_VALUE_NAME as criteriaValueName,"
                + " a.FUZZY_FUNCTION_TYPE as fuzzyFunctionType,"
                + " a.MEMBERSHIP_FUNCTION as membershipFunction,"
                + " a.DESCRIPTION as description"
                + " from criteria_value as a"
                + " inner join kpi_processor_criteria_value_map as b on a.CRITERIA_VALUE_ID = b.CRITERIA_VALUE_ID"
                + " inner join kpi_processor as c on b.PROCESSOR_ID = c.PROCESSOR_ID"
                + " where a.CRITERIA_VALUE_NAME =:name and c.PROCESSOR_ID=:processId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("criteriaValueId", StandardBasicTypes.LONG);
        query.addScalar("criteriaValueName", StandardBasicTypes.STRING);
        query.addScalar("fuzzyFunctionType", StandardBasicTypes.INTEGER);
        query.addScalar("membershipFunction", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.setParameter("name", name);
        query.setParameter("processId", processId);
        query.setResultTransformer(Transformers.aliasToBean(CriteriaValue.class));
        return (CriteriaValue) query.uniqueResult();
    }

    @Override
    public List<FuzzyRule> getListFuzzyForCriteriaValue(Long kpiId) {
        String sql = "select distinct"
                + " a.FUZZY_RULE_ID as fuzzyRuleId,"
                + " a.OWNER_ID as ownerId,"
                + " a.CLASS_ID as classId,"
                + " a.RULE_LEVEL as ruleLevel,"
                + " a.WEIGHT as weight,"
                + " a.DISPLAY_EXPRESSION as displayExpression,"
                + " a.EXPRESSION as expression,"
                + " a.FUZZY_OPERATOR as fuzzyOperator,"
                + " a.DESCRIPTION as description"
                + " from fuzzy_rule as a"
                + " inner join assessment_rule on assessment_rule.rule_id = a.owner_id"
                + " inner join assessment_rule_kpi_processor_map on assessment_rule_kpi_processor_map.rule_id = assessment_rule.rule_id"
                + " inner join kpi_processor on kpi_processor.processor_id = assessment_rule_kpi_processor_map.processor_id"
                + " inner join kpi_processor_criteria_value_map on kpi_processor_criteria_value_map.processor_id = kpi_processor.processor_id"
                + " inner join criteria_value on criteria_value.criteria_value_id = kpi_processor_criteria_value_map.criteria_value_id"
                + " where a.RULE_LEVEL=1 and kpi_processor.processor_id =:kpiId";

        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("fuzzyRuleId", StandardBasicTypes.LONG);
        query.addScalar("ownerId", StandardBasicTypes.LONG);
        query.addScalar("classId", StandardBasicTypes.LONG);
        query.addScalar("ruleLevel", StandardBasicTypes.LONG);
        query.addScalar("weight", StandardBasicTypes.DOUBLE);
        query.addScalar("displayExpression", StandardBasicTypes.STRING);
        query.addScalar("expression", StandardBasicTypes.STRING);
        query.addScalar("fuzzyOperator", StandardBasicTypes.LONG);
        query.addScalar("description", StandardBasicTypes.STRING);

        query.setParameter("kpiId", kpiId);
        query.setResultTransformer(Transformers.aliasToBean(FuzzyRule.class));

        return query.list();
    }

}
