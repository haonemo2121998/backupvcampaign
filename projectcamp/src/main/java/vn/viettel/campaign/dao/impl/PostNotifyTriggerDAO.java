package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.OcsBehaviourDAOInterface;
import vn.viettel.campaign.dao.PostNotifyTriggerDaoInterface;
import vn.viettel.campaign.entities.OcsBehaviour;
import vn.viettel.campaign.entities.PostNotifyTrigger;

import javax.persistence.Query;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class PostNotifyTriggerDAO extends BaseDAOImpl<PostNotifyTrigger> implements PostNotifyTriggerDaoInterface {
	@Override
	public List<PostNotifyTrigger> getLstPostNotigyTriggerByOcsBehaviourId(Long behaviourId) {
		List<PostNotifyTrigger> lstObj = getSession().createQuery("from PostNotifyTrigger where ocsBehaviourId =:behaviourId ")
				.setParameter("behaviourId", behaviourId)
				.list();
		return lstObj;
	}

	@Override
	public List<PostNotifyTrigger> getLstPostNotigyTriggerByNotifyId(Long notifyId) {
		List<PostNotifyTrigger> lstObj = getSession().createQuery("from PostNotifyTrigger where notifyId =:notifyId ")
				.setParameter("notifyId", notifyId)
				.list();
		return lstObj;
	}

	@Override
	public void deletePostNotifyTrigger(Session session, Long ocsBehaviourId) {
		String hql = "delete from PostNotifyTrigger where ocsBehaviourId =:ocsBehaviourId";
		Query query = session.createQuery(hql);
		query.setParameter("ocsBehaviourId", ocsBehaviourId);
		Integer num = query.executeUpdate();
		getLog().info(" number row delete at " + ocsBehaviourId + " is " + num);
	}
}
