/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.ObjectDao;
import vn.viettel.campaign.entities.Objects;

@Repository
@Transactional(rollbackFor = Exception.class)
public class ObjectDaoImpl implements ObjectDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Objects> getLstObjectByName(String name, Long objectId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Objects> lstObj;
        if (objectId != null) {
            lstObj = session.createQuery("from Objects where objectName = :objectName and status = 1 and objectId <> :objectId")
                    .setParameter("objectName", name)
                    .setParameter("objectId", objectId)
                    .list();
        } else {
            lstObj = session.createQuery("from Objects where objectName = :objectName and status = 1").setParameter("objectName", name).list();
        }
        return lstObj.size() > 0 ? lstObj : null;
    }

    @Override
    public List<Objects> getLstObjectByCode(String code, Long objectId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Objects> lstObj;
        if (objectId != null) {
            lstObj = session.createQuery("from Objects where objectCode = :objectCode and status = 1 and objectId <> :objectId")
                    .setParameter("objectCode", code)
                    .setParameter("objectId", objectId)
                    .list();
        } else {
            lstObj = session.createQuery("from Objects where objectCode = :objectCode and status = 1").setParameter("objectCode", code).list();
        }
        return lstObj.size() > 0 ? lstObj : null;
    }

    @Override
    public Boolean saveObject(Objects obj) {
        Session session = this.sessionFactory.getCurrentSession();
        DataUtil.trimObject(obj);
        session.save(obj);
        return true;
    }

    @Override
    public Boolean updateObject(Objects obj) {
        Session session = this.sessionFactory.getCurrentSession();
        DataUtil.trimObject(obj);
        session.update(obj);
        return true;
    }

    @Override
    public List<Objects> getLstObjects() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Objects> lstObj;
        lstObj = session.createQuery("from Objects where status = 1 order by ord").list();
        return lstObj.size() > 0 ? arrangeLstObject(lstObj) : null;
    }

    private List<Objects> arrangeLstObject(List<Objects> lstObj) {
        List<Objects> subObj = new ArrayList();
        lstObj.forEach((obj) -> {
            if (obj.getParentId() != null) {
                subObj.add(obj);
            }
        });
        lstObj.removeAll(subObj);
        lstObj.forEach((obj) -> {
            subObj.forEach((sub) -> {
                if (obj.getObjectId().longValue() == sub.getParentId()) {
                    obj.getLstSub().add(sub);
                }
            });
        });
        return lstObj;
    }

    @Override
    public List<Objects> getLstObjectsByRole(Long[] roleIds) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Objects> lstObj;
        lstObj = session.createQuery("select distinct o from Objects o where o.status = 1 and o.objectId in "
                + "(select objectId from RoleObject where roleId in (:roleId) and isActive = 1)"
                + " order by parentId,ord")
                .setParameterList("roleId", roleIds).list();
        return lstObj.size() > 0 ? arrangeLstObject(lstObj) : null;
    }

}
