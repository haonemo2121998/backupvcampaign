package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.dto.BehaviourDTO;
import vn.viettel.campaign.entities.PromotionBlock;

public interface PromotionBlockDao extends BaseDAOInteface<PromotionBlock> {

    List<PromotionBlock> findByCategory(final List<Long> categoryIds);

    List<BehaviourDTO> findBehaviourByPromotionBlockId(final Long promotionBlockId);

    List<BehaviourDTO> findOcsBehaviour(final List<Long> ocsBehaviourIds);

    List<BehaviourDTO> findNotifyTrigger(final List<Long> ids);

    List<BehaviourDTO> findScenarioTrigger(final List<Long> ids);

    boolean checkDuplicate(final String name, final Long id);
    boolean checkInUseResult(Long promotionBlockId);

    void deleteBlockContainerByPrmId(final Long prmId) throws Exception;
    
    Long genPrmBlockId() throws Exception;

    Long checkUsePromotionBlock(Long promType, Long promId) throws Exception;

}
