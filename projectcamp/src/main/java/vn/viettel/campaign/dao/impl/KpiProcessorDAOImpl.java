/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.KpiProcessorDAO;
import vn.viettel.campaign.entities.AssessmentRuleKpiProcessorMap;
import vn.viettel.campaign.entities.KpiProcessor;

/**
 *
 * @author SON
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class KpiProcessorDAOImpl extends BaseDAOImpl<KpiProcessor> implements KpiProcessorDAO {

    @Override
    public List<KpiProcessor> getLstKpiProcessor() {
        String sql = "SELECT PROCESSOR_ID as processorId,"
                + " PROCESSOR_NAME as processorName,"
                + " INPUT_FIELD as inputField,"
                + " CRITERIA_NAME as criteriaName,"
                + " DESCRIPTION as description,"
                + " CATEGORY_ID as categoryId"
                + " FROM kpi_processor";
        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("processorId", StandardBasicTypes.LONG);
        query.addScalar("processorName", StandardBasicTypes.STRING);
        query.addScalar("inputField", StandardBasicTypes.STRING);
        query.addScalar("criteriaName", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);

        query.setResultTransformer(Transformers.aliasToBean(KpiProcessor.class));

        return query.list();

    }

    @Override
    public List<KpiProcessor> getListKpiProcessorByRuleId(Long ruleId) {
        String sql = "select"
                + " a.PROCESSOR_ID as processorId,"
                + " a.PROCESSOR_NAME as processorName,"
                + " a.INPUT_FIELD as inputField,"
                + " a.CRITERIA_NAME as criteriaName,"
                + " a.DESCRIPTION as description,"
                + " a.CATEGORY_ID as categoryId,"
                + " b.ID as id"
                + " from kpi_processor as a"
                + " inner join assessment_rule_kpi_processor_map as b on a.PROCESSOR_ID = b.PROCESSOR_ID"
                + " where b.RULE_ID=:ruleId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("processorId", StandardBasicTypes.LONG);
        query.addScalar("processorName", StandardBasicTypes.STRING);
        query.addScalar("inputField", StandardBasicTypes.STRING);
        query.addScalar("criteriaName", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);
        query.addScalar("id", StandardBasicTypes.LONG);
        query.setParameter("ruleId", ruleId);
        query.setResultTransformer(Transformers.aliasToBean(KpiProcessor.class));
        return query.list();

    }

    @Override
    public KpiProcessor findOneById(Long id) {
        String sql = "SELECT PROCESSOR_ID as processorId,"
                + " PROCESSOR_NAME as processorName,"
                + " INPUT_FIELD as inputField,"
                + " CRITERIA_NAME as criteriaName,"
                + " DESCRIPTION as description,"
                + " CATEGORY_ID as categoryId"
                + " FROM kpi_processor"
                + " WHERE PROCESSOR_ID=:id";
        SQLQuery query = getSession().createNativeQuery(sql);

        query.addScalar("processorId", StandardBasicTypes.LONG);
        query.addScalar("processorName", StandardBasicTypes.STRING);
        query.addScalar("inputField", StandardBasicTypes.STRING);
        query.addScalar("criteriaName", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);

        query.setResultTransformer(Transformers.aliasToBean(KpiProcessor.class));
        query.setParameter("id", id);

        return (KpiProcessor) query.uniqueResult();
    }

    @Override
    public void onSaveOrUpdateKpiProcessor(KpiProcessor kpiProcessor) {
        getSession().saveOrUpdate(kpiProcessor);
    }

    @Override
    public void onDeleteKpiProcessor(KpiProcessor kpiProcessor) {
        getSession().delete(kpiProcessor);
    }

    @Override
    public KpiProcessor getNextSequense() {
        String update = "UPDATE SEQ_TABLE SET TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"KPI_PROCESSOR\" ";
        getSession().createSQLQuery(update).executeUpdate();

        String sql = "SELECT TABLE_ID as processorId FROM SEQ_TABLE WHERE TABLE_NAME = \"KPI_PROCESSOR\" ";
        SQLQuery query = getSession().createSQLQuery(sql);

        query.addScalar("processorId", StandardBasicTypes.LONG);

        query.setResultTransformer(Transformers.aliasToBean(KpiProcessor.class));

        return (KpiProcessor) query.uniqueResult();
    }

    @Override
    public void onSaveKpiProcessor(KpiProcessor kpiProcessor) {
        getSession().save(kpiProcessor);
    }

    @Override
    public void onUpdateKpiProcessor(KpiProcessor kpiProcessor) {
        getSession().update(kpiProcessor);
    }

    @Override
    public KpiProcessor checkExitsProcessName(String name, Long id) {
        String sql = "select "
                + " PROCESSOR_ID as processorId,"
                + " PROCESSOR_NAME as processorName,"
                + " INPUT_FIELD as inputField,"
                + " CRITERIA_NAME as criteriaName,"
                + " DESCRIPTION as description,"
                + " CATEGORY_ID as categoryId"
                + " from kpi_processor where lower(PROCESSOR_NAME) = :name AND PROCESSOR_ID != :id";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setParameter("name", name.trim().toLowerCase());
        query.setParameter("id", id);

        query.addScalar("processorId", StandardBasicTypes.LONG);
        query.addScalar("processorName", StandardBasicTypes.STRING);
        query.addScalar("inputField", StandardBasicTypes.STRING);
        query.addScalar("criteriaName", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);

        query.setResultTransformer(Transformers.aliasToBean(KpiProcessor.class));

        return (KpiProcessor) query.uniqueResult();
    }

    @Override
    public KpiProcessor checkExitsCriteriaName(String name, Long id) {
        String sql = "select "
                + " PROCESSOR_ID as processorId,"
                + " PROCESSOR_NAME as processorName,"
                + " INPUT_FIELD as inputField,"
                + " CRITERIA_NAME as criteriaName,"
                + " DESCRIPTION as description,"
                + " CATEGORY_ID as categoryId"
                + " from kpi_processor where lower(CRITERIA_NAME) = :name AND PROCESSOR_ID != :id";
        SQLQuery query = getSession().createSQLQuery(sql);

        query.setParameter("name", name.trim().toLowerCase());
        query.setParameter("id", id);
        query.addScalar("processorId", StandardBasicTypes.LONG);
        query.addScalar("processorName", StandardBasicTypes.STRING);
        query.addScalar("inputField", StandardBasicTypes.STRING);
        query.addScalar("criteriaName", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);

        query.setResultTransformer(Transformers.aliasToBean(KpiProcessor.class));

        return (KpiProcessor) query.uniqueResult();
    }

    @Override
    public List<AssessmentRuleKpiProcessorMap> checkDeleteKpiProcessor(KpiProcessor kpiProcessor) {
        String sql = "select * from assessment_rule_kpi_processor_map where processor_id =:id";
        SQLQuery query = getSession().createSQLQuery(sql).setParameter("id", kpiProcessor.getProcessorId());
        return query.list();
    }

    @Override
    public List<KpiProcessor> findAllKpiProcessor(List<Long> categoryIds) {
        String sql = "select"
                + " a.PROCESSOR_ID as processorId,"
                + " a.PROCESSOR_NAME as processorName,"
                + " a.INPUT_FIELD as inputField,"
                + " a.CRITERIA_NAME as criteriaName,"
                + " a.DESCRIPTION as description,"
                + " a.CATEGORY_ID as categoryId"
                + " from kpi_processor as a"
                + " where a.CATEGORY_ID in (:categoryIds)";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("processorId", StandardBasicTypes.LONG);
        query.addScalar("processorName", StandardBasicTypes.STRING);
        query.addScalar("inputField", StandardBasicTypes.STRING);
        query.addScalar("criteriaName", StandardBasicTypes.STRING);
        query.addScalar("description", StandardBasicTypes.STRING);
        query.addScalar("categoryId", StandardBasicTypes.LONG);
        query.setParameterList("categoryIds", categoryIds);
        query.setResultTransformer(Transformers.aliasToBean(KpiProcessor.class));
        return query.list();
    }

}
