/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.entities.PreProcessValueMap;

/**
 *
 * @author admin
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class PreProcessValueMapDAOImpl extends BaseDAOImpl implements vn.viettel.campaign.dao.PreProcessValueMapDAO{

    @Override
    public void onSaveOrUpdatePreProcessValueMap(PreProcessValueMap preProcessValueMap) {
        getSession().saveOrUpdate(preProcessValueMap);
    }

    @Override
    public void onDeletePreProcessValueMap(PreProcessValueMap preProcessValueMap) {
       getSession().delete(preProcessValueMap);
    }
    
}
