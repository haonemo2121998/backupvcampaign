/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.AssessmentRulePreProcessUnitMap;

/**
 *
 * @author ABC
 */
public interface AssessmentRulePreProcessUnitMapDAO {

    public AssessmentRulePreProcessUnitMap createOrUpdateAssessmentRuleProcessUnitMap(AssessmentRulePreProcessUnitMap assessmentRulePreProcessUnitMap);

    public boolean deleteAssessmentRulePreProcessUnitMap(Long id);
    
    public AssessmentRulePreProcessUnitMap getNextAssessmentRulePreProcessUnitMap();
    
    public List<AssessmentRulePreProcessUnitMap> getAllAssessmentRulePreProcessUnitMapById(Long id);
    
    public AssessmentRulePreProcessUnitMap findAssessmentRulePreProcessUnitMapById(Long id);
}
