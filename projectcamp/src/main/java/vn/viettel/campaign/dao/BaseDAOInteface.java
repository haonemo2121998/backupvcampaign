package vn.viettel.campaign.dao;

import org.hibernate.Session;

import java.util.List;
import org.hibernate.criterion.Order;

/**
 * @author truongbx
 * @date 8/24/2019
 */
public interface BaseDAOInteface<T extends Object> {

    //find
    public T findById(Class<T> modelClass, long id);

    public List<T> findByIds(Class<T> modelClass, List<Long> ids);

    public List<T> getAll(Class<T> t);

    public List<T> getAll(Class<T> t, Order order);

    public String saveOrUpdate(T t);

    public String deleteById(Class<T> modelClass, long id);

    public String deleteById(Class<T> modelClass, long id, Session session);

    public String deleteByListIds(Session session, List<Long> ids, Class<T> modelClass);

    public String deleteByObject(T obj);

    public String deleteByObject(Session session, T obj);

    public String deleteListByObject(Session session, List<T> obj);

    public String update(T obj);

    public String update(Session session, T obj);

    public String update(Session session, List<T> lstObj);

    public String save(List<T> lstObj);

    public String save(T obj);

    public String save(Session session, List<T> lstObj);

    public String save(Session session, T obj);

    public long saveAndGetId(Session session, T obj);

    long saveAndGetId(T obj) throws Exception;
    
    public T doSaveOrUpdate(T o);
}
