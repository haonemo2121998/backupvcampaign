package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.dao.*;
import vn.viettel.campaign.entities.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class OcsBehaviourDAO extends BaseDAOImpl<OcsBehaviour> implements OcsBehaviourDAOInterface {
	@Autowired
	OCSBehaviourFieldDaoInterface oCSBehaviourFieldDAO;
	@Autowired
	OCSTemplateFieldDaoInterface oCSTemplateFieldDAO;
	@Autowired
	OCSBehaviourExtendFieldDaoInterface oCSBehaviourExtendFieldDAO;
	@Autowired
	OCSTemplateExtendFieldDaoInterface oCSTemplateExtendFieldDAO;
	@Autowired
	PostNotifyTriggerDaoInterface postNotifyTriggerDAO;

	@Override
	public OcsBehaviour getNextSequence() {
		String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"OCS_BEHAVIOUR\"";
		SQLQuery queryUpdate = getSession().createSQLQuery(update);
		queryUpdate.executeUpdate();
		String sql = "SELECT table_id behaviourId  from SEQ_TABLE WHERE TABLE_NAME = \"OCS_BEHAVIOUR\"";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("behaviourId", StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(OcsBehaviour.class));
		return (OcsBehaviour) query.uniqueResult();
	}

	@Override
	public String onSaveOCSBehaviour(OcsBehaviour ocsBehaviour, List<OcsBehaviourFields> lstField,
									 List<OcsBehaviourExtendFields> lstExtendField, List<PostNotifyTrigger> lstNotifyTrigger) {
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			save(session, ocsBehaviour);

			String resultField = oCSBehaviourFieldDAO.save(session, lstField);
			if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultField)) {
				throw new Exception();
			}
			String resultExtendField = oCSBehaviourExtendFieldDAO.save(session, lstExtendField);
			if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultExtendField)) {
				throw new Exception();
			}
			String resultPostNotify = postNotifyTriggerDAO.save(session, lstNotifyTrigger);
			if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultPostNotify)) {
				throw new Exception();
			}
			tx.commit();

		} catch (Exception ex) {
			ex.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
			return Responses.ERROR.getName();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return Responses.SUCCESS.getName();
	}

	@Override
	public String onUpdateOCSBehaviour(OcsBehaviour ocsBehaviour, List<OcsBehaviourFields> lstField,
									   List<OcsBehaviourExtendFields> lstExtendFields, List<PostNotifyTrigger> lstNotifyTrigger) {
		Session session = null;
		Transaction tx = null;

		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			update(session, ocsBehaviour);

			oCSBehaviourFieldDAO.deleteOCSBehaviourFieldByTemplateId(session, ocsBehaviour.getBehaviourId());
			String result = oCSBehaviourFieldDAO.save(session, lstField);
			if (!Responses.SUCCESS.getName().equalsIgnoreCase(result)) {
				throw new Exception();
			}
			oCSBehaviourExtendFieldDAO.deleteOcsBehaviourExtendFieldByOcsBehavourId(session, ocsBehaviour.getBehaviourId());
			String resultExtend = oCSBehaviourExtendFieldDAO.save(session, lstExtendFields);
			if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultExtend)) {
				throw new Exception();
			}
			postNotifyTriggerDAO.deletePostNotifyTrigger(session, ocsBehaviour.getBehaviourId());
			String resultPostNotify = postNotifyTriggerDAO.save(session, lstNotifyTrigger);
			if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultPostNotify)) {
				throw new Exception();
			}
			tx.commit();

		} catch (Exception ex) {
			ex.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
			return Responses.ERROR.getName();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return Responses.SUCCESS.getName();
	}

	@Override
	public boolean onDeleteOcsBehaviour(Long ocsBehaviourId) {
		Session session = null;
		Transaction tx = null;

		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			deleteById(OcsBehaviour.class, ocsBehaviourId, session);
			oCSBehaviourFieldDAO.deleteOCSBehaviourFieldByTemplateId(session, ocsBehaviourId);
			oCSBehaviourExtendFieldDAO.deleteOcsBehaviourExtendFieldByOcsBehavourId(session, ocsBehaviourId);
			postNotifyTriggerDAO.deletePostNotifyTrigger(session, ocsBehaviourId);
			tx.commit();

		} catch (Exception ex) {
			ex.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
			return false;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return true;
	}

	@Override
	public List<OcsBehaviourFields> getLstBehaviourField(Long behaviourId,Long templateId) {
		List<OcsBehaviourFields> lst = oCSBehaviourFieldDAO.getLstFieldOfOCSBehaviour(behaviourId,templateId);
		Map<Long,OcsBehaviourFields> mapBehaviourField = new HashMap<>();
		lst.forEach(e ->{
			mapBehaviourField.put(e.getCraFieldId(),e);
		});
		List<OcsBehaviourTemplateFields> ocsBehaviourTemplateFields = oCSTemplateFieldDAO.getLstFieldOfTemplate(templateId);
		lst.clear();
		ocsBehaviourTemplateFields.forEach(e ->{
			OcsBehaviourFields fields = mapBehaviourField.get(e.getCraFieldId());
			if (fields == null){
				lst.add(e.toBehaviourField(behaviourId));
			}else {
				if (fields.getTemplateType() == 1){
					fields.setValue(e.getValue());
				}
				lst.add(fields);
			}
		});
		return lst;
	}

	@Override
	public List<OcsBehaviourExtendFields> getLstBehaviourExtendField(Long behaviourId,Long templateId) {

		List<OcsBehaviourExtendFields> ocsBehaviourExtendFields=	oCSBehaviourExtendFieldDAO.getLstExtendFieldOfOcsBehaviour(behaviourId,templateId);
		Map<Long,OcsBehaviourExtendFields> mapBehaviourField = new HashMap<>();
		ocsBehaviourExtendFields.forEach(e ->{
			mapBehaviourField.put(e.getExtendFieldId(),e);
		});
		List<OcsBehaviourTemplateExtendFields> ocsBehaviourTemplateFields = oCSTemplateExtendFieldDAO.getLstExtendFieldOfTemplate(templateId);
		ocsBehaviourExtendFields.clear();
		ocsBehaviourTemplateFields.forEach(e ->{
			if (mapBehaviourField.get(e.getExtendFieldId()) == null){
				ocsBehaviourExtendFields.add(e.toBehaviourExtendField(behaviourId));
			}else {
				ocsBehaviourExtendFields.add(mapBehaviourField.get(e.getExtendFieldId()));
			}
		});
		return  ocsBehaviourExtendFields;
	}

	@Override
	public List<OcsBehaviourTemplateFields> getLstBehaviourTemplateField(Long templateId) {
		List<OcsBehaviourTemplateFields> ocsBehaviourTemplateFields = oCSTemplateFieldDAO.getLstFieldOfTemplate(templateId);
		return ocsBehaviourTemplateFields;
	}

	@Override
	public List<OcsBehaviourTemplateExtendFields> getLstBehaviourTemplateExtendField(Long templateId) {
		List<OcsBehaviourTemplateExtendFields> ocsBehaviourTemplateFields = oCSTemplateExtendFieldDAO.getLstExtendFieldOfTemplate(templateId);
		return ocsBehaviourTemplateFields;
	}

	@Override
	public boolean checkExistOCSBehaviourName(String behaviourName, Long behaviourId) {
		List<Campaign> lst = getSession().createQuery("from OcsBehaviour where lower(behaviourName) = :behaviourName and behaviourId <> :behaviourId ")
				.setParameter("behaviourName", behaviourName.trim().toLowerCase())
				.setParameter("behaviourId", behaviourId)
				.list();
		if (lst != null && lst.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<OcsBehaviour> getLstBehaviourByNotifyId(Long notifyId) {
		List<OcsBehaviour> lst = getSession().createQuery("from OcsBehaviour where notifyId = :notifyId ")
				.setParameter("notifyId", notifyId)
				.list();
		return lst;
	}

	@Override
	public boolean checkOcsUseInUSSDScenario(Long ocsId) {
		List<ScenarioNode> lstObj = getSession().createQuery("from ScenarioNode where  ocsBehaviourId =:ocsId")
				.setParameter("ocsId", ocsId)
				.list();
		if (lstObj == null || lstObj.isEmpty()) {
			return false;
		}
		return true;
	}

	@Override
	public boolean checkOcsUseInPromotionBlock(Long ocsId) {
		List<BlockContainer> lstObj = getSession().createQuery("from BlockContainer where promType = 1 and  promId =:ocsId")
				.setParameter("ocsId", ocsId)
				.list();
		if (lstObj == null || lstObj.isEmpty()) {
			return false;
		}
		return true;
	}
}
