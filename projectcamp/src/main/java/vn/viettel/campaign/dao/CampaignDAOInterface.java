package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.*;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface CampaignDAOInterface extends BaseDAOInteface<Campaign> {
	Campaign getNextSequence();
	public String onSaveNewCampaign(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
									List<InvitationPriority> lstInvitation);

	public String onUpdateCampaign(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
								   List<InvitationPriority> lstInvitation);

	public String onUpdateCampaignOffline(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
										  List<InvitationPriority> lstInvitation, List<CampaignSchedule> campaignSchedules);

	public String onSaveNewCampaignOffline(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
										   List<InvitationPriority> lstInvitation, List<CampaignSchedule> lstCampaignSchedules);

	public boolean checkExisCampaignName(String campaignName, Long campaignId);

	public boolean deleteCampaign(Long campaignId);

	public boolean deleteCampaignOffline(Long campaignId);
        
        public Campaign findCampaignById(Long campaignId);
        
}
