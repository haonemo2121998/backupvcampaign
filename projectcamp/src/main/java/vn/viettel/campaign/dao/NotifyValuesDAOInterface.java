package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.NotifyValues;
import vn.viettel.campaign.entities.StatisticCycle;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface NotifyValuesDAOInterface extends BaseDAOInteface<NotifyValues>{
    public List<NotifyValues> findByNotifyTriggerId(long id);
    public void deleteByNotifyTriggerId(long id);
}
