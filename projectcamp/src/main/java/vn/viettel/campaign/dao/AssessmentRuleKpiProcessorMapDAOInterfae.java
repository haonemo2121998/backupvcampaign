/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.AssessmentRuleKpiProcessorMap;

/**
 *
 * @author ABC
 */
public interface AssessmentRuleKpiProcessorMapDAOInterfae {
    
    public AssessmentRuleKpiProcessorMap createOrUpdateAssessmentRuleKpiProcessorMap(AssessmentRuleKpiProcessorMap assessmentRuleKpiProcessorMap);
    
    public boolean deleteAssessmentRuleKpiProcessorMap(Long id);
    
    public List<AssessmentRuleKpiProcessorMap> getAlllistAssessmentRuleKpiProcessorMapById(Long id);
    
    public AssessmentRuleKpiProcessorMap getNextAssessmentRuleKpiProcessorMap();
    
    public AssessmentRuleKpiProcessorMap findAssessmentRuleKpiProcessorMapById(Long id);
}
