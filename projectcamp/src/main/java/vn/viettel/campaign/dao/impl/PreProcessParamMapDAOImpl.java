/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.entities.PreProcessParamMap;

/**
 *
 * @author admin
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class PreProcessParamMapDAOImpl extends BaseDAOImpl implements vn.viettel.campaign.dao.PreProcessParamMapDAO{

    @Override
    public void onSaveOrUpdatePreProcessParamMap(PreProcessParamMap preProcessParamMap) {
        getSession().saveOrUpdate(preProcessParamMap);
    }

    @Override
    public void onDeletePreProcessParamMap(PreProcessParamMap preProcessParamMap) {
       getSession().delete(preProcessParamMap);
    }
    
}
