/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.viettel.campaign.dao.RolesObjectsDao;
import static vn.viettel.campaign.dao.impl.BaseDAOImpl.getLog;
import vn.viettel.campaign.entities.RoleObject;

@Repository
@Transactional(rollbackFor = Exception.class)
public class RolesObjectsDaoImpl implements RolesObjectsDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Boolean saveRoleObject(RoleObject ro) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(ro);
        return true;
    }

    @Override
    public Boolean updateRoleObject(RoleObject obj) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(obj);
        return true;
    }

    @Override
    public void deleteRoleObjectByRoleId(Long roleId) {
        Session session = this.sessionFactory.getCurrentSession();
        session.createQuery("update RoleObject set isActive = 0 where roleId = :roleId")
                .setParameter("roleId", roleId).executeUpdate();
    }

    @Override
    public void deleteRoleObjectByObjectId(Long objectId) {
        Session session = this.sessionFactory.getCurrentSession();
        session.createQuery("update RoleObject set isActive = 0 where objectId = :objectId")
                .setParameter("objectId", objectId).executeUpdate();
    }

    @Override
    public List<RoleObject> findByRoleId(final Long roleId) {
        if (Objects.isNull(roleId)) {
            return new ArrayList<>();
        }
        try {
            Session session = this.sessionFactory.getCurrentSession();
            final String sql = "from RoleObject where roleId = :roleId ";
            Query query = session.createQuery(sql);
            query.setParameter("roleId", roleId);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }
}
