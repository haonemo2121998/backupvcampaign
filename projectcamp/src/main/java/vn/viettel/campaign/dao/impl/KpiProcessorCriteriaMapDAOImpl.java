/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import vn.viettel.campaign.dao.KpiProcessorCriteriaMapDAO;
import vn.viettel.campaign.entities.KpiProcessorCriteriaValueMap;

/**
 *
 * @author ABC
 */
@Repository
public class KpiProcessorCriteriaMapDAOImpl extends BaseDAOImpl<KpiProcessorCriteriaValueMap> implements KpiProcessorCriteriaMapDAO {

    @Override
    public List<KpiProcessorCriteriaValueMap> getListkpiProcessorCriteriaValueMapByProsessorId(Long processorId) {
        String sql = "select"
                + " a.ID as id,"
                + " a.PROCESSOR_ID as processorId,"
                + " a.CRITERIA_VALUE_ID as criteriaValueId"
                + " from kpi_processor_criteria_value_map as a"
                + " where a.PROCESSOR_ID=:processorId";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("id", StandardBasicTypes.LONG);
        query.addScalar("processorId", StandardBasicTypes.LONG);
        query.addScalar("criteriaValueId", StandardBasicTypes.LONG);
        query.setParameter("processorId", processorId);
        query.setResultTransformer(Transformers.aliasToBean(KpiProcessorCriteriaValueMap.class));
        return query.list();

    }

}
