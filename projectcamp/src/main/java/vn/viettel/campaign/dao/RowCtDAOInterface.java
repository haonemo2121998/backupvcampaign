package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.RowCt;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface RowCtDAOInterface extends BaseDAOInteface<RowCt> {
    List<RowCt> getLstRowCtByConditionTableId(long conditionTableId);

    public void deleteByConditionTableId(Long conditionTableId);
}