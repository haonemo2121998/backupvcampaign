package vn.viettel.campaign.dao;

import java.util.Date;

public interface CommonDAO {

    Date getSysDate() throws Exception;

    Date getSysDateWithoutTime() throws Exception;

    Date getFirstDayOfMonth() throws Exception;

    Date getFirstDayOfMonth(Date checkDate) throws Exception;

    Date getNextDateWithoutTime() throws Exception;

    Date getFirstDateOfMonth() throws Exception;

    Date getLastDate(Date date) throws Exception;

    /**
     *
     * @param id
     * @return
     * @throws com.vietsens.ws.api.exception.Exception
     */
    String generateCodeFromId(Integer id) throws Exception;

    Date addDate(Date date, Integer numPlus) throws Exception;

    Date addDate(Date date, Long numPlus) throws Exception;

    Date getPreviousDateWithoutTime() throws Exception;

}
