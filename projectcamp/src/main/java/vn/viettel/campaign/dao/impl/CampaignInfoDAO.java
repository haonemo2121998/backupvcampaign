package vn.viettel.campaign.dao.impl;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.CampaignInfoDAOInterface;
import vn.viettel.campaign.entities.CampaignInfo;

import javax.persistence.Query;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class CampaignInfoDAO extends BaseDAOImpl<CampaignInfo> implements CampaignInfoDAOInterface {
	@Override
	public void deleteCampaignInforByCampaignId(Session session, Long campaignId) {
		String hql = "delete from CampaignInfo where campaignId =:campaignId";
		Query query = session.createQuery(hql);
		query.setParameter("campaignId", campaignId);
		Integer num = query.executeUpdate();
		getLog().info(" number row delete at " + campaignId + " is " + num);
	}
}
