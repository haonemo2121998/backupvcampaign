package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.ColumnCt;
import vn.viettel.campaign.entities.Rule;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface ColumnCTDAOInterface extends BaseDAOInteface<ColumnCt> {
    List<ColumnCt> getLstColumnctByConditionTableId(Long conditionId);

    public void deleteByConditionTableId(Long conditionTableId);
}
