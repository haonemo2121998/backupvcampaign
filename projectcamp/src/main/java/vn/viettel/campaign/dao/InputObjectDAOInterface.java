package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.InputObject;
import vn.viettel.campaign.entities.PreProcessUnit;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface InputObjectDAOInterface extends BaseDAOInteface<InputObject>{
	List<InputObject> getLstCondition(long inputObjectId);
}
