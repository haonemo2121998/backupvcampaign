package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.ConditionTable;
import vn.viettel.campaign.entities.NotifyStatisticMap;
import vn.viettel.campaign.entities.StatisticCycle;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface NotifyStatisticMapDAOInterface extends BaseDAOInteface<NotifyStatisticMap>{
    public void deleteByNotifyTriggerId(long id);
    public List<NotifyStatisticMap> findByNotifyIdAndCycleId(long notifyId, long cycleId);
}
