/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.FuzzyRule;

/**
 *
 * @author ABC
 */
public interface FuzzyRuleDAOInterface {

    FuzzyRule getNextSequenceFuzzyRule();

    public List<FuzzyRule> getListFuzzyRuleByOwnerIdAndCampaignEvaluationInfoId(Long campaignId, Long campaignEvaluationInfoId);
    
    public List<FuzzyRule> getListFuzzyRuleByCampaignEvaluationInfoId(Long campaignEvaluationInfoId);

    public List<FuzzyRule> getListFuzzyRuleOfSegment(Long segmentId, Long campaignEvaluationInfoId);

    public FuzzyRule doCreateOrUpdateFuzzyRule(FuzzyRule fuzzyRule);

    public boolean deleteFuzzyRule(Long id);

    public List<FuzzyRule> getListFuzzyByRuleId(Long ruleId);
    

}
