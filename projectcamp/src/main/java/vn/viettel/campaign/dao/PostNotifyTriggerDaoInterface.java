package vn.viettel.campaign.dao;

import org.hibernate.Session;
import vn.viettel.campaign.entities.OcsBehaviourTemplate;
import vn.viettel.campaign.entities.OcsBehaviourTemplateExtendFields;
import vn.viettel.campaign.entities.OcsBehaviourTemplateFields;
import vn.viettel.campaign.entities.PostNotifyTrigger;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface PostNotifyTriggerDaoInterface extends BaseDAOInteface<PostNotifyTrigger>{
	List<PostNotifyTrigger> getLstPostNotigyTriggerByOcsBehaviourId(Long behaviourId);
	List<PostNotifyTrigger> getLstPostNotigyTriggerByNotifyId(Long notifyId);
	public void deletePostNotifyTrigger(Session session, Long ocsBehaviourId);
}
