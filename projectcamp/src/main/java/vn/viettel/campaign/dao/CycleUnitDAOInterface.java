package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.CycleUnit;
import vn.viettel.campaign.entities.NotifyValues;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface CycleUnitDAOInterface extends BaseDAOInteface<CycleUnit>{
}
