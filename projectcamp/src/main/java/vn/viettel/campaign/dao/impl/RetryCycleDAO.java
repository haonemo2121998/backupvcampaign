package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.CycleUnitDAOInterface;
import vn.viettel.campaign.dao.RetryCycleDAOInterface;
import vn.viettel.campaign.entities.CycleUnit;
import vn.viettel.campaign.entities.RetryCycle;

/**
 * @author truongbx
 * @date 09/01/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class RetryCycleDAO extends BaseDAOImpl<RetryCycle> implements RetryCycleDAOInterface {


}
