package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.InputObjectDAOInterface;
import vn.viettel.campaign.entities.InputObject;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class InputObjectDAO extends BaseDAOImpl<InputObject> implements InputObjectDAOInterface {
	@Override
	public List<InputObject> getLstCondition(long objectParentId) {
		List<InputObject> lstObj = getSession().createQuery("from InputObject where objectParentId = :objectParentId and objectDataType <> '1' order by objectName asc ")
				.setParameter("objectParentId", objectParentId)
				.list();
		return lstObj;
	}
}
