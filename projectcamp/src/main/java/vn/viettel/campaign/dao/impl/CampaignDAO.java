package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.dao.CampaignDAOInterface;
import vn.viettel.campaign.dao.CampaignInfoDAOInterface;
import vn.viettel.campaign.dao.CampaignScheduleDAOInterface;
import vn.viettel.campaign.dao.InvitationDAOInterface;
import vn.viettel.campaign.entities.*;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class CampaignDAO extends BaseDAOImpl<Campaign> implements CampaignDAOInterface {

    @Autowired
    CampaignInfoDAOInterface campaignInfoDAO;
    @Autowired
    InvitationDAOInterface invitationDAO;
    @Autowired
    CampaignScheduleDAOInterface campaignScheduleDAO;

    @Override
    public String onSaveNewCampaign(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
            List<InvitationPriority> lstInvitation) {
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String campaignId = save(session, campaign);
            Long id = Long.parseLong(campaignId);
            for (CampaignInfo campaignInfo : lstCampaignInfo) {
                campaignInfo.setCampaignId(id);
            }
            String result = campaignInfoDAO.save(session, lstCampaignInfo);
            if (!Responses.SUCCESS.getName().equalsIgnoreCase(result)) {
                throw new Exception();
            }
            for (InvitationPriority invitationPriority : lstInvitation) {
                invitationPriority.setCampaignId(id);
            }
            String resultInvi = invitationDAO.save(session, lstInvitation);
            if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultInvi)) {
                throw new Exception();
            }
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return Responses.ERROR.getName();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return Responses.SUCCESS.getName();
    }

    @Override
    public String onUpdateCampaign(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
            List<InvitationPriority> lstInvitation) {
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            update(session, campaign);
            campaignInfoDAO.deleteCampaignInforByCampaignId(session, campaign.getCampaignId());
            String result = campaignInfoDAO.save(session, lstCampaignInfo);
            if (!Responses.SUCCESS.getName().equalsIgnoreCase(result)) {
                throw new Exception();
            }
            invitationDAO.deleteInviByCampaignId(session, campaign.getCampaignId());
            String resultInvi = invitationDAO.save(session, lstInvitation);
            if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultInvi)) {
                throw new Exception();
            }
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return Responses.ERROR.getName();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return Responses.SUCCESS.getName();
    }

    @Override
    public boolean checkExisCampaignName(String campaignName, Long campaignId) {
        List<Campaign> lst = getSession().createQuery("from Campaign where lower(name) = :campaignName and campaignId <> :campaignId ")
                .setParameter("campaignName", campaignName.trim().toLowerCase())
                .setParameter("campaignId", campaignId)
                .list();
        if (lst != null && lst.size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteCampaign(Long campaignId) {
        Session session = null;
        Transaction tx = null;

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            deleteById(Campaign.class, campaignId, session);
            campaignInfoDAO.deleteCampaignInforByCampaignId(session, campaignId);
            invitationDAO.deleteInviByCampaignId(session, campaignId);
            tx.commit();

        } catch (Exception ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return false;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return true;
    }

    @Override
    public String onUpdateCampaignOffline(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
            List<InvitationPriority> lstInvitation, List<CampaignSchedule> campaignSchedules) {
        Session session = null;
        Transaction tx = null;

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            update(session, campaign);

            campaignInfoDAO.deleteCampaignInforByCampaignId(session, campaign.getCampaignId());
            String result = campaignInfoDAO.save(session, lstCampaignInfo);
            if (!Responses.SUCCESS.getName().equalsIgnoreCase(result)) {
                throw new Exception();
            }
            campaignScheduleDAO.deleteCampaignScheduleByCampaignId(session, campaign.getCampaignId());
            String resultSchedule = campaignScheduleDAO.save(session, campaignSchedules);
            if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultSchedule)) {
                throw new Exception();
            }
            invitationDAO.deleteInviByCampaignId(session, campaign.getCampaignId());
            String resultInvi = invitationDAO.save(session, lstInvitation);
            if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultInvi)) {
                throw new Exception();
            }
            tx.commit();

        } catch (Exception ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return Responses.ERROR.getName();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return Responses.SUCCESS.getName();
    }

    @Override
    public String onSaveNewCampaignOffline(Campaign campaign, List<CampaignInfo> lstCampaignInfo,
            List<InvitationPriority> lstInvitation, List<CampaignSchedule> lstCampaignSchedules) {
        Session session = null;
        Transaction tx = null;

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            String campaignId = save(session, campaign);
            Long id = Long.parseLong(campaignId);
            for (CampaignInfo campaignInfo : lstCampaignInfo) {
                campaignInfo.setCampaignId(id);
            }

            String result = campaignInfoDAO.save(session, lstCampaignInfo);
            if (!Responses.SUCCESS.getName().equalsIgnoreCase(result)) {
                throw new Exception();
            }

            for (CampaignSchedule campaignSchedule : lstCampaignSchedules) {
                campaignSchedule.setCampaignId(id);
            }
            String resultSchedule = campaignScheduleDAO.save(session, lstCampaignSchedules);
            if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultSchedule)) {
                throw new Exception();
            }

            for (InvitationPriority invitationPriority : lstInvitation) {
                invitationPriority.setCampaignId(id);
            }
            String resultInvi = invitationDAO.save(session, lstInvitation);
            if (!Responses.SUCCESS.getName().equalsIgnoreCase(resultInvi)) {
                throw new Exception();
            }
            tx.commit();

        } catch (Exception ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return Responses.ERROR.getName();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return Responses.SUCCESS.getName();
    }

    @Override
    public boolean deleteCampaignOffline(Long campaignId) {
        Session session = null;
        Transaction tx = null;

        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            deleteById(Campaign.class, campaignId, session);
            campaignInfoDAO.deleteCampaignInforByCampaignId(session, campaignId);
            campaignScheduleDAO.deleteCampaignScheduleByCampaignId(session, campaignId);
            invitationDAO.deleteInviByCampaignId(session, campaignId);
            tx.commit();

        } catch (Exception ex) {
            ex.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return false;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return true;
    }

    @Override
    public Campaign getNextSequence() {
        String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"CAMPAIGN\"";
        SQLQuery queryUpdate = getSession().createSQLQuery(update);
        queryUpdate.executeUpdate();
        String sql = "SELECT table_id campaignId  from SEQ_TABLE WHERE TABLE_NAME = \"CAMPAIGN\"";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("campaignId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(Campaign.class));
        
        return (Campaign) query.uniqueResult();
    }

    @Override
    public Campaign findCampaignById(Long campaignId) {
       Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            findById(Campaign.class, campaignId);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return findById(Campaign.class, campaignId);
    }
}
