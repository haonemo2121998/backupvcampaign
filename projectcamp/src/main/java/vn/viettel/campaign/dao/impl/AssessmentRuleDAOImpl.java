/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.AssessmentRuleDAOInterface;
import vn.viettel.campaign.entities.AssessmentRule;
import vn.viettel.campaign.entities.AssessmentRulePreProcessUnitMap;

/**
 *
 * @author ABC
 */
@Repository
public class AssessmentRuleDAOImpl extends BaseDAOImpl<AssessmentRule> implements AssessmentRuleDAOInterface {


    @Override
    public AssessmentRule createOrUpdateAssessmenRule(AssessmentRule assessmentRule) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            doSaveOrUpdate(assessmentRule);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return assessmentRule;
    }

    @Override
    public boolean deleteAssessmentRule(Long ruleId) {
         Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            deleteById(AssessmentRule.class,ruleId);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    @Override
    public AssessmentRule getNextAssessmentRule() {
       String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"ASSESSMENT_RULE\"";
        SQLQuery queryUpdate = getSession().createSQLQuery(update);
        queryUpdate.executeUpdate();
        String sql = "SELECT table_id ruleId from SEQ_TABLE WHERE TABLE_NAME = \"ASSESSMENT_RULE\"";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("ruleId", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(AssessmentRule.class));
        return (AssessmentRule) query.uniqueResult();
    }

    @Override
    public AssessmentRule findAssessmentRuleById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            findById(AssessmentRule.class, id);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return findById(AssessmentRule.class, id);
    }

}
