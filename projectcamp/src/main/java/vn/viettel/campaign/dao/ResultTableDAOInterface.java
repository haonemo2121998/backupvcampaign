package vn.viettel.campaign.dao;

import vn.viettel.campaign.dto.ResultTableDTO;
import vn.viettel.campaign.entities.ResultTable;
import vn.viettel.campaign.entities.Rule;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface ResultTableDAOInterface extends BaseDAOInteface<ResultTable>{
	List<ResultTable> getLstRuleResultTableByRuleIds(Long ruleId);
	List<ResultTable> getLstRuleResultTableByConditionTableIds(Long conditionTableId);
	List<ResultTable> getAllResultTable();
	List<ResultTableDTO> getListResultTableDTO(Long conditionTableId, Long rowIndex);
	int updateByCoditionTableId(Long conditionTableId, List<Long> ids, Long index);
	int deleteByCoditionTableId();

}
