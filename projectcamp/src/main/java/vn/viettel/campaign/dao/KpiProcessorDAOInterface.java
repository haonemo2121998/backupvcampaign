/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.KpiProcessor;

/**
 *
 * @author ABC
 */
public interface KpiProcessorDAOInterface {

    public List<KpiProcessor> getListKpiProcessorByRuleId(Long ruleId);

    public List<KpiProcessor> findAllKpiProcessor(List<Long> categoryIds);

}
