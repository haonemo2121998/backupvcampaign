/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.SegmentEvaluationInfo;

/**
 *
 * @author ABC
 */
public interface SegmentEvaluationInfoDAOInterface {
    
    public List<SegmentEvaluationInfo> getListSegmentEvaluationInfo(Long campaignEvaluationInfoId);
    
    public SegmentEvaluationInfo doCreateOrUpdateSegmentEvaluationInfo(SegmentEvaluationInfo segmentEvaluationInfo);
    
    public boolean deleteSegmentEvaluationInfo(Long id);
    
    public SegmentEvaluationInfo getNextSequenceSegmentEvaluationInfo();
    
    public List<SegmentEvaluationInfo> getListSegmentEvaluationInfoMapAssessment(Long assessmentRuleId);
    
}
