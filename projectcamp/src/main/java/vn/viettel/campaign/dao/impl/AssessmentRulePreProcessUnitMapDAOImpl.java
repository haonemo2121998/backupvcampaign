/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.AssessmentRulePreProcessUnitMapDAO;
import vn.viettel.campaign.entities.AssessmentRulePreProcessUnitMap;

/**
 *
 * @author ABC
 */
@Repository
public class AssessmentRulePreProcessUnitMapDAOImpl extends BaseDAOImpl<AssessmentRulePreProcessUnitMap> implements AssessmentRulePreProcessUnitMapDAO {

    @Override
    public AssessmentRulePreProcessUnitMap createOrUpdateAssessmentRuleProcessUnitMap(AssessmentRulePreProcessUnitMap assessmentRulePreProcessUnitMap) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            doSaveOrUpdate(assessmentRulePreProcessUnitMap);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return assessmentRulePreProcessUnitMap;
    }

    @Override
    public boolean deleteAssessmentRulePreProcessUnitMap(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            deleteById(AssessmentRulePreProcessUnitMap.class, id);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    @Override
    public AssessmentRulePreProcessUnitMap getNextAssessmentRulePreProcessUnitMap() {
        String update = " UPDATE SEQ_TABLE set TABLE_ID = TABLE_ID + 1 WHERE TABLE_NAME = \"ASSESSMENT_RULE_PREPROCESS_UNIT_MAP\"";
        SQLQuery queryUpdate = getSession().createSQLQuery(update);
        queryUpdate.executeUpdate();
        String sql = "SELECT table_id Id from SEQ_TABLE WHERE TABLE_NAME = \"ASSESSMENT_RULE_PREPROCESS_UNIT_MAP\"";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("Id", StandardBasicTypes.LONG);
        query.setResultTransformer(Transformers.aliasToBean(AssessmentRulePreProcessUnitMap.class));
        return (AssessmentRulePreProcessUnitMap) query.uniqueResult();
    }

    @Override
    public List<AssessmentRulePreProcessUnitMap> getAllAssessmentRulePreProcessUnitMapById(Long id) {
         String sql ="select"
                + " a.ID as id,"
                + " a.RULE_ID as ruleId,"
                + " a.PRE_PROCESS_UNIT_ID as preProcessUnitId"
                + " from assessment_rule_pre_process_unit_map as a"
                + " where a.RULE_ID=:id";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.addScalar("id", StandardBasicTypes.LONG);
        query.addScalar("ruleId", StandardBasicTypes.LONG);
        query.addScalar("preProcessUnitId", StandardBasicTypes.LONG);
        query.setParameter("id", id);
        query.setResultTransformer(Transformers.aliasToBean(AssessmentRulePreProcessUnitMap.class));
        return query.list();
    }

    @Override
    public AssessmentRulePreProcessUnitMap findAssessmentRulePreProcessUnitMapById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            findById(AssessmentRulePreProcessUnitMap.class, id);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
            return null;
        } finally {
            session.close();
        }
        return findById(AssessmentRulePreProcessUnitMap.class, id);
    }

}
