package vn.viettel.campaign.dao;

import java.util.List;
import vn.viettel.campaign.entities.RuleResult;


/**
 *
 * @author ConKC
 */
public interface RuleResultDao {
    List<RuleResult> finAllRuleResult();
    RuleResult updateOrSave(RuleResult ruleResult);
    RuleResult getRuleResultById(Long ruleResultId);
    void deleteByResultTableId(long id);
}
