package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.ConditionTableDAOInterface;
import vn.viettel.campaign.dao.ResultTableDAOInterface;
import vn.viettel.campaign.entities.CampaignSchedule;
import vn.viettel.campaign.entities.ConditionTable;
import vn.viettel.campaign.entities.ResultTable;

import java.util.List;

/**
 * @author truongbx
 * @date 09/01/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class ConditionTableDAO extends BaseDAOImpl<ConditionTable> implements ConditionTableDAOInterface {

    @Override
    public ConditionTable getDataConditionTableByName(String name, long conditionTableId) {
        List<ConditionTable> lstObj = getSession().createQuery("from ConditionTable where conditionTableName = :name and conditionTableId != :conditionTableId")
                .setParameter("name", name).setParameter("conditionTableId", conditionTableId)
                .list();
        return !DataUtil.isNullOrEmpty(lstObj) ? lstObj.get(0) : null;
    }
}
