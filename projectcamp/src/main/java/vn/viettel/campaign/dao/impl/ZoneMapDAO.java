package vn.viettel.campaign.dao.impl;

import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.ZoneDAOInterface;
import vn.viettel.campaign.dao.ZoneMapDAOInterface;
import vn.viettel.campaign.entities.Zone;
import vn.viettel.campaign.entities.ZoneMap;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class ZoneMapDAO extends BaseDAOImpl<ZoneMap> implements ZoneMapDAOInterface {

}
