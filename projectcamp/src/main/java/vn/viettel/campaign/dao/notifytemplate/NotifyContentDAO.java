package vn.viettel.campaign.dao.notifytemplate;

import vn.viettel.campaign.entities.NotifyContent;

import java.util.List;

public interface NotifyContentDAO {
    List<NotifyContent> getAllByTemplateId(Long templateId);
    List<NotifyContent> getlstByNotifyId(Long notifyId);
    NotifyContent save(NotifyContent content);
    void removeContent(Long contentId);
    void deleteAllByTemplateId(Long templateId);
    List<NotifyContent> saveContents(List<NotifyContent> contents);
}
