package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.ConditionTable;
import vn.viettel.campaign.entities.ResultTable;

import java.util.List;

/**
 * @author truongbx
 * @date 8/31/2019
 */
public interface ConditionTableDAOInterface extends BaseDAOInteface<ConditionTable>{
    public ConditionTable getDataConditionTableByName(String name, long conditionTableId);
}
