package vn.viettel.campaign.dao;

import org.hibernate.Session;
import vn.viettel.campaign.entities.OcsBehaviourExtendFields;
import vn.viettel.campaign.entities.OcsBehaviourTemplateExtendFields;

import java.util.List;

/**
 * @author truongbx
 * @date 8/25/2019
 */
public interface OCSBehaviourExtendFieldDaoInterface extends BaseDAOInteface<OcsBehaviourExtendFields>{
	public List<OcsBehaviourExtendFields> getLstExtendFieldOfOcsBehaviour(long ocsBehaviourId,Long templateId) ;
	public void deleteOcsBehaviourExtendFieldByOcsBehavourId(Session session, Long ocsBehaviourId);
}

