package vn.viettel.campaign.dao;

import org.hibernate.Session;
import vn.viettel.campaign.entities.PreProcessParamMap;
import vn.viettel.campaign.entities.ProcessParam;

import java.util.List;

/**
 *
 */
public interface PreProcessParamMapDAOInterface extends BaseDAOInteface<PreProcessParamMap> {
	public void deletePreProcessParamMapByPPUid(Session session, Long ppuid);
}
