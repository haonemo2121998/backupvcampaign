/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao;

import vn.viettel.campaign.entities.AssessmentRule;

/**
 *
 * @author ABC
 */
public interface AssessmentRuleDAOInterface {

    public AssessmentRule createOrUpdateAssessmenRule(AssessmentRule assessmentRule);

    public boolean deleteAssessmentRule(Long ruleId);
    
    public AssessmentRule getNextAssessmentRule();
    
    public AssessmentRule findAssessmentRuleById(Long id); 
    
}
