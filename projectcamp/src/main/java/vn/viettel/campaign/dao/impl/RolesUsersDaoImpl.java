/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.dao.RolesUsersDao;
import static vn.viettel.campaign.dao.impl.BaseDAOImpl.getLog;
import vn.viettel.campaign.entities.RoleUser;

@Repository
@Transactional(rollbackFor = Exception.class)
public class RolesUsersDaoImpl implements RolesUsersDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Boolean saveRoleUser(RoleUser obj) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(obj);
        return true;
    }

    @Override
    public Boolean updateRoleUser(RoleUser obj) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(obj);
        return true;
    }

    @Override
    public void deleteRoleUserByUserId(Long userId) {
        Session session = this.sessionFactory.getCurrentSession();
        session.createQuery("update RoleUser set isActive = 0 where userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void deleteRoleUserByRoleId(Long roleId) {
        Session session = this.sessionFactory.getCurrentSession();
        session.createQuery("update RoleUser set isActive = 0 where roleId = :roleId")
                .setParameter("roleId", roleId).executeUpdate();
    }

    @Override
    public List<RoleUser> findByUserId(final Long usersId) {
        if (Objects.isNull(usersId)) {
            return new ArrayList<>();
        }
        try {
            Session session = this.sessionFactory.getCurrentSession();
            final String sql = "from RoleUser where userId = :userId ";
            Query query = session.createQuery(sql);
            query.setParameter("userId", usersId);
            query.setCacheable(true);
            return query.list();
        } catch (Exception ex) {
            getLog().error("Exception " + ex);
        }
        return new ArrayList<>();
    }
}
