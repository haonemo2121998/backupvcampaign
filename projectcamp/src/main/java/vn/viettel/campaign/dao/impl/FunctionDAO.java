package vn.viettel.campaign.dao.impl;

import java.util.ArrayList;
import org.springframework.stereotype.Repository;
import vn.viettel.campaign.dao.FunctionDAOInterface;
import vn.viettel.campaign.dao.InputObjectDAOInterface;
import vn.viettel.campaign.entities.Function;
import vn.viettel.campaign.entities.InputObject;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;
import vn.viettel.campaign.entities.Category;

/**
 * @author truongbx
 * @date 8/25/2019
 */
@Repository
public class FunctionDAO extends BaseDAOImpl<Function> implements FunctionDAOInterface {

    @Override
    public List<Function> getListFunction() {
        List<Function> lstObj = (List<Function>) getSession().createQuery("from Function where type = :type order by functionName ASC ")
                .setParameter("type", 2L)
                .setCacheable(true)
                .list();
        return lstObj;
    }

    @Override
    public List<Function> getListFunctionTypeOne() {
        List<Function> lstObj = (List<Function>) getSession().createQuery("from Function where type = :type order by functionName ASC  ")
                .setParameter("type", 1L)
                .setCacheable(true)
                .list();
        return lstObj;
    }

}
