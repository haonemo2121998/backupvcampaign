package vn.viettel.campaign.dao.impl;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.dao.NotifyTriggerDAOInterface;
import vn.viettel.campaign.dao.RuleDAOInterface;
import vn.viettel.campaign.entities.Category;
import vn.viettel.campaign.entities.NotifyTemplate;
import vn.viettel.campaign.entities.NotifyTrigger;
import vn.viettel.campaign.entities.Rule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author truongbx
 * @date 8/26/2019
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class NotifyTriggerDAO extends BaseDAOImpl<NotifyTrigger> implements NotifyTriggerDAOInterface {
	@Override
	public List<NotifyTrigger> getNotifyTriggerByRuleIds(List<Long> ruleIds) {
		String sql = "SELECT DISTINCT  h.NOTIFY_TRIGGER_ID notifyTriggerId,h.TRIGGER_NAME triggerName,h.NOTIFY_TYPE notifyType , " +
				"h.ALIAS alias, h.NOTIFY_TEMPLATE_ID notifyTemplateId, h.IS_INVITE isInvite , h.NUMBER_RETRY numberRetry,  " +
				"h.REPEAT_TIME repeatTime,h.DESCRIPTION description, h.CATEGORY_ID categoryId,h.CHANNEL_TYPE channelType, " +
				"h.RETRY_CYCLE_ID retryCycleId " +
				"FROM rule_result_table_map a  " +
				"INNER JOIN result_table b  " +
				"INNER JOIN result_table_rule_result_map c  " +
				"INNER JOIN rule_result d  " +
				"INNER JOIN result e  " +
				"INNER JOIN promotion_block f  " +
				"INNER JOIN block_container g  " +
				"INNER JOIN notify_trigger h  " +
				"on a.RESULT_TABLE_ID = b.RESULT_TABLE_ID  " +
				"AND b.RESULT_TABLE_ID = c.RESULT_TABLE_ID  " +
				"  AND c.RULE_RESULT_ID =   d.RULE_RESULT_ID  " +
				"  AND d.RESULT_ID = e.RESULT_ID  " +
				"  AND e.PROMOTION_BLOCK_ID  = f.PROMOTION_BLOCK_ID   " +
				"\tAND f.PROMOTION_BLOCK_ID = g.PROMOTION_BLOCK_ID  " +
				"\tAND g.PROM_ID = h.NOTIFY_TRIGGER_ID  " +
				"  WHERE g.PROM_TYPE = '2'   " +
				"  AND a. rule_id in =:ruleId1   " +
				"UNION  " +
				"SELECT DISTINCT  h.NOTIFY_TRIGGER_ID notifyTriggerId,h.TRIGGER_NAME triggerName,h.NOTIFY_TYPE notifyType ," +
				"h.ALIAS alias, h.NOTIFY_TEMPLATE_ID notifyTemplateId, h.IS_INVITE isInvite , h.NUMBER_RETRY numberRetry, " +
				"h.REPEAT_TIME repeatTime,h.DESCRIPTION description, h.CATEGORY_ID categoryId,h.CHANNEL_TYPE channelType," +
				"h.RETRY_CYCLE_ID retryCycleId   " +
				"FROM rule_result_table_map a  " +
				"INNER JOIN result_table b  " +
				"INNER JOIN result_table_rule_result_map c  " +
				"INNER JOIN rule_result d  " +
				"INNER JOIN result e  " +
				"INNER JOIN promotion_block f  " +
				"INNER JOIN block_container g  " +
				"INNER JOIN ocs_behaviour o  " +
				"INNER JOIN notify_trigger h  " +
				"on a.RESULT_TABLE_ID = b.RESULT_TABLE_ID  " +
				"AND b.RESULT_TABLE_ID = c.RESULT_TABLE_ID  " +
				"  AND c.RULE_RESULT_ID =   d.RULE_RESULT_ID  " +
				"  AND d.RESULT_ID = e.RESULT_ID  " +
				"  AND e.PROMOTION_BLOCK_ID  = f.PROMOTION_BLOCK_ID   " +
				"\tAND f.PROMOTION_BLOCK_ID = g.PROMOTION_BLOCK_ID  " +
				"\tAND g.PROM_ID = o.BEHAVIOUR_ID  " +
				"\tAND o.NOTIFY_ID = h.NOTIFY_TRIGGER_ID  " +
				"  WHERE g.PROM_TYPE = '1'   " +
				"  AND a. rule_id in =:ruleId2 ";

		SQLQuery query = getSession().createSQLQuery(sql);
		query.addScalar("notifyTriggerId",StandardBasicTypes.LONG);
		query.addScalar("triggerName",StandardBasicTypes.STRING);
		query.addScalar("notifyType",StandardBasicTypes.LONG);
		query.addScalar("alias",StandardBasicTypes.STRING);
		query.addScalar("notifyTemplateId",StandardBasicTypes.LONG);
		query.addScalar("isInvite",StandardBasicTypes.LONG);
		query.addScalar("numberRetry",StandardBasicTypes.LONG);
		query.addScalar("repeatTime",StandardBasicTypes.LONG);
		query.addScalar("description",StandardBasicTypes.STRING);
		query.addScalar("categoryId",StandardBasicTypes.LONG);
		query.addScalar("channelType",StandardBasicTypes.LONG);
		query.addScalar("retryCycleId",StandardBasicTypes.LONG);
		query.setResultTransformer(Transformers.aliasToBean(Rule.class));
		query.setParameter("ruleId1",ruleIds);
		query.setParameter("ruleId2",ruleIds);
		return query.list();
	}

	@Override
	public List<NotifyTrigger> getByNotifyTemplateId(Long id) {
		try {
			final String sql = "from NotifyTrigger where notifyTemplateId = :notifyTemplateId";
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(sql);
			query.setParameter("notifyTemplateId", id);
			query.setCacheable(true);
			List<NotifyTrigger> templates = query.list();
			return templates;
		} catch (Exception ex) {
			System.out.println("Exception " + ex);
		}
		return Collections.EMPTY_LIST;
	}

	@Override
	public NotifyTrigger getByName(long id, String name) {
		String sql = "from NotifyTrigger where triggerName = :triggerName and notifyTriggerId != :notifyTriggerId";
		List<NotifyTrigger> lst = getSession().createQuery(sql).setParameter("triggerName", name).setParameter("notifyTriggerId", id).list();
		return !DataUtil.isNullOrEmpty(lst) ? lst.get(0) : null;
	}

	@Override
	public <T>List<T> getLstNotifyTrigger(List<Long> categoryIds) {
		try {
			final String sql = "from " + NotifyTrigger.class.getSimpleName() + " where categoryId in (:categoryIds) and  isInvite = 1";
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(sql);
			query.setParameterList("categoryIds", categoryIds);
			query.setCacheable(true);
			return query.list();
		} catch (Exception ex) {
		}
		return new ArrayList<>();
	}
}
