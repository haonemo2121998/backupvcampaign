/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.config;

import java.io.IOException;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import vn.viettel.campaign.entities.Objects;

@WebFilter("/*")
public class AuthenticationFilter implements Filter {

    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);

        String pageRequested = request.getRequestURI();

        if (pageRequested.equals("/campaign/login.jsf")
                || pageRequested.equals("/campaign/login.xhtml")
                || pageRequested.equals("/campaign/botdetectcaptcha")
                || pageRequested.contains("/javax.faces.resource")
                || pageRequested.equals("/campaign/403.jsf")
                || pageRequested.equals("/campaign/index.jsf")
                || pageRequested.equals("/campaign/")) {
            chain.doFilter(request, response);
            return;
        }
        if (session != null) {
            List<Objects> lst = (List<Objects>) session.getAttribute("lstMenu");
            if (lst != null && lst.size() > 0) {
                for (Objects obj : lst) {
                    for (Objects sub : obj.getLstSub()) {
                        if (sub.getObjectUrl().equals(pageRequested)) {
                            chain.doFilter(request, response);
                            return;
                        }
                    }
                }
            } else {
                response.sendRedirect("/campaign/403.jsf");
            }
        } else {
            response.sendRedirect("/campaign/login.jsf");
        }
    }

    @Override
    public void destroy() {
    }

}
