package vn.viettel.campaign.config;

//import com.viettel.campaign.cache.manager.ResourceManager;
//import com.viettel.vocs.cba.camp.wapi.produce.RequestHandler;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class HibernateConfig {

    private final String HIBERNATE_CONFIG = "hibernate.properties";

    @Bean(name = "dataSource")
    public DataSource getDataSource() throws FileNotFoundException, IOException {
        Properties prop = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(HIBERNATE_CONFIG);
        prop.load(inputStream);
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(prop.getProperty("spring.datasource.driver-class-name"));
        dataSource.setUrl(prop.getProperty("spring.datasource.url"));
        dataSource.setUsername(prop.getProperty("spring.datasource.username"));
        dataSource.setPassword(prop.getProperty("spring.datasource.password"));
        inputStream.close();
        return dataSource;
    }

    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory(DataSource dataSource) throws Exception {
        Properties prop = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(HIBERNATE_CONFIG);
        prop.load(inputStream);

        Properties properties = new Properties();
        properties.put("hibernate.dialect", prop.getProperty("spring.jpa.properties.hibernate.dialect"));
        properties.put("hibernate.show_sql", prop.getProperty("spring.jpa.show-sql"));
        properties.put("current_session_context_class", prop.getProperty("spring.jpa.properties.hibernate.current_session_context_class"));
        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();

        factoryBean.setPackagesToScan("vn.viettel.campaign.entities");
        factoryBean.setDataSource(dataSource);
        factoryBean.setHibernateProperties(properties);
        factoryBean.afterPropertiesSet();
        //
        SessionFactory sf = factoryBean.getObject();
//        if (RequestHandler.getSessionFactory() == null) {
//            RequestHandler.setSessionFactory(sf);
//        }
//        System.out.println("## getSessionFactory: " + sf);
        inputStream.close();
        return sf;
    }

    @Autowired
    @Bean(name = "transactionManager")
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
        return transactionManager;
    }
}
