/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dto.ConditionTableDTO;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.*;
import vn.viettel.campaign.service.impl.TreeServiceImpl;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.util.Objects;
import java.util.*;

import org.primefaces.PrimeFaces;
import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author ConKC
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class ScenarioTriggerController extends BaseController implements Serializable {

    private boolean isDisplay;
    private boolean showDetail;
    private boolean actonSwitch = false;
    private boolean addRoot = true;
    private TreeNode rootNode;
    private TreeNode rootUssdNode;
    private TreeNode rootScenarioNode;
    private TreeNode preprocessUnitRootNode;
    private TreeNode ruleRootNode;
    private TreeNode selectedNode;
    private TreeNode selectedScenarioNode;
    private TreeNode selectedUssdNode;
    private TreeNode selectedNodeTmp;
    private TreeNode selectedPreprocessUnitNode;
    private TreeNode selectedRuleNode;
    private List<Category> categories;
    private List<ScenarioTrigger> scenarioTriggers;
    private List<UssdScenario> ussdScenarios;
    private List<OcsBehaviour> ocsBehaviours;
    private List<Language> languages;
    private Category category;
    private ScenarioTrigger scenarioTrigger;
    private ScenarioNode scenarioNode;
    private Boolean isUpdate;
    private TreeNode root;
    private Segment segment;
    private List<Segment> lstSeg;
    private List<Segment> lstSegFiltered;
    private String searchSeg;
    private Rule ruleSelected;
    private List<Rule> lstRule;
    private String searchRule;
    private Invitation invitation;
    private List<Invitation> lstInvi;
    private List<BlackList> lstBlackList;
    private List<SpecialProm> lstSpecialProm;
    private String searchInvi;
    private String action;
    private String messageConfirm = "Are you sure to create this Category?";
    private String datePattern = "dd/MM/yyyy";
    private Map<Long, Segment> mapKeySegment = new HashMap<>();
    private List<Segment> lstTreeSegment = new ArrayList<>();
    private Rule createNewRule = new Rule();
    private List<CdrService> cdrServicesTable = new ArrayList<>();
    private List<ResultTable> resultTable = new ArrayList<>();
    private List<CdrService> cdrServicesAll = new ArrayList<>();
    private List<ResultTable> resultTableAllData = new ArrayList<>();
    private DualListModel<CdrService> cdrServicePickingList = new DualListModel<>();
    private DualListModel<ResultTable> resultTablePickingList = new DualListModel<>();
    private boolean applyAllCdrToRule = false;
    List<Category> ruleCategories = new ArrayList<>();

    private UploadedFile file;
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TreeServiceImpl treeService;

    @Autowired
    private CampaignOnlineService campaignOnlineService;

    @Autowired
    SpecialPromService specialPromServiceImpl;

    @Autowired
    private UtilsService utilsService;

    @Autowired
    private UssdScenarioService ussdScenarioService;

    @Autowired
    private ScenarioTriggerService scenarioTriggerService;

    @Autowired
    private PromotionBlockService promotionBlockService;

    @PostConstruct
    public void init() {
        this.isDisplay = false;
        this.category = new Category();
        this.categories = new ArrayList<>();
        this.isUpdate = false;
//        messageConfirm = "Are you sure to create this Category?";
        this.root = new DefaultTreeNode(null, null);
        this.ussdScenarios = scenarioTriggerService.getAllUssdScenario();
        initTreeNode();

    }

    public void initTreeNode() {
        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_SCENARIO_TRIGGER_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.scenarioTriggers = categoryService.getScenarioTriggerByCategory(longs);
        rootNode = treeService.createScenarioTriggerTree(categories, scenarioTriggers);
        addExpandedNode(rootNode.getChildren().get(0));
    }

    public void prepareCreateCategory() {
        this.isUpdate = false;
//        messageConfirm = "Are you sure to create this Category?";
        this.category = new Category();
        this.category.setCategoryType(Constants.CatagoryType.CATEGORY_SCENARIO_TRIGGER_TYPE);
        Category parentCat = (Category) selectedNode.getData();
        this.category.setParentId(parentCat.getCategoryId());
        Long id = ussdScenarioService.getNextSequense(Constants.TableName.CATEGORY);
        category.setCategoryId(id);
    }

    public void prepareEditCategory() {
        this.isUpdate = true;
//        messageConfirm = "Are you sure to edit this Category?";
        if (Objects.nonNull(selectedNode)) {
            Category cat = (Category) selectedNode.getData();
            this.category = categoryService.getCategoryById(cat.getCategoryId());
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void doSaveOrUpdateCategory() {
        if (validateCategory()) {
            categoryService.save(category);
            refreshTree();
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public boolean validateCategory() {

        if (DataUtil.isStringNullOrEmpty(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
            return false;
        }
        
        if (!DataUtil.checkNotContainPercentage(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Category name");
            return false;
        }
        boolean rs = true;
        if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId(), category.getCategoryType())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.category.exist"));
            rs = false;
        }
        return rs;
    }

    public boolean validateObject() {
        if (DataUtil.isStringNullOrEmpty(this.scenarioTrigger.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Scenario Trigger name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.scenarioTrigger.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Scenario Trigger name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.scenarioTrigger.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Scenario Trigger name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.scenarioTrigger.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.scenarioTrigger.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Description");
            return false;
        }

        if (DataUtil.isStringNullOrEmpty(this.scenarioTrigger.getAlias())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Alias");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.scenarioTrigger.getAlias())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Alias");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.scenarioTrigger.getAlias())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Alias");
            return false;
        }
        if (this.scenarioTrigger.getUssdScenarioId() == 0) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Scenario ussd name");
            return false;
        }
        if (!scenarioTriggerService.validateAdd(scenarioTrigger.getScenarioTriggerId(), scenarioTrigger.getTriggerName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.scenario.trigger"));
            return false;
        }
        if (!validateTemplateDelete()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.scenario.template.delete"));
            return false;
        }
        return true;
    }

    public boolean validateTemplateDelete() {
        UssdScenario ussdScenario = ussdScenarioService.getUssdScenarioById(scenarioTrigger.getUssdScenarioId());
        return !DataUtil.isNullObject(ussdScenario);
    }

    public void onSelectScenario() {
//        this.scenarioNode = (ScenarioNode) this.selectedScenarioNode.getData();
//        this.showDetail = true;
    }

    public void onSelectUssd() {
        if (this.selectedUssdNode.getData() instanceof UssdScenario) {
            UssdScenario ussdScenario = (UssdScenario) selectedUssdNode.getData();
            this.scenarioTrigger.setUssdScenarioId(ussdScenario.getScenarioId());
            this.scenarioTrigger.setUssdName(ussdScenario.getScenarioName());
            //
            List<ScenarioNode> lstScenarioNode = scenarioTriggerService.getScenarioNodeByUssdId(this.scenarioTrigger.getUssdScenarioId());
            if (!DataUtil.isNullOrEmpty(lstScenarioNode)) {
                this.rootScenarioNode = treeService.createTreeScenario(lstScenarioNode);
            } else {
                this.rootScenarioNode = null;
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ussd"));
            return;
        }
    }

    public void onChooseUssd() {
//        ussdScenarios;
        List<Category> categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_USSD_SCENARIO_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.ussdScenarios = categoryService.getUssdScenarioByCategory(longs);
        rootUssdNode = treeService.createUssdScenarioTree(categories, ussdScenarios);
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('ussdTreePopup').show();");
    }

    public void onSelectUssdScenario() {

        List<ScenarioNode> lstScenarioNode = scenarioTriggerService.getScenarioNodeByUssdId(this.scenarioTrigger.getUssdScenarioId());
        if (!DataUtil.isNullOrEmpty(lstScenarioNode)) {
            this.rootScenarioNode = treeService.createTreeScenario(lstScenarioNode);
        } else {
            this.rootScenarioNode = null;
        }
//        this.scenarioNode = (ScenarioNode) this.selectedScenarioNode.getData();
//        this.showDetail = true;
    }

    public void prepareCreateSubNode() {
        System.out.println("prepareCreateSubNode");
        long nodeId = ussdScenarioService.getNextSequense(Constants.TableName.SCENARIO_NOTE);
        this.scenarioNode = new ScenarioNode();
        scenarioNode.setNodeId(nodeId);
        this.showDetail = true;
        ScenarioNode selected = (ScenarioNode) this.selectedScenarioNode.getData();
        scenarioNode.setParentId(selected.getNodeId());
    }

    public void clearNode() {
    }

    public void onAddRootNode() {
        long nodeId = ussdScenarioService.getNextSequense(Constants.TableName.SCENARIO_NOTE);
        this.scenarioNode = new ScenarioNode();
        scenarioNode.setNodeId(nodeId);
        this.showDetail = true;
    }

    public void doAddNode() {
    }

    public boolean validateAddNode() {
        if (DataUtil.isNullOrEmpty(scenarioNode.getLstNoteContent())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.scenario.node"));
            return false;
        }
        return true;
    }

    private void refreshTree() {
//        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_SCENARIO_TRIGGER_TYPE);
//        rootNode = treeService.createScenarioTriggerTree(categories, scenarioTriggers);
//        rootNode.setSelected(true);

        if (isUpdate) {
//            categories.remove((Category) selectedNode.getData());
//            categories.add(this.category);
            int i = categories.indexOf(selectedNode.getData());
            categories.remove(selectedNode.getData());
            categories.add(i, this.category);
        } else {
            categories.add(this.category);
        }

        rootNode = treeService.createScenarioTriggerTree(categories, scenarioTriggers);
        processRefreshCategory(rootNode, selectedNode, category, isUpdate);
        updateCategoryInformUssd();
    }

    public void refreshData() {
        actonSwitch = false;
        TreeNode treeNode = this.selectedNode;
        if (treeNode != null) {
            this.selectedNodeTmp = treeNode;
        } else {
            this.selectedNode = selectedNodeTmp;
        }
        if (treeNode != null && treeNode.getData() != null && treeNode.getData() instanceof Category) {
            prepareAddObject();
        } else {
            initTreeNode();
            mapTreeStatus(rootNode);
            prepareEditObject();
        }
        this.actonSwitch = true;
    }

    public void doDeleteCategory() {
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
            if (DataUtil.isNullObject(category.getParentId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.parent.category"));
                return;
            }
            if (categoryService.checkDeleteCategory(this.category.getCategoryId(), ScenarioTrigger.class.getSimpleName())) {
                categoryService.deleteCategory(category);
                selectedNode.getChildren().clear();
                TreeNode parNode = selectedNode.getParent();
                selectedNode.getParent().getChildren().remove(selectedNode);
                if (parNode.getChildren().isEmpty()){
                    removeExpandedNode(parNode);
                }
                selectedNode.setParent(null);
                selectedNode = null;
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
                categories.remove(category);
//                parNode.setExpanded(true);
                updateCategoryInformUssd();
            } else {
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void doDeleteNode() {
        System.out.println("doDeleteNode");
    }

    public void prepareEditObject() {
        this.isDisplay = true;
        this.isUpdate = true;
        actonSwitch = false;
        try {

            this.scenarioTrigger = (ScenarioTrigger) selectedNode.getData();
            ScenarioTrigger scenarioTriggerTmp = scenarioTriggerService.findById(scenarioTrigger.getScenarioTriggerId());
            scenarioTrigger.setTriggerName(scenarioTriggerTmp.getTriggerName());
            scenarioTrigger.setCategoryId(scenarioTriggerTmp.getCategoryId());
            scenarioTrigger.setDescription(scenarioTriggerTmp.getDescription());
            scenarioTrigger.setAlias(scenarioTriggerTmp.getAlias());
            scenarioTrigger.setScenarioTriggerId(scenarioTriggerTmp.getScenarioTriggerId());
            UssdScenario ussdScenario = ussdScenarioService.getUssdScenarioById(scenarioTrigger.getUssdScenarioId());
            scenarioTrigger.setUssdName(ussdScenario.getScenarioName());
            List<ScenarioNode> lstScenarioNode = scenarioTriggerService.getScenarioNodeByUssdId(this.scenarioTrigger.getUssdScenarioId());
            if (!DataUtil.isNullOrEmpty(lstScenarioNode)) {
                this.rootScenarioNode = treeService.createTreeScenario(lstScenarioNode);
            } else {
                this.rootScenarioNode = null;
            }
        } catch (Exception e) {
        }

    }

    public void initDataUssdScenario() {

    }

    public void doSaveObject() {
//        ussdScenarioService.doSaveUssd(this.ussdScenario);
        DataUtil.trimObject(scenarioTrigger);
        scenarioTriggerService.doSave(this.scenarioTrigger);
//        initTreeNode();
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        addNoteToCategoryTree(rootNode, scenarioTrigger);
        if (!isUpdate) {
            scenarioTriggers = DataUtil.isNullOrEmpty(scenarioTriggers) ? new ArrayList<>() : scenarioTriggers;
            scenarioTriggers.add(this.scenarioTrigger);
        }
        mapTreeStatus(rootNode);
        isUpdate = true;
        this.actonSwitch = false;
    }

    public TreeNode addNoteToCategoryTree(TreeNode rootNode, ScenarioTrigger conditionTable) {
        if (isUpdate) {
            return null;
        }
        TreeNode note = findParentNoteInTree(rootNode, conditionTable);
        if (note != null) {
            TreeNode newNote = new DefaultTreeNode("scenariodTrigger", conditionTable, note);
            expandCurrentNode(newNote);
            this.selectedNode = newNote;
            return newNote;
        }
        return null;
    }

    public TreeNode findParentNoteInTree(TreeNode root, ScenarioTrigger conditionTable) {
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (Category.class.isInstance(note.getData())) {
                Category data = (Category) note.getData();
                if (DataUtil.safeEqual(data.getCategoryId(), conditionTable.getCategoryId())) {
                    return note;
                }
            }
            currentNote = findParentNoteInTree(note, conditionTable);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

    public void onSelectRow() {

    }

    public void onAddRow() {
        System.out.println("onAddRow");
        List<NoteContent> lstNoteContent = this.scenarioNode.getLstNoteContent();
        lstNoteContent = DataUtil.isNullOrEmpty(lstNoteContent) ? new ArrayList<>() : lstNoteContent;
        NoteContent noteContent = new NoteContent();
        lstNoteContent.add(noteContent);
        this.scenarioNode.setLstNoteContent(lstNoteContent);
    }

    public void onAddColumn() {
//        System.out.println("onAddColumn:");
//        PrimeFaces current = PrimeFaces.current();
//        current.executeScript("PF('preprocessUnitTreePopup').show();");
    }

    public boolean validatePreprocessUnit() {

        return true;
    }

    public void moveUpRow(ConditionTableDTO conditionTableDTO) {
        System.out.println("moveUpRow : " + conditionTableDTO);

    }

    public void moveDownRow(ConditionTableDTO conditionTableDTO) {
        System.out.println("moveDownRow : " + conditionTableDTO);

    }

    public void removeRow(NoteContent noteContent) {
        System.out.println("removeRow : ");
        try {
            this.scenarioNode.getLstNoteContent().remove(noteContent);
        } catch (Exception e) {
            System.out.println("err removeRow");
        }
    }

    public void prepareAddObject() {
        this.isDisplay = true;
        this.isUpdate = false;
        actonSwitch = true;
        long id = ussdScenarioService.getNextSequense(Constants.TableName.SCENARIO_TRIGGER);
        this.scenarioTrigger = new ScenarioTrigger();
        scenarioTrigger.setScenarioTriggerId(id);
        Category category = (Category) this.selectedNode.getData();
        scenarioTrigger.setCategoryId(category.getCategoryId());
        this.rootScenarioNode = null;
        try {
            this.scenarioTrigger.setCategoryId(category.getCategoryId());
        } catch (Exception e) {
        }
    }

    public void doDeleteObject() {
        Long id = ((ScenarioTrigger) this.selectedNode.getData()).getScenarioTriggerId();
        TreeNode parentNode = selectedNode.getParent();
        parentNode.getChildren().remove(selectedNode);
        if (parentNode.getChildren().size() == 0) {
            removeExpandedNode(parentNode);
        }
        scenarioTriggerService.doDelete(id);
        initTreeNode();
        mapTreeStatus(rootNode);
        this.isDisplay = false;
        this.showDetail = false;
        scenarioTriggers.remove(selectedNode.getData());
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public boolean validateDeleteObject() {
        Long id = ((ScenarioTrigger) this.selectedNode.getData()).getScenarioTriggerId();
        boolean rs = true;
//        boolean rs = ussdScenarioService.validateDeleteUssd(id);
//        if (!rs) {
//            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ussd.used"));
//            rs = false;
//        }
        long checkId = promotionBlockService.checkUsePromotionBlock(Constants.PROM_TYPE_3, id);
        if (checkId > 0) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.scenario.block"));
            rs = false;
        }
        return rs;
    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(Boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public Segment getSegment() {
        return segment;
    }

    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    public List<Segment> getLstSeg() {
        return lstSeg;
    }

    public void setLstSeg(List<Segment> lstSeg) {
        this.lstSeg = lstSeg;
    }

    public String getSearchSeg() {
        return searchSeg;
    }

    public void setSearchSeg(String searchSeg) {
        this.searchSeg = searchSeg;
    }

    public Rule getRuleSelected() {
        return ruleSelected;
    }

    public void setRuleSelected(Rule ruleSelected) {
        this.ruleSelected = ruleSelected;
    }

    public List<Rule> getLstRule() {
        return lstRule;
    }

    public void setLstRule(List<Rule> lstRule) {
        this.lstRule = lstRule;
    }

    public String getSearchRule() {
        return searchRule;
    }

    public void setSearchRule(String searchRule) {
        this.searchRule = searchRule;
    }

    public Invitation getInvitation() {
        return invitation;
    }

    public void setInvitation(Invitation invitation) {
        this.invitation = invitation;
    }

    public List<Invitation> getLstInvi() {
        return lstInvi;
    }

    public void setLstInvi(List<Invitation> lstInvi) {
        this.lstInvi = lstInvi;
    }

    public String getSearchInvi() {
        return searchInvi;
    }

    public void setSearchInvi(String searchInvi) {
        this.searchInvi = searchInvi;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<ScenarioTrigger> getScenarioTriggers() {
        return scenarioTriggers;
    }

    public void setScenarioTriggers(List<ScenarioTrigger> scenarioTriggers) {
        this.scenarioTriggers = scenarioTriggers;
    }

    public ScenarioTrigger getScenarioTrigger() {
        return scenarioTrigger;
    }

    public void setScenarioTrigger(ScenarioTrigger scenarioTrigger) {
        this.scenarioTrigger = scenarioTrigger;
    }

    public TreeNode getRoot() {
        return root;
    }

    public boolean getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(boolean isDisplay) {
        this.isDisplay = isDisplay;
    }

    public DualListModel<CdrService> getCdrServicePickingList() {
        return cdrServicePickingList;
    }

    public void setCdrServicePickingList(DualListModel<CdrService> cdrServicePickingList) {
        this.cdrServicePickingList = cdrServicePickingList;
    }

    public List<CdrService> getCdrServicesTable() {
        return cdrServicesTable;
    }

    public void setCdrServicesTable(List<CdrService> cdrServicesTable) {
        this.cdrServicesTable = cdrServicesTable;
    }

    public DualListModel<ResultTable> getResultTablePickingList() {
        return resultTablePickingList;
    }

    public void setResultTablePickingList(DualListModel<ResultTable> resultTablePickingList) {
        this.resultTablePickingList = resultTablePickingList;
    }

    public List<ResultTable> getResultTable() {
        return resultTable;
    }

    public void setResultTable(List<ResultTable> resultTable) {
        this.resultTable = resultTable;
    }

    public boolean isApplyAllCdrToRule() {
        return this.applyAllCdrToRule;
    }

    public void setApplyAllCdrToRule(boolean value) {
        this.applyAllCdrToRule = value;
    }

    public void changeCdrValue() {
        this.applyAllCdrToRule = applyAllCdrToRule;
    }

    public String getDatePattern() {
        return datePattern;
    }

    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }

    public void eventListener(ValueChangeEvent event) {
        System.out.println(event.toString());
    }

    public void valueChangeMethod(ValueChangeEvent e) {
        System.out.println(e.getNewValue().toString());
    }

    public String getMessageConfirm() {
        return messageConfirm;
    }

    public void setMessageConfirm(String messageConfirm) {
        this.messageConfirm = messageConfirm;
    }

    public TreeNode getPreprocessUnitRootNode() {
        return preprocessUnitRootNode;
    }

    public void setPreprocessUnitRootNode(TreeNode preprocessUnitRootNode) {
        this.preprocessUnitRootNode = preprocessUnitRootNode;
    }

    public TreeNode getSelectedPreprocessUnitNode() {
        return selectedPreprocessUnitNode;
    }

    public void setSelectedPreprocessUnitNode(TreeNode selectedPreprocessUnitNode) {
        this.selectedPreprocessUnitNode = selectedPreprocessUnitNode;
    }

    public boolean getActonSwitch() {
//        System.out.println(actonSwitch);
        return actonSwitch;
    }

    public TreeNode getSelectedNodeTmp() {
        return selectedNodeTmp;
    }

    public void setSelectedNodeTmp(TreeNode selectedNodeTmp) {
        this.selectedNodeTmp = selectedNodeTmp;
    }

    public void setActonSwitch(boolean actonSwitch) {
        this.actonSwitch = actonSwitch;
    }

    public void setAddRoot(boolean addRoot) {
        this.addRoot = addRoot;
    }

    public TreeNode getRootScenarioNode() {
        return rootScenarioNode;
    }

    public void setRootScenarioNode(TreeNode rootScenarioNode) {
        this.rootScenarioNode = rootScenarioNode;
    }

    public TreeNode getSelectedScenarioNode() {
        return selectedScenarioNode;
    }

    public void setSelectedScenarioNode(TreeNode selectedScenarioNode) {
        this.selectedScenarioNode = selectedScenarioNode;
    }

    public boolean isShowDetail() {
        return showDetail;
    }

    public void setShowDetail(boolean showDetail) {
        this.showDetail = showDetail;
    }

    public ScenarioNode getScenarioNode() {
        return scenarioNode;
    }

    public void setScenarioNode(ScenarioNode scenarioNode) {
        this.scenarioNode = scenarioNode;
    }

    public List<OcsBehaviour> getOcsBehaviours() {
        return ocsBehaviours;
    }

    public void setOcsBehaviours(List<OcsBehaviour> ocsBehaviours) {
        this.ocsBehaviours = ocsBehaviours;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public List<UssdScenario> getUssdScenarios() {
        return ussdScenarios;
    }

    public void setUssdScenarios(List<UssdScenario> ussdScenarios) {
        this.ussdScenarios = ussdScenarios;
    }
    
    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "Scenario_trigger";
        prepareEdit();
        this.actonSwitch = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "Scenario_trigger";
        prepareEdit();
        this.actonSwitch = true;

    }

    public void prepareEdit() {
        if ("category".equals(selectedNode.getType())) {
            prepareEditCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        }
        if ("scenariodTrigger".equals(selectedNode.getType())) {
            prepareEditObject();
        }

    }

    public void prepareViewObject(ScenarioTrigger scen) {
        this.isDisplay = true;
        this.isUpdate = true;
        actonSwitch = false;
        try {

            this.scenarioTrigger = scen;
            this.scenarioTrigger = scenarioTriggerService.findById(scenarioTrigger.getScenarioTriggerId());
            UssdScenario ussdScenario = ussdScenarioService.getUssdScenarioById(scenarioTrigger.getUssdScenarioId());
            scenarioTrigger.setUssdName(ussdScenario.getScenarioName());
            List<ScenarioNode> lstScenarioNode = scenarioTriggerService.getScenarioNodeByUssdId(this.scenarioTrigger.getUssdScenarioId());
            if (!DataUtil.isNullOrEmpty(lstScenarioNode)) {
                this.rootScenarioNode = treeService.createTreeScenario(lstScenarioNode);
            } else {
                this.rootScenarioNode = null;
            }
        } catch (Exception e) {
        }

    }
}
