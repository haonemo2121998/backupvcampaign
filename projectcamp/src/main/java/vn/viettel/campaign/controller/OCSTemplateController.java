package vn.viettel.campaign.controller;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SortComparator;
import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.OCSTemplateInterface;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.*;

import org.primefaces.PrimeFaces;
import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author truongbx
 * @date 9/15/2019
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class OCSTemplateController extends BaseCategoryController<OcsBehaviourTemplate> implements Serializable {

    @Autowired
    OCSTemplateInterface oCSTemplateServiceImpl;
    private List<MapCommand> lstMapCommand = new ArrayList<>();
    private List<OcsBehaviourTemplateFields> lstOCSField = new ArrayList<>();
    private List<OcsBehaviourTemplateFields> lstRemainOCSField = new ArrayList<>();
    private List<OcsBehaviourTemplateExtendFields> lstOCSExtendField = new ArrayList<>();
    private List<OcsBehaviourTemplateExtendFields> lstRemainOCSExtendField = new ArrayList<>();
    private DualListModel<OcsBehaviourTemplateFields> ocsTemplatePickingList = new DualListModel<>();
    private DualListModel<OcsBehaviourTemplateExtendFields> ocsTemplateExtendPickingList = new DualListModel<>();
    private ExtendField extendField;

    @Override
    public void init() {
        this.categoryType = Constants.CatagoryType.CATEGORY_OCS_BEHAVIOUR_TEMPLATE_TYPE;
        this.currentClass = OcsBehaviourTemplate.class;
        lstMapCommand = oCSTemplateServiceImpl.getLstMapConmand();
        Collections.sort(lstMapCommand,new SortMapcommand());

    }

    @Override
    public void initCurrentValue() {
        currentValue = oCSTemplateServiceImpl.getNextSequense();
    }

    public void prepateToshowListField() {
        List<Long> lstIds = new ArrayList<>();
        for (OcsBehaviourTemplateFields ocsBehaviourTemplateFields : lstOCSField) {
            lstIds.add(ocsBehaviourTemplateFields.getCraFieldId());
        }
        lstRemainOCSField = oCSTemplateServiceImpl.getRemainFieldOfTemplate(currentValue.getTemplateId(), lstIds);
        ocsTemplatePickingList = new DualListModel<>(lstRemainOCSField, lstOCSField);

    }
    public void onSourceTypeChange(OcsBehaviourTemplateFields ocsBehaviourTemplateFields){
        ocsBehaviourTemplateFields.setValue(null);
    }

    public void prepateToshowListExtendField() {
        List<Long> lstExtendIds = new ArrayList<>();
        for (OcsBehaviourTemplateExtendFields ocsBehaviourTemplateExtendFields : lstOCSExtendField) {
            lstExtendIds.add(ocsBehaviourTemplateExtendFields.getExtendFieldId());
        }
        lstRemainOCSExtendField = oCSTemplateServiceImpl.getRemainExtendFieldOfTemplate(currentValue.getTemplateId(), lstExtendIds);
        ocsTemplateExtendPickingList = new DualListModel<>(lstRemainOCSExtendField, lstOCSExtendField);
    }

    public boolean onCreateNewExtendField() {
        if (!validInputField(extendField.getName(), "Field name", true, true, true)) {
            return false;
        }
        if (oCSTemplateServiceImpl.checkExtendFieldName(extendField.getName())) {
            exitsMessage("Field name");
            return false;
        }
        if (!validInputField(extendField.getDescription(), "Description", true, true, true)) {
            return false;
        }
        oCSTemplateServiceImpl.insertExtendField(extendField);
        OcsBehaviourTemplateExtendFields ocsBehaviourTemplateExtendFields = new OcsBehaviourTemplateExtendFields();
        ocsBehaviourTemplateExtendFields.setName(extendField.getName());
        ocsBehaviourTemplateExtendFields.setExtendFieldId(extendField.getId());
        ocsBehaviourTemplateExtendFields.setBehaviourTemplateId(currentValue.getTemplateId());
        ocsTemplateExtendPickingList.getSource().add(ocsBehaviourTemplateExtendFields);
        actionSuccess();
        return true;

    }

    @Override
    public boolean onValidateObject() {
        if (!validInputField(this.currentValue.getTemplateName(), "Template name", true, true, true)) {
            return false;
        }
        if (oCSTemplateServiceImpl.checkTemplateName(this.currentValue.getTemplateName(), this.currentValue.getTemplateId())) {
            duplidateMessage("Template name");
            return false;

        }
        if (!validInputField(this.currentValue.getDescription(), "Description", false, true, true)) {
            return false;
        }
        if (!validRequireField(this.currentValue.getCommandCode(), "Command name")) {
            return false;
        }
        if (!validRequireFieldList(lstOCSField, "OCS template field")) {
            return false;
        }
//        if (!validRequireFieldList(lstOCSExtendField, "OCS template extend field")) {
//            return false;
//        }
        for (OcsBehaviourTemplateFields fields : lstOCSField){
            if (fields.getSourceType() == 1 && DataUtil.isStringNullOrEmpty(fields.getValue())){
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.fieldValue"));
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean doDelete() {
        return oCSTemplateServiceImpl.onDeleteBehaviourTemplate(this.currentValue.getTemplateId());
    }

    @Override
    public boolean onSaveObject() {
        for (MapCommand mapCommand : lstMapCommand) {
            if (mapCommand.getCommandId().equals(this.currentValue.getCommandCode())) {
                this.currentValue.setCommandName(mapCommand.getCommandName());
            }
        }

        boolean result = oCSTemplateServiceImpl.onSaveBehaviourTemplate(this.currentValue, lstOCSField, lstOCSExtendField);
        return result;
    }

    @Override
    public void doEdit() {
        lstOCSField = oCSTemplateServiceImpl.getLstFieldOfTemplate(currentValue.getTemplateId());
        lstOCSExtendField = oCSTemplateServiceImpl.getLstExtendFieldOfTemplate(currentValue.getTemplateId());
    }

    @Override
    public boolean onUpdateObject() {
        for (MapCommand mapCommand : lstMapCommand) {
            if (mapCommand.getCommandId().equals(this.currentValue.getCommandCode())) {
                this.currentValue.setCommandName(mapCommand.getCommandName());
            }
        }
        return oCSTemplateServiceImpl.onUpdateBehaviourTemplate(this.currentValue, lstOCSField, lstOCSExtendField);
    }

    @Override
    public void rollbackData() {
        OcsBehaviourTemplate data = oCSTemplateServiceImpl.findById(this.currentValue.getTemplateId());
        this.currentValue.asMap(data);
    }

    @Override
    public void doAdd() {
        lstOCSField = new ArrayList<>();
        lstOCSExtendField = new ArrayList<>();
    }

    public void removeField(OcsBehaviourTemplateFields ocsBehaviourTemplateFields) {
        lstOCSField.remove(ocsBehaviourTemplateFields);
        actionSuccess();
    }

    public void removeExtendField(OcsBehaviourTemplateExtendFields ocsBehaviourTemplateExtendFields) {
        lstOCSExtendField.remove(ocsBehaviourTemplateExtendFields);
        actionSuccess();
    }

    public void onSaveField() {
        lstOCSField = ocsTemplatePickingList.getTarget();
        lstRemainOCSField = ocsTemplatePickingList.getSource();
    }

    public void onSaveExtendField() {
        lstOCSExtendField = ocsTemplateExtendPickingList.getTarget();
        lstRemainOCSExtendField = ocsTemplateExtendPickingList.getSource();
    }

    public void prepareToShowCreateExtendField() {
        extendField = oCSTemplateServiceImpl.getNextExtendSequense();
    }

    public DualListModel<OcsBehaviourTemplateFields> getOcsTemplatePickingList() {
        return ocsTemplatePickingList;
    }

    public void setOcsTemplatePickingList(DualListModel<OcsBehaviourTemplateFields> ocsTemplatePickingList) {
        this.ocsTemplatePickingList = ocsTemplatePickingList;
    }

    public DualListModel<OcsBehaviourTemplateExtendFields> getOcsTemplateExtendPickingList() {
        return ocsTemplateExtendPickingList;
    }

    public void setOcsTemplateExtendPickingList(DualListModel<OcsBehaviourTemplateExtendFields> ocsTemplateExtendPickingList) {
        this.ocsTemplateExtendPickingList = ocsTemplateExtendPickingList;
    }

    public OCSTemplateInterface getoCSTemplateServiceImpl() {
        return oCSTemplateServiceImpl;
    }

    public void setoCSTemplateServiceImpl(OCSTemplateInterface oCSTemplateServiceImpl) {
        this.oCSTemplateServiceImpl = oCSTemplateServiceImpl;
    }

    public List<MapCommand> getLstMapCommand() {
        return lstMapCommand;
    }

    public void setLstMapCommand(List<MapCommand> lstMapCommand) {
        this.lstMapCommand = lstMapCommand;
    }

    public List<OcsBehaviourTemplateFields> getLstOCSField() {
        return lstOCSField;
    }

    public void setLstOCSField(List<OcsBehaviourTemplateFields> lstOCSField) {
        this.lstOCSField = lstOCSField;
    }

    public List<OcsBehaviourTemplateFields> getLstRemainOCSField() {
        return lstRemainOCSField;
    }

    public void setLstRemainOCSField(List<OcsBehaviourTemplateFields> lstRemainOCSField) {
        this.lstRemainOCSField = lstRemainOCSField;
    }

    public List<OcsBehaviourTemplateExtendFields> getLstOCSExtendField() {
        return lstOCSExtendField;
    }

    public void setLstOCSExtendField(List<OcsBehaviourTemplateExtendFields> lstOCSExtendField) {
        this.lstOCSExtendField = lstOCSExtendField;
    }

    public List<OcsBehaviourTemplateExtendFields> getLstRemainOCSExtendField() {
        return lstRemainOCSExtendField;
    }

    public void setLstRemainOCSExtendField(List<OcsBehaviourTemplateExtendFields> lstRemainOCSExtendField) {
        this.lstRemainOCSExtendField = lstRemainOCSExtendField;
    }

    public ExtendField getExtendField() {
        return extendField;
    }

    public void setExtendField(ExtendField extendField) {
        this.extendField = extendField;
    }
    
    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "ocs_template";
        prepareEdit();
        this.action = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "ocs_template";
        prepareEdit();
        this.action = true;

    }

    public void prepareEdit() {
        if ("category".equals(this.selectedNode.getType())) {
            prepareEditCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        }
        if ("ocs_template".equals(selectedNode.getType())) {
            onEditObject();
        }

    }

    @Override
    public boolean validateBeforeDelete() {
        if (oCSTemplateServiceImpl.checkOcsTemplateInUse(currentValue.getTemplateId())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("ocstemplate.used.in.ocsbehaviour"));
            return false;
        }
        return true;
    }
    class SortMapcommand implements Comparator<MapCommand> {
        public int compare(MapCommand a, MapCommand b)
        {
            return a.getCommandName().compareTo( b.getCommandName());
        }
    }
}
