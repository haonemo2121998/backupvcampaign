package vn.viettel.campaign.controller;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.beans.factory.annotation.Autowired;

import vn.viettel.campaign.entities.Result;

/**
 *
 * @author ConKC
 */
@ManagedBean
@ViewScoped
public class Home extends BaseController implements Serializable {

    private String title;
    private List<Result> listResult;

    @Autowired

    @PostConstruct
    public void init() {
//        listResult = resultServiceImpl.finAllResult();
//        title = "Home";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
