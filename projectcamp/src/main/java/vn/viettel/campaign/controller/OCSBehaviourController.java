package vn.viettel.campaign.controller;

import lombok.Getter;
import lombok.Setter;
import org.primefaces.PrimeFaces;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.OCSBehaviourInterface;
import vn.viettel.campaign.service.OCSTemplateInterface;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedProperty;
import static vn.viettel.campaign.controller.BaseController.errorMsg;
import vn.viettel.campaign.dao.NotifyTriggerDAOInterface;
import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author truongbx
 * @date 9/15/2019
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class OCSBehaviourController extends BaseCategoryController<OcsBehaviour> implements Serializable {

    @Autowired
    OCSBehaviourInterface oCSBehaviourServiceImpl;
    @Autowired
    OCSTemplateInterface oCSTemplateServiceImpl;
    
    @Autowired
    NotifyTriggerDAOInterface notifyTriggerDAO;
    
    @ManagedProperty(value = "#{notifyTriggerController}")
    NotifyTriggerController notifyTriggerController;

    List<NotifyContent> lstNotifyContent = new ArrayList<>();
    List<PostNotifyTrigger> lstPostNotifyTrigger = new ArrayList<>();
    List<OcsBehaviourTemplate> lstOcsBehaviour = new ArrayList<>();
    Map<Long,OcsBehaviourTemplate> mapOcsTemplate = new HashMap<>() ;
    List<OcsBehaviourFields> lstOCSField = new ArrayList<>();
    List<OcsBehaviourExtendFields> lstOCSExtendField = new ArrayList<>();
    Map<Long, String> mapNotifyTrigger = new HashMap<>();
    TreeNode ocsTemplateNote ;
    TreeNode selectedOcsTemplateNote ;
    TreeNode notifyTriggerNote ;
    TreeNode selectedNotifyTriggerNote ;
    Long templateId;
    Object currentPostNotifyTrigger;
    Integer activeIndex = 0 ;
    @Override
    public void init() {
        this.categoryType = Constants.CatagoryType.CATEGORY_OCS_BEHAVIOUR_TYPE;
        this.currentClass = OcsBehaviour.class;
        initOCsTemplateTree();
        initNotifyTriggerTree(true);
    }

    public void initOCsTemplateTree(){
        List<Category> categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_OCS_BEHAVIOUR_TEMPLATE_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        List<BaseCategory> lstData = categoryService.getDataFromCatagoryId(longs, OcsBehaviourTemplate.class.getSimpleName());
        lstOcsBehaviour.clear();
        lstData.forEach(e ->{
            OcsBehaviourTemplate data = (OcsBehaviourTemplate)e;
            mapOcsTemplate.put(data.getTemplateId(),data);
            lstOcsBehaviour.add(data);
        });
        ocsTemplateNote = treeService.createTreeCategoryAndChild(categories, lstData);
        ocsTemplateNote.setExpanded(true);
    }
    public void initNotifyTriggerTree(boolean isTab){
        List<Category> categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_NOTIFY_TRIGGER_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        List<BaseCategory> lstData ;
        if (isTab) {
            lstData = categoryService.getDataFromCatagoryId(longs, NotifyTrigger.class.getSimpleName());
            lstData.forEach(e ->{
                NotifyTrigger data = (NotifyTrigger)e;
                mapNotifyTrigger.put(data.getNotifyTriggerId(), data.getTriggerName());
            });
        } else {
            lstData = oCSBehaviourServiceImpl.getLstNotifyTrigger(longs);
        }
        notifyTriggerNote = treeService.createTreeCategoryAndChild(categories, lstData);
        notifyTriggerNote.setExpanded(true);
    }
    @Override
    public void initCurrentValue() {
        currentValue = oCSBehaviourServiceImpl.getNextSequense();
    }

    @Override
    public boolean onValidateObject() {
        if (!validInputField(this.currentValue.getBehaviourName(), "OCS behaviour name", true, true, true)) {
            return false;
        }
        if (oCSBehaviourServiceImpl.checkExisOCSBehaviourName(this.currentValue.getBehaviourName(), this.currentValue.getBehaviourId())) {
            duplidateMessage("Behaviour name");
            return false;

        }
        if (!validInputField(this.currentValue.getDescription(), "Description", false, true, true)) {
            return false;
        }
        if (!validRequireField(this.currentValue.getTemplateId(), "Template name")) {
            return false;
        }
        Map<Long, Long> mapPostNotify = new HashMap<>();
        for (PostNotifyTrigger postNotifyTrigger : lstPostNotifyTrigger) {
            Long value = mapPostNotify.get(postNotifyTrigger.getErrorCode());
            if (value != null && value.equals(postNotifyTrigger.getNotifyId())) {
                errorMsgParams(Constants.REMOTE_GROWL, this.utilsService.getTex("check.postNotify"),
                        postNotifyTrigger.getErrorCode() + "", mapNotifyTrigger.get(postNotifyTrigger.getNotifyId()));
                return false;
            }
            mapPostNotify.put(postNotifyTrigger.getErrorCode(), postNotifyTrigger.getNotifyId());
            if ((postNotifyTrigger.getErrorCode() == null || postNotifyTrigger.getErrorCode() == 0)
                    || (postNotifyTrigger.getNotifyId() == null || postNotifyTrigger.getNotifyId() == 0)) {
                errorMsgParams(Constants.REMOTE_GROWL, this.utilsService.getTex("check.notcomplete"), "Post notify");
                return false;
            }

        }

        for (OcsBehaviourFields ocsBehaviourField : lstOCSField){
            if (DataUtil.isStringNullOrEmpty(ocsBehaviourField.getValue())){
                errorMsg(Constants.REMOTE_GROWL, this.utilsService.getTex("check.valueField"));
                return false;
            }
            if (!DataUtil.checkMaxlength(ocsBehaviourField.getValue())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Value");
                return false;
            }
        }

        for (OcsBehaviourExtendFields ocsBehaviourExtendFields : lstOCSExtendField){
            if (DataUtil.isStringNullOrEmpty(ocsBehaviourExtendFields.getValue())){
                errorMsg(Constants.REMOTE_GROWL, this.utilsService.getTex("check.valueExtendField"));
                return false;
            }
            if (!DataUtil.checkMaxlength(ocsBehaviourExtendFields.getValue())) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Value");
                return false;
            }
        }
        return  !checkObjectChange();
    }

    @Override
    public boolean doDelete() {
        return oCSBehaviourServiceImpl.onDeleteOcsBehaviour(this.currentValue.getBehaviourId());
    }

    @Override
    public boolean onSaveObject() {

        OcsBehaviourTemplate template = mapOcsTemplate.get(this.currentValue.getTemplateId());
    	if (template != null){
			lstPostNotifyTrigger.forEach(e -> {
				e.setCommandId(template.getCommandCode());
			});
		}
    	List<OcsBehaviourFields> lstField = new ArrayList<>();
        lstOCSField.forEach((e)->{
//            remove field has value 1
            if (e.getTemplateType() == 2){
                lstField.add(e);
            }
        });
        boolean result = oCSBehaviourServiceImpl.onSaveOCSBehaviour(this.currentValue, lstField, lstOCSExtendField, lstPostNotifyTrigger);
        if (result) {
            this.templateId = this.currentValue.getTemplateId();
        }
        return result;
    }

    public boolean checkObjectChange(){

        List<OcsBehaviourTemplateFields> ocsBehaviourTemplateFields = oCSBehaviourServiceImpl.getLstBehaviourTemplateField(this.currentValue.getTemplateId());
        List<OcsBehaviourTemplateExtendFields> ocsBehaviourTemplateExtendFields = oCSBehaviourServiceImpl.getLstBehaviourTemplateExtendField(this.currentValue.getTemplateId());

        boolean isChange = false;
        if (ocsBehaviourTemplateExtendFields.size() != lstOCSExtendField.size() || ocsBehaviourTemplateFields.size() != lstOCSField.size()) {
            isChange = true;
        }else {
            Map<Long, OcsBehaviourFields> mapBehaviourField = new HashMap<>();
            lstOCSField.forEach(e -> {
                mapBehaviourField.put(e.getCraFieldId(), e);
            });

            for (OcsBehaviourTemplateFields fields : ocsBehaviourTemplateFields) {
                OcsBehaviourFields ocsBehaviourFields = mapBehaviourField.get(fields.getCraFieldId());
                if (ocsBehaviourFields == null) {
                    isChange = true;
                    break;
                }else if (ocsBehaviourFields.getTemplateType()  != fields.getSourceType()){
                    isChange = true;
                    break;
                }else if (ocsBehaviourFields.getTemplateType() == 1 && !ocsBehaviourFields.getValue().equalsIgnoreCase(fields.getValue())){
                    isChange = true;
                    break;
                }

            }

            Map<Long, OcsBehaviourExtendFields> mapBehaviourExtendField = new HashMap<>();
            lstOCSExtendField.forEach(e -> {
                mapBehaviourExtendField.put(e.getExtendFieldId(), e);
            });

            for (OcsBehaviourTemplateExtendFields fields : ocsBehaviourTemplateExtendFields) {
                OcsBehaviourExtendFields ocsBehaviourExtendFields = mapBehaviourExtendField.get(fields.getExtendFieldId());
                if (ocsBehaviourExtendFields == null) {
                    isChange = true;
                    break;
                }
            }
        }
        if (isChange){
            errorMsg(Constants.REMOTE_GROWL, this.utilsService.getTex("check.dataocs"));
            onChangeTemplate();
        }
        return isChange;

    }
    @Override
    public void doEdit() {
        lstPostNotifyTrigger = oCSBehaviourServiceImpl.getLstPostNotifyTrigger(this.currentValue.getBehaviourId());
        lstPostNotifyTrigger.forEach(e -> {
            e.setNotifyName(mapNotifyTrigger.get(e.getNotifyId()));
        });
        lstOCSField = oCSBehaviourServiceImpl.getLstBehaviourField(currentValue.getBehaviourId(),currentValue.getTemplateId());
        lstOCSExtendField = oCSBehaviourServiceImpl.getLstBehaviourExtendField(currentValue.getBehaviourId(),currentValue.getTemplateId());
        templateId = this.currentValue.getTemplateId();
        this.currentValue.setNotifyName(mapNotifyTrigger.get(this.currentValue.getNotifyId()));
        activeIndex = 0;
    }

    @Override
    public boolean onUpdateObject() {
		OcsBehaviourTemplate template = mapOcsTemplate.get(this.currentValue.getTemplateId());
		if (template != null){
			lstPostNotifyTrigger.forEach(e -> {
				e.setCommandId(template.getCommandCode());
			});
		}
        List<OcsBehaviourFields> lstField = new ArrayList<>();
        lstOCSField.forEach((e)->{
//            remove field has value 1
            if (e.getTemplateType() == 2){
                lstField.add(e);
            }
        });
        boolean result = oCSBehaviourServiceImpl.onUpdateOCSBehaviour(this.currentValue, lstField, lstOCSExtendField, lstPostNotifyTrigger);
        return result;
    }

    @Override
    public void rollbackData() {
        OcsBehaviour data = oCSBehaviourServiceImpl.findById(this.currentValue.getBehaviourId());
        this.currentValue.asMap(data);
        OcsBehaviourTemplate ocsBehaviourTemplate = mapOcsTemplate.get(data.getTemplateId());
        if (ocsBehaviourTemplate != null){
        this.currentValue.setTemplateName(ocsBehaviourTemplate.getTemplateName());
        }
    }

    @Override
    public void doAdd() {
        lstOCSField = new ArrayList<>();
        lstOCSExtendField = new ArrayList<>();
        lstPostNotifyTrigger = new ArrayList<>();
        activeIndex = 0;
    }

    public void onChangeTemplate() {
        if (this.editMode && this.currentValue.getTemplateId() == templateId) {
            lstOCSField = oCSBehaviourServiceImpl.getLstBehaviourField(currentValue.getBehaviourId(),templateId);
            lstOCSExtendField = oCSBehaviourServiceImpl.getLstBehaviourExtendField(currentValue.getBehaviourId(),templateId);
            return;
        }
        List<OcsBehaviourTemplateFields> lstField = oCSTemplateServiceImpl.getLstFieldOfTemplate(this.currentValue.getTemplateId());
        lstOCSField.clear();
        for (OcsBehaviourTemplateFields fields : lstField) {
            lstOCSField.add(fields.toBehaviourField(this.currentValue.getBehaviourId()));
        }
        List<OcsBehaviourTemplateExtendFields> lstExtend = oCSTemplateServiceImpl.getLstExtendFieldOfTemplate(this.currentValue.getTemplateId());
        lstOCSExtendField.clear();
        for (OcsBehaviourTemplateExtendFields ocsBehaviourTemplateExtendFields : lstExtend) {
            lstOCSExtendField.add(ocsBehaviourTemplateExtendFields.toBehaviourExtendField(this.currentValue.getBehaviourId()));
        }
    }



    public List<OcsBehaviourFields> getLstOCSField() {
        return lstOCSField;
    }

    public void setLstOCSField(List<OcsBehaviourFields> lstOCSField) {
        this.lstOCSField = lstOCSField;
    }

    public List<OcsBehaviourExtendFields> getLstOCSExtendField() {
        return lstOCSExtendField;
    }

    public void setLstOCSExtendField(List<OcsBehaviourExtendFields> lstOCSExtendField) {
        this.lstOCSExtendField = lstOCSExtendField;
    }

    public void onAddNewNotify() {
        PostNotifyTrigger postNotifyTrigger = new PostNotifyTrigger();
        postNotifyTrigger.setOcsBehaviourId(this.currentValue.getBehaviourId());
        lstPostNotifyTrigger.add(postNotifyTrigger);
    }

    public void onRemoveNotify(PostNotifyTrigger notifyTrigger) {
        lstPostNotifyTrigger.remove(notifyTrigger);
        actionSuccess();
    }
    public void onClickViewDetail(PostNotifyTrigger postNotify) {
        if (java.util.Objects.nonNull(postNotify)) {        
                NotifyTrigger notifyTrigger = notifyTriggerDAO.findById(NotifyTrigger.class, postNotify.getNotifyId());
                notifyTriggerController.prepareViewObject(notifyTrigger);
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }
    
    
    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "ocs_behaviour";
        prepareEdit();
        this.action = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "ocs_behaviour";
        prepareEdit();
        this.action = true;

    }
    
    public void prepareEdit() {
        if ("category".equals(this.selectedNode.getType())) {
            prepareEditCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        }
        if ("ocs_behaviour".equals(selectedNode.getType())) {
            onEditObject();
        }
    }
    public boolean onSelectOcsTemplateTree(){
        if (selectedOcsTemplateNote.getData() instanceof Category){
            errorMsgParams(Constants.REMOTE_GROWL, this.utilsService.getTex("check.chooseOcsTemplate"));
            return false;
        }
        OcsBehaviourTemplate data =  (OcsBehaviourTemplate)selectedOcsTemplateNote.getData() ;
        this.currentValue.setTemplateId(data.getTemplateId());
        this.currentValue.setTemplateName(data.getTemplateName());
        onChangeTemplate();
        return true;
    }

    public boolean onSelectNotifyTriggerTree(){
        if (selectedNotifyTriggerNote.getData() instanceof Category){
            errorMsgParams(Constants.REMOTE_GROWL, this.utilsService.getTex("check.chooseNotifyTrigger"));
            return false;
        }
        NotifyTrigger data =  (NotifyTrigger)selectedNotifyTriggerNote.getData() ;
        if (currentPostNotifyTrigger instanceof  PostNotifyTrigger){
            PostNotifyTrigger postNotifyTrigger = (PostNotifyTrigger)currentPostNotifyTrigger;
            postNotifyTrigger.setNotifyId(data.getNotifyTriggerId());
            postNotifyTrigger.setNotifyName(data.getTriggerName());
        }else if (currentPostNotifyTrigger instanceof  OcsBehaviour){
            OcsBehaviour ocsBehaviour = (OcsBehaviour)currentPostNotifyTrigger;
            ocsBehaviour.setNotifyId(data.getNotifyTriggerId());
            ocsBehaviour.setNotifyName(data.getTriggerName());
        }
        return true;
    }
    public void prepareToShowNotifyTrigger(Object notifyTrigger){
		if (notifyTrigger instanceof PostNotifyTrigger) {
			initNotifyTriggerTree(true);
		} else {
			initNotifyTriggerTree(false);

		}
        currentPostNotifyTrigger =notifyTrigger ;
    }
    @Override
    public boolean validateBeforeDelete() {
        if (oCSBehaviourServiceImpl.checkOcsUseInPromotionBlock(currentValue.getBehaviourId())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("ocstemplate.used.in.promotionBlock"));
            return false;
        }
        if (oCSBehaviourServiceImpl.checkOcsUseInUSSDScenario(currentValue.getBehaviourId())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("ocstemplate.used.in.scenario"));
            return false;
        }
        return true;
    }
}
