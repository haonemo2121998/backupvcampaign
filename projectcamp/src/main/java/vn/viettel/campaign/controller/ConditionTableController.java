/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.primefaces.poseidon.view.data.datatable.ColumnsView;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dto.ConditionTableDTO;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.*;
import vn.viettel.campaign.service.impl.PreprocessUnitServiceImpl;
import vn.viettel.campaign.service.impl.TreeServiceImpl;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIOutput;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Objects;
import java.util.*;
import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author ConKC
 */
@ManagedBean(name = "conditionTableController")
@ViewScoped
@Getter
@Setter
public class ConditionTableController extends BaseController implements Serializable {

    TreeNode parentNode;

    private boolean isDisplay;
    private boolean actonSwitch = false;
    private TreeNode rootNode;
    private TreeNode preprocessUnitRootNode;
    private TreeNode ruleRootNode;
    private TreeNode selectedNode;
    private TreeNode selectedNodeTmp;
    private TreeNode selectedPreprocessUnitNode;
    private TreeNode selectedRuleNode;
    private List<Category> categories;
    private List<ConditionTable> conditionTables;
    private Category category;
    private ConditionTable conditionTable;
    private Boolean isUpdate;
    private TreeNode root;
    private Segment segment;
    private List<Segment> lstSeg;
    private List<Segment> lstSegFiltered;
    private String searchSeg;
    private Rule ruleSelected;
    private List<Rule> lstRule;
    private String searchRule;
    private Invitation invitation;
    private PreProcessUnit preProcessUnit;
    private List<Invitation> lstInvi;
    private List<BlackList> lstBlackList;
    private List<SpecialProm> lstSpecialProm;
    private String searchInvi;
    private String action;
    private String messageConfirm = "Are you sure to create this Category?";
    private String datePattern = "dd/MM/yyyy";
    private Map<Long, Segment> mapKeySegment = new HashMap<>();
    private List<Segment> lstTreeSegment = new ArrayList<>();
    private Rule createNewRule = new Rule();
    private List<CdrService> cdrServicesTable = new ArrayList<>();
    private List<ResultTable> resultTable = new ArrayList<>();
    private List<CdrService> cdrServicesAll = new ArrayList<>();
    private List<ResultTable> resultTableAllData = new ArrayList<>();
    private DualListModel<CdrService> cdrServicePickingList = new DualListModel<>();
    private DualListModel<ResultTable> resultTablePickingList = new DualListModel<>();
    private boolean applyAllCdrToRule = false;
    List<Category> ruleCategories = new ArrayList<>();

    private List<ColumnsView.ColumnModel> columns;

    private List<Long> preProcessIds;

    private List<PreProcessUnit> preProcessUnits;

    private List<ColumnCt> lstColumnCt = new ArrayList<>();

    private List<ProcessValue> lstProcessValue;

    private List<Object> lstProcessValueObject;

    private Map<String, String> mapProcessValue = new HashMap<>();

    private List<ConditionTableDTO> conditionTableDTOS;

    private ConditionTableDTO conditionTableDTO;

    private UploadedFile file;
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TreeServiceImpl treeService;
    @Autowired
    private UssdScenarioService ussdScenarioService;
    @Autowired
    private CampaignOnlineService campaignOnlineService;

    @Autowired
    SpecialPromService specialPromServiceImpl;

    @Autowired
    private UtilsService utilsService;

    @Autowired
    private ConditionTableService conditionTableService;

    @Autowired
    PreprocessUnitInterface preprocessUnitServiceImpl;

    private String editObject;
    @ManagedProperty(value = "#{preProcessController}")
    PreProcessController preProcessController;

    @ManagedProperty(value = "#{preProcessNumberController}")
    PreProcessNumberController preProcessNumberController;

    @ManagedProperty(value = "#{preProcessExistElementController}")
    PreProcessExistElementController preProcessExistElementController;

    @ManagedProperty(value = "#{preProcessTimeController}")
    PreProcessTimeController preProcessTimeController;

    @ManagedProperty(value = "#{preProcessDateController}")
    PreProcessDateController preProcessDateController;

    @ManagedProperty(value = "#{preProcessNumberRangeController}")
    PreProcessNumberRangeController preProcessNumberRangeController;

    @ManagedProperty(value = "#{preProcessCompareNumberController}")
    PreProcessCompareNumberController preProcessCompareNumberController;

    @ManagedProperty(value = "#{preProcessZoneController}")
    PreProcessZoneController preProcessZoneController;

    @ManagedProperty(value = "#{preProcessSameElementController}")
    PreProcessSameElementController preProcessSameElementController;

    @PostConstruct
    public void init() {
        this.isDisplay = false;
        this.category = new Category();
        this.categories = new ArrayList<>();
        this.isUpdate = false;
//        messageConfirm = "Are you sure to create this Category?";
        this.root = new DefaultTreeNode(null, null);
        this.lstSeg = new ArrayList<>();
        this.lstRule = new ArrayList<>();
        this.lstInvi = new ArrayList<>();
        initTreeNode();
        initPPU();
    }

    public void initPPU() {
        List<Integer> types = new ArrayList<>();
        for (int i = 17; i <= 25; i++) {
            types.add(i);
        }
        List<Category> categoriePreProcessUnit = categoryService.findCategoryByTypes(types);
//        List<Category> categoriePreProcessUnit = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_PRE_PROCESS_UNIT_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categoriePreProcessUnit.isEmpty()) {
            categoriePreProcessUnit.forEach(item -> longs.add(item.getCategoryId()));
        }
        List<PreProcessUnit> lstPreProcessUnit = categoryService.getPreProcessUnitByCategory(longs);
        this.preprocessUnitRootNode = treeService.createPreProcessUnitTree(categoriePreProcessUnit, lstPreProcessUnit);
    }

    public void initTreeNode() {
        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_CONDITION_TABLE_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.conditionTables = categoryService.getConditionTableByCategory(longs);
        rootNode = treeService.createConditionTree(categories, conditionTables);
        addExpandedNode(rootNode.getChildren().get(0));
    }

    public void prepareCreateCategory() {
        this.isUpdate = false;
//        messageConfirm = "Are you sure to create this Category?";
        this.category = new Category();
        this.category.setCategoryType(Constants.CatagoryType.CATEGORY_CONDITION_TABLE_TYPE);
        Category parentCat = (Category) selectedNode.getData();
        this.category.setParentId(parentCat.getCategoryId());
        Long id = ussdScenarioService.getNextSequense(Constants.TableName.CATEGORY);
        category.setCategoryId(id);
    }

    public void prepareEditCategory() {
        this.isUpdate = true;
//        messageConfirm = "Are you sure to edit this Category?";
        if (Objects.nonNull(selectedNode)) {
            Category cat = (Category) selectedNode.getData();
            this.category = categoryService.getCategoryById(cat.getCategoryId());
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void doSaveOrUpdateCategory() {
        if (validateCategory()) {
            categoryService.save(category);
            refreshTree();
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public boolean validateCategory() {

        if (DataUtil.isStringNullOrEmpty(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Category name");
            return false;
        }
        
        boolean rs = true;
        if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId(), category.getCategoryType())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.category.exist"));
            rs = false;
        }
        return rs;
    }

    public boolean validateCondition() {
        if (DataUtil.isStringNullOrEmpty(this.conditionTable.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Condition Table name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.conditionTable.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Condition Table name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.conditionTable.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Condition Table name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.conditionTable.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.conditionTable.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Description");
            return false;
        }
        // validate nghiep vu
        // valid
        if (DataUtil.isNullOrEmpty(this.preProcessIds)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.condition.column"));
            return false;
        }
        if (DataUtil.isNullOrEmpty(this.conditionTableDTOS)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.condition.row"));
            return false;
        }
        boolean chooseDefault = false;
        boolean isDuplicateCombine = false;
        Map<String, String> mapsCombine = new HashMap<>();
        for (ConditionTableDTO item : this.conditionTableDTOS) {
            if (DataUtil.safeEqual("1", item.getIsDefault())) {
                chooseDefault = true;
            }
            String combine = item.getCombine();
            if (!mapsCombine.containsKey(combine)) {
                mapsCombine.put(combine, combine);
            } else {
                isDuplicateCombine = true;
            }
            if (combine.contains("-1")) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.combine.choose"));
                return false;
            }
        }
        if (!chooseDefault) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.condition.default"));
            return false;
        }
        if (isDuplicateCombine) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.condition.combine"));
            return false;
        }
        //
        if (!DataUtil.isNullObject(conditionTableService.getDataConditionTableByName(conditionTable.getName(), conditionTable.getConditionTableId()))) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.condition.name"));
            return false;
        }
        if (!validateChangePPU()) {
            getDataProcessValue();
            //this.columns; conditionTableDTOS
//            int col = this.columns.size();
//            int row = this.conditionTableDTOS.size();
//            for (int c = 0; c < col; c++) {
//                for (int r = 0; r < row; r++) {
//                    onSelectRowProcessValue(r, c);
//                }
//            }

            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.ppu.change"));
            return false;
        }
        //
        return true;
    }

    public boolean validateChangePPU() {
        boolean rs = true;
        int i = 0;
        for (PreProcessUnit item : this.preProcessUnits) {
            List<ProcessValue> lstProcessValueReal = item.getLstProcessValue();
            List<Long> ids = new ArrayList<>();
            ids.add(item.getPreProcessId());
            List<ProcessValue> lstProcessValueDb = conditionTableService.getProcessValueByValuePreProcessIds(ids);
            lstProcessValueReal = lstProcessValueReal == null ? new ArrayList<>() : lstProcessValueReal;
            lstProcessValueDb = lstProcessValueDb == null ? new ArrayList<>() : lstProcessValueDb;
            //
            boolean isOk = false;
            String valueChange = "";
            for (ProcessValue real : lstProcessValueReal) {
                valueChange = real.getValueId() + "";
                isOk = false;
                for (ProcessValue db : lstProcessValueDb) {
                    if (db.getProcessValueId() == real.getProcessValueId() && db.getValueId() == real.getValueId() && DataUtil.safeEqual(db.getValueName(), real.getValueName())) {
                        isOk = true;
                        break;
                    }
                }
            }
            if (!isOk) {
                rs = false;
                //
                if (DataUtil.isNullOrEmpty(this.conditionTableDTOS)) {
                    this.conditionTableDTOS = new ArrayList<>();
                }
                for(ConditionTableDTO conditionTableDTO : conditionTableDTOS) {
                    List<String> valuesIds = conditionTableDTO.getValueIds();
                    String id = valuesIds.get(i);
                    if (DataUtil.safeEqual(id, valueChange)) {
                        List<String> values = conditionTableDTO.getValues();
                        List<String> valuesColor = conditionTableDTO.getValueColor();
                        String combine = "";
                        int size = this.lstColumnCt.size();

                        valuesIds.set(i, "-1");
                        values.set(i, "");
                        valuesColor.set(i, "fffcfd");
                        for (int j = 0; j < size; j++) {
                            combine += valuesIds.get(j) + "/";
                        }
                        combine = combine.substring(0, combine.length() - 1);
                        conditionTableDTO.setCombine(combine);
                    }
                }
            }
            i++;
        }
        return rs;
    }

    private void refreshTree() {
//        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_CONDITION_TABLE_TYPE);
//        rootNode = treeService.createConditionTree(categories, conditionTables);
//        rootNode.setSelected(true);

        if (isUpdate) {
//            categories.remove((Category) selectedNode.getData());
//            categories.add(this.category);
            this.selectedNode = this.selectedNode == null ? new DefaultTreeNode() : this.selectedNode;
            int i = categories.indexOf(selectedNode.getData());
            categories.remove(selectedNode.getData());
            categories.add(i, this.category);
        } else {
            categories.add(this.category);
        }

        rootNode = treeService.createConditionTree(categories, conditionTables);
        processRefreshCategory(rootNode, selectedNode, category, isUpdate);
        updateCategoryInformCondition();
    }

    public void refreshData() {
        TreeNode treeNode = this.selectedNode;
        if (treeNode != null) {
            this.selectedNodeTmp = treeNode;
        }
        if (treeNode!= null && treeNode.getData() != null && treeNode.getData() instanceof Category) {
            prepareAddObject();
        } else {
            initTreeNode();
            mapTreeStatus(rootNode);
            prepareEditObject();
        }
        this.actonSwitch = true;
    }

    public void doDeleteCategory() {
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
            if (DataUtil.isNullObject(category.getParentId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.parent.category"));
                return;
            }
            if (categoryService.checkDeleteCategory(this.category.getCategoryId(), ConditionTable.class.getSimpleName())) {
                categoryService.deleteCategory(category);
                selectedNode.getChildren().clear();
                TreeNode parNode = selectedNode.getParent();
                selectedNode.getParent().getChildren().remove(selectedNode);
                if (parNode.getChildren().isEmpty()) {
                    removeExpandedNode(parNode);
                }
                selectedNode.setParent(null);
                selectedNode = null;
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
                categories.remove(category);
//                parNode.setExpanded(true);
                updateCategoryInformCondition();
            } else {
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void prepareEditObject() {
        this.isDisplay = true;
        this.isUpdate = true;
        actonSwitch = false;
        try {
            selectedNode.getChildren().clear();
            doAddColumnToCondition(selectedNode);
            selectedNode.setExpanded(true);
            this.conditionTable = (ConditionTable) selectedNode.getData();
        } catch (Exception e) {
            this.conditionTable = (ConditionTable) this.selectedNodeTmp.getData();
        }

        ConditionTableDTO conditionTableDTO = conditionTableService.getDataConditionTable(conditionTable.getConditionTableId());
        conditionTable.setCategoryId(conditionTableDTO.getConditionTable().getCategoryId());
        conditionTable.setConditionTableId(conditionTableDTO.getConditionTable().getConditionTableId());
        conditionTable.setConditionTableName(conditionTableDTO.getConditionTable().getConditionTableName());
        conditionTable.setDescription(conditionTableDTO.getConditionTable().getDescription());
        initDataCondition(conditionTableDTO);

    }

    public void doAddColumnToCondition(TreeNode conditionNote) {
        ConditionTable conditionTable = (ConditionTable) conditionNote.getData();
        List<PreProcessUnit> lstPPU = campaignOnlineService.getListPPUByConditionId(conditionTable.getConditionTableId());
        if (lstPPU != null && !lstPPU.isEmpty()) {
            for (PreProcessUnit preProcessUnit : lstPPU) {
                TreeNode columnNote = new DefaultTreeNode("ppu", preProcessUnit, conditionNote);
            }
        }
    }

    public void initDataCondition(ConditionTableDTO conditionTableDTO) {
        columns = new ArrayList<ColumnsView.ColumnModel>();
//        columns.add(new ColumnsView.ColumnModel("Index", "index"));
//        columns.add(new ColumnsView.ColumnModel("Default", "isDefault"));
        lstColumnCt = conditionTableDTO.getLstColumnCt();
        List<RowCt> lstRowCt = conditionTableDTO.getLstRowCt();
        Map<String, ProcessValue> mapProcessValue = new HashMap<>();
//        List<ProcessValue> lstProcessValue = conditionTableDTO.getLstProcessValue();
//        if (!DataUtil.isNullOrEmpty(lstProcessValue)) {
//            for (ProcessValue processValue : lstProcessValue) {
//                mapProcessValue.put(processValue.getValueId(), processValue);
//            }
//        }

        this.preProcessUnits = new ArrayList<>();
        this.preProcessIds = new ArrayList<>();
        if (!DataUtil.isNullOrEmpty(lstColumnCt)) {
            for (ColumnCt columnCt : lstColumnCt) {
                columns.add(new ColumnsView.ColumnModel(columnCt.getColumnName(), "value"));
                preProcessIds.add(columnCt.getPreProcessId());
                PreProcessUnit ppu = preprocessUnitServiceImpl.findById(columnCt.getPreProcessId());
                ppu.setColumnName(columnCt.getColumnName());
                preProcessUnits.add(ppu);
                List<Long> ids = new ArrayList<>();
                ids.add(columnCt.getPreProcessId());
                List<ProcessValue> lstProcessValue = conditionTableService.getProcessValueByValuePreProcessIds(ids);
                for (ProcessValue processValue : lstProcessValue) {
                    mapProcessValue.put(processValue.getValueId() + "_" + columnCt.getPreProcessId(), processValue);
                }
            }
        }

        getDataProcessValue();
//        columns.add(new ColumnsView.ColumnModel("Combine", "combine"));
        // init data conditionTableDTOS
        this.conditionTableDTOS = new ArrayList<>();
        long defaultResultIndex = conditionTableDTO.getConditionTable().getDefaultResultIndex();
        if (!DataUtil.isNullOrEmpty(lstRowCt)) {

            for (RowCt rowCt : lstRowCt) {
                ConditionTableDTO item = new ConditionTableDTO();
                item.setPreProcessIds(new ArrayList<>(this.preProcessIds));
                item.setIndex(rowCt.getRowIndex() + "");
                if (defaultResultIndex == rowCt.getRowIndex()) {
                    item.setIsDefault("1");
                }
                String rowValue = rowCt.getValue();
                List<String> values = new ArrayList<>();
                List<String> valueIds = new ArrayList<>();
                List<String> valueColor = new ArrayList<>();
                item.setCombine(rowValue);
                if (!DataUtil.isNullOrEmpty(rowValue)) {
                    List<String> lstIds = DataUtil.splitListFile(rowValue, "/");
                    if (!DataUtil.isNullOrEmpty(lstIds)) {
                        int indexCol = 0;
                        for (String valueId : lstIds) {
                            Long id = preProcessIds.get(indexCol++);
                            ProcessValue processValue = mapProcessValue.get(Long.parseLong(valueId) + "_" + id);
                            if (processValue != null) {
                                values.add(processValue.getValueName());
                                valueIds.add(processValue.getValueId() + "");
                                valueColor.add(processValue.getValueColor() + "");
                            } else {
                                values.add("");
                                valueIds.add("-1");
                                valueColor.add("");
                            }
                        }
                    }
                }
                String combine = "";
                if (!DataUtil.isNullOrEmpty(valueIds)) {
                    for (String id : valueIds) {
                        combine += id + "/";
                    }
                    combine = combine.substring(0, combine.lastIndexOf("/"));
                }
                item.setCombine(combine);
                item.setValues(values);
                item.setValueIds(valueIds);
                item.setValueColor(valueColor);
                item.setLstResultTableDTO(rowCt.getLstResultTableDTO());
                conditionTableDTOS.add(item);
            }
        }
        //
        if (!DataUtil.isNullOrEmpty(conditionTableDTOS)) {
            for (ConditionTableDTO dto : conditionTableDTOS) {
                if ("1".equalsIgnoreCase(dto.getIsDefault())) {
                    this.conditionTableDTO = dto;
                    break;
                }
            }
        }
    }

    public boolean doSaveConditionTable() {
        DataUtil.trimObject(conditionTable);
        try {
            conditionTableDTOS.get(0).setPreProcessIds(this.preProcessIds);
        } catch (Exception e) {

        }
        if (!DataUtil.isNullOrEmpty(lstColumnCt)) {
            for (ColumnCt columnCt : lstColumnCt) {
                DataUtil.trimObject(columnCt);
            }
        }
        conditionTableService.doSave(this.conditionTable, this.conditionTableDTOS, lstColumnCt);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));


        if ("result".equals(CheckOnEdit.onEdit)) {
            try {
                ELContext elContext = FacesContext.getCurrentInstance().getELContext();
                ResultTableController resultTableController = (ResultTableController) elContext.getELResolver().getValue(elContext, null, "resultTableController");

                resultTableController.getParentNode().getChildren().remove(resultTableController.getSelectedNode());
                resultTableController.setSelectedNode(new DefaultTreeNode("condition", this.conditionTable, resultTableController.getParentNode()));
                resultTableController.getSelectedNode().setSelected(true);

                resultTableController.prepareEdit();
                resultTableController.setEditObject("condition");
                return true;
            } catch (Exception e) {

            }
        }

        if ("rule_online".equals(CheckOnEdit.onEdit)) {
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            RuleOnlineController ruleOnlineController = (RuleOnlineController) elContext.getELResolver().getValue(elContext, null, "ruleOnlineController");

            ruleOnlineController.getParentNode().getChildren().remove(ruleOnlineController.getSelectedNode());
            ruleOnlineController.setSelectedNode(new DefaultTreeNode("condition", this.conditionTable, ruleOnlineController.getParentNode()));
            ruleOnlineController.getSelectedNode().setSelected(true);


            ruleOnlineController.prepareEdit();
            ruleOnlineController.setEditObject("condition");
            return true;
        }

        if ("rule_offline".equals(CheckOnEdit.onEdit)) {
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            RuleOfflineController ruleOfflineController = (RuleOfflineController) elContext.getELResolver().getValue(elContext, null, "ruleOfflineController");

            ruleOfflineController.getParentNode().getChildren().remove(ruleOfflineController.getSelectedNode());
            ruleOfflineController.setSelectedNode(new DefaultTreeNode("condition", this.conditionTable, ruleOfflineController.getParentNode()));
            ruleOfflineController.getSelectedNode().setSelected(true);

            ruleOfflineController.prepareEdit();
            ruleOfflineController.setEditObject("condition");
            return true;
        }

        if ("cam_online".equals(CheckOnEdit.onEdit)) {
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            CampaignOnlineController campaignOnlineController = (CampaignOnlineController) elContext.getELResolver().getValue(elContext, null, "campaignOnlineController");

            campaignOnlineController.getParentNode().getChildren().remove(campaignOnlineController.getSelectedNode());
            campaignOnlineController.setSelectedNode(new DefaultTreeNode("condition", this.conditionTable, campaignOnlineController.getParentNode()));
            campaignOnlineController.getSelectedNode().setSelected(true);

            campaignOnlineController.prepareEdit();
            campaignOnlineController.setEditObject("condition");
            return true;
        }

        if ("cam_offline".equals(CheckOnEdit.onEdit)) {
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            CampaignOfflineController campaignOfflineController = (CampaignOfflineController) elContext.getELResolver().getValue(elContext, null, "campaignOfflineController");

            campaignOfflineController.getParentNode().getChildren().remove(campaignOfflineController.getSelectedNode());
            campaignOfflineController.setSelectedNode(new DefaultTreeNode("condition", this.conditionTable, campaignOfflineController.getParentNode()));
            campaignOfflineController.getSelectedNode().setSelected(true);

            campaignOfflineController.prepareEdit();
            campaignOfflineController.setEditObject("condition");
            return true;
        }


//        initTreeNode();
        if (isUpdate) {
            TreeNode parentNote = selectedNode.getParent();
            if (parentNote != null) {
                parentNote.getChildren().remove(selectedNode);
                if (parentNote.getChildren().size() == 0) {
                    removeExpandedNode(parentNote);
                }
            }
        }
        if (!isUpdate) {
            conditionTables = DataUtil.isNullOrEmpty(conditionTables) ? new ArrayList<>() : conditionTables;
            conditionTables.add(this.conditionTable);
        }
        addNoteToCategoryTree(rootNode, conditionTable);
        mapTreeStatus(rootNode);
        isUpdate = true;
        actonSwitch = false;


        prepareEditObject();
        return true;
    }

    public TreeNode addNoteToCategoryTree(TreeNode rootNode, ConditionTable conditionTable) {
        TreeNode note = findParentNoteInTree(rootNode, conditionTable);
        if (note != null) {
            TreeNode newNote = new DefaultTreeNode("condition", conditionTable, note);
            expandCurrentNode(newNote);
            this.selectedNode = newNote;
            return newNote;
        }
        return null;
    }

    public TreeNode findParentNoteInTree(TreeNode root, ConditionTable conditionTable) {
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (Category.class.isInstance(note.getData())) {
                Category data = (Category) note.getData();
                if (DataUtil.safeEqual(data.getCategoryId(), conditionTable.getCategoryId())) {
                    return note;
                }
            }
            currentNote = findParentNoteInTree(note, conditionTable);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

    public void onSelectRow() {
        System.out.println("rowIndex:" + this.conditionTableDTO.getIndex());
        try {
            int index = Integer.parseInt(this.conditionTableDTO.getIndex()) - 1;
            for (ConditionTableDTO dto : conditionTableDTOS) {
                dto.setIsDefault("");
            }
            this.conditionTableDTOS.get(index).setIsDefault("1");
        } catch (Exception e) {

        }
    }

    public void onSelectRowProcessValue(int rowIndex, int colIndex) {
        ConditionTableDTO dto = conditionTableDTOS.get(rowIndex);
        List<ProcessValue> lstProcessValue = this.preProcessUnits.get(colIndex).getLstProcessValue();
        String idPv = dto.getValueIds().get(colIndex);
        String color = "fffcfd";
        String name = "";
        int index = 0;
        for (ProcessValue processValue : lstProcessValue) {
            if (processValue.getValueId() == Long.parseLong(idPv)) {
                color = processValue.getValueColor();
                name = processValue.getValueName();
                break;
            }
            index++;
        }
        dto.getValueColor().set(colIndex, color);
        dto.getValues().set(colIndex, name);
        for (ConditionTableDTO conditionTableDTO1 : this.conditionTableDTOS) {
            String combine = "";
            if (!DataUtil.isNullOrEmpty(conditionTableDTO1.getValueIds())) {
                for (String id : conditionTableDTO1.getValueIds()) {
                    combine += id + "/";
                }
                combine = combine.substring(0, combine.lastIndexOf("/"));
            }
//            if (combine.contains("-1")) {
//                combine = Constants.COMBINE;
//            }
            conditionTableDTO1.setCombine(combine);
        }
    }

    public void onAddRowPPU() {
        System.out.println("onAddRowPPU:");
        preProcessUnits = DataUtil.isNullOrEmpty(preProcessUnits) ? new ArrayList<>() : preProcessUnits;
        PreProcessUnit preProcessUnit = new PreProcessUnit();
        preProcessUnits.add(preProcessUnit);
    }

    public void onAddRow() {
        System.out.println("onAddRow:");
        if (DataUtil.isNullOrEmpty(lstColumnCt)) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.ppu"));
            return;
        }

        if (DataUtil.isNullOrEmpty(this.conditionTableDTOS)) {
            this.conditionTableDTOS = new ArrayList<>();
        }
        ConditionTableDTO newItem = new ConditionTableDTO();
        int size = this.lstColumnCt.size();
        List<String> valuesIds = new ArrayList<>();
        List<String> values = new ArrayList<>();
        List<String> valuesColor = new ArrayList<>();
        String combine = "";
        for (int i = 0; i < size; i++) {
            values.add("");
            valuesIds.add("-1");
            valuesColor.add("fffcfd"); // ma mau trang
            combine += "-1" + "/";
        }
        combine = combine.substring(0, combine.length() - 1);
        newItem.setCombine(combine);
        newItem.setValueIds(valuesIds);
        newItem.setValues(values);
        newItem.setValueColor(valuesColor);
        newItem.setIndex("" + (this.conditionTableDTOS.size() + 1));
        newItem.setPreProcessIds(new ArrayList<>(preProcessIds));
        this.conditionTableDTOS.add(newItem);
    }

    public void onAddColumn() {
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('addColumnPopup').show();");
    }

    public void onAddColumnPPU(PreProcessUnit preProcessUnit) {
        this.preProcessUnit = preProcessUnit;
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('preprocessUnitTreePopup').show();");
    }

    public void onClearColumn() {
        System.out.println("onClearColumn:");
        this.preProcessIds = new ArrayList<>();
        this.columns = new ArrayList<>();
        this.lstColumnCt = new ArrayList<>();
        this.conditionTableDTOS = new ArrayList<>();
        this.lstProcessValue = new ArrayList<>();
    }

    public boolean validateChoosePPU() {

        if (this.selectedPreprocessUnitNode == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.preprocess"));
            return false;
        }
        if (this.selectedPreprocessUnitNode.getData() instanceof PreProcessUnit) {

        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.preprocess"));
            return false;
        }
        PreProcessUnit preProcessUnit = (PreProcessUnit) selectedPreprocessUnitNode.getData();
        this.preProcessUnit.setPreProcessId(preProcessUnit.getPreProcessId());
        this.preProcessUnit.setPreProcessName(preProcessUnit.getPreProcessName());
        this.preProcessUnit.setColumnName(preProcessUnit.getPreProcessName());
        return true;
    }

    public boolean validatePreprocessUnit() {
        if (DataUtil.isNullOrEmpty(preProcessUnits)) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.preprocess.one"));
            return false;
        } else {
            Map<String, String> mapsName = new HashMap<>();
            Map<Long, Long> mapsId = new HashMap<>();
            for (PreProcessUnit item : preProcessUnits) {
                if (DataUtil.isStringNullOrEmpty(item.getColumnName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Column name");
                    return false;
                }
                if (!DataUtil.checkMaxlength(item.getColumnName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Column name");
                    return false;
                }
                if (!DataUtil.checkNotContainPercentage(item.getColumnName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Column name");
                    return false;
                }
                if (item.getPreProcessId() == 0) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.choose.ppu"));
                    return false;
                }
                if (mapsName.containsKey(item.getColumnName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.preprocess.name.duplicate"));
                    return false;
                } else {
                    mapsName.put(item.getColumnName(), item.getColumnName());
                }

                if (mapsId.containsKey(item.getPreProcessId())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.preprocess.duplicate"));
                    return false;
                } else {
                    mapsId.put(item.getPreProcessId(), item.getPreProcessId());
                }
            }
        }

        this.preProcessIds = new ArrayList<>();
        preProcessUnits.forEach((e) -> {
            preProcessIds.add(e.getPreProcessId());
        });
//        PreProcessUnit preProcessUnit = (PreProcessUnit) this.selectedPreprocessUnitNode.getData();
//        preProcessUnits.add(preProcessUnit);
//        long preprocessId = preProcessUnit.getPreProcessId();
        getDataProcessValue();
        columns = new ArrayList<>();
        lstColumnCt = new ArrayList<>();
        preProcessUnits.forEach((e) -> {
            e.setColumnName(e.getColumnName().trim());
            ColumnCt itemAdd = new ColumnCt();
            String columnName = e.getColumnName();
            itemAdd.setColumnName(columnName);
            lstColumnCt.add(itemAdd);
            columns.add(new ColumnsView.ColumnModel(itemAdd.getColumnName(), "value"));
        });

        // update lai cac row cu
        if (!DataUtil.isNullOrEmpty(this.conditionTableDTOS)) {
            for (ConditionTableDTO dto : conditionTableDTOS) {
                List<String> values = dto.getValues();
                List<String> valueIds = dto.getValueIds();
                List<String> valueColor = dto.getValueColor();
                List<Long> preProcessIds = dto.getPreProcessIds();
                Map<Long, String> mapsPpuId = new HashMap<>();
                Map<Long, String> mapsColor = new HashMap<>();
                Map<Long, String> mapsValues = new HashMap<>();
                int index = 0;
                for (Long preProcessId : preProcessIds) {
                    mapsPpuId.put(preProcessId, valueIds.get(index));
                    mapsColor.put(preProcessId, valueColor.get(index));
                    mapsValues.put(preProcessId, values.get(index));
                    index++;
                }
                valueIds = new ArrayList<>();
                valueColor = new ArrayList<>();
                values = new ArrayList<>();

                for (Long preProcessId : this.preProcessIds) {
                    String id = DataUtil.isNullObject(mapsPpuId.get(preProcessId)) ? "-1" : mapsPpuId.get(preProcessId);
                    valueIds.add(id);
                    valueColor.add(mapsColor.get(preProcessId));
                    values.add(mapsValues.get(preProcessId));
                }

                String combine = "";
                for (String id : valueIds) {
                    combine += id + "/";
                }
                combine = combine.substring(0, combine.length() - 1);
                if (combine.contains("null")) {
                    combine = combine.replaceAll("null", "-1");
                }
                dto.setPreProcessIds(new ArrayList<>(this.preProcessIds));
                dto.setValueIds(valueIds);
                dto.setValues(values);
                dto.setValueColor(valueColor);
                dto.setCombine(combine);

//                dto.setValueIds(valueIds);
            }
        }
//        System.out.println("1212121212");
        return true;
    }

    public void getDataProcessValue() {
        this.lstProcessValueObject = new ArrayList<>();
//        this.lstProcessValue = conditionTableService.getProcessValueByValuePreProcessIds(this.preProcessIds);
        for (PreProcessUnit unit : preProcessUnits) {
            List<Long> ids = new ArrayList<>();
            ids.add(unit.getPreProcessId());
            List<ProcessValue> lstProcessValue = conditionTableService.getProcessValueByValuePreProcessIds(ids);
            unit.setLstProcessValue(lstProcessValue);
//            if (DataUtil.isNullOrEmpty(unit.getLstProcessValue())) {
//                unit.setLstProcessValue(lstProcessValue);
//            }
            this.lstProcessValueObject.add(lstProcessValue);
        }
//        this.mapProcessValue = new HashMap<>();
//        if (!DataUtil.isNullOrEmpty(lstProcessValue)) {
//            for (ProcessValue processValue : lstProcessValue) {
//                this.mapProcessValue.put(processValue.getValueId() + "", processValue.getValueName());
//            }
//        }
    }

    public void moveUpRowPPU(PreProcessUnit preProcessUnit) {
        if (!DataUtil.isNullOrEmpty(this.preProcessUnits) && preProcessUnits.size() > 1) {
            try {
                int index = preProcessUnits.indexOf(preProcessUnit);
                Collections.swap(this.preProcessUnits, index - 1, index);
            } catch (Exception e) {
                System.out.println("err moveUpRow");
            }
        }
    }

    public void moveDownRowPPU(PreProcessUnit preProcessUnit) {
        if (!DataUtil.isNullOrEmpty(this.preProcessUnits) && preProcessUnits.size() > 1) {
            try {
                int index = preProcessUnits.indexOf(preProcessUnit);
                Collections.swap(this.preProcessUnits, index, index + 1);
            } catch (Exception e) {
                System.out.println("err moveUpRow");
            }
        }
    }

    public void removeRowPPU(PreProcessUnit preProcessUnit) {
        try {
            this.preProcessUnits.remove(preProcessUnit);
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        } catch (Exception e) {
            System.out.println("err removeRow");
        }
    }

    public void moveUpRow(ConditionTableDTO conditionTableDTO) {
        System.out.println("moveUpRow : " + conditionTableDTO);
        if (!DataUtil.isNullOrEmpty(this.conditionTableDTOS) && conditionTableDTOS.size() > 1) {
            try {
                int index = Integer.parseInt(conditionTableDTO.getIndex()) - 1;
                Collections.swap(this.conditionTableDTOS, index - 1, index);
                int i = 1;
                for (ConditionTableDTO item : this.conditionTableDTOS) {
                    item.setIndex((i++) + "");
                }
            } catch (Exception e) {
                System.out.println("err moveUpRow");
            }
        }
    }

    public void moveDownRow(ConditionTableDTO conditionTableDTO) {
        System.out.println("moveDownRow : " + conditionTableDTO);
        if (!DataUtil.isNullOrEmpty(this.conditionTableDTOS) && conditionTableDTOS.size() > 1) {
            try {
                int index = Integer.parseInt(conditionTableDTO.getIndex()) - 1;
                Collections.swap(this.conditionTableDTOS, index, index + 1);
                int i = 1;
                for (ConditionTableDTO item : this.conditionTableDTOS) {
                    item.setIndex((i++) + "");
                }
            } catch (Exception e) {
                System.out.println("err moveDownRow");
            }
        }
    }

    public void removeRow(ConditionTableDTO conditionTableDTO) {

        System.out.println("removeRow : " + conditionTableDTO);
        try {
            int index = Integer.parseInt(conditionTableDTO.getIndex()) - 1;
            this.conditionTableDTOS.remove(index);
            if ("1".equalsIgnoreCase(conditionTableDTO.getIsDefault())) {
                this.conditionTableDTO = null;
                for (ConditionTableDTO item : this.conditionTableDTOS) {
                    item.setIsDefault("");
                }
            }
            int i = 1;
            for (ConditionTableDTO item : this.conditionTableDTOS) {
                item.setIndex((i++) + "");
            }
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        } catch (Exception e) {
            System.out.println("err removeRow");
        }
    }

    public void prepareAddObject() {
        this.editObject = "condition";
        long id = ussdScenarioService.getNextSequense(Constants.TableName.CONDITION_TABLE);
        this.isDisplay = true;
        this.isUpdate = false;
        actonSwitch = true;
        this.conditionTable = new ConditionTable();
        this.conditionTableDTOS = new ArrayList<>();
        Category category = (Category) this.selectedNode.getData();
        this.conditionTableDTO = new ConditionTableDTO();
        conditionTable.setCategoryId(category.getCategoryId());
        conditionTable.setConditionTableId(id);
        this.columns = new ArrayList<>();
        this.lstColumnCt = new ArrayList<>();
        this.lstProcessValue = new ArrayList<>();
        this.preProcessIds = new ArrayList<>();
        this.preProcessUnits = new ArrayList<>();
    }

    public void doDeleteObject() {
        System.out.println("doDeleteObject:" + ((ConditionTable) this.selectedNode.getData()).getConditionTableId());
        conditionTableService.doDelete((ConditionTable) this.selectedNode.getData());
        TreeNode parentNode = selectedNode.getParent();
        parentNode.getChildren().remove(selectedNode);
        if (parentNode.getChildren().size() == 0) {
            removeExpandedNode(parentNode);
        }
        initTreeNode();
        mapTreeStatus(rootNode);
        this.isDisplay = false;
        conditionTables.remove(this.selectedNode.getData());
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public boolean validateDeleteCondition() {
        Long id = ((ConditionTable) this.selectedNode.getData()).getConditionTableId();
        boolean rs = conditionTableService.checkDelete(id);
        if (!rs) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.condition.used"));
            rs = false;
        }
        return rs;
    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(Boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public Segment getSegment() {
        return segment;
    }

    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    public List<Segment> getLstSeg() {
        return lstSeg;
    }

    public void setLstSeg(List<Segment> lstSeg) {
        this.lstSeg = lstSeg;
    }

    public String getSearchSeg() {
        return searchSeg;
    }

    public void setSearchSeg(String searchSeg) {
        this.searchSeg = searchSeg;
    }

    public Rule getRuleSelected() {
        return ruleSelected;
    }

    public void setRuleSelected(Rule ruleSelected) {
        this.ruleSelected = ruleSelected;
    }

    public List<Rule> getLstRule() {
        return lstRule;
    }

    public void setLstRule(List<Rule> lstRule) {
        this.lstRule = lstRule;
    }

    public String getSearchRule() {
        return searchRule;
    }

    public void setSearchRule(String searchRule) {
        this.searchRule = searchRule;
    }

    public Invitation getInvitation() {
        return invitation;
    }

    public void setInvitation(Invitation invitation) {
        this.invitation = invitation;
    }

    public List<Invitation> getLstInvi() {
        return lstInvi;
    }

    public void setLstInvi(List<Invitation> lstInvi) {
        this.lstInvi = lstInvi;
    }

    public String getSearchInvi() {
        return searchInvi;
    }

    public void setSearchInvi(String searchInvi) {
        this.searchInvi = searchInvi;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<ConditionTable> getConditionTables() {
        return conditionTables;
    }

    public void setConditionTables(List<ConditionTable> conditionTables) {
        this.conditionTables = conditionTables;
    }

    public ConditionTable getConditionTable() {
        return conditionTable;
    }

    public void setConditionTable(ConditionTable conditionTable) {
        this.conditionTable = conditionTable;
    }

    public TreeNode getRoot() {
        return root;
    }

    public boolean getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(boolean isDisplay) {
        this.isDisplay = isDisplay;
    }

    public DualListModel<CdrService> getCdrServicePickingList() {
        return cdrServicePickingList;
    }

    public void setCdrServicePickingList(DualListModel<CdrService> cdrServicePickingList) {
        this.cdrServicePickingList = cdrServicePickingList;
    }

    public List<CdrService> getCdrServicesTable() {
        return cdrServicesTable;
    }

    public void setCdrServicesTable(List<CdrService> cdrServicesTable) {
        this.cdrServicesTable = cdrServicesTable;
    }

    public DualListModel<ResultTable> getResultTablePickingList() {
        return resultTablePickingList;
    }

    public void setResultTablePickingList(DualListModel<ResultTable> resultTablePickingList) {
        this.resultTablePickingList = resultTablePickingList;
    }

    public List<ResultTable> getResultTable() {
        return resultTable;
    }

    public void setResultTable(List<ResultTable> resultTable) {
        this.resultTable = resultTable;
    }

    public boolean isApplyAllCdrToRule() {
        return this.applyAllCdrToRule;
    }

    public void setApplyAllCdrToRule(boolean value) {
        this.applyAllCdrToRule = value;
    }

    public void changeCdrValue() {
        this.applyAllCdrToRule = applyAllCdrToRule;
    }

    public String getDatePattern() {
        return datePattern;
    }

    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }

    public void eventListener(ValueChangeEvent event) {
        System.out.println(event.toString());
    }

    public void valueChangeMethod(ValueChangeEvent e) {
        System.out.println(e.getNewValue().toString());
    }

    public String getMessageConfirm() {
        return messageConfirm;
    }

    public void setMessageConfirm(String messageConfirm) {
        this.messageConfirm = messageConfirm;
    }

    public List<ColumnsView.ColumnModel> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnsView.ColumnModel> columns) {
        this.columns = columns;
    }

    public List<ConditionTableDTO> getConditionTableDTOS() {
        return conditionTableDTOS;
    }

    public void setConditionTableDTOS(List<ConditionTableDTO> conditionTableDTOS) {
        this.conditionTableDTOS = conditionTableDTOS;
    }

    public ConditionTableDTO getConditionTableDTO() {
        return conditionTableDTO;
    }

    public void setConditionTableDTO(ConditionTableDTO conditionTableDTO) {
        this.conditionTableDTO = conditionTableDTO;
    }

    public List<Long> getPreProcessIds() {
        return preProcessIds;
    }

    public void setPreProcessIds(List<Long> preProcessIds) {
        this.preProcessIds = preProcessIds;
    }

    public List<ProcessValue> getLstProcessValue() {
        return lstProcessValue;
    }

    public void setLstProcessValue(List<ProcessValue> lstProcessValue) {
        this.lstProcessValue = lstProcessValue;
    }

    public Map<String, String> getMapProcessValue() {
        return mapProcessValue;
    }

    public void setMapProcessValue(Map<String, String> mapProcessValue) {
        this.mapProcessValue = mapProcessValue;
    }

    public TreeNode getPreprocessUnitRootNode() {
        return preprocessUnitRootNode;
    }

    public void setPreprocessUnitRootNode(TreeNode preprocessUnitRootNode) {
        this.preprocessUnitRootNode = preprocessUnitRootNode;
    }

    public TreeNode getSelectedPreprocessUnitNode() {
        return selectedPreprocessUnitNode;
    }

    public void setSelectedPreprocessUnitNode(TreeNode selectedPreprocessUnitNode) {
        this.selectedPreprocessUnitNode = selectedPreprocessUnitNode;
    }

    public boolean getActonSwitch() {
//        System.out.println(actonSwitch);
        return actonSwitch;
    }

    public TreeNode getSelectedNodeTmp() {
        return selectedNodeTmp;
    }

    public void setSelectedNodeTmp(TreeNode selectedNodeTmp) {
        this.selectedNodeTmp = selectedNodeTmp;
    }

    public void setActonSwitch(boolean actonSwitch) {
        this.actonSwitch = actonSwitch;
    }

    public List<Object> getLstProcessValueObject() {
        return lstProcessValueObject;
    }

    public void setLstProcessValueObject(List<Object> lstProcessValueObject) {
        this.lstProcessValueObject = lstProcessValueObject;
    }

    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "condition";
        prepareEdit();
        this.actonSwitch = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "condition";
        prepareEdit();
        this.actonSwitch = true;

    }

    public void prepareEdit() {

        this.parentNode = selectedNode.getParent();
        if ("category".equals(selectedNode.getType())) {
            prepareEditCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        }
        if ("condition".equals(selectedNode.getType())) {
            this.editObject = "condition";
            prepareEditObject();
        }

        if ("ppu".equals(selectedNode.getType())) {
//            ColumnCt columnCt = (ColumnCt) selectedNode.getData();
//            PreProcessUnit ppu = preprocessUnitServiceImpl.findById(columnCt.getPreProcessId());
//            Category cat = categoryService.getCategoryById(ppu.getCategoryId());

            PreProcessUnit ppu = (PreProcessUnit) selectedNode.getData();

            if (Constants.PRE_PROCESS_UNIT_STRING == ppu.getPreProcessType()) {
                this.editObject = "ppu_string";
                preProcessController.setSelectedNode(selectedNode);
                preProcessController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_NUMBER == ppu.getPreProcessType()) {
                this.editObject = "ppu_number";
                preProcessNumberController.setSelectedNode(selectedNode);
                preProcessNumberController.onEditObject();
            }

            if (Constants.PROCESS_UNIT_EXIST_ELEMENT == ppu.getPreProcessType()) {
                this.editObject = "ppu_exist_element";
                preProcessExistElementController.setSelectedNode(selectedNode);
                preProcessExistElementController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_TIME == ppu.getPreProcessType()) {
                this.editObject = "ppu_time";
                preProcessTimeController.setSelectedNode(selectedNode);
                preProcessTimeController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_DATE == ppu.getPreProcessType()) {
                this.editObject = "ppu_date";
                preProcessDateController.setSelectedNode(selectedNode);
                preProcessDateController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_NUMBER_RANGE == ppu.getPreProcessType()) {
                this.editObject = "ppu_number_range";
                preProcessNumberRangeController.setSelectedNode(selectedNode);
                preProcessNumberRangeController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_COMPARE_NUMBER == ppu.getPreProcessType()) {
                this.editObject = "ppu_compare_nember";
                preProcessCompareNumberController.setSelectedNode(selectedNode);
                preProcessCompareNumberController.onEditObject();
            }

            if (Constants.PROCESS_UNIT_ZONE == ppu.getPreProcessType()) {
                this.editObject = "ppu_zone";
                preProcessZoneController.setSelectedNode(selectedNode);
                preProcessZoneController.onEditObject();
            }

            if (Constants.PRE_PROCESS_UNIT_SAME_ELEMENT == ppu.getPreProcessType()) {
                this.editObject = "ppu_same_element";
                preProcessSameElementController.setSelectedNode(selectedNode);
                preProcessSameElementController.onEditObject();
            }

        }

    }
}
