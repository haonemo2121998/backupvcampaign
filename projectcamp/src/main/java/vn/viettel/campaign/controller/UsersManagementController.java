/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import static vn.viettel.campaign.controller.BaseController.successMsg;
import vn.viettel.campaign.entities.Objects;
import vn.viettel.campaign.entities.RoleUser;
import vn.viettel.campaign.entities.Roles;
import vn.viettel.campaign.entities.Users;
import vn.viettel.campaign.service.ObjectService;
import vn.viettel.campaign.service.RolesObjectsService;
import vn.viettel.campaign.service.RolesService;
import vn.viettel.campaign.service.RolesUsersService;
import vn.viettel.campaign.service.UsersService;
import vn.viettel.campaign.service.UtilsService;
import vn.viettel.campaign.service.impl.LoginServiceImpl;

@ManagedBean
@ViewScoped
public class UsersManagementController extends BaseController implements Serializable {

    private Boolean userIschoose;

    private TreeNode rootUser;
    private TreeNode selectedNode;
    private String action;
    private Boolean actonSwitch;
    private Users userToken;

    private Users userToReset;

    private TreeNode rootMenu;
    private List<Objects> lstParent;
    private TreeNode[] chooseMenus;

    private List<Roles> lstRoles;
    private Long[] roleIds;
    private Long roleId;
    private String status;
    private String fullName;
    private String password;
    private String gender;
    private Date dob;
    private String identityCard;
    private String description;
    private String userName;

    private Users userSelected;
    private Long userSelectId;

    private String roleName;
    private String roleCode;
    private Roles roleSelected;

    private static final String PASS_MD = "$2a$10$/5Jo9m9qc4fIgY1fxQGCdejh3ufbRfJT0Ll.NfFRas2EndNuH/vJa";

    @Autowired
    private ObjectService objectServiceImpl;

    @Autowired
    private UsersService usersServiceImpl;

    @Autowired
    private RolesService rolesServiceImpl;

    @Autowired
    private RolesUsersService rolesUsersServiceImpl;

    @Autowired
    private RolesObjectsService rolesOjectsServiceImpl;

    @Autowired
    private UtilsService utilsService;

    @Autowired
    private LoginServiceImpl loginService;

    @PostConstruct
    public void init() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = (HttpSession) attr.getRequest().getSession(true);
        userToken = (Users) session.getAttribute("userToken");
        reloadTreeUsers();
        reloadRoles();

    }

    private void reloadTreeUsers() {
        List<Users> lst = usersServiceImpl.getLstUsers();
        rootUser = createTreeUsers(lst);
    }

    private TreeNode createTreeUsers(List<Users> lstParent) {
        //root
        TreeNode root1 = new CheckboxTreeNode(new Users(), null);
        lstParent.forEach((obj) -> {
            TreeNode parent = new CheckboxTreeNode(obj, root1);
            parent.setExpanded(true);
        });
        root1.setExpanded(true);
        return root1;
    }

    /*private void reloadTreeMenu() {
        lstParent = objectServiceImpl.getLstObjects();
        rootMenu = createTreeMenu(lstParent, null);
    }*/
    private TreeNode createTreeMenu(List<Objects> lstParent, List<Objects> lstChecked) {
        //root
        TreeNode root1 = new DefaultTreeNode(new Objects(), null);
        lstParent.forEach((obj) -> {
            TreeNode parent = new DefaultTreeNode(obj, root1);
            obj.getLstSub().forEach((sub) -> {
                TreeNode child = new DefaultTreeNode(sub, parent);
                if (lstChecked != null) {
                    lstChecked.forEach((check) -> {
                        if (check.getObjectId().longValue() == sub.getObjectId()) {
                            child.setSelected(true);
                            parent.setSelected(true);
                        }
                    });
                }
            });
            if (lstChecked != null) {
                lstChecked.forEach((check) -> {
                    if (check.getObjectId().longValue() == obj.getObjectId()) {
                        parent.setSelected(true);
                    }
                });
            }
            if (!obj.getLstSub().isEmpty() && obj.getLstSub().size() > 0) {
                parent.setExpanded(true);
            }
        });
        root1.setExpanded(true);
        return root1;
    }

    public void prepareManageRole() {
        rootMenu = createTreeMenu(lstParent, null);
        roleCode = null;
        roleName = null;
    }

    public void onAddNodeUser() {
        if ("edit".equals(action) && selectedNode != null) {
            selectedNode.setSelected(false);
            selectedNode = null;
        }
        action = "add";
        userSelected = new Users();
        setDataToForm(userSelected);
        rootMenu = null;
        roleIds = null;
        userIschoose = true;
        actonSwitch = true;
    }

    public void onRemoveNodeUser() throws IOException {
        Users user = (Users) selectedNode.getData();
        user.setStatus(0L);
        user.setUpdateDate(new Date(System.currentTimeMillis()));
        usersServiceImpl.updateUsers(user);
        rolesUsersServiceImpl.deleteRoleUserByUserId(user.getUserId());
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        if ("edit".equals(action) && userToReset.getUserId().equals(user.getUserId())) {
            userIschoose = false;
        }
        reloadTreeUsers();
        actonSwitch = false;

        if (userToken != null
                && StringUtils.isNotBlank(userToken.getUserName())
                && userToken.getUserName().equalsIgnoreCase(user.getUserName())) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/campaign/login.jsf");
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        }
    }

    public void onNodeUserSelect(NodeSelectEvent event) {
        userIschoose = true;
        Users user = (Users) event.getTreeNode().getData();
        loadDateToEdit(user);
        userToReset = user;
        actonSwitch = false;
    }

    public void loadDateToEdit(Users user) {
        userSelected = user;

        List<Long> lstRole = rolesServiceImpl.getRolesByUserId(userSelected.getUserId());
        if (lstRole != null) {
            roleIds = lstRole.toArray(new Long[]{});
            List<Objects> lst = objectServiceImpl.getLstObjectsByRole(roleIds);
            rootMenu = createTreeMenu(lst, null);
            genTooltipForLstRole(roleIds);
        } else {
            tooltipForLstRole = "";
            rootMenu = null;
            roleIds = null;
        }
        setDataToForm(userSelected);
        action = "edit";
    }

    public void resetForm() {
        if ("add".equals(action)) {
            onAddNodeUser();
        }
        if ("edit".equals(action)) {
            loadDateToEdit(userToReset);
        }
    }

    public void setDataToForm(Users obj) {
        userName = obj.getUserName();
        fullName = obj.getFullName();
        dob = obj.getDateOfBirth() != null ? obj.getDateOfBirth() : null;
        gender = obj.getGender() != null ? obj.getGender().toString() : null;
        description = obj.getDescription();
        identityCard = obj.getIdentifyCard();
        status = obj.getStatus() != null ? obj.getStatus().toString() : "1";
        userSelectId = obj.getUserId() != null ? obj.getUserId() : null;
    }

    private Boolean validateForm() {

        if (userName == null || "".equals(userName.trim())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "User name is required!", null));
            return false;
        } else {
            List lst = usersServiceImpl.getLstUsersByName(userName, userSelected != null ? userSelected.getUserId() : null);
            if (lst != null && lst.size() > 0) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "This user name is exist in system!", null));
                return false;
            }
            if (userName.contains("%")) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "User name not contains % character!", null));
                return false;
            }
            if (userName.length() > 255) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "User name must be a maximum of 255 characters!", null));
                return false;
            }
        }

        if (fullName == null || "".equals(fullName.trim())) {
        } else {

            if (fullName.contains("%")) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Full name not contains % character!", null));
                return false;
            }
            if (fullName.length() > 255) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Full name must be a maximum of 255 characters!", null));
                return false;
            }
        }

        if (identityCard == null || "".equals(identityCard.trim())) {
        } else {

            if (identityCard.contains("%")) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Identity card not contains % character!", null));
                return false;
            }
            if (identityCard.length() > 255) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Identity card must be a maximum of 255 characters!", null));
                return false;
            }
        }

        if (description == null || "".equals(description.trim())) {
        } else {

            if (description.contains("%")) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Description not contains % character!", null));
                return false;
            }
            if (description.length() > 255) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Description must be a maximum of 255 characters!", null));
                return false;
            }
        }

        if (roleIds == null || roleIds.length == 0) {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Use must choose role!", null));
            return false;

        }
//        lst = objectServiceImpl.getLstObjectByName(objectCode, userSelected.getObjectId());
//        if (lst != null && lst.size() > 0) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "duplicate object code!", null));
//            return false;
//        }
//        if (order != null && Long.valueOf(order) <= 0) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "order is integer!", null));
//            return false;
//        }

        return true;
    }

    public void actionObject() {
        if (!validateForm()) {
            return;
        }
        setDataToObjects();
        try {
            if ("edit".equals(action)) {
                userSelected.setUpdateDate(new Date(System.currentTimeMillis()));
                userSelected.setUpdateUser(userToken.getUserName());
                usersServiceImpl.updateUsers(userSelected);
            } else {
                String passDefault = (String) utilsService.getObject("password.user.create.default", PASS_MD);
                userSelected.setCreateDate(new Date(System.currentTimeMillis()));
                userSelected.setUpdateDate(new Date(System.currentTimeMillis()));
                userSelected.setCreateUser(userToken.getUserName());
                userSelected.setUpdateUser(userToken.getUserName());
                userSelected.setPassWord(passDefault);
                if (usersServiceImpl.saveUsers(userSelected)) {
                    action = "edit";
                    userSelectId = userSelected.getUserId();
                }
            }

            this.saveRoleUser(userSelected.getUserId());

        } catch (NumberFormatException e) {
            return;
        }

        actonSwitch = false;
        reloadTreeUsers();
        userToReset = userSelected;
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));

        loginService.setSessionMenu();
    }

    private void saveRoleUser(Long userId) {
        List<RoleUser> roleUsers = rolesUsersServiceImpl.findByUserId(userId);
        List<Long> list = Arrays.asList(roleIds);
        RoleUser roleUser;
        if (roleUsers.isEmpty() && list != null && list.size() > 0) {
            for (Long roleId1 : list) {
                roleUser = new RoleUser();
                roleUser.setIsActive(1l);
                roleUser.setRoleId(roleId1);
                roleUser.setUserId(userId);
                rolesUsersServiceImpl.saveRoleUser(roleUser);
            }
        } else {
            if (list != null && list.size() > 0) {
                List<Long> list1 = new ArrayList<>();
                roleUsers.stream().forEach((item) -> {
                    if (item.getRoleId() != null && list.contains(item.getRoleId())) {
                        item.setIsActive(1l);
                        rolesUsersServiceImpl.updateRoleUser(item);
                        list1.add(item.getRoleId());
                    } else {
                        item.setIsActive(0l);
                        rolesUsersServiceImpl.updateRoleUser(item);
                    }
                });

                for (Long roleId1 : list) {
                    if (!list1.contains(roleId1)) {
                        roleUser = new RoleUser();
                        roleUser.setIsActive(1l);
                        roleUser.setRoleId(roleId1);
                        roleUser.setUserId(userId);
                        rolesUsersServiceImpl.saveRoleUser(roleUser);
                    }
                }
            }
        }
    }

    /*private List<Long> getLstObjectId() {
        List<Long> lst = new ArrayList();
        for (TreeNode chooseMenu : chooseMenus) {
            Objects obj = (Objects) chooseMenu.getData();
            lst.add(obj.getObjectId());
        }
        return lst;
    }*/
    private void setDataToObjects() {
        userSelected.setUserId(userSelectId != null ? userSelectId : null);
        userSelected.setUserName(StringUtils.isNotBlank(userName) ? userName.trim() : StringUtils.EMPTY);
        userSelected.setFullName(StringUtils.isNotBlank(fullName) ? fullName.trim() : StringUtils.EMPTY);
        userSelected.setStatus(status != null && !"".equals(status) ? Long.valueOf(status) : 1L);
        if (dob != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(dob);
//            c.add(Calendar.DATE, 1);
            dob = c.getTime();
            userSelected.setDateOfBirth(dob != null ? (dob) : null);
        }
        userSelected.setGender(Long.valueOf(gender));
        userSelected.setIdentifyCard(identityCard.trim());
        userSelected.setDescription(description.trim());
    }

    public void changeRole() {
        if (roleIds != null && roleIds.length > 0) {
            List<Objects> lst = objectServiceImpl.getLstObjectsByRole(roleIds);
            rootMenu = createTreeMenu(lst, null);
            genTooltipForLstRole(roleIds);
//            roleSelected = rolesServiceImpl.getRolesById(Long.valueOf(roleId));
//            roleName = roleSelected.getRoleName();
//            roleCode = roleSelected.getRoleCode();
        } else {
            tooltipForLstRole ="";
            rootMenu = null;
            roleName = null;
            roleCode = null;
        }
    }

    public void reloadRoles() {
        lstRoles = rolesServiceImpl.getLstRoles();
    }

    public void resetPassWord() {
        String passDefault = (String) utilsService.getObject("password.user.reset.default", PASS_MD);
        usersServiceImpl.changePassword(userSelected.getUserId(), passDefault, null);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public TreeNode getRootUser() {
        return rootUser;
    }

    public void setRootUser(TreeNode rootUser) {
        this.rootUser = rootUser;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Boolean getActonSwitch() {
        return actonSwitch;
    }

    public void setActonSwitch(Boolean actonSwitch) {
        this.actonSwitch = actonSwitch;
    }

    public Users getUserToken() {
        return userToken;
    }

    public void setUserToken(Users userToken) {
        this.userToken = userToken;
    }

    public TreeNode getRootMenu() {
        return rootMenu;
    }

    public void setRootMenu(TreeNode rootMenu) {
        this.rootMenu = rootMenu;
    }

    public TreeNode[] getChooseMenus() {
        return chooseMenus;
    }

    public void setChooseMenus(TreeNode[] chooseMenus) {
        this.chooseMenus = chooseMenus;
    }

    public List<Objects> getLstParent() {
        return lstParent;
    }

    public void setLstParent(List<Objects> lstParent) {
        this.lstParent = lstParent;
    }

    public List<Roles> getLstRoles() {
        return lstRoles;
    }

    public void setLstRoles(List<Roles> lstRoles) {
        this.lstRoles = lstRoles;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public Long[] getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(Long[] roleIds) {
        this.roleIds = roleIds;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Boolean getUserIschoose() {
        return userIschoose;
    }

    public void setUserIschoose(Boolean userIschoose) {
        this.userIschoose = userIschoose;
    }
    
    
    
    

    private String tooltipForLstRole = "";

    public String getTooltipForLstRole() {
        return tooltipForLstRole;
    }

    public void setTooltipForLstRole(String tooltipForLstRole) {
        this.tooltipForLstRole = tooltipForLstRole;
    }

    public void genTooltipForLstRole(Long[] listRoleId) {
        tooltipForLstRole = "";
        for (Long item : listRoleId) {
            Roles role = rolesServiceImpl.getRolesById(item);
            tooltipForLstRole = tooltipForLstRole + role.getRoleName() + " - " + role.getRoleCode() + "\n\n";
        }
    }

}
