/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.dto.ConditionTableDTO;
import vn.viettel.campaign.dto.NotifyContentDTO;
import vn.viettel.campaign.dto.NotifyTemplateDetailDTO;
import vn.viettel.campaign.dto.NotifyTriggerDTO;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.*;
import vn.viettel.campaign.service.impl.TreeServiceImpl;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Objects;
import java.util.*;

import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author ConKC
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class NotifyTriggerController extends BaseController implements Serializable {

    private boolean isDisplay;
    private boolean showDetail;
    private boolean actonSwitch = false;
    private boolean addRoot = true;
    private TreeNode rootNode;
    private TreeNode rootStatisticNode;
    private TreeNode rootScenarioNode;
    private TreeNode preprocessUnitRootNode;
    private TreeNode notifyTemplateRootNode;
    private TreeNode tempRootNode;
    private TreeNode ruleRootNode;
    private TreeNode selectedNode;
    private TreeNode selectedScenarioNode;
    private TreeNode selectedStatisticNode;
    private TreeNode selectedNodeTmp;
    private TreeNode selectedPreprocessUnitNode;
    private TreeNode selectedNotifyTemplateNode;
    private TreeNode selectedTemmpNode;
    private TreeNode selectedRuleNode;
    private List<Category> categories;
    private List<String> lstParamString;
    private List<RetryCycle> retryCycles;
    private List<DataType> dataTypes;
    private List<CycleUnit> cycleUnits;
    private List<NotifyTrigger> notifyTriggers;
    private NotifyTrigger notifyTrigger;
    private StatisticCycle statisticCycle;
    boolean addStatistic;
    private List<UssdScenario> ussdScenarios;
    private List<OcsBehaviour> ocsBehaviours;
    private List<Language> languages;
    private Category category;
    private ScenarioTrigger scenarioTrigger;
    private ScenarioNode scenarioNode;
    private Boolean isUpdate;
    private TreeNode root;
    private Segment segment;
    NotifyValues notifyValues;
    private List<Segment> lstSeg;
    private List<Segment> lstSegFiltered;
    private String searchSeg;
    private Rule ruleSelected;
    private List<Rule> lstRule;
    private String searchRule;
    private Invitation invitation;
    private List<Invitation> lstInvi;
    private List<BlackList> lstBlackList;
    private List<SpecialProm> lstSpecialProm;
    private String searchInvi;
    private String action;
    private String messageConfirm = "Are you sure to create this Category?";
    private String datePattern = "dd/MM/yyyy";
    private Map<Long, Segment> mapKeySegment = new HashMap<>();
    private List<Segment> lstTreeSegment = new ArrayList<>();
    private Rule createNewRule = new Rule();
    private List<CdrService> cdrServicesTable = new ArrayList<>();
    private List<ResultTable> resultTable = new ArrayList<>();
    private List<CdrService> cdrServicesAll = new ArrayList<>();
    private List<ResultTable> resultTableAllData = new ArrayList<>();
    private DualListModel<CdrService> cdrServicePickingList = new DualListModel<>();
    private DualListModel<ResultTable> resultTablePickingList = new DualListModel<>();
    private boolean applyAllCdrToRule = false;
    List<Category> ruleCategories = new ArrayList<>();
    @Autowired
    private UssdScenarioService ussdScenarioService;
    private UploadedFile file;
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TreeServiceImpl treeService;

    @Autowired
    private CampaignOnlineService campaignOnlineService;

    @Autowired
    SpecialPromService specialPromServiceImpl;

    @Autowired
    private UtilsService utilsService;

    @Autowired
    private ScenarioTriggerService scenarioTriggerService;

    @Autowired
    private NotifyTriggerService notifyTriggerService;

    @Autowired
    private NotifyTemplateService notifyTemplateService;

    @Autowired
    private PromotionBlockService promotionBlockService;

    @Autowired
    OCSBehaviourInterface oCSBehaviourServiceImpl;

    @PostConstruct
    public void init() {
        this.isDisplay = false;
        this.category = new Category();
        this.categories = new ArrayList<>();
        this.isUpdate = false;
        this.root = new DefaultTreeNode(null, null);
        this.retryCycles = notifyTriggerService.getAllRetryCycle();
        this.cycleUnits = notifyTriggerService.getAllCycleUnit();
        this.dataTypes = notifyTriggerService.getAllDataType();
        this.statisticCycle = new StatisticCycle();
        this.lstParamString = new ArrayList<>();
        initTreeNode();
        addExpandedNode(rootNode.getChildren().get(0));

    }

    public void initTreeNode() {
        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_NOTIFY_TRIGGER_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.notifyTriggers = categoryService.getNotifyTriggerByCategory(longs);
        rootNode = treeService.createNotifyTriggerTree(categories, notifyTriggers);
    }

    public void prepareCreateCategory() {
        this.isUpdate = false;
//        messageConfirm = "Are you sure to create this Category?";
        this.category = new Category();
        this.category.setCategoryType(Constants.CatagoryType.CATEGORY_NOTIFY_TRIGGER_TYPE);
        Category parentCat = (Category) selectedNode.getData();
        this.category.setParentId(parentCat.getCategoryId());
        Long id = ussdScenarioService.getNextSequense(Constants.TableName.CATEGORY);
        category.setCategoryId(id);
    }

    public void prepareEditCategory() {
        this.isUpdate = true;
//        messageConfirm = "Are you sure to edit this Category?";
        if (Objects.nonNull(selectedNode)) {
            Category cat = (Category) selectedNode.getData();
            this.category = categoryService.getCategoryById(cat.getCategoryId());
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void doSaveOrUpdateCategory() {
        if (validateCategory()) {
            categoryService.save(category);
            refreshTree();
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    public boolean validateCategory() {

        if (DataUtil.isStringNullOrEmpty(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Category name");
            return false;
        }
        boolean rs = true;
        if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId(), category.getCategoryType())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.category.exist"));
            rs = false;
        }
        return rs;
    }

    public boolean
    validateObject() {
        if (DataUtil.isStringNullOrEmpty(this.notifyTrigger.getTriggerName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Notification name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.notifyTrigger.getTriggerName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Notification name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.notifyTrigger.getTriggerName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Notification name");
            return false;
        }
        // desc
        if (!DataUtil.checkMaxlength(this.notifyTrigger.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.notifyTrigger.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Description");
            return false;
        }
        // Alias
        if (DataUtil.isStringNullOrEmpty(this.notifyTrigger.getAlias())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Alias");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.notifyTrigger.getAlias())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Alias");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.notifyTrigger.getAlias())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Alias");
            return false;
        }
        // StatisticCycles
        if (DataUtil.isNullOrEmpty(notifyTrigger.getStatisticCycles())) {
//            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("empty.statistic.cycle"));
//            return false;
        } else {
            for (StatisticCycle item : notifyTrigger.getStatisticCycles()) {
                if (DataUtil.isStringNullOrEmpty(item.getName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Statistic name");
                    return false;
                }
//                if (!DataUtil.checkMaxlength(item.getName())) {
//                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Statistic name");
//                    return false;
//                }
                if (!DataUtil.checkNotContainPercentage(item.getName())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Statistic name");
                    return false;
                }
            }
        }
        // template
        if (DataUtil.isNullObject(notifyTrigger.getNotifyTemplateDetailDTO())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("empty.notifytemplate.cycle"));
            return false;
        }
        // notify values
        if (DataUtil.isNullOrEmpty(notifyTrigger.getNotifyValues())) {
//            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("empty.notifyvalue.cycle"));
//            return false;
        } else {
            Map<String, String> maps = new HashMap<>();
            for (NotifyValues item : notifyTrigger.getNotifyValues()) {
                if (maps.containsKey(item.getParamString())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.parameter.duplicate"), item.getParamString());
                    return false;
                } else {
                    maps.put(item.getParamString(), item.getParamString());
                }
                if (DataUtil.isStringNullOrEmpty(item.getParamString())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Parameter");
                    return false;
                }
                if (!DataUtil.checkMaxlength(item.getParamString(), 45)) {
                    errorMsgParams(Constants.REMOTE_GROWL, "Parameter must be a maximum of 45 characters");
                    return false;
                }
                if (!DataUtil.checkNotContainPercentage(item.getParamString())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Parameter");
                    return false;
                }
                if (DataUtil.isStringNullOrEmpty(item.getValue())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Replace value");
                    return false;
                }
                if (DataUtil.isNullObject(item.getDataType())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Data type");
                    return false;
                }
                if (!DataUtil.checkMaxlength(item.getValue())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Replace value");
                    return false;
                }
                if (!DataUtil.checkNotContainPercentage(item.getValue())) {
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Replace value");
                    return false;
                }
            }
        }
        if (!notifyTriggerService.validateAdd(notifyTrigger.getNotifyTriggerId(), notifyTrigger.getTriggerName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notify.trigger.exist"));
            return false;
        }
        if (!validateChangeTemplate()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notify.template.change"));
//            NotifyTemplate notifyTemplate = (NotifyTemplate) this.selectedNotifyTemplateNode.getData();

            NotifyTemplateDetailDTO notifyTemplateDetailDTO = notifyTemplateService.getDetail(notifyTrigger.getNotifyTemplateDetailDTO().getId());
            this.notifyTrigger.setNotifyTemplateDetailDTO(notifyTemplateDetailDTO);
            // xu ly cat chuoi
            this.lstParamString = new ArrayList<>();
            List<NotifyContentDTO> contents = notifyTemplateDetailDTO.getContents();
            splitContent(contents);
//            Collections.sort(lstParamString);
            if (!DataUtil.isNullOrEmpty(lstParamString)) {
                Map<String, String> mapsParamString = new HashMap<>();
                List<NotifyValues> lstTmp = notifyTrigger.getNotifyValues();
                List<NotifyValues> lst = new ArrayList<>();
                if (!DataUtil.isNullOrEmpty(lstTmp)) {
                    for (String s : lstParamString) {
                        mapsParamString.put(s, s);
                    }
                    for (NotifyValues item : lstTmp) {
                        if (mapsParamString.containsKey(item.getParamString())) {
                            lst.add(item);
                        }
                    }
                    if (!DataUtil.isNullOrEmpty(lstParamString)) {
                        for (String s : lstParamString) {
                            boolean contain = false;
                            for (NotifyValues item : lstTmp) {
                                if (item.getParamString().equalsIgnoreCase(s)) {
                                    contain = true;
                                }
                            }
                            if (!contain) {
                                NotifyValues notifyValues = new NotifyValues();
                                notifyValues.setSourceType(1L);
                                notifyValues.setParamString(s);
                                lst.add(notifyValues);
                            }
                        }
                    }
                } else {
                    if (!DataUtil.isNullOrEmpty(lstParamString)) {
                        for (String s : lstParamString) {
                            NotifyValues notifyValues = new NotifyValues();
                            notifyValues.setSourceType(1L);
                            notifyValues.setParamString(s);
                            lst.add(notifyValues);
                        }
                    }
                }
                notifyTrigger.setNotifyValues(lst);
            }
            return false;
        }
        return true;
    }


    public void onSelectScenario() {
//        this.scenarioNode = (ScenarioNode) this.selectedScenarioNode.getData();
//        this.showDetail = true;
    }

    public void onSelectNotifyType() {

//        List<ScenarioNode> lstScenarioNode = scenarioTriggerService.getScenarioNodeByUssdId(this.scenarioTrigger.getUssdScenarioId());
//        if (!DataUtil.isNullOrEmpty(lstScenarioNode)) {
//            this.rootScenarioNode = treeService.createTreeScenario(lstScenarioNode);
//        } else {
//            this.rootScenarioNode = null;
//        }
//        this.scenarioNode = (ScenarioNode) this.selectedScenarioNode.getData();
//        this.showDetail = true;
    }

    public void onSelectSourceType(NotifyValues notifyValues) {
        System.out.println("onSelectSourceType");
        notifyValues.setValue("");
    }

    public void prepareCreateSubNode() {
        System.out.println("prepareCreateSubNode");
        long nodeId = ussdScenarioService.getNextSequense(Constants.TableName.SCENARIO_NOTE);
        this.scenarioNode = new ScenarioNode();
        scenarioNode.setNodeId(nodeId);
        this.showDetail = true;
        ScenarioNode selected = (ScenarioNode) this.selectedScenarioNode.getData();
        scenarioNode.setParentId(selected.getNodeId());
    }

    public void clearNode() {
    }

    public void onAddRootNode() {
        long nodeId = ussdScenarioService.getNextSequense(Constants.TableName.SCENARIO_NOTE);
        this.scenarioNode = new ScenarioNode();
        scenarioNode.setNodeId(nodeId);
        this.showDetail = true;
    }

    public void doAddNode() {
    }

    public boolean validateAddNode() {
        if (DataUtil.isNullOrEmpty(scenarioNode.getLstNoteContent())) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.scenario.node"));
            return false;
        }
        return true;
    }

    private void refreshTree() {
//        this.categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_NOTIFY_TRIGGER_TYPE);
//        rootNode = treeService.createNotifyTriggerTree(categories, notifyTriggers);
//        rootNode.setSelected(true);

        if (isUpdate) {
//            categories.remove((Category) selectedNode.getData());
//            categories.add(this.category);
            this.selectedNode = this.selectedNode == null ? new DefaultTreeNode() : this.selectedNode;
            int i = categories.indexOf(selectedNode.getData());
            categories.remove(selectedNode.getData());
            categories.add(i, this.category);
        } else {
            categories.add(this.category);
        }

        rootNode = treeService.createNotifyTriggerTree(categories, notifyTriggers);
        processRefreshCategory(rootNode, selectedNode, category, isUpdate);
        updateCategoryInformNotify();
    }
    public void refreshData() {
        TreeNode treeNode = this.selectedNode;
        if (treeNode!= null) {
            this.selectedNodeTmp = treeNode;
        }
        if (treeNode != null && treeNode.getData() != null & treeNode.getData() instanceof Category) {
            prepareAddObject();
        } else {
            initTreeNode();
            mapTreeStatus(rootNode);
            prepareEditObject();
        }
        this.actonSwitch = true;
    }



    public void doDeleteCategory() {
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
            if (DataUtil.isNullObject(category.getParentId())) {
                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("chek.parent.category"));
                return;
            }
            if (categoryService.checkDeleteCategory(this.category.getCategoryId(), NotifyTrigger.class.getSimpleName())) {
                categoryService.deleteCategory(category);
                selectedNode.getChildren().clear();
                TreeNode parNode = selectedNode.getParent();
                selectedNode.getParent().getChildren().remove(selectedNode);
                if (parNode.getChildren().isEmpty()) {
                    removeExpandedNode(parNode);
                }
                selectedNode.setParent(null);
                selectedNode = null;
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
                categories.remove(category);
//                parNode.setExpanded(true);
                updateCategoryInformNotify();
            } else {
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void doDeleteNode() {
        System.out.println("doDeleteNode");
    }

    public void prepareEditObject() {
        this.isDisplay = true;
        this.isUpdate = true;
        actonSwitch = false;
        lstParamString = new ArrayList<>();
        try {

            try {
                this.notifyTrigger = (NotifyTrigger) selectedNode.getData();
            } catch (Exception e) {
                this.notifyTrigger = (NotifyTrigger) selectedNodeTmp.getData();
            }
            NotifyTrigger notifyTriggerTmp = notifyTriggerService.getDetail(notifyTrigger.getNotifyTriggerId());
            notifyTrigger.setStatisticCycles(notifyTriggerTmp.getStatisticCycles());
            notifyTrigger.setNotifyValues(notifyTriggerTmp.getNotifyValues());
            notifyTrigger.setCategoryId(notifyTriggerTmp.getCategoryId());
            notifyTrigger.setTriggerName(notifyTriggerTmp.getTriggerName());
            notifyTrigger.setDescription(notifyTriggerTmp.getDescription());
            notifyTrigger.setAlias(notifyTriggerTmp.getAlias());
            notifyTrigger.setNotifyType(notifyTriggerTmp.getNotifyType());
            notifyTrigger.setRetryCycleId(notifyTriggerTmp.getRetryCycleId());
            notifyTrigger.setRepeatTime(notifyTriggerTmp.getRepeatTime());
            notifyTrigger.setNumberRetry(notifyTriggerTmp.getNumberRetry());
//            NotifyTemplateDetailDTO notifyTemplateDetailDTO = notifyTemplateService.getDetail(notifyTrigger.getNotifyTemplateId());
            notifyTrigger.setNotifyTemplateDetailDTO(notifyTriggerTmp.getNotifyTemplateDetailDTO());
            notifyTrigger.setbInvite(1 == notifyTrigger.getIsInvite());
            splitContent(notifyTrigger.getNotifyTemplateDetailDTO().getContents());
//            Collections.sort(lstParamString);
            if (!DataUtil.isNullOrEmpty(lstParamString)) {
                Map<String, String> mapsParamString = new HashMap<>();
                List<NotifyValues> lstTmp = notifyTrigger.getNotifyValues();
                List<NotifyValues> lst = new ArrayList<>();
                for (String s : lstParamString) {
                    mapsParamString.put(s, s);
                }
                for (NotifyValues item : lstTmp) {
                    if (mapsParamString.containsKey(item.getParamString())) {
                        lst.add(item);
                    }
                }
                for (String s : lstParamString) {
                    boolean contain = false;
                    for (NotifyValues item : lstTmp) {
                        if (item.getParamString().equalsIgnoreCase(s)) {
                            contain = true;
                        }
                    }
                    if (!contain) {
                        NotifyValues notifyValues = new NotifyValues();
                        notifyValues.setSourceType(1L);
                        notifyValues.setParamString(s);
                        lst.add(notifyValues);
                    }
                }
                notifyTrigger.setNotifyValues(lst);
            }

        } catch (Exception e) {
        }

    }

    public void initDataUssdScenario() {

    }

    public boolean validateChangeTemplate() {
        NotifyTemplateDetailDTO notifyTemplateDetailDTO = this.notifyTrigger.getNotifyTemplateDetailDTO();
        NotifyTemplateDetailDTO notifyTemplateDetailDTODb = notifyTemplateService.getDetail(notifyTemplateDetailDTO.getId());

        List<NotifyContentDTO> contents = notifyTemplateDetailDTODb.getContents();
        List<String> lstParamStringDb = splitContentDb(contents);
//        Collections.sort(lstParamStringDb);
//        System.out.println(lstParamStringDb.equals(lstParamString));
        return lstParamStringDb.equals(lstParamString);
    }

    public List<String> splitContentDb(List<NotifyContentDTO> contents) {
        List<String> lstParamString = new ArrayList<>();
        if (!DataUtil.isNullOrEmpty(contents)) {
            for (NotifyContentDTO notifyContentDTO : contents) {
                String content = notifyContentDTO.getContent();
                content = content.replaceAll("\\r\\n", " ");
                if (!DataUtil.isNullOrEmpty(content) && content.contains("{") && content.contains("}")) {
//                    List<String> strings = Arrays.asList(content.replaceAll("^.*?\\{", "").split("}.*?(\\{|$)"));
                    List<String> strings = getListParam(content);
                    if (!DataUtil.isNullOrEmpty(strings)) {
                        for (String item : strings) {
//                            item = "" + item + "";
                            if (!DataUtil.isNullOrEmpty(item) && !lstParamString.contains(item)) {
                                lstParamString.add(item);
                            }
                        }

                    }
                }
            }
        }
        return lstParamString;
    }

    public void doSaveObject() {
        if (!validateChangeTemplate()) {
            return;
        }
//        ussdScenarioService.doSaveUssd(this.ussdScenario);
        if (notifyTrigger.getbInvite()) {
            notifyTrigger.setIsInvite(1L);
        } else {
            notifyTrigger.setIsInvite(0L);
            notifyTrigger.setRetryCycleId(null);
            notifyTrigger.setRepeatTime(null);
            notifyTrigger.setNumberRetry(null);
        }
        DataUtil.trimObject(notifyTrigger);
        notifyTriggerService.doSave(this.notifyTrigger);
//        initTreeNode();
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        addNoteToCategoryTree(rootNode, notifyTrigger);
        if (!isUpdate) {
            notifyTriggers = DataUtil.isNullOrEmpty(notifyTriggers) ? new ArrayList<>() : notifyTriggers;
            notifyTriggers.add(this.notifyTrigger);
        }
        mapTreeStatus(rootNode);
        isUpdate = true;
        actonSwitch = false;
    }

    public TreeNode addNoteToCategoryTree(TreeNode rootNode, NotifyTrigger conditionTable) {
        if (isUpdate) {
            return null;
        }
        TreeNode note = findParentNoteInTree(rootNode, conditionTable);
        if (note != null) {
            TreeNode newNote = new DefaultTreeNode("notifyTrigger", conditionTable, note);
            expandCurrentNode(newNote);
            this.selectedNode = newNote;
            return newNote;
        }
        return null;
    }

    public TreeNode findParentNoteInTree(TreeNode root, NotifyTrigger conditionTable) {
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (Category.class.isInstance(note.getData())) {
                Category data = (Category) note.getData();
                if (DataUtil.safeEqual(data.getCategoryId(), conditionTable.getCategoryId())) {
                    return note;
                }
            }
            currentNote = findParentNoteInTree(note, conditionTable);
            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

    public void onSelectRow() {

    }

    public void onChoosePath(NotifyValues notifyValues) {
        this.notifyValues = notifyValues;
        PrimeFaces current = PrimeFaces.current();
        this.tempRootNode = treeService.createTreePath(null);
        current.executeScript("PF('pathTreePopup').show();");
        System.out.println("onChoosePath");
    }

    public void onChooseNotifyTemplate() {
        System.out.println("onChooseNotifyTemplate");
        List<Category> categorieNT = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_NOTIFY_TEMPLATE_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categorieNT.isEmpty()) {
            categorieNT.forEach(item -> longs.add(item.getCategoryId()));
        }
        List<NotifyTemplate> notifyTemplateList = categoryService.getNotifyTemplateByCategory(longs);
        this.notifyTemplateRootNode = treeService.createNotifyTemplateTree(categorieNT, notifyTemplateList);
        this.notifyTrigger.setNotifyValues(null);

        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('notifyTemplateTreePopup').show();");
    }

    public boolean validateChooseNT() {
        try {
            if (this.selectedNotifyTemplateNode.getData() instanceof NotifyTemplate) {

            } else {

                errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.notify.template"));
                return false;
            }
        } catch (Exception e) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.notify.template"));
            return false;
        }
        NotifyTemplate notifyTemplate = (NotifyTemplate) this.selectedNotifyTemplateNode.getData();

        NotifyTemplateDetailDTO notifyTemplateDetailDTO = notifyTemplateService.getDetail(notifyTemplate.getId());
        this.notifyTrigger.setNotifyTemplateDetailDTO(notifyTemplateDetailDTO);
        // xu ly cat chuoi
        this.lstParamString = new ArrayList<>();
        List<NotifyContentDTO> contents = notifyTemplateDetailDTO.getContents();
        splitContent(contents);
//        Collections.sort(lstParamString);
        if (!DataUtil.isNullOrEmpty(lstParamString)) {
            List<NotifyValues> lst = new ArrayList<>();
            for (String s : lstParamString) {
                NotifyValues notifyValues = new NotifyValues();
                notifyValues.setSourceType(1L);
                notifyValues.setParamString(s);
                lst.add(notifyValues);
            }
            notifyTrigger.setNotifyValues(lst);
        }
        //
        return true;
    }

    public List<String> getListParam(String chuoi) {
        List<String> listParam = new ArrayList();
        try {
            byte ptext[] = chuoi.getBytes("ISO-8859-1");
            chuoi = new String(ptext, "UTF-8");

            byte bStart[] = "{".getBytes("ISO-8859-1");
            String strStart = new String(bStart, "UTF-8");

            byte bEnd[] = "}".getBytes("ISO-8859-1");
            String strEnd = new String(bEnd, "UTF-8");

            Integer start = null;
            Integer end = null;
            for (int i = 0; i < chuoi.length(); i++) {
                String str = chuoi.substring(i, i + 1);
                if (strStart.equals(str)) {
                    start = i;
                }

                if (start != null && strEnd.equals(str)) {
                    end = i;
                    listParam.add(chuoi.substring(start + 1, end));
                    start = null;
                    end = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listParam;
    }

    public void splitContent(List<NotifyContentDTO> contents) {
        if (!DataUtil.isNullOrEmpty(contents)) {
            for (NotifyContentDTO notifyContentDTO : contents) {
                String content = notifyContentDTO.getContent();
                content = content.replaceAll("\\r\\n", " ");
                if (!DataUtil.isNullOrEmpty(content) && content.contains("{") && content.contains("}")) {
//                    List<String> strings = Arrays.asList(content.replaceAll("^.*?\\{", "").split("}.*?(\\{|$)"));
                    List<String> strings = getListParam(content);
                    if (!DataUtil.isNullOrEmpty(strings)) {
                        for (String item : strings) {
//                            item = "{" + item + "}";
                            if (!DataUtil.isNullOrEmpty(item) && !lstParamString.contains(item)) {
                                lstParamString.add(item);
                            }
                        }

                    }
                }
            }
        }
        //

    }

    public boolean validateChoosePath() {
        NotifyTriggerDTO notifyTriggerDTO = (NotifyTriggerDTO) this.selectedTemmpNode.getData();
        this.notifyValues.setValue(notifyTriggerDTO.getName());
        return true;
    }

    public void onAddRowNV() {
        if (this.selectedNotifyTemplateNode.getData() instanceof NotifyTemplate) {

        } else {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notify.template.first"));
            return;
        }
        if (DataUtil.isNullOrEmpty(lstParamString)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notify.template.content"));
            return;
        }
        List<NotifyValues> notifyValues = this.notifyTrigger.getNotifyValues();
        notifyValues = DataUtil.isNullOrEmpty(notifyValues) ? new ArrayList<>() : notifyValues;
        NotifyValues newItem = new NotifyValues();
        newItem.setSourceType(1L);
        notifyValues.add(newItem);
        notifyTrigger.setNotifyValues(notifyValues);
        System.out.println("onAddRowNV");
    }

    public void onAddRowStatstic() {
        List<Category> categories = categoryService.getCategoryByType(Constants.CatagoryType.CATEORY_STATISTIC_CYCLE_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        List<StatisticCycle> statisticCycles = categoryService.getDataFromCatagoryId(longs, StatisticCycle.class.getSimpleName());
        rootStatisticNode = treeService.createStatisticCycleTree(categories, statisticCycles);
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('addStatisticTreePopup').show();");
    }

    public boolean validateAddRowStatstic() {
        if (!DataUtil.isNullObject(selectedStatisticNode)) {
            if (selectedStatisticNode.getData() instanceof StatisticCycle) {
                StatisticCycle cycle = (StatisticCycle) selectedStatisticNode.getData();
                List<StatisticCycle> statisticCycles = this.notifyTrigger.getStatisticCycles();
                statisticCycles = DataUtil.isNullOrEmpty(statisticCycles) ? new ArrayList<>() : statisticCycles;
                //
                for (StatisticCycle item : statisticCycles) {
                    if (item.getId() == cycle.getId()) {
                        errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.statistic.duplicate"));
                        return false;
                    }
                }
                statisticCycles.add(cycle);
                this.notifyTrigger.setStatisticCycles(statisticCycles);
            } else {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.statistic"));
                return false;
            }
        }
        return true;
    }

    public void onAddColumn() {
//        System.out.println("onAddColumn:");
//        PrimeFaces current = PrimeFaces.current();
//        current.executeScript("PF('preprocessUnitTreePopup').show();");
    }

    public boolean validateAddStatistic() {
        if (DataUtil.isStringNullOrEmpty(statisticCycle.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Statistic name");
            return false;
        }
        if (!DataUtil.checkMaxlength(statisticCycle.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Statistic name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(statisticCycle.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Statistic name");
            return false;
        }
        if (DataUtil.isNullObject(statisticCycle.getCycleUnitId())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Cycle unit id");
            return false;
        }
        if (DataUtil.isNullObject(statisticCycle.getCycleUnitTimes())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Cycle unit times");
            return false;
        }
        if (DataUtil.isNullObject(statisticCycle.getMaxNotify())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Max notify");
            return false;
        }
        List<StatisticCycle> statisticCycles = notifyTrigger.getStatisticCycles();
        statisticCycles = DataUtil.isNullOrEmpty(statisticCycles) ? new ArrayList<>() : statisticCycles;
        statisticCycles.add(statisticCycle);
        notifyTrigger.setStatisticCycles(statisticCycles);
        return true;
    }

    public void onchangeInvitation() {
        System.out.println("onchangeInvitation : ");
        if (notifyTrigger.getNumberRetry() != null && notifyTrigger.getNumberRetry() == -1) {
            notifyTrigger.setNumberRetry(null);
            notifyTrigger.setRepeatTime(null);
            notifyTrigger.setRetryCycleId(null);
        }
    }

    public void moveUpRow(ConditionTableDTO conditionTableDTO) {
        System.out.println("moveUpRow : " + conditionTableDTO);

    }

    public void moveDownRow(ConditionTableDTO conditionTableDTO) {
        System.out.println("moveDownRow : " + conditionTableDTO);

    }

    public void removeRow(NoteContent noteContent) {
        System.out.println("removeRow : ");
        try {
            this.scenarioNode.getLstNoteContent().remove(noteContent);
        } catch (Exception e) {
            System.out.println("err removeRow");
        }
    }

    public void removeRowStatistic(StatisticCycle statisticCycle) {

        if (DataUtil.isNullOrEmpty(notifyTriggerService.findByNotifyIdAndCycleId(notifyTrigger.getNotifyTriggerId(), statisticCycle.getId()))) {
            try {
                this.notifyTrigger.getStatisticCycles().remove(statisticCycle);
            } catch (Exception e) {

            }
        } else {
            // show confirm dlgConfirmDelete
            this.statisticCycle = statisticCycle;
            PrimeFaces current = PrimeFaces.current();
            current.executeScript("PF('dlgConfirmDelete').show();");
        }
        System.out.println("removeRowStatistic : ");
    }

    public void removeRowStatisticCfm() {
        try {
            this.notifyTrigger.getStatisticCycles().remove(this.statisticCycle);
        } catch (Exception e) {

        }

        System.out.println("removeRowStatistic : ");
    }

    public void viewStatistic(StatisticCycle statisticCycle) {
        this.addStatistic = false;
        System.out.println("viewStatistic : ");
        this.statisticCycle = statisticCycle;
        PrimeFaces current = PrimeFaces.current();
        current.executeScript("PF('statisticTreePopup').show();");
    }

    public void removeRowNotifyContent(NotifyValues notifyValues) {
        try {
            this.notifyTrigger.getNotifyValues().remove(notifyValues);
        } catch (Exception e) {

        }
        System.out.println("removeRowNotifyContent : ");
    }

    public void prepareAddObject() {
        this.isDisplay = true;
        this.isUpdate = false;
        actonSwitch = true;
        long id = ussdScenarioService.getNextSequense(Constants.TableName.NOTIFY_TRIGGER);
        notifyTrigger = new NotifyTrigger();
        Category category = (Category) this.selectedNode.getData();
        notifyTrigger.setCategoryId(category.getCategoryId());
        notifyTrigger.setNotifyTriggerId(id);
    }

    public void doDeleteObject() {
        Long id = ((NotifyTrigger) this.selectedNode.getData()).getNotifyTriggerId();
        notifyTriggerService.doDelete(id);
        TreeNode parentNode = selectedNode.getParent();
        parentNode.getChildren().remove(selectedNode);
        if (parentNode.getChildren().size() == 0) {
            removeExpandedNode(parentNode);
        }
        initTreeNode();
        mapTreeStatus(rootNode);
        this.isDisplay = false;
        this.showDetail = false;
        notifyTriggers.remove(this.selectedNode.getData());
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
    }

    public boolean validateDeleteObject() {
//        boolean rs = true;
        Long id = ((NotifyTrigger) this.selectedNode.getData()).getNotifyTriggerId();
        long checkId = promotionBlockService.checkUsePromotionBlock(Constants.PROM_TYPE_2, id);
        if (checkId > 0) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.notify.block"));
            return false;
        }
        boolean rs = oCSBehaviourServiceImpl.validateDeleteNotifyTrigger(id);
        if (!rs) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.ocs.block"));
            return false;
        }
        return true;
    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(Boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public Segment getSegment() {
        return segment;
    }

    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    public List<Segment> getLstSeg() {
        return lstSeg;
    }

    public void setLstSeg(List<Segment> lstSeg) {
        this.lstSeg = lstSeg;
    }

    public String getSearchSeg() {
        return searchSeg;
    }

    public void setSearchSeg(String searchSeg) {
        this.searchSeg = searchSeg;
    }

    public Rule getRuleSelected() {
        return ruleSelected;
    }

    public void setRuleSelected(Rule ruleSelected) {
        this.ruleSelected = ruleSelected;
    }

    public List<Rule> getLstRule() {
        return lstRule;
    }

    public void setLstRule(List<Rule> lstRule) {
        this.lstRule = lstRule;
    }

    public String getSearchRule() {
        return searchRule;
    }

    public void setSearchRule(String searchRule) {
        this.searchRule = searchRule;
    }

    public Invitation getInvitation() {
        return invitation;
    }

    public void setInvitation(Invitation invitation) {
        this.invitation = invitation;
    }

    public List<Invitation> getLstInvi() {
        return lstInvi;
    }

    public void setLstInvi(List<Invitation> lstInvi) {
        this.lstInvi = lstInvi;
    }

    public String getSearchInvi() {
        return searchInvi;
    }

    public void setSearchInvi(String searchInvi) {
        this.searchInvi = searchInvi;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<NotifyTrigger> getNotifyTriggers() {
        return notifyTriggers;
    }

    public void setNotifyTriggers(List<NotifyTrigger> notifyTriggers) {
        this.notifyTriggers = notifyTriggers;
    }

    public NotifyTrigger getNotifyTrigger() {
        return notifyTrigger;
    }

    public void setNotifyTrigger(NotifyTrigger notifyTrigger) {
        this.notifyTrigger = notifyTrigger;
    }

    public ScenarioTrigger getScenarioTrigger() {
        return scenarioTrigger;
    }

    public void setScenarioTrigger(ScenarioTrigger scenarioTrigger) {
        this.scenarioTrigger = scenarioTrigger;
    }

    public TreeNode getRoot() {
        return root;
    }

    public boolean getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(boolean isDisplay) {
        this.isDisplay = isDisplay;
    }

    public DualListModel<CdrService> getCdrServicePickingList() {
        return cdrServicePickingList;
    }

    public void setCdrServicePickingList(DualListModel<CdrService> cdrServicePickingList) {
        this.cdrServicePickingList = cdrServicePickingList;
    }

    public List<CdrService> getCdrServicesTable() {
        return cdrServicesTable;
    }

    public void setCdrServicesTable(List<CdrService> cdrServicesTable) {
        this.cdrServicesTable = cdrServicesTable;
    }

    public DualListModel<ResultTable> getResultTablePickingList() {
        return resultTablePickingList;
    }

    public void setResultTablePickingList(DualListModel<ResultTable> resultTablePickingList) {
        this.resultTablePickingList = resultTablePickingList;
    }

    public List<ResultTable> getResultTable() {
        return resultTable;
    }

    public void setResultTable(List<ResultTable> resultTable) {
        this.resultTable = resultTable;
    }

    public boolean isApplyAllCdrToRule() {
        return this.applyAllCdrToRule;
    }

    public void setApplyAllCdrToRule(boolean value) {
        this.applyAllCdrToRule = value;
    }

    public void changeCdrValue() {
        this.applyAllCdrToRule = applyAllCdrToRule;
    }

    public String getDatePattern() {
        return datePattern;
    }

    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }

    public void eventListener(ValueChangeEvent event) {
        System.out.println(event.toString());
    }

    public void valueChangeMethod(ValueChangeEvent e) {
        System.out.println(e.getNewValue().toString());
    }

    public String getMessageConfirm() {
        return messageConfirm;
    }

    public void setMessageConfirm(String messageConfirm) {
        this.messageConfirm = messageConfirm;
    }

    public TreeNode getPreprocessUnitRootNode() {
        return preprocessUnitRootNode;
    }

    public void setPreprocessUnitRootNode(TreeNode preprocessUnitRootNode) {
        this.preprocessUnitRootNode = preprocessUnitRootNode;
    }

    public TreeNode getSelectedPreprocessUnitNode() {
        return selectedPreprocessUnitNode;
    }

    public void setSelectedPreprocessUnitNode(TreeNode selectedPreprocessUnitNode) {
        this.selectedPreprocessUnitNode = selectedPreprocessUnitNode;
    }

    public boolean getActonSwitch() {
//        System.out.println(actonSwitch);
        return actonSwitch;
    }

    public TreeNode getSelectedNodeTmp() {
        return selectedNodeTmp;
    }

    public void setSelectedNodeTmp(TreeNode selectedNodeTmp) {
        this.selectedNodeTmp = selectedNodeTmp;
    }

    public void setActonSwitch(boolean actonSwitch) {
        this.actonSwitch = actonSwitch;
    }

    public void setAddRoot(boolean addRoot) {
        this.addRoot = addRoot;
    }

    public TreeNode getRootScenarioNode() {
        return rootScenarioNode;
    }

    public void setRootScenarioNode(TreeNode rootScenarioNode) {
        this.rootScenarioNode = rootScenarioNode;
    }

    public TreeNode getSelectedScenarioNode() {
        return selectedScenarioNode;
    }

    public void setSelectedScenarioNode(TreeNode selectedScenarioNode) {
        this.selectedScenarioNode = selectedScenarioNode;
    }

    public boolean isShowDetail() {
        return showDetail;
    }

    public void setShowDetail(boolean showDetail) {
        this.showDetail = showDetail;
    }

    public ScenarioNode getScenarioNode() {
        return scenarioNode;
    }

    public void setScenarioNode(ScenarioNode scenarioNode) {
        this.scenarioNode = scenarioNode;
    }

    public List<OcsBehaviour> getOcsBehaviours() {
        return ocsBehaviours;
    }

    public void setOcsBehaviours(List<OcsBehaviour> ocsBehaviours) {
        this.ocsBehaviours = ocsBehaviours;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public List<UssdScenario> getUssdScenarios() {
        return ussdScenarios;
    }

    public void setUssdScenarios(List<UssdScenario> ussdScenarios) {
        this.ussdScenarios = ussdScenarios;
    }

    public List<RetryCycle> getRetryCycles() {
        return retryCycles;
    }

    public void setRetryCycles(List<RetryCycle> retryCycles) {
        this.retryCycles = retryCycles;
    }

    public StatisticCycle getStatisticCycle() {
        return statisticCycle;
    }

    public void setStatisticCycle(StatisticCycle statisticCycle) {
        this.statisticCycle = statisticCycle;
    }

    public List<CycleUnit> getCycleUnits() {
        return cycleUnits;
    }

    public void setCycleUnits(List<CycleUnit> cycleUnits) {
        this.cycleUnits = cycleUnits;
    }

    public TreeNode getSelectedNotifyTemplateNode() {
        return selectedNotifyTemplateNode;
    }

    public void setSelectedNotifyTemplateNode(TreeNode selectedNotifyTemplateNode) {
        this.selectedNotifyTemplateNode = selectedNotifyTemplateNode;
    }

    public TreeNode getNotifyTemplateRootNode() {
        return notifyTemplateRootNode;
    }

    public void setNotifyTemplateRootNode(TreeNode notifyTemplateRootNode) {
        this.notifyTemplateRootNode = notifyTemplateRootNode;
    }

    public List<String> getLstParamString() {
        return lstParamString;
    }

    public void setLstParamString(List<String> lstParamString) {
        this.lstParamString = lstParamString;
    }

    public TreeNode getTempRootNode() {
        return tempRootNode;
    }

    public void setTempRootNode(TreeNode tempRootNode) {
        this.tempRootNode = tempRootNode;
    }

    public TreeNode getSelectedTemmpNode() {
        return selectedTemmpNode;
    }

    public void setSelectedTemmpNode(TreeNode selectedTemmpNode) {
        this.selectedTemmpNode = selectedTemmpNode;
    }

    public boolean isAddStatistic() {
        return addStatistic;
    }

    public void setAddStatistic(boolean addStatistic) {
        this.addStatistic = addStatistic;
    }

    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "notify_trigger";
        prepareEdit();
        this.actonSwitch = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "notify_trigger";
        prepareEdit();
        this.actonSwitch = true;

    }

    public void prepareEdit() {
        if ("category".equals(selectedNode.getType())) {
            prepareEditCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");
        }
        if ("notifyTrigger".equals(selectedNode.getType())) {
            prepareEditObject();
        }

    }

    public void prepareViewObject(NotifyTrigger notify) {
        this.isDisplay = true;
        this.isUpdate = true;
        actonSwitch = false;
        lstParamString = new ArrayList<>();
        try {

            this.notifyTrigger = notify;
            this.notifyTrigger = notifyTriggerService.getDetail(notifyTrigger.getNotifyTriggerId());
            notifyTrigger.setbInvite(1 == notifyTrigger.getIsInvite());
            splitContent(notifyTrigger.getNotifyTemplateDetailDTO().getContents());
//            Collections.sort(lstParamString);
            if (!DataUtil.isNullOrEmpty(lstParamString)) {
                Map<String, String> mapsParamString = new HashMap<>();
                List<NotifyValues> lstTmp = notifyTrigger.getNotifyValues();
                List<NotifyValues> lst = new ArrayList<>();
                for (String s : lstParamString) {
                    mapsParamString.put(s, s);
                }
                for (NotifyValues item : lstTmp) {
                    if (mapsParamString.containsKey(item.getParamString())) {
                        lst.add(item);
                    }
                }
                for (String s : lstParamString) {
                    boolean contain = false;
                    for (NotifyValues item : lstTmp) {
                        if (item.getParamString().equalsIgnoreCase(s)) {
                            contain = true;
                        }
                    }
                    if (!contain) {
                        NotifyValues notifyValues = new NotifyValues();
                        notifyValues.setSourceType(1L);
                        notifyValues.setParamString(s);
                        lst.add(notifyValues);
                    }
                }
                notifyTrigger.setNotifyValues(lst);
            }

        } catch (Exception e) {
        }

    }
}
