/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import vn.viettel.campaign.constants.Constants;
import static vn.viettel.campaign.controller.BaseController.successMsg;
import vn.viettel.campaign.entities.Objects;
import vn.viettel.campaign.entities.RoleObject;
import vn.viettel.campaign.entities.Roles;
import vn.viettel.campaign.entities.Users;
import vn.viettel.campaign.service.ObjectService;
import vn.viettel.campaign.service.RolesObjectsService;
import vn.viettel.campaign.service.RolesService;
import vn.viettel.campaign.service.RolesUsersService;
import vn.viettel.campaign.service.UtilsService;
import vn.viettel.campaign.service.impl.LoginServiceImpl;

@ManagedBean
@ViewScoped
public class RolesManagementController extends BaseController implements Serializable {

    private Roles roleToReset;
    private Boolean roleIsChoose;
    private TreeNode rootMenu;
    private TreeNode rootRole;
    private TreeNode chooseRole;
    private List<Objects> lstParent;
    private TreeNode[] chooseMenus;
    private List<Roles> lstRoles;
    private Long[] roleIds;
    private Long roleId;
    private String roleName;
    private String roleCode;
    private Roles roleSelected;
    private String action;
    private Boolean actonSwitch;
    private Users userToken;

    @Autowired
    private ObjectService objectServiceImpl;

    @Autowired
    private RolesService rolesServiceImpl;

    @Autowired
    private RolesObjectsService rolesObjectsServiceImpl;

    @Autowired
    private RolesUsersService rolesUsersServiceImpl;

    @Autowired
    private UtilsService utilsService;

    @Autowired
    private LoginServiceImpl loginService;

    @PostConstruct
    public void init() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = (HttpSession) attr.getRequest().getSession(true);
        reloadTreeMenu();
        action = "add";
        userToken = (Users) session.getAttribute("userToken");
        reloadTreeRoles();
        roleSelected = new Roles();
        actonSwitch = false;
    }

    private void reloadTreeMenu() {
        lstParent = objectServiceImpl.getLstObjects();
        rootMenu = createTreeMenu(lstParent, null);
    }

    public void reloadTreeRoles() {
        lstRoles = rolesServiceImpl.getLstRoles();
        rootRole = createTreeRole(lstRoles);
    }

    private TreeNode createTreeMenu(List<Objects> lstParent, List<Objects> lstChecked) {
        //root
        TreeNode root1 = new DefaultTreeNode(new Objects(), null);
        lstParent.forEach((obj) -> {
            TreeNode parent = new DefaultTreeNode(obj, root1);
            obj.getLstSub().forEach((sub) -> {
                TreeNode child = new DefaultTreeNode(sub, parent);
                if (lstChecked != null) {
                    lstChecked.forEach((check) -> {
                        check.getLstSub().forEach((subCheck) -> {
                            if (subCheck.getObjectId().longValue() == sub.getObjectId()) {
                                child.setSelected(true);
                                parent.setSelected(true);
                            }
                        });
                    });
                }
            });
            if (lstChecked != null && !parent.isSelected()) {
                lstChecked.forEach((check) -> {
                    if (check.getObjectId().longValue() == obj.getObjectId()) {
                        parent.setSelected(true);
                    }
                });
            }
            if (!obj.getLstSub().isEmpty() && obj.getLstSub().size() > 0) {
                parent.setExpanded(true);
            }
        });
        root1.setExpanded(true);
        return root1;
    }

    private TreeNode createTreeRole(List<Roles> lstRoles) {
        //root
        TreeNode root2 = new DefaultTreeNode(new Roles(), null);
        lstRoles.forEach((obj) -> {
            TreeNode parent = new DefaultTreeNode(obj, root2);
        });
        root2.setExpanded(true);
        return root2;
    }

    public void changeRole() {
        if (roleId != null) {
            roleIds = new Long[]{roleId};
            List<Objects> lst = objectServiceImpl.getLstObjectsByRole(roleIds);
            rootMenu = createTreeMenu(lstParent, lst);
            roleSelected = rolesServiceImpl.getRolesById((roleId));
            roleName = roleSelected.getRoleName();
            roleCode = roleSelected.getRoleCode();
            action = "edit";
        } else {
            rootMenu = createTreeMenu(lstParent, null);
            roleName = null;
            roleCode = null;
            action = "add";
        }
        actonSwitch = true;
    }

    public void onEditRole() {
        Roles roleIschoose = (Roles) chooseRole.getData();
        loadDateToEdit(roleIschoose);
        action = "edit";
        actonSwitch = false;
        roleIsChoose = true;
        roleToReset = roleIschoose;
    }

    public void loadDateToEdit(Roles role) {
        roleId = role.getRoleId();
        roleIds = new Long[]{roleId};
        List<Objects> lst = objectServiceImpl.getLstObjectsByRole(roleIds);
        rootMenu = createTreeMenu(lstParent, lst);
        roleSelected = rolesServiceImpl.getRolesById((roleId));
        roleName = roleSelected.getRoleName();
        roleCode = roleSelected.getRoleCode();
    }

    public void onCreateRole() {
        if ("edit".equals(action) && chooseRole != null) {
            chooseRole.setSelected(false);
            chooseRole = null;
        }
        roleId = null;
        rootMenu = createTreeMenu(lstParent, null);
        roleName = null;
        roleCode = null;
        action = "add";
        actonSwitch = true;
        roleIsChoose = true;
    }

    public void removeRole() {
        Roles roleIschoose = (Roles) chooseRole.getData();
        roleId = roleIschoose.getRoleId();
        actonSwitch = false;
        roleIschoose.setStatus(0l);
        roleIschoose.setUpdateDate(new Date());
        rolesServiceImpl.updateRoles(roleIschoose);
        rolesObjectsServiceImpl.deleteRoleObjectByRoleId(roleId);
        rolesUsersServiceImpl.deleteRoleUserByRoleId(roleId);
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        reloadTreeRoles();
        if ("edit".equals(action) && roleId.equals(roleToReset.getRoleId())) {
            roleIsChoose = false;
        }

        loginService.setSessionMenu();
    }

    private List<Long> getLstObjectId() {
        List<Long> lst = new ArrayList();
        for (TreeNode chooseMenu : chooseMenus) {
            Objects obj = (Objects) chooseMenu.getData();
            Long parentId = obj.getParentId();
            if (parentId != null && !lst.contains(parentId)) {
                lst.add(parentId);
            }
            lst.add(obj.getObjectId());
        }
        return lst;
    }

    public void reset() {
        if ("add".equals(action)) {
            onCreateRole();
        }
        if ("edit".equals(action)) {
            loadDateToEdit(roleToReset);
        }
    }

    public void actionRole() {
        if (!validateRole()) {
            return;
        }
        List<Long> lst = getLstObjectId();
        roleSelected.setRoleName(StringUtils.isNotBlank(roleName) ? roleName.trim() : StringUtils.EMPTY);
        roleSelected.setRoleCode(StringUtils.isNotBlank(roleCode) ? roleCode.trim() : StringUtils.EMPTY);
        roleSelected.setStatus(1L);
        if (java.util.Objects.nonNull(roleId)) {
            roleSelected.setUpdateDate(new Date());
            roleSelected.setUpdateUser(userToken.getUserName());
            rolesServiceImpl.updateRoles(roleSelected);
        } else {
            roleSelected.setCreateDate(new Date());
            roleSelected.setUpdateDate(new Date());
            roleSelected.setCreateUser(userToken.getUserName());
            roleSelected.setUpdateUser(userToken.getUserName());
            rolesServiceImpl.saveRoles(roleSelected);
        }

        saveRoleObjects(roleSelected.getRoleId(), lst);

        reloadTreeRoles();
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        actonSwitch = false;
        action = "edit";
        roleToReset = roleSelected;

        loginService.setSessionMenu();
    }

    private void saveRoleObjects(Long roleId, List<Long> objectIds) {
        List<RoleObject> roleObjects = rolesObjectsServiceImpl.findByRoleId(roleId);
        RoleObject roleObject;
        if (roleObjects.isEmpty() && objectIds != null && objectIds.size() > 0) {
            for (Long roleId1 : objectIds) {
                roleObject = new RoleObject();
                roleObject.setIsActive(1l);
                roleObject.setObjectId(roleId1);
                roleObject.setRoleId(roleId);
                rolesObjectsServiceImpl.saveRoleObject(roleObject);
            }
        } else {
            if (objectIds != null && objectIds.size() > 0) {
                List<Long> list1 = new ArrayList<>();
                roleObjects.stream().forEach((item) -> {
                    if (item.getObjectId() != null && objectIds.contains(item.getObjectId())) {
                        item.setIsActive(1l);
                        rolesObjectsServiceImpl.updateRoleObject(item);
                        list1.add(item.getObjectId());
                    } else {
                        item.setIsActive(0l);
                        rolesObjectsServiceImpl.updateRoleObject(item);
                    }
                });

                for (Long objectId : objectIds) {
                    if (!list1.contains(objectId)) {
                        roleObject = new RoleObject();
                        roleObject.setIsActive(1l);
                        roleObject.setObjectId(objectId);
                        roleObject.setRoleId(roleId);
                        rolesObjectsServiceImpl.saveRoleObject(roleObject);
                    }
                }
            }
        }
    }

    private Boolean validateRole() {

        if (StringUtils.isNotBlank(roleCode)) {
            if (rolesServiceImpl.getLstRolesByCode(roleCode, java.util.Objects.nonNull(roleId) ? roleId : null) != null) {
                errorMsg(Constants.REMOTE_GROWL, "This role code is exist in system!");
                return false;
            }
            if (roleCode.contains("%")) {
                errorMsg(Constants.REMOTE_GROWL, "Role code not contains % character!");
                return false;
            }
            if (roleCode.length() > 255) {
                errorMsg(Constants.REMOTE_GROWL, "Role code must be a maximum of 255 characters!");
                return false;
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, "Role code is required!");
            return false;
        }
        if (StringUtils.isNotBlank(roleName)) {
            if (rolesServiceImpl.getLstRolesByName(roleName, java.util.Objects.nonNull(roleId) ? roleId : null) != null) {
                errorMsg(Constants.REMOTE_GROWL, "This role name is exist in system!");
                return false;
            }
            if (roleName.contains("%")) {
                errorMsg(Constants.REMOTE_GROWL, "Role name not contains % character!");
                return false;
            }
            if (roleName.length() > 255) {
                errorMsg(Constants.REMOTE_GROWL, "Role name must be a maximum of 255 characters!");
                return false;
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, "Role name is required!");
            return false;
        }

        if (chooseMenus == null || chooseMenus.length == 0) {
            errorMsg(Constants.REMOTE_GROWL, "You must choose menu!");
            return false;
        }
        return true;
    }

    public TreeNode getRootMenu() {
        return rootMenu;
    }

    public void setRootMenu(TreeNode rootMenu) {
        this.rootMenu = rootMenu;
    }

    public TreeNode[] getChooseMenus() {
        return chooseMenus;
    }

    public void setChooseMenus(TreeNode[] chooseMenus) {
        this.chooseMenus = chooseMenus;
    }

    public List<Roles> getLstRoles() {
        return lstRoles;
    }

    public void setLstRoles(List<Roles> lstRoles) {
        this.lstRoles = lstRoles;
    }

    public Long[] getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(Long[] roleIds) {
        this.roleIds = roleIds;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public Roles getRoleSelected() {
        return roleSelected;
    }

    public void setRoleSelected(Roles roleSelected) {
        this.roleSelected = roleSelected;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Boolean getActonSwitch() {
        return actonSwitch;
    }

    public void setActonSwitch(Boolean actonSwitch) {
        this.actonSwitch = actonSwitch;
    }

    public Users getUserToken() {
        return userToken;
    }

    public void setUserToken(Users userToken) {
        this.userToken = userToken;
    }

    public TreeNode getRootRole() {
        return rootRole;
    }

    public void setRootRole(TreeNode rootRole) {
        this.rootRole = rootRole;
    }

    public TreeNode getChooseRole() {
        return chooseRole;
    }

    public void setChooseRole(TreeNode chooseRole) {
        this.chooseRole = chooseRole;
    }

    public Boolean getRoleIsChoose() {
        return roleIsChoose;
    }

    public void setRoleIsChoose(Boolean roleIsChoose) {
        this.roleIsChoose = roleIsChoose;
    }

}
