/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Order;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import vn.viettel.campaign.common.DataUtil;
import vn.viettel.campaign.common.DateUtils;
import vn.viettel.campaign.common.NotifyType;
import vn.viettel.campaign.common.Responses;
import vn.viettel.campaign.constants.Constants;
import vn.viettel.campaign.entities.*;
import vn.viettel.campaign.service.*;
import vn.viettel.campaign.service.impl.TreeServiceImpl;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Objects;
import java.util.*;
import javax.faces.bean.ManagedProperty;

import static vn.viettel.campaign.constants.Constants.CAMPAIGN_OFFLINE_TYPE;
import vn.viettel.campaign.validate.CheckOnEdit;

/**
 * @author Truongbx
 */
@ManagedBean(name = "campaignOfflineController")
@ViewScoped
@Getter
@Setter
public class CampaignOfflineController extends BaseController implements Serializable {

    TreeNode parentNode;

    private boolean isDisplay;
    private TreeNode rootNode;
    private TreeNode segmentRootNode;
    private TreeNode ruleRootNode;
    private TreeNode selectedNode;
    private TreeNode currentNote;
    private TreeNode selectedSegmentNode;
    private TreeNode selectedRuleNode;
    private List<Category> categories;
    private List<Campaign> campaigns;
    private Category category;
    private Campaign campaign;
    private boolean update;
    private TreeNode root;
    private Segment segment;
    private List<Segment> lstSeg;
    private List<Segment> lstSegInRuleTable = new ArrayList<>();
    private List<Segment> lstSegFiltered;
    private List<Rule> lstRuleFiltered;
    private List<InvitationPriority> lstInviFiltered;
    private String searchSeg;
    private Rule ruleSelected;
    private List<Rule> lstRule;
    private List<CampaignSchedule> lstCampaignSchedule;
    private List<CampaignSchedule> lstCampaignScheduleFilter;
    private String searchRule;
    private InvitationPriority invitation;
    private List<InvitationPriority> lstInvi;
    private List<BlackList> lstBlackList;
    private List<SpecialProm> lstSpecialProm;
    private String searchInvi;
    private boolean action;
    private String datePattern = "dd/MM/yyyy";
    private Map<Long, Segment> mapKeySegment = new HashMap<>();
    private List<Long> lstTreeSegment = new ArrayList<>();
    private Rule createNewRule = new Rule();
    private CampaignSchedule createNewSchedule = new CampaignSchedule();
    private List<CdrService> cdrServicesTable = new ArrayList<>();
    private List<ResultTable> resultTable = new ArrayList<>();
    private List<CdrService> cdrServicesAll = new ArrayList<>();
    private List<ResultTable> resultTableAllData = new ArrayList<>();
    private DualListModel<CdrService> cdrServicePickingList = new DualListModel<>();
    private DualListModel<ResultTable> resultTablePickingList = new DualListModel<>();
    private boolean applyAllCdrToRule = false;
    private boolean priority = false;
    private boolean editMode = false;
    private Map<Long, String> mapNotifyType = new HashMap<>();
    private final String All = "all";
    private Object objectData;
    List<Category> ruleCategories = new ArrayList<>();
    boolean viewSchedule = true;
    @Autowired
    private UssdScenarioService ussdScenarioService;
    private UploadedFile file;
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TreeServiceImpl treeService;

    @Autowired
    private CampaignOnlineService campaignOnlineService;

    @Autowired
    private BlacklistService backlistService;

    @Autowired
    SpecialPromService specialPromServiceImpl;

    @Autowired
    private UtilsService utilsService;

    private String editObject;

    @Autowired
    PreprocessUnitInterface preprocessUnitServiceImpl;

    @ManagedProperty(value = "#{ruleOfflineController}")
    RuleOfflineController ruleOfflineController;

    @ManagedProperty(value = "#{resultTableController}")
    ResultTableController resultTableController;

    @ManagedProperty(value = "#{conditionTableController}")
    ConditionTableController conditionTableController;

    @ManagedProperty(value = "#{preProcessController}")
    PreProcessController preProcessController;

    @ManagedProperty(value = "#{preProcessNumberController}")
    PreProcessNumberController preProcessNumberController;

    @ManagedProperty(value = "#{preProcessExistElementController}")
    PreProcessExistElementController preProcessExistElementController;

    @ManagedProperty(value = "#{preProcessTimeController}")
    PreProcessTimeController preProcessTimeController;

    @ManagedProperty(value = "#{preProcessDateController}")
    PreProcessDateController preProcessDateController;

    @ManagedProperty(value = "#{preProcessNumberRangeController}")
    PreProcessNumberRangeController preProcessNumberRangeController;

    @ManagedProperty(value = "#{preProcessCompareNumberController}")
    PreProcessCompareNumberController preProcessCompareNumberController;

    @ManagedProperty(value = "#{preProcessZoneController}")
    PreProcessZoneController preProcessZoneController;

    @ManagedProperty(value = "#{preProcessSameElementController}")
    PreProcessSameElementController preProcessSameElementController;

    private Segment selectedSegment;
    private Segment deletedSegment;
    List<Category> segmentCategories;

    @PostConstruct
    public void init() {
        this.isDisplay = false;
        this.category = new Category();
        this.categories = new ArrayList<>();
        this.update = false;
        this.root = new DefaultTreeNode(null, null);
        this.lstSeg = new ArrayList<>();
        this.lstRule = new ArrayList<>();
        this.lstInvi = new ArrayList<>();
        this.mapNotifyType = NotifyType.asMap();
        lstTreeSegment = new ArrayList<>();
        this.lstCampaignSchedule = new ArrayList<>();
        initTreeNode();

    }

    public void initTreeNode() {
        this.categories = categoryService.getCategoryByType(CAMPAIGN_OFFLINE_TYPE);
        List<Long> longs = new ArrayList<>();
        if (!categories.isEmpty()) {
            categories.forEach(item -> longs.add(item.getCategoryId()));
        }
        this.campaigns = categoryService.getObjectByCategory(longs, Constants.CAMPAIGN);
        rootNode = treeService.createTreeCategoryAndComponent(categories, campaigns);
        rootNode.getChildren().get(0).setExpanded(true);
        addExpandedNode(rootNode.getChildren().get(0));
    }

    public void initSegmentNode() {
        List<Category> segmentCategories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_SEGMENT_TYPE);
        List<Long> longs = new ArrayList<Long>();
        for (Category category : segmentCategories) {
            longs.add(category.getCategoryId());
        }
        List<Segment> segments = categoryService.getObjectByCategory(longs);
        segmentRootNode = treeService.createSegmentTree(segmentCategories, segments);
        segmentRootNode.getChildren().get(0).setSelectable(false);
    }

    public void initRuleNode(Rule ruleData) {
        ruleCategories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_RULE_OFFLINE_TYPE);
        List<Long> longs = new ArrayList<Long>();
        for (Category item : ruleCategories) {
            longs.add(item.getCategoryId());
        }
        List<Rule> rule = categoryService.getRuleByCategory(longs);
        ruleRootNode = treeService.createRule(ruleCategories, rule);
        ruleRootNode.getChildren().get(0).setSelectable(false);
        objectData = ruleData;
    }

    public void addSegmentToTree() {
        for (Segment segment : lstSeg) {
            mapKeySegment.put(segment.getSegmentId(), segment);
        }
        for (Rule rule : lstRule) {
            Segment segment = mapKeySegment.get(rule.getSegmentId());
            if (!lstTreeSegment.contains(segment.getSegmentId())) {
                lstTreeSegment.add(segment.getSegmentId());
                TreeNode segmentNote = new DefaultTreeNode("segment", segment, selectedNode);
                segmentNote.setExpanded(true);
                doAddRuleToSegment(segmentNote);
            }
        }
    }

    public void addRuleToTree() {
        for (Rule rule : lstRule) {
            TreeNode ruleNote = new DefaultTreeNode("rule", rule, selectedNode);
            doAddResultTableToRule(ruleNote);
        }
    }

    public void doAddRuleToSegment(TreeNode segmentNote) {
        Segment segment = (Segment) segmentNote.getData();
        for (Rule rule : lstRule) {
            if (rule.getSegmentId() == segment.getSegmentId()) {
                TreeNode ruleNote = new DefaultTreeNode("rule", rule, segmentNote);
                doAddResultTableToRule(ruleNote);
            }
        }
    }

    public void doAddResultTableToRule(TreeNode ruleNote) {
        Rule rule = (Rule) ruleNote.getData();
        List<ResultTable> resultTables = campaignOnlineService.getListResultByRuleId(rule.getRuleId());
        if (resultTables != null && !resultTables.isEmpty()) {
            for (ResultTable resultTable : resultTables) {
                TreeNode resultNote = new DefaultTreeNode("result", resultTable, ruleNote);
                doAddConditionToResultTable(resultNote);
            }
        }
    }

    public void doAddConditionToResultTable(TreeNode resultNote) {
        ResultTable resultTable = (ResultTable) resultNote.getData();
        if (resultTable.getConditionTableId() == null) {
            return;
        }
        ConditionTable conditionTable = campaignOnlineService.getListConditionById(resultTable.getConditionTableId());
        if (conditionTable != null) {
            TreeNode conditionNote = new DefaultTreeNode("condition", conditionTable, resultNote);
            doAddColumnToCondition(conditionNote);
        }
    }

    public void doAddColumnToCondition(TreeNode conditionNote) {
        ConditionTable conditionTable = (ConditionTable) conditionNote.getData();
        List<PreProcessUnit> lstPPU = campaignOnlineService.getListPPUByConditionId(conditionTable.getConditionTableId());
        if (lstPPU != null && !lstPPU.isEmpty()) {
            for (PreProcessUnit preProcessUnit : lstPPU) {
                TreeNode columnNote = new DefaultTreeNode("ppu", preProcessUnit, conditionNote);
            }
        }
    }

    public void prepareCreateCategory() {

        this.update = false;
        this.category = new Category();
        this.category.setCategoryType(CAMPAIGN_OFFLINE_TYPE);
        Long id = ussdScenarioService.getNextSequense(Constants.TableName.CATEGORY);
        category.setCategoryId(id);
        Category parentCat = (Category) selectedNode.getData();
        this.category.setParentId(parentCat.getCategoryId());
    }

    public void prepareEditCategory() {
        this.update = true;
        if (Objects.nonNull(selectedNode)) {
            Category cat = (Category) selectedNode.getData();
            this.category = categoryService.getCategoryById(cat.getCategoryId());
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void doSaveOrUpdateCategory() {
        if (validateCategory()) {
            categoryService.save(category);
            refreshTree();
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        }
    }

    private void refreshTree() {
        if (update) {
            categories.remove((Category) selectedNode.getData());
            categories.add(this.category);
        } else {
            categories.add(this.category);
        }

        rootNode = treeService.createTreeCategoryAndComponent(categories, campaigns);
        processRefreshCategory(rootNode, selectedNode, category, update);
        updateCategoryInformCampaign();
    }

    public void doDeleteCategory() {
        if (Objects.nonNull(selectedNode)) {
            this.category = (Category) selectedNode.getData();
            if (inValidCategoryNode()) {
                return;
            }
            if (categoryService.checkDeleteCategory(this.category.getCategoryId())) {
                categoryService.deleteCategory(category);
                selectedNode.getChildren().clear();
                TreeNode parNode = selectedNode.getParent();
                selectedNode.getParent().getChildren().remove(selectedNode);
                if (parNode.getChildren().isEmpty()) {
                    removeExpandedNode(parNode);
                }
                selectedNode.setParent(null);
                selectedNode = null;
                successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
                categories.remove(category);
//                parNode.setExpanded(true);
                updateCategoryInformCampaign();
            } else {
                errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.no.record"));
        }
    }

    public void prepareCreateObject() {

    }

    public void prepareEditObject() {

        if (Arrays.asList(selectedNode.getData().toString().split(",")).contains(Integer.toString(CAMPAIGN_OFFLINE_TYPE))) {
            selectedNode.getChildren().clear();
            this.editMode = true;
            this.isDisplay = true;
            this.campaign = (Campaign) selectedNode.getData();
            this.root = campaignOnlineService.createDocumentsByCampaignId(campaign.getCampaignId());
            if (this.campaign.getDefaultStatus()) {
                this.lstSeg = new ArrayList<>();
                this.lstRule = campaignOnlineService.getLstRuleForDefaultCampaign(campaign.getCampaignId());
                this.lstInvi = campaignOnlineService.getInvitationForDefaultCampaign(campaign.getCampaignId());
            } else {
                this.lstSeg = campaignOnlineService.getListSegmentByCampaignId(campaign.getCampaignId());
                this.lstRule = campaignOnlineService.getListRuleByCampaignId(campaign.getCampaignId());
                this.lstInvi = campaignOnlineService.getListInvitationByCampaignId(campaign.getCampaignId());
            }
            this.lstCampaignSchedule = campaignOnlineService.getListCampaignSchedule(campaign.getCampaignId());
            this.lstBlackList = backlistService.getAll(Order.asc("name"));
            this.lstSpecialProm = specialPromServiceImpl.getAll(Order.asc("name"));
            priority = utilsService.getConfig("list.invitation.show");
            mapKeySegment = new HashMap<>();
            createNewRule = new Rule();
            lstSegInRuleTable = new ArrayList<>();
            lstTreeSegment = new ArrayList<>();
            lstSegInRuleTable.addAll(lstSeg);
            this.currentNote = selectedNode;
            initSegmentNode();
            setAction(true);
            if (this.campaign.getDefaultStatus()) {
                // add rule to campaign
                addRuleToTree();
            } else {
                //add rule, segment to campaign
                addSegmentToTree();
            }
            selectedNode.setExpanded(true);
            clearFillter();
        }
    }

    public void onAddNewCampaign() {
        this.editObject = "campaign";
        Category category = (Category) selectedNode.getData();
        this.editMode = false;
        this.isDisplay = true;
        this.campaign = campaignOnlineService.getNextSequense();
        campaign.setCategoryId(category.getCategoryId());
        campaign.setCampaignType(CAMPAIGN_OFFLINE_TYPE);
        campaign.setEffDate(new Date());
        campaign.setExpiredDate(new Date());
        campaign.setPriority(0);
        this.root = new DefaultTreeNode(null, null);
        this.lstSeg = new ArrayList<>();
        this.lstRule = new ArrayList<>();
        this.lstInvi = new ArrayList<>();
        this.lstBlackList = backlistService.getAll(Order.asc("name"));
        this.lstSpecialProm = specialPromServiceImpl.getAll(Order.asc("name"));
        priority = utilsService.getConfig("list.invitation.show");
        lstCampaignSchedule = new ArrayList<>();
        mapKeySegment = new HashMap<>();
        createNewRule = new Rule();
        lstSegInRuleTable = new ArrayList<>();
        lstTreeSegment = new ArrayList<>();
        lstSegInRuleTable = new ArrayList<>();
        this.currentNote = selectedNode;
        initSegmentNode();
        setAction(true);
        clearFillter();

    }

    public void onReset() {
        this.selectedNode = currentNote;
//		initTreeNode();
        if (this.editMode) {
            Campaign campaignDb = campaignOnlineService.getCampaignById(this.campaign.getCampaignId());
            if (campaignDb != null) {
                this.campaign.mapData(campaignDb);
            }
            prepareEditObject();
        } else {
            //add new
            onAddNewCampaign();
        }

    }

    public boolean doDeleteObject() {
        this.campaign = (Campaign) selectedNode.getData();
        this.isDisplay = false;
        boolean result = campaignOnlineService.deleteCampaignOffline(campaign.getCampaignId());
        TreeNode parentNode = selectedNode.getParent();
        if (result) {
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
            selectedNode.getParent().getChildren().remove(selectedNode);
        } else {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.fail"));
        }
        if (result) {
            parentNode.getChildren().remove(selectedNode);
            if (parentNode.getChildren().size() == 1) {
                removeExpandedNode(parentNode);
            }
            campaigns.remove(campaign);
        }
        return result;
    }

    public boolean validateCategory() {
        if (DataUtil.isStringNullOrEmpty(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Category name");
            return false;
        }
        if (!DataUtil.checkMaxlength(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Category name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(this.category.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Category name");
            return false;
        }
        boolean rs = true;
        if (!categoryService.checkDuplicate(category.getName(), category.getCategoryId(), category.getCategoryType())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Category name");
            rs = false;
        }
        return rs;
    }

    public boolean validateSegment() {
        if (selectedSegmentNode == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.segment.choose"));
            return false;
        }
        if (checkExistSegment(this.lstSeg, (Segment) selectedSegmentNode.getData())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Segment");
            return false;
        }
        Segment segment = (Segment) selectedSegmentNode.getData();
        segment.setNew(true);
        Segment currentSegment = (Segment) objectData;
        currentSegment.mapData(segment);
        lstSegInRuleTable.add(currentSegment);
        return true;
    }

    public boolean validateRule() {
        if (selectedRuleNode == null) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.rule.choose"));
            return false;
        }
        if (checkExistRuleSegment(this.lstRule, (Rule) selectedRuleNode.getData())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.ruleSegment"));
            return false;
        }
        Rule currentRule = (Rule) objectData;
        Rule rule = (Rule) selectedRuleNode.getData();
        currentRule.setRuleId(rule.getRuleId());
        currentRule.setRuleName(rule.getRuleName());
        doUpdateTree(currentRule);
        return true;
    }

    public boolean onCreateRule() {
        if (DataUtil.isStringNullOrEmpty(createNewRule.getRuleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Rule name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(createNewRule.getRuleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Rule name");
            return false;
        }
        if (!DataUtil.checkMaxlength(createNewRule.getRuleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Rule name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(createNewRule.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Description");
            return false;
        }
        if (!DataUtil.checkMaxlength(createNewRule.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }

        if (campaignOnlineService.checExisRuleName(createNewRule.getRuleName(), createNewRule.getRuleId())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Rule name");
            return false;
        }

        if (createNewRule.getCategoryId() == 0) {
            errorMsg(Constants.REMOTE_GROWL, utilsService.getTex("check.rule.category"));
            return false;
        }
        if (resultTablePickingList.getTarget().size() == 0) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Result table");
            return false;
        }
        createNewRule.setCdrServiceId("-1");
        boolean result = campaignOnlineService.saveNewRule(createNewRule, resultTablePickingList.getTarget());
        if (result) {
            TreeNode note = findParentRuleInRuleTree(ruleRootNode, createNewRule);
            if (note != null) {
                TreeNode ruleNote = new DefaultTreeNode("rule", createNewRule, note);
                note.setExpanded(true);
                PrimeFaces.current().ajax().update("@([id$=ruleTree])");
            }
        }
        return result;
    }

    public boolean onCreateSchedule() {
        DataUtil.trimObject(createNewSchedule);
        if (DataUtil.isStringNullOrEmpty(createNewSchedule.getScheduleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Schedule name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(createNewSchedule.getScheduleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Schedule name");
            return false;
        }
        if (!DataUtil.checkMaxlength(createNewSchedule.getScheduleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Schedule name");
            return false;
        }
        if (onCheckScheduleExist(createNewSchedule.getScheduleName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Schedule name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(createNewSchedule.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Description");
            return false;
        }
        if (!DataUtil.checkMaxlength(createNewSchedule.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }

        if (createNewSchedule.getMode() == 2 && createNewSchedule.getChooseDate() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Choose date");
            return false;
        }

        if (createNewSchedule.getMode() == 1 && DataUtil.isStringNullOrEmpty(createNewSchedule.getSchedulePattern())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Pattern");
            return false;
        }
        String partten = "";
        if (createNewSchedule.getMode() == 2) {

            Calendar cal = Calendar.getInstance();
            cal.setTime(createNewSchedule.getChooseDate());
            cal.set(Calendar.SECOND,createNewSchedule.getSecond());
            cal.set(Calendar.MINUTE,createNewSchedule.getMinute());
            cal.set(Calendar.HOUR,createNewSchedule.getHour());
            Calendar now = Calendar.getInstance();
            if ( cal.before(now)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.date"), "Choose date time", "current " +
                        "date time");
                return false;
            }
            int month = cal.get(Calendar.MONTH) + 1;
            partten = +createNewSchedule.getSecond() + " " + createNewSchedule.getMinute() + " " + createNewSchedule.getHour()
                    + " " + cal.get(Calendar.DAY_OF_MONTH) + " " + month +  " " + cal.get(Calendar.YEAR);
            createNewSchedule.setSchedulePattern(partten);
        } else {
            String[] element = createNewSchedule.getSchedulePattern().split(" ");
            if (element.length < 6 || element.length > 7) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.pattern"), "Pattern");
                return false;
            }
            if (!element[0].matches(DateUtils.SECOND_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Second");
                return false;
            }
            if (!element[1].matches(DateUtils.MINUTE_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Minute");
                return false;
            }
            if (!element[2].matches(DateUtils.HOUR_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Hour");
                return false;
            }
            if (!element[3].matches(DateUtils.DAY_OF_MONTH_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Day of month");
                return false;
            }
            if (!element[4].matches(DateUtils.MONTH_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Month");
                return false;
            }
            if (!element[5].matches(DateUtils.DAY_OF_WEEK_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Day of week");
                return false;
            }
            if (element.length == 7 && !element[6].matches(DateUtils.YEAR_REGEX)) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.regexPattern"), "Year");
                return false;
            }

            if (element.length == 7){
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.SECOND,Integer.parseInt(element[0]));
                cal.set(Calendar.MINUTE,Integer.parseInt(element[1]));
                cal.set(Calendar.HOUR_OF_DAY,Integer.parseInt(element[2]));
                cal.set(Calendar.DAY_OF_MONTH,Integer.parseInt(element[3]));
                cal.set(Calendar.MONTH,Integer.parseInt(element[4])-1);
                cal.set(Calendar.YEAR,Integer.parseInt(element[6]));
                Calendar now = Calendar.getInstance();
                if (cal.before(now)){
                    errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.date"), "Choose date time", "current " +
                            "date time");
                    return false;
                }
            }

        }
        createNewSchedule.setScheduleType(1);
        lstCampaignSchedule.add(createNewSchedule);
        if (lstCampaignScheduleFilter != null) {
            lstCampaignScheduleFilter.clear();
            lstCampaignScheduleFilter.addAll(lstCampaignSchedule);
        }
        return true;
    }

    public boolean onCheckScheduleExist(String scheduleName) {
//		boolean exist = campaignOnlineService.checExisScheduleName(createNewSchedule.getScheduleName());
//		if (exist) {
//			return exist;
//		}
        for (CampaignSchedule campaignSchedule : lstCampaignSchedule) {
            if (campaignSchedule.getScheduleName().trim().equalsIgnoreCase(scheduleName.trim())) {
                return true;
            }
        }

        return false;
    }

    public boolean onSaveCampaingn() {
        DataUtil.trimObject(this.campaign);
//        if (!validateCampain(this.campaign)) {
//            if (this.editMode) {
//                Campaign campaignDb = campaignOnlineService.getCampaignById(this.campaign.getCampaignId());
//                if (campaignDb != null) ;
//                this.campaign.mapData(campaignDb);
//            }
//            return false;
//        }

        Map<Long, Segment> lstMap = new HashMap<>();
        for (Segment segment : lstSeg) {
            lstMap.put(segment.getSegmentId(), segment);
        }
//			campaignInfo
        List<CampaignInfo> lstCampaignInfos = new ArrayList<>();
        for (Rule rule : lstRule) {
            CampaignInfo campaignInfo = new CampaignInfo();
            campaignInfo.setPriority(rule.getPriority());
            campaignInfo.setRuleId(rule.getRuleId());
            campaignInfo.setSegmentId(rule.getSegmentId());
            campaignInfo.setPriorityGroup(-1);
            campaignInfo.setCampaignId(campaign.getCampaignId());
            Segment segment = lstMap.get(rule.getSegmentId());
            if (segment != null && !this.campaign.getDefaultStatus()) {
                campaignInfo.setPriorityGroup(segment.getPriorityGroup());
            }
            lstCampaignInfos.add(campaignInfo);
        }
        List<InvitationPriority> lstInviTemp = new ArrayList<>();
        for (InvitationPriority invitationPriority : lstInvi) {
            invitationPriority.setCampaignId(campaign.getCampaignId());
            if (invitationPriority.getInvite() != null && invitationPriority.getInvite().equals(1L)) {
                lstInviTemp.add(invitationPriority);
            }
        }

        for (CampaignSchedule campaignSchedule : lstCampaignSchedule) {
            campaignSchedule.setCampaignId(campaign.getCampaignId());
        }
        boolean result = false;
        if (this.editMode) {
            result = doUpdateCampaign(this.campaign, lstCampaignInfos, lstInviTemp, this.lstCampaignSchedule);
            if (result) {
//				update tree in case category
                TreeNode node = findCampaignInTree(this.campaign, rootNode);
                if (node != null) {
                    TreeNode parentNote = selectedNode.getParent();
                    parentNote.getChildren().remove(node);
                    if (parentNote.getChildren().size() == 0) {
                        removeExpandedNode(parentNote);
                    }
                    doUpdateCategory();
                }
            }
        } else {
            result = doSaveCampaignOffline(this.campaign, lstCampaignInfos, lstInviTemp, lstCampaignSchedule);
        }
        this.action = false;
        return result;
    }

    public boolean doSaveCampaignOffline(Campaign campaign, List<CampaignInfo> lstCampaignInfos,
            List<InvitationPriority> lstInvi, List<CampaignSchedule> lstCampaignSchedule) {
        String result = campaignOnlineService.onSaveNewCampaignOffline(campaign, lstCampaignInfos, lstInvi, lstCampaignSchedule);
        if (!result.equalsIgnoreCase(Responses.SUCCESS.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("action.fail"));
            return false;
        } else {
            doUpdateCategory();
            this.editMode = true;
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
            return true;
        }
    }

    public boolean doUpdateCampaign(Campaign campaign, List<CampaignInfo> lstCampaignInfos, List<InvitationPriority> lstInvi, List<CampaignSchedule> lstCampaignSchedule) {
        String result = campaignOnlineService.onUpdateCampaignOffline(campaign, lstCampaignInfos, lstInvi, lstCampaignSchedule);
        if (!result.equalsIgnoreCase(Responses.SUCCESS.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("action.fail"));
            return false;
        } else {
//			doUpdateCategory();
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
            return true;
        }
    }

    public void doUpdateCategory() {
        TreeNode note = findCategoryInTree(rootNode, this.campaign);
        if (note != null) {
            TreeNode campaignNote = new DefaultTreeNode("campaign", campaign, note);
            expandCurrentNode(campaignNote);
            this.selectedNode = campaignNote;
            if (this.campaign.getDefaultStatus()) {
                doAddRuleToCampaign(campaignNote, lstRule);

            } else {

                addSegmentToTree();
            }
        }
    }

    public TreeNode findCategoryInTree(TreeNode root, Campaign campaign) {
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (note.getData() instanceof Category) {
                Category data = (Category) note.getData();
                if (data.getCategoryId().equals(campaign.getCategoryId())) {
                    return note;
                }
            }
            currentNote = findCategoryInTree(note, campaign);

            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

    public void doAddRuleToCampaign(TreeNode campainNote, List<Rule> lstRule) {
        for (Rule rule : lstRule) {
            TreeNode ruleNote = new DefaultTreeNode("rule", rule, campainNote);
            doAddResultTableToRule(ruleNote);
        }
    }

    public TreeNode findParentRuleInRuleTree(TreeNode root, Rule rule) {
        List<TreeNode> lstChildrent = root.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (note.getData() instanceof Category) {
                Category data = (Category) note.getData();
                if (data.getCategoryId() == rule.getCategoryId()) {
                    return note;
                }
            }
            currentNote = findParentRuleInRuleTree(note, rule);

            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

    public boolean checkExistSegment(List<Segment> segments, Segment segment) {
        boolean exist = false;
        for (Segment item : segments) {
            if (item.getSegmentId() == segment.getSegmentId()) {
                exist = true;
            }
        }
        return exist;
    }

    public boolean checkExistRule(List<Rule> rules, Rule rule) {
        boolean exist = false;
        for (Rule item : rules) {
            if (item.getRuleId() == rule.getRuleId()) {
                exist = true;
            }
        }
        return exist;
    }

    public boolean filterDate(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        if (value == null) {
            return false;
        }
        Date date = (Date) value;
        DateFormat dateFormat = new SimpleDateFormat(datePattern);
        String result = dateFormat.format(date);
        return result.contains(filterText);
    }

    public void onAddNewSegment() {
        if (!lstSeg.isEmpty()) {
            Segment segment = lstSeg.get(lstSeg.size() - 1);
            if (DataUtil.isStringNullOrEmpty(segment.getSegmentName())) {
                lstSeg.remove(segment);
            }
        }
        Segment segment = new Segment();
        segment.setPriorityGroup(0);
        lstSeg.add(segment);
        if (lstSegFiltered != null) {
            lstSegFiltered.clear();
            lstSegFiltered.addAll(lstSeg);
        }
    }

    public void prepareToShowSchedule() {
        this.createNewSchedule = campaignOnlineService.getNextSchedule();
        createNewSchedule.setMode(2);
        this.viewSchedule = false;
    }

    public void onAddRule() {
        if (!lstRule.isEmpty()) {
            Rule rule = lstRule.get(lstRule.size() - 1);
            if (DataUtil.isStringNullOrEmpty(rule.getSegmentName()) || DataUtil.isStringNullOrEmpty(rule.getRuleName())) {
                return;
            }
        }
        Rule rule = new Rule();
        rule.setPriority(0);
        if (this.campaign.getDefaultStatus()) {
            rule.setSegmentName(All);
            rule.setSegmentId(-1L);
        }
        lstRule.add(rule);
        if (lstRuleFiltered != null) {
            lstRuleFiltered.clear();
            lstRuleFiltered.addAll(lstRule);
        }
    }

    public void removeSegment() {
        Segment o = deletedSegment;
        lstSeg.remove(o);
        if (lstSegFiltered != null && lstSegFiltered.contains(o)) {
            lstSegFiltered.remove(o);
        }
        if (lstSegInRuleTable.contains(o)) {
            lstSegInRuleTable.remove(o);
        }
        List<Rule> lstRuleClone = new ArrayList<>();
        lstRuleClone.addAll(lstRule);
        boolean check = true;
        for (Rule rule : lstRuleClone) {
            if (rule.getSegmentId() == o.getSegmentId()) {
                removeRule(rule, check);
                check = false;
            }
        }
        if (check) {
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
        }

    }
    public boolean isShowConfirmation(Segment o){
        deletedSegment = o;
        if (editMode) {
            return true;
        }
        for (Rule rule : lstRule) {
            if (rule.getSegmentId() == o.getSegmentId()) {
                return true;
            }
        }
        removeSegment();
        FacesContext.getCurrentInstance().validationFailed();
        return false;
    }
    public void removeRule(Rule rule, boolean check) {
        lstRule.remove(rule);
        if (lstRuleFiltered != null && lstRuleFiltered.contains(rule)) {
            lstRuleFiltered.remove(rule);
        }
        if (check) {
            successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
        }
        if (DataUtil.isStringNullOrEmpty(rule.getSegmentName()) && DataUtil.isStringNullOrEmpty(rule.getRuleName())) {
            return;
        }
        doRemoveInvitation(lstRule);
        if (!this.editMode) {
            return;
        }
        TreeNode treeNode = null;
        if (!this.campaign.getDefaultStatus()) {
            treeNode = findSegmentInTree(rule, selectedNode);
        } else {

            treeNode = selectedNode;
        }
        if (treeNode != null) {
            List<TreeNode> childrent = treeNode.getChildren();
            for (TreeNode child : childrent) {
                Rule item = (Rule) child.getData();
                if (item.getRuleId() == rule.getRuleId()) {
                    treeNode.getChildren().remove(child);
                    break;
                }
            }
            if (treeNode.getChildren().isEmpty() && !this.campaign.getDefaultStatus()) {

                treeNode.getParent().getChildren().remove(treeNode);
            }

        }
    }

    public void doRemoveInvitation(List<Rule> lstRule) {
        lstInvi.clear();
        for (Rule rule : lstRule) {
            List<InvitationPriority> lstData = campaignOnlineService.getNotifyTriggerByRuleId(rule.getRuleId(), rule.getSegmentName(), rule.getSegmentId());
            if (lstData != null && !lstData.isEmpty()) {
                lstInvi.addAll(lstData);
            }
        }
        removeInvitationDuplicate(lstInvi);
    }

    public boolean onchangeSegment(Rule rule) {
        if (checkExistSegmentRule(this.lstRule)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.ruleSegment"));
            return false;
        }
        for (Segment item : lstSeg) {
            if (rule.getSegmentId() == item.getSegmentId()) {
                rule.setSegmentName(item.getSegmentName());
            }
        }
        doRemoveInvitation(lstRule);
        return true;
    }

    public void doUpdateTree(Rule rule) {
        if (rule.getSegmentName() != null && rule.getRuleName() != null) {

            TreeNode note = findSegmentInTree(rule, rootNode);
            if (note != null) {
                TreeNode ruleNote = new DefaultTreeNode("rule", rule, note);
                doAddResultTableToRule(ruleNote);
            } else {
                Segment segmentData = null;
                for (Segment segment : lstSegInRuleTable) {
                    if (segment.getSegmentId() == rule.getSegmentId()) {
                        segmentData = segment;
                    }
                }
                TreeNode campaignNote = findCampaignInTree(this.campaign, rootNode);
                if (campaignNote != null) {
                    TreeNode ruleNote = null;
                    campaignNote.setExpanded(true);
                    if (!this.campaign.getDefaultStatus()) {
                        TreeNode segmentNote = new DefaultTreeNode("segment", segmentData, campaignNote);
                        segmentNote.setExpanded(true);
                        ruleNote = new DefaultTreeNode("rule", rule, segmentNote);
                    } else {
                        ruleNote = new DefaultTreeNode("rule", rule, campaignNote);
                    }

                    doAddResultTableToRule(ruleNote);
                }
            }
            doUpdateInvitationTable(rule);
        }
    }

    public void doUpdateInvitationTable(Rule rule) {
        if (rule.getSegmentName() != null && rule.getRuleName() != null) {
            String segment = rule.getSegmentName();
            long segmentId = rule.getSegmentId();
            if (this.campaign.getDefaultStatus()) {
                segment = All;
                segmentId = -1l;
            }
            List<InvitationPriority> lstData = campaignOnlineService.getNotifyTriggerByRuleId(rule.getRuleId(), segment, segmentId);
            lstInvi.addAll(lstData);
            removeInvitationDuplicate(lstInvi);
        }
    }

    public void removeInvitationDuplicate(List<InvitationPriority> lstInvi) {
        Collections.sort(lstInvi, new SortInvitationByObjectKey());
        List<InvitationPriority> lstInviDuplicate = new ArrayList<>();
        String currentObjectKey = "";
        for (InvitationPriority invitationPriority : lstInvi) {
            if (currentObjectKey.equalsIgnoreCase(invitationPriority.getObjectKey())) {
                lstInviDuplicate.add(invitationPriority);
                continue;
            }
            currentObjectKey = invitationPriority.getObjectKey();
        }
        lstInvi.removeAll(lstInviDuplicate);
        if (lstInviFiltered != null) {
            lstInviFiltered.clear();
            lstInviFiltered.addAll(lstInvi);
        }
    }

    public TreeNode findSegmentInTree(Rule rule, TreeNode rootNode) {
        if (rule == null) {
            return null;
        }
        List<TreeNode> lstChildrent = rootNode.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (note.getData() instanceof Segment) {
                Segment data = (Segment) note.getData();
                if (data.getSegmentId() == rule.getSegmentId()) {
                    return note;
                }
            }
            currentNote = findSegmentInTree(rule, note);

            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

    public TreeNode findCampaignInTree(Campaign campaign, TreeNode rootNode) {
        if (campaign == null) {
            return null;
        }
        List<TreeNode> lstChildrent = rootNode.getChildren();
        for (TreeNode note : lstChildrent) {
            TreeNode currentNote = null;
            if (note.getData() instanceof Campaign) {
                Campaign data = (Campaign) note.getData();
                if (data.getCampaignId() == campaign.getCampaignId()) {
                    return note;
                }
            }
            currentNote = findCampaignInTree(campaign, note);

            if (currentNote != null) {
                return currentNote;
            }
        }
        return null;
    }

    public void moveResulTableUp(ResultTable o) {
        int i = resultTable.indexOf(o);
        if (i > 0) {
            resultTable.remove(o);
            resultTable.add(i - 1, o);
        }
    }

    public void moveResulTableDown(ResultTable o) {
        int i = resultTable.indexOf(o);
        if (i < resultTable.size() - 1) {
            resultTable.remove(o);
            resultTable.add(i + 1, o);
        }
    }

    public void prepateDataToShowAddRulePopup() {
        cdrServicesAll = campaignOnlineService.getAllCdrService();
        resultTableAllData = campaignOnlineService.getAllResultTable();
        cdrServicesTable = new ArrayList<>();
        resultTable = new ArrayList<>();
        applyAllCdrToRule = false;
        createNewRule = campaignOnlineService.getNextRule();
        cdrServicePickingList = new DualListModel<>(cdrServicesAll, cdrServicesTable);
        resultTablePickingList = new DualListModel<>(resultTableAllData, resultTable);
    }

    public void prepareDataToShowResultTablePopup() {
        List<ResultTable> resultTableAllDataDup = new ArrayList<>();
        List<ResultTable> resultTableDup = new ArrayList<>();
        resultTableAllDataDup.addAll(resultTableAllData);
        resultTableDup.addAll(resultTable);
        resultTablePickingList = new DualListModel<>(resultTableAllDataDup, resultTableDup);
    }

    public void onSaveCdrService() {
        cdrServicesAll = cdrServicePickingList.getSource();
        cdrServicesTable = cdrServicePickingList.getTarget();
    }

    public void onSaveResultTable() {
        resultTableAllData = resultTablePickingList.getSource();
        resultTable = resultTablePickingList.getTarget();
    }

    public void removeResultTable(ResultTable o) {
        resultTable.remove(o);
        resultTableAllData.add(o);
    }

    public void removeCdrServiceTable(CdrService o) {
        cdrServicesTable.remove(o);
        cdrServicesAll.add(o);
    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public Segment getSegment() {
        return segment;
    }

    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    public List<Segment> getLstSeg() {
        return lstSeg;
    }

    public void setLstSeg(List<Segment> lstSeg) {
        this.lstSeg = lstSeg;
    }

    public String getSearchSeg() {
        return searchSeg;
    }

    public void setSearchSeg(String searchSeg) {
        this.searchSeg = searchSeg;
    }

    public Rule getRuleSelected() {
        return ruleSelected;
    }

    public void setRuleSelected(Rule ruleSelected) {
        this.ruleSelected = ruleSelected;
    }

    public List<Rule> getLstRule() {
        return lstRule;
    }

    public void setLstRule(List<Rule> lstRule) {
        this.lstRule = lstRule;
    }

    public String getSearchRule() {
        return searchRule;
    }

    public void setSearchRule(String searchRule) {
        this.searchRule = searchRule;
    }

    public InvitationPriority getInvitation() {
        return invitation;
    }

    public void setInvitation(InvitationPriority invitation) {
        this.invitation = invitation;
    }

    public List<InvitationPriority> getLstInvi() {
        return lstInvi;
    }

    public void setLstInvi(List<InvitationPriority> lstInvi) {
        this.lstInvi = lstInvi;
    }

    public String getSearchInvi() {
        return searchInvi;
    }

    public void setSearchInvi(String searchInvi) {
        this.searchInvi = searchInvi;
    }

    public boolean getAction() {
        return action;
    }

    public void setAction(boolean action) {
        this.action = action;
    }

    public List<Campaign> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(List<Campaign> campaigns) {
        this.campaigns = campaigns;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    public TreeNode getRoot() {
        return root;
    }

    public boolean getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(boolean isDisplay) {
        this.isDisplay = isDisplay;
    }

    public DualListModel<CdrService> getCdrServicePickingList() {
        return cdrServicePickingList;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public void setCdrServicePickingList(DualListModel<CdrService> cdrServicePickingList) {
        this.cdrServicePickingList = cdrServicePickingList;
    }

    public List<CdrService> getCdrServicesTable() {
        return cdrServicesTable;
    }

    public void setCdrServicesTable(List<CdrService> cdrServicesTable) {
        this.cdrServicesTable = cdrServicesTable;
    }

    public DualListModel<ResultTable> getResultTablePickingList() {
        return resultTablePickingList;
    }

    public void setResultTablePickingList(DualListModel<ResultTable> resultTablePickingList) {
        this.resultTablePickingList = resultTablePickingList;
    }

    public List<ResultTable> getResultTable() {
        return resultTable;
    }

    public void setResultTable(List<ResultTable> resultTable) {
        this.resultTable = resultTable;
    }

    public boolean isApplyAllCdrToRule() {
        return this.applyAllCdrToRule;
    }

    public void setApplyAllCdrToRule(boolean value) {
        this.applyAllCdrToRule = value;
    }

    public void changeCdrValue() {
        this.applyAllCdrToRule = applyAllCdrToRule;
    }

    public String getDatePattern() {
        return datePattern;
    }

    public void setDatePattern(String datePattern) {
        this.datePattern = datePattern;
    }

    public void eventListener(ValueChangeEvent event) {
        System.out.println(event.toString());
    }

    public void valueChangeMethod(ValueChangeEvent e) {
        System.out.println(e.getNewValue().toString());
    }

    public boolean isPriority() {
        return priority;
    }

    public void setPriority(boolean priority) {
        this.priority = priority;
    }

    public boolean validateCampain() {
        Campaign campaign = this.campaign;
        if (DataUtil.isStringNullOrEmpty(campaign.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Campaign name");
            return false;
        }
        if (!DataUtil.checkMaxlength(campaign.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Campaign name");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(campaign.getName())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Campaign name");
            return false;
        }
        if (!DataUtil.checkMaxlength(campaign.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.maxlength"), "Description");
            return false;
        }
        if (!DataUtil.checkNotContainPercentage(campaign.getDescription())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.percentage"), "Description");
            return false;
        }
        if (campaign.getPriority() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.emptyNumber"), "Priority");
            return false;
        }
        if (campaign.getEffDate() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Effect date");
            return false;
        }
        if (campaign.getExpiredDate() == null) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Expire date");
            return false;
        }
        if (!campaign.getEffDate().before(campaign.getExpiredDate())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.date"), "Expire date", "Effect date");
            return false;
        }

        if (!this.campaign.getDefaultStatus() && (this.lstSeg == null || this.lstSeg.isEmpty())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Segment");
            return false;
        }
        if (this.lstRule == null || this.lstRule.isEmpty()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Rule");
            return false;
        }

        if (this.lstCampaignSchedule == null || this.lstCampaignSchedule.isEmpty()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Schedule");
            return false;
        }
        if (campaignOnlineService.checkExisCampaignName(campaign.getName(), campaign.getCampaignId() == null ? -1 : campaign.getCampaignId())) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate"), "Campaign name");
            return false;
        }
        List<Segment> segmentList = new ArrayList<>();
        segmentList.addAll(lstSeg);
        Collections.sort(segmentList, new SortSegment());
        int priorityGroup = -1;
        for (Segment segment : segmentList) {
            if (segment.getSegmentId() == 0) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), "Segment");
                return false;
            }
            if (segment.getPriorityGroup() != priorityGroup) {
                priorityGroup = segment.getPriorityGroup();
            } else {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.priority"), priorityGroup + "", "Segment");
                return false;
            }
        }

        List<Rule> ruleList = new ArrayList<>();
        ruleList.addAll(lstRule);
        Collections.sort(ruleList, new SortRule());
        int priorityRule = -1;
        for (Rule rule : ruleList) {
            if (rule.getRuleId() == 0) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.notComplete"), "Rule");
                return false;
            }
            if (rule.getSegmentId() == 0) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.rulenotmap"));
                return false;
            }
            if (rule.getPriority() != priorityRule) {
                priorityRule = rule.getPriority();
            } else {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.priority"), priorityRule + "", "Rule");
                return false;
            }
        }
        if (checkExistSegmentRule(this.lstRule)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.duplicate.ruleSegment"));
            return false;
        }
        if (this.lstInvi == null || this.lstInvi.isEmpty()) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.empty"), "Invitation");
            return false;
        }
        Map<Long, Rule> segmentInTree = new HashMap<>();
        this.lstRule.forEach(e -> {
            segmentInTree.put(e.getSegmentId(), e);
        });
        for (Segment segment : lstSeg) {
            if (segmentInTree.get(segment.getSegmentId()) == null) {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.segmentNotUse"));
                return false;
            }
        }

        List<InvitationPriority> invitationPriorityList = new ArrayList<>();
        if (!this.priority) {
            for (InvitationPriority invitationPriority : lstInvi) {
                invitationPriority.setPriority(-1L);
            }
        }
        invitationPriorityList.addAll(lstInvi);
        Collections.sort(invitationPriorityList, new SortInvitation());
        Map<String, String> mapSegmentPriority = new HashMap<>();
        Long priorityInvi = -1L;
        for (InvitationPriority invi : invitationPriorityList) {
            if (invi.getPriority() == -1) {
                continue;
            }
            String key = invi.getSegmentId() + "_" + invi.getPriority();
            if (mapSegmentPriority.isEmpty() || mapSegmentPriority.get(key) == null) {
                mapSegmentPriority.put(key, key);
            } else {
                errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("check.priority.invitation"));
                return false;
            }
        }
        return true;
    }

    public void onChangeDefaulCampaign() {
        if (!editMode) {
            lstSeg = new ArrayList<>();
            lstSegInRuleTable = new ArrayList<>();
            lstInvi = new ArrayList<>();
            lstRule.forEach(e -> {
                e.setSegmentId(0);
                e.setSegmentName(null);
                if (this.campaign.getDefaultStatus()) {
                    e.setSegmentName(All);
                    e.setSegmentId(-1L);
                    doUpdateInvitationTable(e);
                }
            });
        }

    }

    class SortSegment implements Comparator<Segment> {

        public int compare(Segment a, Segment b) {
            return a.getPriorityGroup() - b.getPriorityGroup();
        }
    }

    class SortRule implements Comparator<Rule> {

        public int compare(Rule a, Rule b) {
            return a.getPriority() - b.getPriority();
        }
    }

    class SortInvitation implements Comparator<InvitationPriority> {

        public int compare(InvitationPriority a, InvitationPriority b) {
            return a.getPriority() > b.getPriority() ? 1 : -1;
        }
    }

    class SortInvitationByObjectKey implements Comparator<InvitationPriority> {

        public int compare(InvitationPriority a, InvitationPriority b) {
            if (a.getSegmentId() > b.getSegmentId()) {
                return 1;
            }
            if (a.getSegmentId() < b.getSegmentId()) {
                return -1;
            }
            return a.getObjectKey().compareTo(b.getObjectKey());
        }
    }

    public List<CampaignSchedule> getLstCampaignSchedule() {
        return lstCampaignSchedule;
    }

    public void setLstCampaignSchedule(List<CampaignSchedule> lstCampaignSchedule) {
        this.lstCampaignSchedule = lstCampaignSchedule;
    }

    public void viewScheduleDetail(CampaignSchedule campaignSchedule) {
        this.createNewSchedule = campaignSchedule;
        if (campaignSchedule.getMode() == 2) {
            String[] date = createNewSchedule.getSchedulePattern().split(" ");
            String dateString = date[3] + "/" + date[4] + "/" + date[5];
            campaignSchedule.setChooseDate(DateUtils.parseVINDate(dateString));
        }
        this.viewSchedule = true;

    }

    public void removeSchedule(CampaignSchedule campaignSchedule) {
        lstCampaignSchedule.remove(campaignSchedule);
        if (lstCampaignScheduleFilter != null && lstCampaignScheduleFilter.contains(campaignSchedule)) {
            lstCampaignScheduleFilter.remove(campaignSchedule);
        }
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("action.success"));
    }

    public boolean inValidCategoryNode() {
        if (DataUtil.isNullObject(category.getParentId())) {
            errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.parent.category"));
            return true;
        }
        if (!selectedNode.getChildren().isEmpty()) {
            errorMsg(Constants.REMOTE_GROWL, StringUtils.EMPTY, utilsService.getTex("chek.relation.category"), this.category.getName());
            return true;
        }
        return false;
    }

    public boolean checkExistRuleSegment(List<Rule> rules, Rule selectedRule) {
        boolean exist = false;
        if (rules.isEmpty()) {
            return false;
        }
        Rule lastRule = rules.get(rules.size() - 1);
        if (DataUtil.isStringNullOrEmpty(lastRule.getSegmentName())) {
            return false;
        }
        for (Rule item : rules) {
            if (item.getSegmentId() == lastRule.getSegmentId()
                    && item.getRuleId() == selectedRule.getRuleId()) {
                return true;
            }
        }
        return false;
    }

    public boolean checkExistSegmentRule(List<Rule> rules) {
        boolean exist = false;
        if (rules.isEmpty()) {
            return false;
        }
        Rule lastRule = rules.get(rules.size() - 1);
        if (DataUtil.isStringNullOrEmpty(lastRule.getRuleName())) {
            return false;
        }
        for (int i = 0; i < rules.size() - 1; i++) {
            if (rules.get(i).getSegmentId() == lastRule.getSegmentId()
                    && rules.get(i).getRuleId() == lastRule.getRuleId()) {
                return true;
            }
        }
        return false;
    }

    public void prepareEditWhenClick() {
        CheckOnEdit.onEdit = "cam_offline";
        prepareEdit();
        this.action = false;
    }

    public void prepareEditFromContextMenu() {
        CheckOnEdit.onEdit = "cam_offline";
        prepareEdit();
        this.action = true;

    }

    public void prepareEdit() {

        this.parentNode = selectedNode.getParent();

        if ("category".equals(selectedNode.getType())) {
            prepareEditCategory();
            PrimeFaces.current().executeScript("PF('carDialog').show();");

        }
        if ("campaign".equals(selectedNode.getType())) {
            this.editObject = "campaign";
            prepareEditObject();
        }

        if ("segment".equals(selectedNode.getType())) {
            this.editObject = "segment";
            selectedSegment = (Segment) selectedNode.getData();
            segmentCategories = categoryService.getCategoryByType(Constants.CatagoryType.CATEGORY_SEGMENT_TYPE);
            selectedNode.getChildren().clear();
            doAddRuleToSegment(selectedNode);
            selectedNode.setExpanded(true);

        }

        if ("rule".equals(selectedNode.getType())) {
            this.editObject = "rule";
            ruleOfflineController.setSelectedNode(selectedNode);
            ruleOfflineController.onEditObject();
            ruleOfflineController.setAction(false);
            selectedNode.getChildren().clear();
            doAddResultTableToRule(selectedNode);
            selectedNode.setExpanded(true);
        }

        if ("result".equals(selectedNode.getType())) {
            this.editObject = "result";
            resultTableController.setSelectedNode(selectedNode);
            resultTableController.prepareEditObject();
            resultTableController.setActonSwitch(false);
        }

        if ("condition".equals(selectedNode.getType())) {
            this.editObject = "condition";
            conditionTableController.setSelectedNode(selectedNode);
            conditionTableController.prepareEditObject();
            conditionTableController.setActonSwitch(false);
        }

        if ("ppu".equals(selectedNode.getType())) {
//            ColumnCt columnCt = (ColumnCt) selectedNode.getData();
//            PreProcessUnit ppu = preprocessUnitServiceImpl.findById(columnCt.getPreProcessId());
//            Category cat = categoryService.getCategoryById(ppu.getCategoryId());
            PreProcessUnit ppu = (PreProcessUnit) selectedNode.getData();

            if (Constants.PRE_PROCESS_UNIT_STRING == ppu.getPreProcessType()) {
                this.editObject = "ppu_string";
                preProcessController.setSelectedNode(selectedNode);
                preProcessController.onEditObject();
                preProcessController.setAction(false);
            }

            if (Constants.PRE_PROCESS_UNIT_NUMBER == ppu.getPreProcessType()) {
                this.editObject = "ppu_number";
                preProcessNumberController.setSelectedNode(selectedNode);
                preProcessNumberController.onEditObject();
                preProcessNumberController.setAction(false);
            }

            if (Constants.PROCESS_UNIT_EXIST_ELEMENT == ppu.getPreProcessType()) {
                this.editObject = "ppu_exist_element";
                preProcessExistElementController.setSelectedNode(selectedNode);
                preProcessExistElementController.onEditObject();
                preProcessExistElementController.setAction(false);
            }

            if (Constants.PRE_PROCESS_UNIT_TIME == ppu.getPreProcessType()) {
                this.editObject = "ppu_time";
                preProcessTimeController.setSelectedNode(selectedNode);
                preProcessTimeController.onEditObject();
                preProcessTimeController.setAction(false);
            }

            if (Constants.PRE_PROCESS_UNIT_DATE == ppu.getPreProcessType()) {
                this.editObject = "ppu_date";
                preProcessDateController.setSelectedNode(selectedNode);
                preProcessDateController.onEditObject();
                preProcessDateController.setAction(false);
            }

            if (Constants.PRE_PROCESS_UNIT_NUMBER_RANGE == ppu.getPreProcessType()) {
                this.editObject = "ppu_number_range";
                preProcessNumberRangeController.setSelectedNode(selectedNode);
                preProcessNumberRangeController.onEditObject();
                preProcessNumberRangeController.setAction(false);
            }

            if (Constants.PRE_PROCESS_UNIT_COMPARE_NUMBER == ppu.getPreProcessType()) {
                this.editObject = "ppu_compare_nember";
                preProcessCompareNumberController.setSelectedNode(selectedNode);
                preProcessCompareNumberController.onEditObject();
                preProcessCompareNumberController.setAction(false);
            }

            if (Constants.PROCESS_UNIT_ZONE == ppu.getPreProcessType()) {
                this.editObject = "ppu_zone";
                preProcessZoneController.setSelectedNode(selectedNode);
                preProcessZoneController.onEditObject();
                preProcessZoneController.setAction(false);
            }

            if (Constants.PRE_PROCESS_UNIT_SAME_ELEMENT == ppu.getPreProcessType()) {
                this.editObject = "ppu_same_element";
                preProcessSameElementController.setSelectedNode(selectedNode);
                preProcessSameElementController.onEditObject();
                preProcessSameElementController.setAction(false);
            }

        }

    }

    public Segment getSelectedSegment() {
        return selectedSegment;
    }

    public void setSelectedSegment(Segment selectedSegment) {
        this.selectedSegment = selectedSegment;
    }

    public List<Category> getSegmentCategories() {
        return segmentCategories;
    }

    public void setSegmentCategories(List<Category> segmentCategories) {
        this.segmentCategories = segmentCategories;
    }

    public String getMinStartDate() {
        if (editMode) {
            return DateUtils.formatDateToString(this.campaign.getEffDate());
        }
        return DateUtils.formatDateToString(new Date());
    }

    public List<Segment> getLstSegFiltered() {
        return lstSegFiltered;
    }

    public void setLstSegFiltered(List<Segment> lstSegFiltered) {
        this.lstSegFiltered = lstSegFiltered;
    }

    public List<Rule> getLstRuleFiltered() {
        return lstRuleFiltered;
    }

    public void setLstRuleFiltered(List<Rule> lstRuleFiltered) {
        this.lstRuleFiltered = lstRuleFiltered;
    }

    public void clearFillter() {
        if (lstInviFiltered != null) {
            lstInviFiltered.clear();
            lstInviFiltered.addAll(lstInvi);
        }
        if (lstSegFiltered != null) {
            lstSegFiltered.clear();
            lstSegFiltered.addAll(lstSeg);
        }
        if (lstRuleFiltered != null) {
            lstRuleFiltered.clear();
            lstRuleFiltered.addAll(lstRule);
        }
        if (lstCampaignScheduleFilter != null) {
            lstCampaignScheduleFilter.clear();
            lstCampaignScheduleFilter.addAll(lstCampaignSchedule);
        }
    }

    public void prepareShowPopup(Object obj) {
        objectData = obj;
    }

    public Object getObjectData() {
        return objectData;
    }

    public void setObjectData(Object objectData) {
        this.objectData = objectData;
    }

    public List<CampaignSchedule> getLstCampaignScheduleFilter() {
        return lstCampaignScheduleFilter;
    }

    public void setLstCampaignScheduleFilter(List<CampaignSchedule> lstCampaignScheduleFilter) {
        this.lstCampaignScheduleFilter = lstCampaignScheduleFilter;
    }

    public boolean isViewSchedule() {
        return viewSchedule;
    }

    public void setViewSchedule(boolean viewSchedule) {
        this.viewSchedule = viewSchedule;
    }
}
