/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import vn.viettel.campaign.constants.Constants;
import static vn.viettel.campaign.controller.BaseController.errorMsgParams;
import vn.viettel.campaign.service.UtilsService;

@ManagedBean
@ViewScoped
public class LoginController extends BaseController implements Serializable {

    private String username;
    private String password;

    @Autowired
    private UtilsService utilsService;

    @PostConstruct
    public void init() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = attr.getRequest().getSession(true);
        if (session.getAttribute("loginFaile") != null || session.getAttribute("doLogin") != null) {
            session.removeAttribute("loginFaile");
            session.removeAttribute("doLogin");
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("invalid.username.or.password"));
        }
    }

    public void validateLogin() {
        if (StringUtils.isBlank(username)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("empty.username"));
        } else if (StringUtils.isBlank(password)) {
            errorMsgParams(Constants.REMOTE_GROWL, utilsService.getTex("empty.password"));
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
