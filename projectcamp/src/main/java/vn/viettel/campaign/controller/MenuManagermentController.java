package vn.viettel.campaign.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import vn.viettel.campaign.constants.Constants;
import static vn.viettel.campaign.controller.BaseController.successMsg;
import vn.viettel.campaign.entities.Objects;
import vn.viettel.campaign.entities.Users;
import vn.viettel.campaign.service.ObjectService;
import vn.viettel.campaign.service.RolesObjectsService;
import vn.viettel.campaign.service.UtilsService;
import vn.viettel.campaign.service.impl.LoginServiceImpl;

@ManagedBean
@ViewScoped
public class MenuManagermentController extends BaseController implements Serializable {

    private Objects menuToReset;
    private Boolean menuIschoose;

    private TreeNode rootMenu;

    private TreeNode selectedNode;

    private String action;
    private Boolean disabledParent;
    private Boolean actonSwitch;

    private String parentId;
    private List<Objects> lstParent;
    private Long objectId;
    private String status;
    private String objectName;
    private String objectCode;
    private String objectUrl;
    private String order;
    private String description;
    private String icon;
    private String objectType;
    private Objects objectSelected;
    private Users userToken;

    @Autowired
    private ObjectService objectServiceImpl;
    @Autowired
    private RolesObjectsService rolesObjectsServiceImpl;
    @Autowired
    private UtilsService utilsService;
    @Autowired
    private LoginServiceImpl loginService;

    @PostConstruct
    public void init() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = (HttpSession) attr.getRequest().getSession(true);
        reloadTreeMenu();
        action = "add";
        objectSelected = new Objects();
        userToken = (Users) session.getAttribute("userToken");
        disabledParent = false;
    }

    private void reloadTreeMenu() {
        lstParent = objectServiceImpl.getLstObjects();
        rootMenu = createCataMenu(lstParent);
    }

    private TreeNode createCataMenu(List<Objects> lstParent) {
        //root
        TreeNode root1 = new DefaultTreeNode(new Objects(), null);
        lstParent.forEach((obj) -> {
            TreeNode parent = new DefaultTreeNode("document", obj, root1);
            obj.getLstSub().forEach((sub) -> {
                TreeNode child = new DefaultTreeNode("picture", sub, parent);
            });
            if (!obj.getLstSub().isEmpty() && obj.getLstSub().size() > 0) {
                parent.setExpanded(true);
            }
        });
        root1.setExpanded(true);
        return root1;
    }

    public void setDataToForm(Objects obj) {
        objectName = obj.getObjectName();
        objectCode = obj.getObjectCode();
        objectUrl = obj.getObjectUrl();
        order = obj.getOrd() != null ? obj.getOrd().toString() : null;
        description = obj.getDescription();
        icon = obj.getPathImg();
        parentId = obj.getParentId() != null ? obj.getParentId().toString() : null;
        status = obj.getStatus() != null ? obj.getStatus().toString() : "1";
        objectId = obj.getObjectId() != null ? obj.getObjectId() : null;
    }

    public void onNodeSelect(NodeSelectEvent event) {
        objectSelected = (Objects) event.getTreeNode().getData();
        setDataToForm(objectSelected);
        action = "edit";
        menuIschoose = true;
        menuToReset = objectSelected;
        actonSwitch = false;
    }

    public void onSwitch() {
        if (StringUtils.isBlank(parentId)) {
            disabledParent = true;
        } else {
            disabledParent = false;
        }
    }

    public void onAddNodeMenu() {
        disabledParent = false;
        objectSelected = new Objects();
        setDataToForm(objectSelected);
        if ("edit".equals(action) && selectedNode != null) {
            selectedNode.setSelected(false);
            selectedNode = null;
        }
        action = "add";
        menuIschoose = true;
        actonSwitch = true;

    }

    public void onAddNodeSubMenu() {
        disabledParent = false;
        objectSelected = new Objects();
        setDataToForm(objectSelected);
        Objects obj = (Objects) selectedNode.getData();
        parentId = obj.getParentId() != null ? obj.getParentId().toString() : obj.getObjectId().toString();
        action = "add";
        menuIschoose = true;
        actonSwitch = true;
    }

    public void onRemoveNodeMenu() throws IOException {
        objectSelected = (Objects) selectedNode.getData();
        if (objectSelected.getLstSub() != null && objectSelected.getLstSub().size() > 0) {
            errorMsg(Constants.REMOTE_GROWL, "You can’t do this action, the menu is not empty!");
            return;
        }
        objectSelected.setStatus(0l);
        objectServiceImpl.updateObject(objectSelected);
        rolesObjectsServiceImpl.deleteRoleObjectByObjectId(objectSelected.getObjectId());
        reloadTreeMenu();
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        if ("edit".equals(action) && objectSelected.getObjectId().equals(menuToReset.getObjectId())) {
            menuIschoose = false;
        }
        actonSwitch = false;

        loginService.setSessionMenu();
    }

    private Boolean validateForm() {
        List lst;
        if (StringUtils.isNotBlank(objectName)) {
            if (objectName.contains("%")) {
                errorMsg(Constants.REMOTE_GROWL, "Object name not contains % character!");
                return false;
            }
            lst = objectServiceImpl.getLstObjectByName(objectName, objectSelected.getObjectId());
            if (lst != null && lst.size() > 0) {
                errorMsg(Constants.REMOTE_GROWL, "This object name is exist in system!");
                return false;
            }
            if (objectName.length() > 255) {
                errorMsg(Constants.REMOTE_GROWL, "Object name must be a maximum of 255 characters!");
                return false;
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, "Object name is required!");
            return false;
        }
        if (StringUtils.isNotBlank(objectCode)) {
            if (objectCode.contains("%")) {
                errorMsg(Constants.REMOTE_GROWL, "Object code not contains % character!");
                return false;
            }
            lst = objectServiceImpl.getLstObjectByCode(objectCode, objectSelected.getObjectId());
            if (lst != null && lst.size() > 0) {
                errorMsg(Constants.REMOTE_GROWL, "This object code is exist in system!");
                return false;
            }
            if (objectCode.length() > 255) {
                errorMsg(Constants.REMOTE_GROWL, "Object code must be a maximum of 255 characters!");
                return false;
            }
        } else {
            errorMsg(Constants.REMOTE_GROWL, "Object code is required!");
            return false;
        }

        if (StringUtils.isNotBlank(objectUrl)) {
            if (objectUrl.contains("%")) {
                errorMsg(Constants.REMOTE_GROWL, "Object url not contains % character!");
                return false;
            }
            if (objectUrl.length() > 255) {
                errorMsg(Constants.REMOTE_GROWL, "Object url must be a maximum of 255 characters!");
                return false;
            }
        }

        if (StringUtils.isNotBlank(icon)) {
            if (icon.contains("%")) {
                errorMsg(Constants.REMOTE_GROWL, "Icon not contains % character!");
                return false;
            }
            if (icon.length() > 255) {
                errorMsg(Constants.REMOTE_GROWL, "Icon must be a maximum of 255 characters!");
                return false;
            }
        }

        if (StringUtils.isNotBlank(description)) {
            if (description.contains("%")) {
                errorMsg(Constants.REMOTE_GROWL, "Description not contains % character!");
                return false;
            }
            if (description.length() > 255) {
                errorMsg(Constants.REMOTE_GROWL, "Description must be a maximum of 255 characters!");
                return false;
            }
        }

        if (StringUtils.isNotBlank(order)) {
            Pattern pattern = Pattern.compile("[0-9]+");
            if (!pattern.matcher(order).matches() || Long.valueOf(order) < 0) {
                errorMsg(Constants.REMOTE_GROWL, "Order is integer!");
                return false;
            }

            if (!pattern.matcher(order).matches() || order.length() > 10) {
                errorMsg(Constants.REMOTE_GROWL, "Order must be int!");
                return false;
            }
        }

        return true;
    }

    public void reset() {
        if ("add".equals(action)) {
            onAddNodeMenu();
        } else if ("edit".equals(action)) {
            objectSelected = menuToReset;
            setDataToForm(objectSelected);
        }
    }

    public void actionObject() {
        if (!validateForm()) {
            return;
        }
        setDataToObjects();
        try {
            if ("edit".equals(action)) {
                objectSelected.setUpdateDate(new Date());
                objectSelected.setUpdateUser(userToken.getUserName());
                if (objectServiceImpl.updateObject(objectSelected)) {

                }
            } else {
                objectSelected.setCreateDate(new Date());
                objectSelected.setUpdateDate(new Date());
                objectSelected.setCreateUser(userToken.getUserName());
                objectSelected.setUpdateUser(userToken.getUserName());
                if (objectServiceImpl.saveObject(objectSelected)) {
                    action = "edit";
                    objectId = objectSelected.getObjectId();
                }
            }
        } catch (Exception e) {
        }
        actonSwitch = false;
        reloadTreeMenu();
        menuToReset = objectSelected;
        successMsg(Constants.REMOTE_GROWL, utilsService.getTex("common.success"));
        loginService.setSessionMenu();
    }

    private void setDataToObjects() {
        objectSelected.setObjectId(objectId != null ? objectId : null);
        objectSelected.setObjectName(StringUtils.isNotBlank(objectName) ? objectName.trim() : StringUtils.EMPTY);
        objectSelected.setObjectCode(StringUtils.isNotBlank(objectCode) ? objectCode.trim() : StringUtils.EMPTY);
        objectSelected.setStatus(status != null && !"".equals(status) ? Long.valueOf(status) : 1L);
        objectSelected.setObjectUrl(objectUrl.trim());
        objectSelected.setOrd(order != null && !"".equals(order) ? Long.valueOf(order) : null);
        objectSelected.setParentId(parentId != null && !"".equals(parentId) ? Long.valueOf(parentId) : null);
        objectSelected.setPathImg(icon.trim());
        objectSelected.setDescription(description.trim());
    }

    public TreeNode getRootMenu() {
        return rootMenu;
    }

    public void setRootMenu(TreeNode rootMenu) {
        this.rootMenu = rootMenu;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<Objects> getLstParent() {
        return lstParent;
    }

    public void setLstParent(List<Objects> lstParent) {
        this.lstParent = lstParent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectCode() {
        return objectCode;
    }

    public void setObjectCode(String objectCode) {
        this.objectCode = objectCode;
    }

    public String getObjectUrl() {
        return objectUrl;
    }

    public void setObjectUrl(String objectUrl) {
        this.objectUrl = objectUrl;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getActonSwitch() {
        return actonSwitch;
    }

    public void setActonSwitch(Boolean actonSwitch) {
        this.actonSwitch = actonSwitch;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public Boolean getMenuIschoose() {
        return menuIschoose;
    }

    public void setMenuIschoose(Boolean menuIschoose) {
        this.menuIschoose = menuIschoose;
    }

    public Objects getMenuToReset() {
        return menuToReset;
    }

    public void setMenuToReset(Objects menuToReset) {
        this.menuToReset = menuToReset;
    }

    public Boolean getDisabledParent() {
        return disabledParent;
    }

    public void setDisabledParent(Boolean disabledParent) {
        this.disabledParent = disabledParent;
    }

}
