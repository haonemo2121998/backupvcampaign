/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.viettel.campaign.controller;

import javax.faces.bean.ManagedBean;
import com.captcha.botdetect.web.jsf.JsfCaptcha;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@ManagedBean
@ViewScoped
public class CaptchaController implements Serializable {

    private JsfCaptcha captcha;

    private String captchaCode;
    private String username;
    private String password;

    private Boolean checkedOK;

    public JsfCaptcha getCaptcha() {
        return captcha;
    }

    public void setCaptcha(JsfCaptcha captcha) {
        this.captcha = captcha;
    }

    @PostConstruct
    public void init() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = attr.getRequest().getSession(true);
        if (session.getAttribute("loginFaile") != null) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, session.getAttribute("loginFaile").toString(), null));
            session.removeAttribute("loginFaile");
            FacesContext.getCurrentInstance().validationFailed();
        }
    }

    public void checkCaptcha() {
        if (username == null || "".equals(username.trim())) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "User name is require !", null));
            FacesContext.getCurrentInstance().validationFailed();
        }
        if (password == null || "".equals(password.trim())) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "password is require !", null));
            FacesContext.getCurrentInstance().validationFailed();
        }
        if (!captcha.validate(captchaCode)) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Wrong captcha !", null));
            FacesContext.getCurrentInstance().validationFailed();
        }
        captchaCode = "";
    }

    public String getCaptchaCode() {
        return captchaCode;
    }

    public void setCaptchaCode(String captchaCode) {
        this.captchaCode = captchaCode;
    }

    public Boolean getCheckedOK() {
        return checkedOK;
    }

    public void setCheckedOK(Boolean checkedOK) {
        this.checkedOK = checkedOK;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
